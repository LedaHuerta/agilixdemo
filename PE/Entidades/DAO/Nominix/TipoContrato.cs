﻿using Aplics.Servicios.Data.Entidades.Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_705]")]
	public class TipoContrato:TablaMaestra
	{
		public String U_NCO { get; set; }
	}
}