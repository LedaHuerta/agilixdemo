﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_511]")]
	public class EdoCuentaVacaciones:TablaDocumento
	{
		public string U_EMP { get; set; }
		public short? U_EVAC { get; set; }
		public short? U_ANTA { get; set; }
		public DateTime? U_FAV { get; set; }
		public String U_IFAVAC { get; set; }
		public String U_CIA { get; set; }
		public Decimal? U_DVAC { get; set; }
		public Decimal? U_CA6VAC { get; set; }
		public Decimal? U_DVDI { get; set; }
		public Decimal? U_DVPER { get; set; }
		public Decimal? U_DVPE { get; set; }
		public String U_CAT { get; set; }
		public String U_LOC { get; set; }
		public short? U_NTB { get; set; }
		public String U_EDOCTA { get; set; }
	}
}