﻿using Aplics.Servicios.Data.Entidades.Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_791]")]
	public class Mensajes : TablaDocumento
	{
		public String U_EMPORI { get; set; }
		public String U_EMPDEST { get; set; }
		public String U_MENSAJE { get; set; }
		public DateTime? U_FFM { get; set; }
		public DateTime? U_FRM { get; set; }
		public String U_STAT { get; set; }
		public String U_JSONPR { get; set; }
		public String U_IDPROC { get; set; }
		public String U_ROLORG { get; set; }
		public String U_ROLDEST { get; set; }
		public String U_IDNOT { get; set; }

	}
}