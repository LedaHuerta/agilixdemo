﻿using Aplics.Servicios.Entidades.Horarios;
using GRC.Servicios.Data.DAO.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	public class DAOEmpleados : DAOCRUDNominixTM<Empleados>
	{
		DbContext contexto;

		public DAOEmpleados(DbContext pDbContext)
		{
			contexto = pDbContext;
		}

		public DAOEmpleados(int _UserSign, DbContext pDbContext)
		{
			this.Log(_UserSign);
			contexto = pDbContext;
		}

		public List<String> CorreosEmpleados(String filtro)
		{
			List<String> correos = new List<String>();
			String sql = @"SELECT U_EMAIL FROM dbo.[@AST_011] WHERE ISNULL(U_EMAIL,'')<>'' AND Code IN (" + filtro + ")";
			correos = contexto.Database.SqlQuery<String>(sql).ToList();
			return correos;
		}
	}
}
