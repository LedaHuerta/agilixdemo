﻿using Aplics.Servicios.Modelos.Generales;
using GRC.Servicios.Data.DAO.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PortalEmpleadosV2.Entidades.DAO.Nominix
{
	public class DAONominix 
    {
		DbContext contexto;

		public DAONominix(DbContext pDbContext)
		{
			contexto = pDbContext;
		}

		public List<CatalogoGeneral> GetCatalogoGeneral(String tabla)
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
            {
				List<CatalogoGeneral> listaTabla = new List<CatalogoGeneral>();
				listaTabla = contexto.Database.SqlQuery<CatalogoGeneral>(@"SELECT Code as Id, Name as Descripcion FROM " + tabla).ToList();
				Listado.Add(new CatalogoGeneral() {Id = "", Descripcion = "Seleccionar" });
				Listado.AddRange(listaTabla);
			} catch (Exception ex)
            {
				throw ex;
            }
			return Listado;
		}

		public List<CatalogoGeneral> GetCatalogoNivelEsc()
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGeneral("[dbo].[@AST_773]");
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		//replicar las demas
		public List<CatalogoGeneral> GetCatalogoSituacionEsc()
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGeneral("[dbo].[@AST_R34]");
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		public List<CatalogoGeneral> GetCatalogoGradoEsc()
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGeneral("[dbo].[@AST_R32]");
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		public List<CatalogoGeneral> GetCatalogoPeriodoEsc()
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGeneral("[dbo].[@AST_R33]");
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		public List<CatalogoGeneral> GetCatalogoProfesiones()
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGeneral("[dbo].[@AST_785]");
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		public List<CatalogoGeneral> GetCatalogoUniversidades()
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGeneral("[dbo].[@AST_R31]");
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}
				
		public List<CatalogoGeneral> GetCatalogoGenerales(String tabla, String clave)
		{
			List<CatalogoGeneral> listaTabla = new List<CatalogoGeneral>();
			try
			{
				listaTabla = contexto.Database.SqlQuery<CatalogoGeneral>(@"SELECT Code as Id, Name as Descripcion FROM " + tabla + " WHERE [Code] = '" + clave + "'").ToList();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return listaTabla;
		}

		public List<CatalogoGeneral> GetCatalogoDepartamentos(String claveDepartamento)
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGenerales("[dbo].[@AST_723]", claveDepartamento);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		public List<CatalogoGeneral> GetCatalogoPuestos(String clavePuesto)
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGenerales("[dbo].[@AST_702]", clavePuesto);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		public List<CatalogoGeneral> GetCatalogoCategoria(String claveCategoria)
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGenerales("[dbo].[@AST_704]", claveCategoria);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

		public List<CatalogoGeneral> GetEmpleadoSuperior(String EmpSup)
		{
			List<CatalogoGeneral> Listado = new List<CatalogoGeneral>();
			try
			{
				Listado = GetCatalogoGenerales("[dbo].[@AST_011]", EmpSup);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Listado;
		}

	}
}