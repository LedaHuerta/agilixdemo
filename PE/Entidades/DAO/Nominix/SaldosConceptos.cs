﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_522]")]
	public class SaldosConceptos : TablaLineaMaestra
	{
		public String U_CON { get; set; }
		public Decimal? U_SALDO { get; set; }
	}
}
