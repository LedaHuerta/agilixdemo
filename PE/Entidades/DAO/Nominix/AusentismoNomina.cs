﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{

	[Table("[@AST_361]")]
	public class AusentismoNomina:TablaDocumento
	{
		public string U_EMP { get; set; }
		public string U_CON { get; set; }
		public DateTime? U_FAU { get; set; }
		public short? U_DAUS { get; set; }
		public string U_REF { get; set; }
		public short? U_DAPL { get; set; }
		public DateTime? U_FFU { get; set; }
		public DateTime? U_FCAUS { get; set; }
		public short? U_DAUC { get; set; }
		public short? U_DCAPL { get; set; }
		public short? U_PTJINC { get; set; }
		public string U_TIPRIE { get; set; }
		public string U_SECUEL { get; set; }
		public string U_CTLINC { get; set; }
		public string U_RAMASUA { get; set; }
		public string U_TNOACT { get; set; }
		public short? U_APACT { get; set; }
		public short? U_NNOACT { get; set; }
		public string U_CIA { get; set; }
		public string U_CRP { get; set; }
		public string U_CONSUA { get; set; }
		public short? U_DAUSUA { get; set; }
		public DateTime? U_FISUA { get; set; }
		public DateTime? U_FFSUA { get; set; }
		public DateTime? U_FFUC { get; set; }
		public short? U_DIASUB { get; set; }
	}
}
