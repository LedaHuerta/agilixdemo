﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRC.Servicios.Data.DAO.EF;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	public class DAOAusentismo: DAOCRUDNominixTD<AusentismoNomina>
	{
		DbContext contexto;

		public DAOAusentismo(DbContext pDbContext)
		{
			contexto = pDbContext;
		}

		public DAOAusentismo(int _UserSign, DbContext pDbContext)
		{
			this.Log(_UserSign);
			contexto = pDbContext;
		}

		public List<AusentismoNominaTotales> AusentismoEmpleado(String codEmpleado)
		{
			List<AusentismoNominaTotales> Listado = contexto.Database.SqlQuery<AusentismoNominaTotales>(@"SELECT U_EMP,U_CON,SUM(ISNULL(U_DAPL,0) AS U_DAPL) 
					From [dbo].[@AST_361] Where E.U_EMP = @U_EMP  Group by U_EMP,U_CON", new SqlParameter("@U_EMP", codEmpleado)).ToList();
			return Listado;
		}

		public List<AusentismoNominaTotales> Ausentismo()
		{
			List<AusentismoNominaTotales> Listado = contexto.Database.SqlQuery<AusentismoNominaTotales>(@"SELECT U_EMP,U_CON,SUM(ISNULL(U_DAPL,0) AS U_DAPL From [dbo].[@AST_361] Group by U_EMP,U_CON").ToList();
			return Listado;
		}
	}
}
