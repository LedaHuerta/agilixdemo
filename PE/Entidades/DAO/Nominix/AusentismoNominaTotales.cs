﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	public class AusentismoNominaTotales
	{
		public String U_EMP { get; set; }
		public String U_CON { get; set; }
		public short? U_DAPL { get; set; }
	}
}
