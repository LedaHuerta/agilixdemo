﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRC.Servicios.Data.DAO.EF;
using System.Data.Entity;
using System.Data.SqlClient;
using Aplics.Servicios.Modelos.Solicitudes;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{
   public class DAOConceptos : DAOCRUDNominixTM<Conceptos>
	{
		DbContext contexto;

		public DAOConceptos(DbContext pDbContext)
		{
			contexto = pDbContext;
		}

		public DAOConceptos(int _UserSign, DbContext pDbContext)
		{
			//this.Log(_UserSign);
			contexto = pDbContext;
		}

		/// <summary>
		/// Regresa conceptos de ausentismo (TODO: QUITAR CODIGO DURO CODIGOS DE AUSENTISMO)
		/// </summary>
		/// <returns></returns>
		public List<ItemSolicitiud> ListaConceptosAusentismo()
		{
			List<ItemSolicitiud> Listado = contexto.Database.SqlQuery<ItemSolicitiud>(@"SELECT Code,Name,0 AS Politica 
																							    FROM [dbo].[@AST_031] WHERE (U_CVAA IN ('H', 'N'))").ToList();
			return Listado;
		}
	}
}
