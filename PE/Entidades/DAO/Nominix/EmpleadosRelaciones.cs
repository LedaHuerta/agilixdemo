﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{

	[Table("[@AST_013]")]
	public class EmpleadosRelaciones: TablaLineaMaestra
	{
		public String U_EMPREL { get; set; }
		public String U_TIPREL { get; set; }
		public DateTime? U_FVIGINI { get; set; }
		public DateTime? U_FVIGFIN { get; set; }
	}
}
