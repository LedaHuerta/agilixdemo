﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;
using System.ComponentModel.DataAnnotations;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_042]")]
	public class CriteriosConceptosH:TablaLineaMaestra
	{
		public String U_IFVAC { get; set; }
		public int? U_AGRUPACION { get; set; }
		public String U_CLAVEANT { get; set; }
		public Decimal? U_UNID { get; set; }
		public Boolean? U_ACTIVO { get; set; }
		public String U_BCOLOR { get; set; }
		public String U_COLOR { get; set; }
		public Boolean U_XCAT { get; set; }
		public Boolean U_XSCAT { get; set; }
		public String U_NOTIF { get; set; }

		private String _U_TDIAS = String.Empty;
        public String U_TDIAS { get => _U_TDIAS; set => _U_TDIAS = value; }
		
		private String _U_CONNOM = String.Empty;
		public String U_CONNOM { get => _U_CONNOM; set => _U_CONNOM = value; }
	}
}
