﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_D21]")]
	public class Usuarios : TablaMaestra
	{
		public string U_TIPUSU { get; set; }
		public string U_NIVUSU { get; set; }
		public string U_GPOUSU { get; set; }
		public string U_UBCUSU { get; set; }
		public string U_EMP { get; set; }
		public string U_PWD { get; set; }
		public string U_CNFFTE { get; set; }
		public string U_CNFCFV { get; set; }
		public string U_CNFCT { get; set; }
		public string U_CNFCFT { get; set; }
		public string U_CNFCB { get; set; }
		public string U_CNFCFB { get; set; }
		public string U_CNFTCF { get; set; }
		public string U_CNFTC { get; set; }
		public string U_CNFTCE { get; set; }
		public string U_CNFTFE { get; set; }
		public string U_CNFIF { get; set; }
		public string U_EMAIL { get; set; }
		public short? U_CNFIFF { get; set; }
		public byte? U_INTREP { get; set; }
		public byte? U_MODALT { get; set; }
		public string U_JOBQ { get; set; }
	}
}
