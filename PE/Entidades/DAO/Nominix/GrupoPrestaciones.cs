﻿using Aplics.Servicios.Data.Entidades.Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_133]")]
	public class GrupoPrestaciones:TablaMaestra
	{
		public String U_IFVAC { get; set; }
		public String U_IFSDI { get; set; }
		public String U_DAGUIN { get; set; }
		public String U_PRIVAC { get; set; }
		public String U_IFPV { get; set; }
		public String U_IFAGUIN { get; set; }
		public String U_PTJFA { get; set; }
		public String U_IMPVD { get; set; }
		public decimal? U_TOPEAN { get; set; }
	}
}
