﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_043]")]
	public class CriteriosConceptos : TablaLineaMaestra
	{
		public String U_IFVAC { get; set; }
		public int? U_AGRUPACION { get; set; }
		public String U_CAMPO { get; set; }
		public String U_VALOR { get; set; }
		public Boolean U_MULTIPLE { get; set; }
		private string _U_FORMULA = String.Empty;
		public String U_FORMULA { get => _U_FORMULA; set => _U_FORMULA = value; }
		public Boolean? U_ACTIVO { get; set; }
	}
}
