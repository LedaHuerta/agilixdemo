﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_011]")]
	public class Empleados:TablaMaestra
	{
		private String _U_STAT = String.Empty;
		[MaxLength(1)]
		public String U_Stat { get => _U_STAT; set => _U_STAT = value; }

		private String _U_APEPAT = String.Empty;
		public String U_Apepat { get => _U_APEPAT; set => _U_APEPAT = value; }

		private String _U_APEMAT = String.Empty;
		public String U_Apemat { get => _U_APEMAT; set => _U_APEMAT = value; }

		private String _U_NOMP = String.Empty;
		public String U_Nomp { get => _U_NOMP; set => _U_NOMP = value; }

		[MaxLength(8)]
		private String _U_TNOE = String.Empty;
		public String U_Tnoe { get => _U_TNOE; set => _U_TNOE = value; }

		private String _U_CAT = String.Empty;
		public String U_Cat { get => _U_CAT; set => _U_CAT = value; }

		private String _U_ECI = String.Empty;
		public String U_Eci { get => _U_ECI; set => _U_ECI = value; }

		private String _U_SEX = String.Empty;
		public String U_Sex { get => _U_SEX; set => _U_SEX = value; }

		private String _U_IMPEST = String.Empty;
		public String U_Impest { get => _U_IMPEST; set => _U_IMPEST = value; }

		private String _U_IFAVAC = String.Empty;
		public String U_Ifavac { get => _U_IFAVAC; set => _U_IFAVAC = value; }

		private short? _U_EVAC = 0;
		public short? U_Evac { get => _U_EVAC; set => _U_EVAC = value; }

		private String _U_TCCE = String.Empty;
		public String U_Tcce { get => _U_TCCE; set => _U_TCCE = value; }

		private String _U_HOR = String.Empty;
		public String U_Hor { get => _U_HOR; set => _U_HOR = value; }

		private String _U_FPA = String.Empty;
		[MaxLength(1)]
		public String U_Fpa { get => _U_FPA; set => _U_FPA = value; }

		private String _U_BAN = String.Empty;
		[MaxLength(8)]
		public String U_Ban { get => _U_BAN; set => _U_BAN = value; }

		private String _U_SUCB = String.Empty;
		public String U_Sucb { get => _U_SUCB; set => _U_SUCB = value; }

		private String _U_CTAB = String.Empty;
		public String U_Ctab { get => _U_CTAB; set => _U_CTAB = value; }

		private String _U_CRE1 = String.Empty;
		[MaxLength(8)]
		public String U_Cre1 { get => _U_CRE1; set => _U_CRE1 = value; }

		private String _U_CRE2 = String.Empty;
		[MaxLength(8)]
		public String U_Cre2 { get => _U_CRE2; set => _U_CRE2 = value; }

		private String _U_CRE3 = String.Empty;
		[MaxLength(8)]
		public String U_Cre3 { get => _U_CRE3; set => _U_CRE3 = value; }

		private String _U_CRE4 = String.Empty;
		[MaxLength(8)]
		public String U_Cre4 { get => _U_CRE4; set => _U_CRE4 = value; }

		private String _U_CRE5 = String.Empty;
		[MaxLength(8)]
		public String U_Cre5 { get => _U_CRE5; set => _U_CRE5 = value; }

		private String _U_CRE6 = String.Empty;
		[MaxLength(8)]
		public String U_Cre6 { get => _U_CRE6; set => _U_CRE6 = value; }

		private String _U_CRE7 = String.Empty;
		[MaxLength(8)]
		public String U_Cre7 { get => _U_CRE7; set => _U_CRE7 = value; }

		private String _U_CRE8 = String.Empty;
		[MaxLength(8)]
		public String U_Cre8 { get => _U_CRE8; set => _U_CRE8 = value; }

		private DateTime? _U_FIN = null;
		public DateTime? U_Fin { get => _U_FIN; set => _U_FIN = value; }

		private DateTime? _U_FAN = null;
		public DateTime? U_Fan { get => _U_FAN; set => _U_FAN = value; }

		private DateTime? _U_FVC = null;
		public DateTime? U_Fvc { get => _U_FVC; set => _U_FVC = value; }

		private DateTime? _U_FPL = null;
		public DateTime? U_Fpl { get => _U_FPL; set => _U_FPL = value; }

		private DateTime? _U_FAP = null;
		public DateTime? U_Fap { get => _U_FAP; set => _U_FAP = value; }

		private DateTime? _U_FNAC = null;
		public DateTime? U_Fnac { get => _U_FNAC; set => _U_FNAC = value; }

		private DateTime? _U_FCS = null;
		public DateTime? U_Fcs { get => _U_FCS; set => _U_FCS = value; }

		private DateTime? _U_FTR = null;
		public DateTime? U_Ftr { get => _U_FTR; set => _U_FTR = value; }

		private DateTime? _U_FR = null;
		public DateTime? U_Fr { get => _U_FR; set => _U_FR = value; }

		private String _U_MCAM = String.Empty;
		[MaxLength(8)]
		public String U_Mcam { get => _U_MCAM; set => _U_MCAM = value; }

		private String _U_TCO = String.Empty;
		[MaxLength(10)]
		public String U_Tco { get => _U_TCO; set => _U_TCO = value; }

		private String _U_TUR = String.Empty;
		[MaxLength(8)]
		public String U_Tur { get => _U_TUR; set => _U_TUR = value; }

		private String _U_CIA = String.Empty;
		[MaxLength(8)]
		public String U_Cia { get => _U_CIA; set => _U_CIA = value; }

		private String _U_LOC = String.Empty;
		[MaxLength(8)]
		public String U_Loc { get => _U_LOC; set => _U_LOC = value; }

		private String _U_DIR = String.Empty;
		[MaxLength(8)]
		public String U_Dir { get => _U_DIR; set => _U_DIR = value; }

		private String _U_DEP = String.Empty;
		[MaxLength(8)]
		public String U_Dep { get => _U_DEP; set => _U_DEP = value; }

		private String _U_PUE = String.Empty;
		[MaxLength(8)]
		public String U_Pue { get => _U_PUE; set => _U_PUE = value; }

		private short? _U_NTB = 0;
		public short? U_Ntb { get => _U_NTB; set => _U_NTB = value; }

		private String _U_CTSUE = String.Empty;
		[MaxLength(8)]
		public String U_Ctsue { get => _U_CTSUE; set => _U_CTSUE = value; }

		private String _U_EMPSUP = String.Empty;
		public String U_Empsup { get => _U_EMPSUP; set => _U_EMPSUP = value; }

		private String _U_EMPSUP2 = String.Empty;
		public String U_Empsup2 { get => _U_EMPSUP2; set => _U_EMPSUP2 = value; }

		private String _U_STPR = String.Empty;
		public String U_Stpr { get => _U_STPR; set => _U_STPR = value; }

		private String _U_NIVJ = String.Empty;
		public String U_Nivj { get => _U_NIVJ; set => _U_NIVJ = value; }

		private String _U_CRP = String.Empty;
		[MaxLength(8)]
		public String U_Crp { get => _U_CRP; set => _U_CRP = value; }

		private String _U_CCOS = String.Empty;
		[MaxLength(12)]
		public String U_Ccos { get => _U_CCOS; set => _U_CCOS = value; }

		private String _U_PROY = String.Empty;
		[MaxLength(8)]
		public String U_Proy { get => _U_PROY; set => _U_PROY = value; }

		private String _U_ZSM = String.Empty;
		public String U_Zsm { get => _U_ZSM; set => _U_ZSM = value; }

		private decimal? _U_SUE = 0.00M;
		public decimal? U_Sue { get => _U_SUE; set => _U_SUE = value; }

		private String _U_TSUE = String.Empty;
		public String U_Tsue { get => _U_TSUE; set => _U_TSUE = value; }

		private decimal? _U_SDI = 0.00M;
		public decimal? U_Sdi { get => _U_SDI; set => _U_SDI = value; }

		private decimal? _U_SIST = 0.00M;
		public decimal? U_Sist { get => _U_SIST; set => _U_SIST = value; }

		private decimal? _U_SFIJ = 0.00M;
		public decimal? U_Sfij { get => _U_SFIJ; set => _U_SFIJ = value; }

		private decimal? _U_SVAR = 0.00M;
		public decimal? U_Svar { get => _U_SVAR; set => _U_SVAR = value; }

		private decimal? _U_INTF = 0.00M;
		public decimal? U_Intf { get => _U_INTF; set => _U_INTF = value; }

		private String _U_CICA = String.Empty;
		public String U_Cica { get => _U_CICA; set => _U_CICA = value; }

		private decimal? _U_IRE1 = 0.00M;
		public decimal? U_Ire1 { get => _U_IRE1; set => _U_IRE1 = value; }

		private decimal? _U_IRE2 = 0.00M;
		public decimal? U_Ire2 { get => _U_IRE2; set => _U_IRE2 = value; }

		private decimal? _U_BONO = 0.00M;
		public decimal? U_Bono { get => _U_BONO; set => _U_BONO = value; }

		private decimal? _U_SN = 0.00M;
		public decimal? U_Sn { get => _U_SN; set => _U_SN = value; }

		private decimal? _U_SPRO = 0.00M;
		public decimal? U_Spro { get => _U_SPRO; set => _U_SPRO = value; }

		private String _U_RFC = String.Empty;
		[MaxLength(13)]
		public String U_Rfc { get => _U_RFC; set => _U_RFC = value; }

		private String _U_CURP = String.Empty;
		[MaxLength(18)]
		public String U_Curp { get => _U_CURP; set => _U_CURP = value; }

		private String _U_IMSS = String.Empty;
		[MaxLength(11)]
		public String U_Imss { get => _U_IMSS; set => _U_IMSS = value; }

		private String _U_TS = String.Empty;
		[MaxLength(1)]
		public String U_Ts { get => _U_TS; set => _U_TS = value; }

		private String _U_CLNA = String.Empty;
		public String U_Clna { get => _U_CLNA; set => _U_CLNA = value; }

		private short? _U_UMF = 0;
		public short? U_Umf { get => _U_UMF; set => _U_UMF = value; }

		private short? _U_JR = 0;
		public short? U_Jr { get => _U_JR; set => _U_JR = value; }

		private String _U_AFO = String.Empty;
		public String U_Afo { get => _U_AFO; set => _U_AFO = value; }

		private String _U_NCON = String.Empty;
		public String U_Ncon { get => _U_NCON; set => _U_NCON = value; }

		private String _U_DOM = String.Empty;
		public String U_Dom { get => _U_DOM; set => _U_DOM = value; }

		private String _U_COL = String.Empty;
		public String U_Col { get => _U_COL; set => _U_COL = value; }

		private String _U_DEL = String.Empty;
		public String U_Del { get => _U_DEL; set => _U_DEL = value; }

		private String _U_EDO = String.Empty;
		public String U_Edo { get => _U_EDO; set => _U_EDO = value; }

		private String _U_CODP = String.Empty;
		public String U_Codp { get => _U_CODP; set => _U_CODP = value; }

		private String _U_PAIS = String.Empty;
		public String U_Pais { get => _U_PAIS; set => _U_PAIS = value; }

		private String _U_TELDOM = String.Empty;
		public String U_Teldom { get => _U_TELDOM; set => _U_TELDOM = value; }

		private String _U_TELCEL = String.Empty;
		public String U_Telcel { get => _U_TELCEL; set => _U_TELCEL = value; }

		private String _U_TELOFI = String.Empty;
		public String U_Telofi { get => _U_TELOFI; set => _U_TELOFI = value; }

		private String _U_TELREC = String.Empty;
		public String U_Telrec { get => _U_TELREC; set => _U_TELREC = value; }

		private String _U_EMAIL = String.Empty;
		public String U_Email { get => _U_EMAIL; set => _U_EMAIL = value; }

		private DateTime? _U_FBJ = null;
		public DateTime? U_Fbj { get => _U_FBJ; set => _U_FBJ = value; }

		private String _U_MBAJ = String.Empty;
		[MaxLength(8)]
		public String U_Mbaj { get => _U_MBAJ; set => _U_MBAJ = value; }

		private DateTime? _U_FREN = null;
		public DateTime? U_Fren { get => _U_FREN; set => _U_FREN = value; }

		private DateTime? _U_FEBAJ = null;
		public DateTime? U_Febaj { get => _U_FEBAJ; set => _U_FEBAJ = value; }

		private String _U_STTER = String.Empty;
		public String U_Stter { get => _U_STTER; set => _U_STTER = value; }

		private String _U_NIVESC = String.Empty;
		[MaxLength(8)]
		public String U_Nivesc { get => _U_NIVESC; set => _U_NIVESC = value; }

        /// <summary>
        /// inicio: Por revisar con Gerardo
        /// </summary>

        private string _U_GRAESC = String.Empty;
        public string U_GRAESC { get => _U_GRAESC; set => _U_GRAESC = value; }

        private string _U_PERESC = String.Empty;
		public string U_PERESC { get => _U_PERESC; set => _U_PERESC = value; }

		private string _U_SITESC = String.Empty;
		public string U_SITESC { get => _U_SITESC; set => _U_SITESC = value; }

		private string _U_UNIV = String.Empty;
		public string U_UNIV { get => _U_UNIV; set => _U_UNIV = value; }

		/// <summary>
		/// fin
		/// </summary>

		private String _U_PROF = String.Empty;
		[MaxLength(8)]
		public String U_Prof { get => _U_PROF; set => _U_PROF = value; }

		private String _U_NACIO = String.Empty;
		[MaxLength(8)]
		public String U_Nacio { get => _U_NACIO; set => _U_NACIO = value; }

		private String _U_NOMGAF = String.Empty;
		public String U_Nomgaf { get => _U_NOMGAF; set => _U_NOMGAF = value; }

		private short? _U_SECC = 0;
		public short? U_Secc { get => _U_SECC; set => _U_SECC = value; }

		private String _U_LDAP = String.Empty;
		public String U_Ldap { get => _U_LDAP; set => _U_LDAP = value; }

		private String _U_PWDLDAP = String.Empty;
		public String U_Pwdldap { get => _U_PWDLDAP; set => _U_PWDLDAP = value; }

		private String _U_THOR = String.Empty;
		public String U_Thor { get => _U_THOR; set => _U_THOR = value; }

		private String _U_EMP2 = String.Empty;
		public String U_Emp2 { get => _U_EMP2; set => _U_EMP2 = value; }

		private String _U_EMPPG = String.Empty;
		public String U_Emppg { get => _U_EMPPG; set => _U_EMPPG = value; }

		private String _U_PATS = String.Empty;
		public String U_Pats { get => _U_PATS; set => _U_PATS = value; }

		private String _U_ROL = String.Empty;
		public String U_Rol { get => _U_ROL; set => _U_ROL = value; }

		private String _U_FUNC = String.Empty;
		public String U_Func { get => _U_FUNC; set => _U_FUNC = value; }

		private String _U_TDEP = String.Empty;
		public String U_Tdep { get => _U_TDEP; set => _U_TDEP = value; }

		private String _U_ZONA = String.Empty;
		public String U_Zona { get => _U_ZONA; set => _U_ZONA = value; }

		private String _U_PROYCTE = String.Empty;
		public String U_Proycte { get => _U_PROYCTE; set => _U_PROYCTE = value; }

		private String _U_CBCTE = String.Empty;
		public String U_Cbcte { get => _U_CBCTE; set => _U_CBCTE = value; }

		private String _U_LOCCTE = String.Empty;
		public String U_Loccte { get => _U_LOCCTE; set => _U_LOCCTE = value; }

		private String _U_DEPCTE = String.Empty;
		public String U_Depcte { get => _U_DEPCTE; set => _U_DEPCTE = value; }

		private String _U_CTACTE = String.Empty;
		public String U_Ctacte { get => _U_CTACTE; set => _U_CTACTE = value; }

		private decimal? _U_DBANT = 0.00M;
		public decimal? U_Dbant { get => _U_DBANT; set => _U_DBANT = value; }

		private decimal? _U_VBANT = 0.00M;
		public decimal? U_Vbant { get => _U_VBANT; set => _U_VBANT = value; }

		private String _U_CTAV = String.Empty;
		public String U_Ctav { get => _U_CTAV; set => _U_CTAV = value; }

		private DateTime? _U_FNVC = null;
		public DateTime? U_Fnvc { get => _U_FNVC; set => _U_FNVC = value; }

		private String _U_TCTAB = String.Empty;
		public String U_Tctab { get => _U_TCTAB; set => _U_TCTAB = value; }

		private String _U_SUB = String.Empty;
		public String U_Sub { get => _U_SUB; set => _U_SUB = value; }

		private DateTime? _U_PFE = null;
		public DateTime? U_Pfe { get => _U_PFE; set => _U_PFE = value; }

		private String _U_ATI = String.Empty;
		public String U_Ati { get => _U_ATI; set => _U_ATI = value; }

		private String _U_DURPZA = String.Empty;
		public String U_Durpza { get => _U_DURPZA; set => _U_DURPZA = value; }

		private String _U_PZASUP = String.Empty;
		public String U_Pzasup { get => _U_PZASUP; set => _U_PZASUP = value; }

		private String _U_PZA = String.Empty;
		[MaxLength(8)]
		public String U_Pza { get => _U_PZA; set => _U_PZA = value; }

		private short? _U_NTBBON = 0;
		public short? U_Ntbbon { get => _U_NTBBON; set => _U_NTBBON = value; }

		private decimal? _U_PTJAUM = 0.00M;
		public decimal? U_Ptjaum { get => _U_PTJAUM; set => _U_PTJAUM = value; }

		private String _U_LUGVAL = String.Empty;
		public String U_Lugval { get => _U_LUGVAL; set => _U_LUGVAL = value; }

		private String _U_TIPEV = String.Empty;
		public String U_Tipev { get => _U_TIPEV; set => _U_TIPEV = value; }

		private decimal? _U_SUED = 0.00M;
		public decimal? U_Sued { get => _U_SUED; set => _U_SUED = value; }

		private decimal? _U_IFV = 0.00M;
		public decimal? U_Ifv { get => _U_IFV; set => _U_IFV = value; }

		private String _U_LIC = String.Empty;
		[MaxLength(18)]
		public String U_Lic { get => _U_LIC; set => _U_LIC = value; }

		private DateTime? _U_FVLIC = null;
		public DateTime? U_Fvlic { get => _U_FVLIC; set => _U_FVLIC = value; }

		private DateTime? _U_FILIC = null;
		public DateTime? U_Filic { get => _U_FILIC; set => _U_FILIC = value; }

		private DateTime? _U_FR2 = null;
		public DateTime? U_Fr2 { get => _U_FR2; set => _U_FR2 = value; }

		private decimal? _U_CALFN = 0.00M;
		public decimal? U_Calfn { get => _U_CALFN; set => _U_CALFN = value; }

		private String _U_CALFA = String.Empty;
		public String U_Calfa { get => _U_CALFA; set => _U_CALFA = value; }

		private String _U_CLABE = String.Empty;
		[MaxLength(18)]
		public String U_Clabe { get => _U_CLABE; set => _U_CLABE = value; }

		private String _U_NUMEXT = String.Empty;
		[MaxLength(50)]
		public String U_Numext { get => _U_NUMEXT; set => _U_NUMEXT = value; }

		private String _U_NUMINT = String.Empty;
		[MaxLength(50)]
		public String U_Numint { get => _U_NUMINT; set => _U_NUMINT = value; }

	}
}
