﻿using Aplics.Servicios.Data.Entidades.Generales;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Entidades.DAO.Nominix
{
	[Table("[@AST_P99]")]
	public class ParametrosAPP:TablaDocumento
	{
		public String U_MOD { get; set; }
		public String U_SUBMOD { get; set; }
		public String U_PARAM { get; set; }
		public String U_VALOR { get; set; }
	}
}