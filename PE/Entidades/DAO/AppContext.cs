﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Entidades.Solicitudes;
using Aplics.Servicios.Modelos.Nominix;

namespace Aplics.Servicios.Entidades
{
    public class AppContext : DbContext
    {
                
        public AppContext() : base("name = DbContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<AppContext>(null);
		}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

		public DbSet<Empleados> Empleados { get; set; }
		public DbSet<EmpleadoAsignacion> EmpleadoAsignacion { get; set; }
		public DbSet<EmpleadoEstudianteHorario> EmpleadoEstudianteHorario { get; set; }
		public DbSet<TablaSolicitudes> TablaSolicitudes { get; set; }
		public DbSet<Usuarios> Usuarios { get; set; }
		public DbSet<AusentismoNomina> AusentismoNomina { get; set; }
		public DbSet<Conceptos> Conceptos { get; set; }
		public DbSet<CriteriosConceptosH> CriteriosConceptosH { get; set; }
		public DbSet<CriteriosConceptos> CriteriosConceptos { get; set; }
		public DbSet<EmpleadosRelaciones> EmpleadosRelaciones { get; set; }
		public DbSet<GrupoPrestaciones> GrupoPrestaciones { get; set; }
		public DbSet<SaldosConceptos> SaldosConceptos { get; set; }
		public DbSet<Universidades> Universidades { get; set; }
		public DbSet<Mensajes> Mensajes { get; set; }
		public DbSet<EdoCuentaVacaciones> EdoCuentaVacaciones { get; set; }
		public DbSet<ParametrosAPP> ParametrosAPP { get; set; }
	}
}
