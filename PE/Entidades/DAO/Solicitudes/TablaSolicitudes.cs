﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplics.Servicios.Data.Entidades.Generales;
using System.ComponentModel.DataAnnotations.Schema;


namespace Aplics.Servicios.Entidades.Solicitudes
{
	[Table("[@AST_513]")]
	public class TablaSolicitudes : TablaDocumento
	{
		public string U_EMP { get; set; }
		public DateTime? U_FECSOL { get; set; }
		public decimal? U_DSOL { get; set; }
		public string U_STSV { get; set; }
		public string U_CON { get; set; }
		public string U_CIA { get; set; }
		public string U_LOC { get; set; }
		public DateTime? U_FIV { get; set; }
		public DateTime? U_FTV { get; set; }
		public string U_SSUP { get; set; }
		public string U_SRH { get; set; }
        public string U_SNOMINIX { get; set; }
		public string U_MOTR { get; set; }
		public string U_EMPSUP { get; set; }
		public string U_OBS { get; set; }
	}
}
