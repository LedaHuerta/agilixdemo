﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Entidades.DAO.Nominix;
using System.Data.Entity;
using System.Data.SqlClient;
using Aplics.Servicios.Modelos.Solicitudes;


namespace Aplics.Servicios.Entidades.Solicitudes
{
   public class DAOSolicitudDashBoard : DAOCRUDNominixTD<TablaSolicitudes>
	{
		DbContext contexto;

		public DAOSolicitudDashBoard(DbContext pDbContext)
		{
			contexto = pDbContext;
		}

		public DAOSolicitudDashBoard(int _UserSign, DbContext pDbContext)
		{
			//this.Log(_UserSign);
			contexto = pDbContext;
		}

		public List<TablaSolicitudes> ListaSolicitudesTipo(int _UserSign)
		{
			//Le indico solo algunos campos ya que no se cuales van a indicarse en la consulta, supongo que habra algun identificador de tipo
			List<TablaSolicitudes> Listado = contexto.Database.SqlQuery<TablaSolicitudes>(@"SELECT U_EMP,U_FECSOL,U_DSOL 
																							    FROM [dbo].[@AST_513]
																								WHERE UserSign = @UserSign", new SqlParameter("@UserSign", _UserSign)).ToList();
			return Listado;
		}

		public List<DatosSolicitud> ListaSolicitudesGeneral(String Concepto, int numSolicitud,int Anio=0)
		{
			String sql = @"SELECT S.DocEntry AS Id, S.U_EMP AS NumEmpleado, S.U_CON TipoSolicitud, S.U_FECSOL AS FechaSolicitud, 
						  S.U_FIV AS FechaInicial,S.U_FTV AS FechaFinal ,E.[Name] AS NombreEmpleado,C.Name AS Descripcion,ISNULL(S.U_OBS,'') AS Observaciones,S.U_DSOL as Dias,
                          E.U_CAT AS Categoria,S.U_SSUP AS StatusSup,S.U_SRH as StatusRH,ISNULL(P.[Name],'') AS Puesto,ISNULL(ES.[Name],'') AS Superior, C.U_UME AS UMed 
						  FROM [dbo].[@AST_513] S INNER JOIN [dbo].[@AST_011] E ON S.U_EMP=E.CODE
                          INNER JOIN [dbo].[@AST_031] C ON S.U_CON=C.Code 
						  LEFT OUTER JOIN [dbo].[@AST_702] P on E.U_PUE=P.[Code]
						  LEFT OUTER JOIN [dbo].[@AST_011] ES on E.U_EMPSUP=ES.[Code] WHERE 1=1";
			if (Anio != 0) { sql += " AND YEAR(S.U_FIV) = "  + Anio.ToString(); }
			if (!String.IsNullOrEmpty(Concepto)) { sql += " AND S.U_CON='" + Concepto + "'"; }
			if (numSolicitud != 0) { sql += " AND S.DocEntry=" + numSolicitud; }
			List<DatosSolicitud> Listado = contexto.Database.SqlQuery<DatosSolicitud>(sql).ToList();
			return Listado;
		}

		public List<DatosSolicitud> ListaSolicitudes(String Concepto, int numSolicitud, String cveEmp = "")
		{
			String sql = @"SELECT S.DocEntry AS Id, S.U_EMP AS NumEmpleado, S.U_CON TipoSolicitud, S.U_FECSOL AS FechaSolicitud, 
						  S.U_FIV AS FechaInicial,S.U_FTV AS FechaFinal ,E.[Name] AS NombreEmpleado,C.Name AS Descripcion,ISNULL(S.U_OBS,'') AS Observaciones,S.U_DSOL as Dias,
                          E.U_CAT AS Categoria,S.U_SSUP AS StatusSup,S.U_SRH as StatusRH,ISNULL(P.[Name],'') AS Puesto,ISNULL(ES.[Name],'') AS Superior, C.U_UME AS UMed
						  FROM [dbo].[@AST_513] S INNER JOIN [dbo].[@AST_011] E ON S.U_EMP=E.CODE
                          INNER JOIN [dbo].[@AST_031] C ON S.U_CON=C.Code 
						  LEFT OUTER JOIN [dbo].[@AST_702] P on E.U_PUE=P.[Code]
						  LEFT OUTER JOIN [dbo].[@AST_011] ES on E.U_EMPSUP=ES.[Code] WHERE 1=1
						  AND YEAR(S.U_FIV) = " + System.DateTime.Today.Year.ToString(); 
			if (!String.IsNullOrEmpty(Concepto)) { sql += " AND S.U_CON='" + Concepto + "'"; }
			if (numSolicitud != 0) { sql += " AND S.DocEntry=" + numSolicitud; }
			if (!String.IsNullOrEmpty(cveEmp))
            {
				sql += "AND S.U_EMP = '" + cveEmp + "'";

			}
            //sql = sql + " ORDER BY S.DocEntry desc";

            List<DatosSolicitud> Listado = contexto.Database.SqlQuery<DatosSolicitud>(sql).ToList();
			return Listado;
		}

		//NUEVO CPHA
		public List<DatosSolicitud> ListaHistorialSolicitudes(String Empleado, String Anio="",string conceptos="")
        {
            String sql = @"SELECT S.DocEntry AS Id, S.U_EMP AS NumEmpleado, S.U_CON TipoSolicitud, S.U_FECSOL AS FechaSolicitud, 
						  S.U_FIV AS FechaInicial,S.U_FTV AS FechaFinal ,E.Name AS NombreEmpleado,C.Name AS Descripcion, E.U_CAT AS Categoria,S.U_SSUP AS StatusSup,S.U_SRH as StatusRH,
                          S.U_DSOL as Dias
						  FROM [dbo].[@AST_513] S INNER JOIN [dbo].[@AST_011] E ON S.U_EMP=E.CODE
                          INNER JOIN [dbo].[@AST_031] C ON S.U_CON=C.Code WHERE 1=1 AND E.CODE = '" + Empleado + "'" ;
			if (!String.IsNullOrEmpty(Anio)) { sql += " AND YEAR(S.U_FIV) = " + Anio; }
            if (!String.IsNullOrEmpty(conceptos)) { sql += " AND " + conceptos; }
            //sql = sql + " AND " + conceptos;

            //sql = sql + " ORDER BY S.DocEntry desc";

            List<DatosSolicitud> Listado = contexto.Database.SqlQuery<DatosSolicitud>(sql).ToList();
            return Listado;
        }
        //NUEVO CPHA
        public List<DatosSolicitudCardsHistorico> ListaSolicitudesCardsHistorico(String NumEmpleado)
        {
            String sql = @"select U_EMP,SUM(U_DSOL_TOTAL) AS DSOL_TOTAL,U_CIA,ANIO,sum(convert(decimal,DiasCorrespondientes)) AS DCorrespondientes,
            (case when sum(convert(decimal,DiasDisfrutados)) is not null then sum(convert(decimal,DiasDisfrutados)) else 0 end) as DDisfrutados,
            (case when sum(convert(decimal,DiasDisponibles)) is not null then sum(convert(decimal,DiasDisponibles)) else 0 end) as DDisponibles
            from (
            select S.U_EMP,SUM(S.U_DSOL) as U_DSOL_TOTAL,S.U_CON,S.U_CIA,YEAR(S.U_FIV) AS ANIO,
	            (case when H.U_BCOLOR is not null then H.U_BCOLOR else 'White' end) as ColorFondo,
	            (case when H.U_COLOR is not null then H.U_COLOR else 'Black' end) as ColorTexto,
	            c.Name as NombreConcepto, 
	            (case when S.U_CON = '201' then convert(float,H.U_UNID/24) else 0 end) as DiasCorrespondientes,
	            --convert(float,(H.U_UNID / 24))  as DiasCorrespondientes,
	            SUM(VAC.TOTAL) as DiasDisfrutados,
	            (convert(float,(H.U_UNID / 24)) - SUM(S.U_DSOL)) as DiasDisponibles
	            from [@AST_031] CONC
	            inner join [@AST_513] S on S.U_CON = CONC.Code
	            left join [@AST_042] H ON H.Code = S.U_CON
	            left join [@AST_031] C ON C.Code = S.U_CON
	            left join (
		            select U_CON,U_EMP,YEAR(U_FAU) as U_FAU,SUM(U_DAPL) AS TOTAL from [@AST_361]
		            WHERE U_CON IN ('201','1110')
		            group by U_EMP,YEAR(U_FAU),U_CON
	            ) VAC ON VAC.U_CON = S.U_CON AND VAC.U_EMP = S.U_EMP AND VAC.U_FAU = YEAR(S.U_FIV)
	            WHERE CONC.Code IN ('201','1110') AND S.U_EMP = '" + NumEmpleado + "' " +
             "GROUP BY YEAR(S.U_FIV),S.U_EMP,S.U_CON, S.U_CIA,H.U_BCOLOR,H.U_COLOR,c.Name,H.U_UNID) a group by U_EMP,U_CIA,ANIO ORDER BY ANIO DESC";

            List<DatosSolicitudCardsHistorico> Listado = contexto.Database.SqlQuery<DatosSolicitudCardsHistorico>(sql).ToList();
            return Listado;
        }
        public List<CalendarioItem> ListaEventosCalendario(String Empleado, Boolean Confirmados = false)
        {
            String sql = @"select c.Name + ' ' + e.Name as titulo,c.Name as descripcion,
CONVERT(varchar, s.U_FIV, 23) as inicio,
CONVERT(varchar, dateadd(day,1,s.U_FTV), 23) as fin,
'#3A87AD' as color,
'#ffffff' as colorTexto,
e.Name + ' Fecha de Inicio: ' + CONVERT(varchar, s.U_FIV, 23) + '. Fecha Final: ' + CONVERT(varchar, s.U_FTV, 23) + '.' as descripcion2,
s.U_EMP as claveEmpleado
from [@AST_513] s
left join [@AST_031] c on c.Code = s.U_CON
left join [@AST_011] e on s.U_EMP = e.[Code]
where YEAR(s.U_FIV) = year(getdate())";
			if (Confirmados)
			{
				sql += " AND U_SSUP = 'A'";
			}
			if (!String.IsNullOrEmpty(Empleado))
			{
				sql += " AND s.U_EMP = '" + Empleado + "'";
			}

            List<CalendarioItem> Listado = contexto.Database.SqlQuery<CalendarioItem>(sql).ToList();
            return Listado;
        }

		public List<CalendarioItem> ListaEventosCalendarioManager(String Empleados, Boolean Confirmados = false)
		{
			String sql = @"select c.Name + ' ' + e.Name as titulo,c.Name as descripcion,
CONVERT(varchar, s.U_FIV, 23) as inicio,
CONVERT(varchar, dateadd(day,1,s.U_FTV), 23) as fin,
'#3A87AD' as color,
'#ffffff' as colorTexto,
e.Name + ' Fecha de Inicio: ' + CONVERT(varchar, s.U_FIV, 23) + '. Fecha Final: ' + CONVERT(varchar, s.U_FTV, 23) + '.' as descripcion2,
s.U_EMP as claveEmpleado
from [@AST_513] s
left join [@AST_031] c on c.Code = s.U_CON
left join [@AST_011] e on s.U_EMP = e.[Code]
where s.U_EMP IN (" + Empleados + ") and YEAR(s.U_FIV) = year(getdate())";
			if(Confirmados) {
				sql += " AND U_SSUP = 'A'";
			}
			List<CalendarioItem> Listado = contexto.Database.SqlQuery<CalendarioItem>(sql).ToList();
			return Listado;
		}

        public List<EmpalmeFechas> ValidacionEmpalmeFechas(DateTime fechaInicio, DateTime fechaFin, string concepto,string empleado)
        {
            //            String sql = string.Format(@"select count(*) as TotalResultado from [@AST_513]
            //where U_FIV BETWEEN '{0}' and '{1}'
            //or U_FTV BETWEEN '{0}' and '{1}' and U_CON = {2} and U_EMP = {3}", fechaInicio.ToString("yyyyMMdd"), fechaFin.ToString("yyyyMMdd"),concepto,empleado);

            String sql = string.Format(@"select count(*) as TotalResultado from [@AST_513]
			where (U_SSUP IN ('A','P') OR U_SRH IN ('A','P')) AND (U_FIV BETWEEN '{0}' and '{1}'
			or U_FTV BETWEEN '{0}' and '{1}' or '{1}' BETWEEN U_FIV AND U_FTV) and U_EMP = {2}", fechaInicio.ToString("yyyyMMdd"), fechaFin.ToString("yyyyMMdd"), empleado);
			List<EmpalmeFechas> Resultado = contexto.Database.SqlQuery<EmpalmeFechas>(sql).ToList();
            return Resultado;
        }

        public void ConsultaVacacionesGrupo()
		{
			
		}
	}
}
