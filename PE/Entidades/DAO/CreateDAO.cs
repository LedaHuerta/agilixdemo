﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Entidades.Solicitudes;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Nominix;

namespace Aplics.Servicios.Entidades.DAO
{
	public class CreateDAO
	{

		public DAOCRUDNominixTD<EmpleadoAsignacion> GeneraDAOEmpleadoAsignacion(int _UserSign)
		{
			return new DAOCRUDNominixTD<EmpleadoAsignacion>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTM<Empleados> GeneraDAOEmpleados(int _UserSign)
		{
			return new DAOCRUDNominixTM<Empleados>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTLD<EmpleadoEstudianteHorario> GeneraDAOEmpleadoEstudianteHorario(int _UserSign)
		{
			return new DAOCRUDNominixTLD<EmpleadoEstudianteHorario>(_UserSign, new AppContext());
		}

		public DAOEmpleadoAsignacion GeneraDAOEmpleadoAsignacionCustom(int _UserSign)
		{
			return new DAOEmpleadoAsignacion(_UserSign, new AppContext());
		}

		public DAOConceptos GeneraDAOSolicitudConceptos(int _UserSign)
		{
			return new DAOConceptos(_UserSign, new AppContext());
		}

		public DAOSolicitudDashBoard GeneraDAOSolicitudDashBoard(int _UserSign)
		{
			return new DAOSolicitudDashBoard(_UserSign, new AppContext());
		}

        //NUEVO CPHA
        public DAOSolicitudDashBoard GeneraDAOHistorialSolicitudDashBoard(int _UserSign)
        {
            return new DAOSolicitudDashBoard(_UserSign, new AppContext());
        }

        //NUEVO CPHA
        public DAOSolicitudDashBoard GeneraDAOCardsHistorial(int _UserSign)
        {
            return new DAOSolicitudDashBoard(_UserSign, new AppContext());
        }

        //NUEVO CPHA
        public DAOSolicitudDashBoard GeneraDAOEventosCalendario(int _UserSign)
        {
            return new DAOSolicitudDashBoard(_UserSign, new AppContext());
        }

        public DAOCRUDNominixTD<TablaSolicitudes> GeneraDAOSolicitudes(int _UserSign)
		{
			return new DAOCRUDNominixTD<TablaSolicitudes>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTM<Usuarios> GeneraDAOUsuarios(int _UserSign)
		{
			return new DAOCRUDNominixTM<Usuarios>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTM<Universidades> GeneraDAOUniversidades(int _UserSign)
		{
			return new DAOCRUDNominixTM<Universidades>(_UserSign, new AppContext());
		}

		public DAOEmpleados GeneraDAOEmpleadosCustom(int _UserSign)
		{
			return new DAOEmpleados(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTLM<EmpleadosRelaciones> GeneraDAOEmpleadosRelaciones(int _UserSign)
		{
			return new DAOCRUDNominixTLM<EmpleadosRelaciones>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTLM<SaldosConceptos> GeneraDAOSaldosConceptos(int _UserSign)
		{
			return new DAOCRUDNominixTLM<SaldosConceptos>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTLM<CriteriosConceptosH> GeneraDAOCriteriosConceptosHeader(int _UserSign)
		{
			return new DAOCRUDNominixTLM<CriteriosConceptosH>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTLM<CriteriosConceptos> GeneraDAOCriteriosConceptos(int _UserSign)
		{
			return new DAOCRUDNominixTLM<CriteriosConceptos>(_UserSign, new AppContext());
		}

		public DAOAusentismo GeneraDAOAusentismo(int _UserSign)
		{
			return new DAOAusentismo(_UserSign, new AppContext());
		}

        public DAOCRUDNominixTD<AusentismoNomina> GeneraDAOAusentismoGenerico(int _UserSign)
        {
            return new DAOCRUDNominixTD<AusentismoNomina>(_UserSign, new AppContext());
        }

        public DAOCRUDNominixTM<Conceptos> GeneraDAOConceptos(int _UserSign)
		{
			return new DAOCRUDNominixTM<Conceptos>(_UserSign, new AppContext());
		}

		public DAOConceptos GeneraDAOConceptosCustom(int _UserSign)
		{
			return new DAOConceptos(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTM<GrupoPrestaciones> GeneraDAOGrupoPrestaciones(int _UserSign)
		{
			return new DAOCRUDNominixTM<GrupoPrestaciones>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTM<TipoContrato> GeneraDAOTipoContrato(int _UserSign)
		{
			return new DAOCRUDNominixTM<TipoContrato>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTD<Mensajes> GeneraDAOMensajes(int _UserSign)
		{
			return new DAOCRUDNominixTD<Mensajes>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTD<EdoCuentaVacaciones> GeneraDAOEdoCuentaVacaciones(int _UserSign)
		{
			return new DAOCRUDNominixTD<EdoCuentaVacaciones>(_UserSign, new AppContext());
		}

		public DAOCRUDNominixTD<ParametrosAPP> GeneraDAOParametrosAPP(int _UserSign)
		{
			return new DAOCRUDNominixTD<ParametrosAPP>(_UserSign, new AppContext());
		}

        //NUEVO CPHA
        public DAOSolicitudDashBoard EmpalmeFechas(int _UserSign)
        {
            return new DAOSolicitudDashBoard(_UserSign, new AppContext());
        }

        //NUEVO CPHA
        public DAOCRUDNominixTD<AusentismoNomina> GeneraDAOAusentismoNomina(int _UserSign)
        {
            return new DAOCRUDNominixTD<AusentismoNomina>(_UserSign, new AppContext());
        }
    }
}
