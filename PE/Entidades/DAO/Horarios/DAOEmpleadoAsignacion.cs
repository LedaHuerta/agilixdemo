﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Modelos.Horarios;
using System.Data.SqlClient;
using System.Data.Entity;
using Aplics.Servicios.Modelos.Nominix;

namespace Aplics.Servicios.Entidades.Horarios
{
	public class DAOEmpleadoAsignacion : DAOCRUDNominixTD<EmpleadoAsignacion>
	{
		DbContext contexto;

		public DAOEmpleadoAsignacion(DbContext pDbContext)
		{
			contexto = pDbContext;
		}

		public DAOEmpleadoAsignacion(int _UserSign, DbContext pDbContext)
		{
			this.Log(_UserSign);
			contexto = pDbContext;
		}


		public List<EmpleadoHorario> ListaEmpleadosXSuperior(String codSuperior)
		{
			List<EmpleadoHorario> Listado = contexto.Database.SqlQuery<EmpleadoHorario>(@"SELECT EH.DocEntry,EH.U_EMP AS NumEmpleado,
					E.U_APEPAT + ' ' + E.U_APEMAT + ' ' + E.U_NOMP AS Nombre,
					ES.U_APEPAT + ' ' + ES.U_APEMAT + ' ' + ES.U_NOMP AS NombreSuperior,
					EH.U_FINI AS Inicio,U_FFIN AS Fin,EH.U_Aprobado AS Aprobado,EH.U_HorarioESC,EH.U_MOTIVOREC,EH.U_NUMREC
                    From dbo.[@AST_H05] EH INNER JOIN dbo.[@AST_011] E  ON EH.U_EMP = E.Code 
					LEFT OUTER JOIN dbo.[@AST_011] ES  ON E.U_EMPSUP = ES.Code
					Where E.U_EMPSUP = @U_EMPSUP", new SqlParameter("@U_EMPSUP", codSuperior)).ToList();
			return Listado;
		}

        public List<Empleado> ListaEmpleados(String codSuperior)
        {
            List<Empleado> Listado = contexto.Database.SqlQuery<Empleado>(@"SELECT E.[DocEntry] As Id
                            ,E.[Name] As nombre
	                        ,ISNULL(P.Name,'') As puesto
                            ,E.Code as codigo
                            FROM [dbo].[@AST_011] E
                            Left Outer Join [dbo].[@AST_702] P ON E.U_PUE=P.Code
					        Where E.U_EMPSUP = @U_EMPSUP", new SqlParameter("@U_EMPSUP", codSuperior)).ToList();
            return Listado;
        }

        public List<EmpleadoHorario> ListaEmpleadosActivos()
		{
			String query = @"SELECT EH.DocEntry,EH.U_EMP AS NumEmpleado,
					E.U_APEPAT + ' ' + E.U_APEMAT + ' ' + E.U_NOMP AS Nombre,
					ES.U_APEPAT + ' ' + ES.U_APEMAT + ' ' + ES.U_NOMP AS NombreSuperior,
					EH.U_FINI AS Inicio,U_FFIN AS Fin,EH.U_Aprobado AS Aprobado,EH.U_HorarioESC,EH.U_MOTIVOREC,EH.U_NUMREC
                    From dbo.[@AST_H05] EH INNER JOIN dbo.[@AST_011] E  ON EH.U_EMP = E.Code
					LEFT OUTER JOIN dbo.[@AST_011] ES  ON E.U_EMPSUP = ES.Code
					WHERE ISNULL(EH.Canceled,'0')='0' AND EH.U_HorarioESC=1";
				List<EmpleadoHorario> Listado = contexto.Database.SqlQuery<EmpleadoHorario>(query).ToList();
			return Listado;
		}
    }
}
