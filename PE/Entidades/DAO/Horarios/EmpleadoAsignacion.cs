﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.Horarios
{
	[Table("[@AST_H05]")]
	public class EmpleadoAsignacion : TablaDocumento
	{
		[Required]
		[MaxLength(8)]
		public String U_EMP { get; set; }
		private short _U_ANIO = (short)System.DateTime.Today.Year;
		[Display(Description = "Año")]
		[Required]
		public short U_ANIO { get=> _U_ANIO; set => _U_ANIO = value; }
		[Display(Description = "Período")]
		[Required]
		public short U_PER { get; set; }
		[Display(Description = "Duración")]
		[Required]
		public short U_DUR { get; set; }
		[Display(Description = "Fecha Inicial")]
		[Required]
		public DateTime U_FINI { get; set; }
		[Display(Description = "Fecha Final")]
		[Required]
		public DateTime U_FFIN { get; set; }
		[Display(Description = "Aprobado")]
		[Required]
		public Boolean U_Aprobado { get; set; }
		[Display(Description = "Horario académico")]
		[Required]
		public Boolean U_HorarioESC { get; set; }
		private String _motivoRechazo = String.Empty;
		[Display(Description = "Motivo de Rechazo")]
		public String U_MOTIVOREC { get => _motivoRechazo; set => _motivoRechazo = value; }
		private short _U_NUMREC = 0;
		[Display(Description = "No. Rechazos")]
		public short U_NUMREC { get => _U_NUMREC; set => _U_NUMREC = value; }
		private String _U_CVEUNI = String.Empty;
		[Display(Description = "Cve. Universidad")]
		public String U_CVEUNI { get => _U_CVEUNI; set => _U_CVEUNI = value; }
		private String _U_OBS = String.Empty;
		[Display(Description = "Observaciones")]
		public String U_OBS { get => _U_OBS; set => _U_OBS=value; }
	}
}
