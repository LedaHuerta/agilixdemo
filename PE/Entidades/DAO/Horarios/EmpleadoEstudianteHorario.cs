﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplics.Servicios.Data.Entidades.Generales;
namespace Aplics.Servicios.Entidades.Horarios
{
	[Table("[@AST_H06]")]
	public class EmpleadoEstudianteHorario:TablaLineaDocumento
	{
		public byte U_TIPOH { get; set; }
		public byte U_DIAINI { get; set; }
		public byte U_HORAINI { get; set; }
		public byte U_MINUTOINI { get; set; }
		public byte U_DIAFIN { get; set; }
		public byte U_HORAFIN { get; set; }
		public byte U_MINUTOFIN { get; set; }
	}
}
