﻿using System.Web;

using System.Web.Optimization;

namespace PortalEmpleadosV2
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //         bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //                     "~/assets/libs/jquery-{version}.js"));

            //         bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //                     "~/assets/libs/jquery.validate*"));

            //         // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            //         // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            //         bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //                     "~/assets/libs/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //                   "~/assets/libs/bootsrap4/js/bootstrap.js",
            //                   "~/assets/libs/Popper/popper.min.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //                   "~/assets/libs/styles/bootsrap4/css/bootstrap.css",
            //                   "~/assets/libs/styles/site.css"));



            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootsrap4/js/bootstrap.js",
                      "~/Scripts/Popper/popper.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootsrap4/css/bootstrap.css",
                      "~/Content/site.css"));

        }
    }
}
