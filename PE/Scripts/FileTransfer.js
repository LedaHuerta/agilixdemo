﻿var postUrl;
var folderUpload;
var keyProcess;
var rowCount = 0;

function estadopanel(obpanel, pvisible) {
    var visible = "visible";
    var disp = "";
    if (pvisible === "0") {
        visible = "hidden";
        disp = "none";
    }
    obpanel.style.visibility = visible;
    obpanel.style.display = disp;
    return false;
}

var objActual;
var loading;
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function leer(ev) {
    objActual.style.backgroundImage = "url('" + ev.target.result + "')";
}

function drop(ev, ob, URL,funcion) {
    postUrl = URL;
    loading = $("#imgCopiar");
    objActual = ob;
    ev.preventDefault();
    var arch = new FileReader();
    if (ev.dataTransfer.files.length > 1) {
        $(loading).show();
    } else {
        arch.addEventListener('load', leer, false);
        arch.readAsDataURL(ev.dataTransfer.files[0]);
    }
    handleFileUpload(ev.dataTransfer.files, funcion);
}

function sendFileToServer(formData, status, funcion) {
    //Upload URL
    var uploadURL = postUrl + "?FolderKey=" + folderUpload + "&keyProcess=" + keyProcess;
    var extraData = {}; //Extra Data.
    var jqXHR = $.ajax({
        xhr: function () {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                xhrobj.upload.addEventListener('progress', function (event) {
                    var percent = 0;
                    var position = event.loaded || event.position;
                    var total = event.total;
                    if (event.lengthComputable) {
                        percent = Math.ceil(position / total * 100);
                    }
                    //Set progress
                    status.setProgress(percent);
                }, false);
            }
            return xhrobj;
        },
        url: uploadURL,
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        async: false,
        data: formData,
        success: function (data) {
            status.setProgress(100);
            $("#status1").append("Archivo transferido<br>");
            location.reload(true);
            funcion(folderUpload);
        },
        error: function (xhr, status, p3, p4) {
            var err = "Error " + " " + status + " " + p3 + " " + p4;
            if (xhr.responseText && xhr.responseText[0] === "{") {
                err = JSON.parse(xhr.responseText).Message;
                $("#status1").append(err);
            }
        }
    });
    status.setAbort(jqXHR);
}

function createStatusbar() {
    var obj = $("#StatusBar");
    rowCount++;
    var row = "odd";
    if (rowCount % 2 === 0) row = "even";
    this.statusbar = $("<div class='statusbar " + row + "'></div>");
    this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
    this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
    this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
    this.abort = $("<div class='abort'>Abort</div>").appendTo(this.statusbar);
    obj.after(this.statusbar);

    this.setFileNameSize = function (name, size, tipo) {
        var sizeStr = "";
        var sizeKB = size / 1024;
        if (parseInt(sizeKB) > 1024) {
            var sizeMB = sizeKB / 1024;
            sizeStr = sizeMB.toFixed(2) + " MB";
        }
        else {
            sizeStr = sizeKB.toFixed(2) + " KB";
        }

        this.filename.html(name + " " + tipo);
        this.size.html(sizeStr);
    },
        this.setProgress = function (progress) {
            var progressBarWidth = progress * this.progressBar.width() / 100;
            this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
            if (parseInt(progress) >= 100) {
                this.abort.hide();
            }
        },
        this.setAbort = function (jqxhr) {
            var sb = this.statusbar;
            this.abort.click(function () {
                jqxhr.abort();
                sb.hide();
            });
        };
}

function handleFileUpload(files, funcion) {
    var tipo = "";
    var multiple = false;
    if (files.length > 1) {
        multiple = true;
    }
    var name = "";
    for (var i = 0; i < files.length; i++) {
        tipo = files[i].type;
        name = "SendForm" + i;
        var forma = $('<form id="' + name + '" name="' + name + '" enctype="multipart/form-data"></form>');
        var fd = new FormData($(forma));
        fd.append('file', files[i]);
        var status = new createStatusbar(); //Using this we can set progress.
        status.setFileNameSize(files[i].name, files[i].size, tipo);
        sendFileToServer(fd, status, funcion);
    }
    $(loading).hide();
 }