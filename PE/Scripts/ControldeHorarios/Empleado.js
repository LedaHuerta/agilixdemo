﻿//import de from "../../assets/modulos/moment/src/locale/de";
//import moment from "../../assets/modulos/moment/src/moment";


var horariosGenerales;

function EliminarConstancia(obj, parchivo) {
    $('#divProgreso').modal('show');
    $.ajax({
        // En data puedes utilizar un objeto JSON, un array o un query string
        data: { archivo: parchivo },
        //Cambiar a type: POST si necesario
        type: "POST",
        // Formato de datos que se espera en la respuesta
        dataType: "json",
        // URL a la que se enviará la solicitud Ajax
        url: "../ControldeHorarios/EliminarConstancia"
    })
        .done(function (data, textStatus, jqXHR) {
            $('#divProgreso').modal('hide');
            var r = $(obj).parent().parent();
            $(r).remove();
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            $('#divProgreso').modal('hide');
            alert("La solicitud a fallado: " + textStatus);
     });
}

function fnMuestraConstancia(key) {
    $("#claveEmpleado").val(key);
    $("#formVisor").submit();
}

var lastIndexH = 4;

function numHorariosEmpleado(numhorarios) {
    lastIndexH = numhorarios;
}

function changeId(obj,newid) {
    $(obj).attr("id", newid);
    $(obj).attr("name", newid);
}

//function verificaHorarios() {
//    var dia;
//    var horarioInicial;
//    var horarioFinal;
//    var errorHorario;
//    var hayErrores = false;
//    var numhorarios = 0;
//    var objHorario;
//    var realIndex = 0;
//    var ajustarIndex=false;
//    for (var index = 0; index <= lastIndexH; index++) {
//        objHorario = document.getElementById("Horarios[" + index + "].HoraInicial");
//        if (objHorario !== undefined && objHorario !== null) {
//            if (ajustarIndex) {
//                dia = document.getElementById("Horarios[" + index + "].dia");
//                horarioInicial = document.getElementById("Horarios[" + index + "].HoraInicial");
//                horarioFinal = document.getElementById("Horarios[" + index + "].HoraFinal");
//                errorHorario = document.getElementById("Error_" + index);
//                changeId($(dia), "Horarios[" + realIndex + "].dia");
//                changeId($(horarioInicial), "Horarios[" + realIndex + "].HoraInicial");
//                changeId($(horarioFinal), "Horarios[" + realIndex + "].HoraFinal");
//                changeId($(errorHorario), "Error_" + realIndex);
//            }
//            horarioInicial = document.getElementById("Horarios[" + realIndex + "].HoraInicial").value;
//            horarioFinal = document.getElementById("Horarios[" + realIndex + "].HoraFinal").value;
//            document.getElementById("Error_" + realIndex).style.display = "none";
//            if (horarioInicial !== "" || horarioFinal !== "") {
//                if (horarioInicial >= horarioFinal || horarioInicial.length < 5 || horarioFinal.length < 5) {
//                    hayErrores = true;
//                    document.getElementById("Error_" + realIndex).style.display = "";
//                }
//                numhorarios += 1;
//            }
//            realIndex += 1;
//        } else {
//            ajustarIndex = true;
//        }
//    }
//    if (ajustarIndex) {
//        lastIndexH = realIndex;
//    }
//    if (numhorarios === 0)
//    {

//        hayErrores = true;
//    }
//    if (hayErrores) {
//        document.getElementById("Error_" + 0).style.display = "";
//        document.getElementById("Error_" + 4).style.display = "";
//        return false;
//    } else {
//        //$("#form1").submit();
//        return true;
//    }  
//}


function SolicitudDetalle(pid) {
    $("#id").val(pid);
    Data = { "id": $("#id").val(), "filtroPerfil": $("#filtroPerfil").val(), "Status": $("#Status").val() };
    senDataAjaxJsonDefault(Data, "../Horarios/BuscarSolicitudxId", consultaSol);
}

function consultaSol(Data) {
    $("#DetalleSolicitudHorario").html(Data);
    recalcularHoras();
}

function SolicitudDetalleHistorico(pid) {
    $("#id").val(pid);
    window.open("../Horarios/GetSolicitudxId?id=" + $("#id").val() + "&Status=" + $("#Status").val());
}

function consultaSolDetalleHistorico(Data) {
    $("#collapse-hist-horarios1").html(Data);
}

function filtroSolicitudesAnio(anio) {
    var form = $('<form action="../Horarios/FiltroAnio" method="post"></form>');
    var inputAnio = $("<input name='Anio' value='" + $("#filtroAnio").val() + "'/>");
    var inputStatus = $("<input name='Status' value='" + $("#StatusFiltroSol").val() + "'/>");
    $(inputAnio).appendTo($(form));
    $(inputStatus).appendTo($(form));
    $(form).appendTo('body').submit();
}

function filtroSolicitudesEmp() {
    var form = $('<form action="../Horarios/FiltroEmpleado" method="post"></form>');
    var inputEmp = $("<input name='Empleado' value='" + $("#filtroEmpleado").val() + "'/>");
    var inputStatus = $("<input name='Status' value='" + $("#StatusFiltroSol").val() + "'/>");
    $(inputEmp).appendTo($(form));
    $(inputStatus).appendTo($(form));
    $(form).appendTo('body').submit();
}

function aprobarSolPermiso() {
    Data = { "motivoRechazo": "", "id": $("#idSolApr").val(), "SolComentariosRec": $("#ObservacionesSolicitud").val(), "horarios": HorariosEmpleado() };
    $("#aprob-sol").attr("disabled", "disabled");
    senDataAjaxJsonDefault(Data, "../Horarios/AprobarHorario", resultAprobacionRechazo);
}

function HorariosEmpleado() {
    horarios = [];
    var arrhorarios;
    for (var xTipo = 0; xTipo < horariosGenerales.length; xTipo++) {
        arrhorarios = horariosGenerales[xTipo].Horarios;
        for (var xh = 0; xh < arrhorarios.length; xh++) {
            if (arrhorarios[xh].HoraInicial !== "" && arrhorarios[xh].HoraFinal !== "") {
                horarios.push(
                    {
                        "TipoHorario": horariosGenerales[xTipo].Tipo,
                        "dia": arrhorarios[xh].dia,
                        "HoraInicial": arrhorarios[xh].HoraInicial,
                        "HoraFinal": arrhorarios[xh].HoraFinal
                    }
                );
            }
        }
    }
    return horarios;
}

function rechazarSolPermiso() {
    MotivoRechazo = document.getElementById("mot-rechazo").value;
    var pValErr = document.getElementById("error-rechazo");
    if (MotivoRechazo !== "" && MotivoRechazo !== undefined) {
        pValErr.innerHTML = "";
        $("#rechazar-solicitud").attr("disabled", "disabled");
        Data = { "motivoRechazo": MotivoRechazo, "id": $("#idSolApr").val(), "SolComentariosRec": $("#SolComentariosHor").val(), "horarios": HorariosEmpleado() };
        senDataAjaxJsonDefault(Data, "../Horarios/RechazarHorario", resultAprobacionRechazo);
    } else {
        pValErr.innerHTML = "Debes indicar el motivo de rechazo";
    }
}

function resultAprobacionRechazo(Data) {
    if (Data === '') {
        $("#modal-confirm-aprob").modal("hide");
        $("#modal-confirm-recha").modal("hide");
        $("#btnAprobarHorario").hide();
        window.location.replace("../Horarios/HorariosAdminStatus");
    } else {
        alert("Error: " + Data);
    }
}
function irAAdminStatus(Data) {

}

function validaFechaHor() {
    var fechaInicial = moment($("#fini").val(), 'YYYY-MM-DD', true);
    var fechaFinal = moment($("#ffin").val(), 'YYYY-MM-DD', true);

    if (fechaInicial.isAfter(fechaFinal)) {
        document.getElementById("parrafoHor").innerHTML = "La fecha inicial no puede ser posterior a la fecha final";
        return false;
    } else if ($("#univer").val() === "") {
        document.getElementById("parrafo-univ").innerHTML = "La universidad es obligatoria";
        return false;
    } else {
        document.getElementById("parrafoHor").innerHTML = "";
        document.getElementById("parrafo-univ").innerHTML = "";
        return true;
    }
}

function ValidaArchivoConstancia() {
    if ($("#divsol-constancia").html().trim() === "")
    {
        document.getElementById("valida-errores-const").innerHTML = "Debe subir su constancia para continuar."
        //alert("Debe subir su constancia.");
        return false;
    }
    else
    {
        document.getElementById("valida-errores-const").innerHTML = "";
        return true;
    }
}

function back() {
    window.history.back();
}

function addDiaHorario(obj) {
    var divdia = $(obj).parent();
    var newdia = $(divdia).clone();
    $(newdia).insertAfter($(divdia));
    var nodos = $(newdia).children();
    var nomobj = $(obj).attr("data-name");
    var ob;
    var obHoras;
    var obHorasF;
    lastIndexH += 1;
    var newId;
    ob = nodos[0];
    if (nomobj !== "HorarioTipo") {
        newId = nomobj + "[" + lastIndexH + "].";
        $(ob).children(".SelectHorario").attr("id", newId + "dia");
        $(ob).children(".SelectHorario").attr("name", newId + "dia");
        ob = nodos[1];
        obHoras = $(ob).children(0).children(0).children(".InputHorario");
        $(obHoras).attr("valor-index", lastIndexH);
        ob = nodos[2];
        obHorasF = $(ob).children(0).children(0).children(".InputHorario");
        $(obHorasF).attr("valor-index", lastIndexH);
        $(newdia).children(".msg-err").attr("id", "Error_" + lastIndexH);
    } else {
        var ri = lastIndexH - 1;
        newId = nomobj + "[" + ri + "].";
        obHoras = $(ob).children(".InputHorario");
        $(obHoras).attr("valor-index", lastIndexH-1);
        ob = nodos[1];
        obHorasF = $(ob).children(".InputHorario");
        $(obHorasF).attr("valor-index", lastIndexH-1);
        ob = $(newdia).parent().children(".box-error-val").children(".valMsg");
        $(ob).attr("id", "valMsg-" + lastIndexH + "-laboral");
    }
    $(obHoras).attr("id", newId + "HoraInicial");
    $(obHoras).attr("name", newId + "HoraInicial");
    $(obHorasF).attr("id", newId + "HoraFinal");
    $(obHorasF).attr("name", newId + "HoraFinal");
    ob = $(newdia).children(".addHorario");
    $(ob).attr("data-index", lastIndexH);
    ob = $(newdia).children(".deleteHorario");
    $(ob).attr("data-index", lastIndexH);
    $(ob).show();
    $(obHoras).val("");
    $(obHorasF).val("");
    //llamar agregarHorario(newdia);
    if (nomobj === "HorarioTipo") {
        var countElements = agregarHorario($(obHorasF), "a");
        $(obHoras).attr("valor-matriz", countElements);
        $(obHorasF).attr("valor-matriz", countElements);
    } else {
        agregarHorario($(obHorasF),"e")
    }
}

function deleteDiaHorario(obj) {
    var divdia = $(obj).parent();
    var nodos = $(divdia).children();
    var nomobj = $(obj).attr("data-name");
    var ob;
    var obHorasF;
    var obHoras;
    ob = nodos[0];
    obHorasF = $(ob).children(".InputHorario");
    obHoras = $(ob).children(".InputHorario");
    if (nomobj === "HorarioTipo") {
        var countElements = eliminarHorario($(obHorasF));
        recalcularHoras();
        $(divdia).remove();
    } else if (nomobj === "Horarios") {
        $(divdia).remove();
    }
}
function eliminarHorario(obj) {
    var obHorario = obtenerHorariosDom(obj);
    horariosGenerales[obHorario.tipo - 1].Horarios.splice(-1, 1);
    var countElements = horariosGenerales[obHorario.tipo - 1].Horarios.length;
    return countElements;
};

function recalcularHoras() {
    var acumuladosTipo = [{ "dia": 1, "horarios": [] }, { "dia": 2, "horarios": [] }, { "dia": 3, "horarios": [] }, { "dia": 4, "horarios": [] }, { "dia": 5, "horarios": [] }];
    horarios = horariosGenerales[1].Horarios;
    for (x = 0; x < horarios.length; x++) {
        var horaInicial = horarios[x].HoraInicial;
        var horaFinal = horarios[x].HoraFinal;
            
        if (horariosGenerales[1].Tipo === 2) {
            var indexdia = acumuladosTipo.findIndex(function (element) { return element.dia === horarios[x].dia });
            acumuladosTipo[indexdia].horarios.push({ "HoraInicial": horaInicial, "HoraFinal": horaFinal });
            calcularHoras(acumuladosTipo, horarios[x].dia);
        }
    }
    calculoGeneralHoras();
}

function calcularDiferencia(HoraInicial, HoraFinal) {
    var horarioInicial = HoraInicial.split(":");
    var horarioFinal = HoraFinal.split(":");
    var horasIni = parseInt(horarioInicial[0]) + parseInt(horarioInicial[1]) / 60;
    var horasFin = parseInt(horarioFinal[0]) + parseInt(horarioFinal[1]) / 60;
    var resHoras = horasFin - horasIni;
    return resHoras;
}

function calcularHoras(acumuladosTipo, dia) {
    var acumuladosDia = acumuladosTipo.find(function (element) { return element.dia === dia; });
    var totalHoras = 0;
    var totalDifHour = 0;
    var totalDifMin = 0;
    
    for (i = 0; i < acumuladosDia.horarios.length; i++) {
        var elemResInicial = acumuladosDia.horarios[i].HoraInicial;
        var elemResFinal = acumuladosDia.horarios[i].HoraFinal;

        if (elemResInicial !== "" && elemResFinal !== "") {
            totalHoras += calcularDiferencia(elemResInicial, elemResFinal);
        }

    }   
    var pHorasLabel = document.getElementById("HorasLaborables_" + dia);
    var pMinutosLabel = document.getElementById("MinutosLaborables_" + dia);
    totalDifHour = Math.trunc(totalHoras);
    totalDifMin = (totalHoras - totalDifHour) * 60;

    pHorasLabel.innerHTML = totalDifHour + " Horas";
    pMinutosLabel.innerHTML = parseInt(totalDifMin) + " Mins";

  
}

function calculoGeneralHoras() {
    var totalGeneral = 0;
    var totalLaboral = 0;
    var totalComida = 0;
    var diferencia = 0;
    var tHorasLabel = document.getElementById("totalhoraslab");
    var tMinutosLabel = document.getElementById("totalminutoslab");
    var pHorasCom = document.getElementById('totalhorascomsemana');
    var pMinsCom = document.getElementById('totalminutoscomsemana');
    var pHorasSem = document.getElementById('totalhoraslabsemana');
    var pMinsSem = document.getElementById('totalminutoslabsemana');

    for (i = 1; i < horariosGenerales.length; i++) {
        horarios = horariosGenerales[i].Horarios;
        for (x = 0; x < horarios.length; x++) {
            var horaInicial = horarios[x].HoraInicial;
            var horaFinal = horarios[x].HoraFinal;
            if (horaInicial !== "" && horaFinal !== "") {
                diferencia = calcularDiferencia(horaInicial, horaFinal);
                if (horariosGenerales[i].Tipo === 3) {
                    totalComida += diferencia;
                } else {
                    totalLaboral += diferencia;
                }
            }
        }
    }

    var horasLab = Math.trunc(totalLaboral);
    var minLab = (totalLaboral - horasLab) * 60;
    var horasCom = Math.trunc(totalComida);
    var minCom = (totalComida - horasCom) * 60;
    var semDecimal = totalLaboral - totalComida;
    var horasSem = Math.trunc(semDecimal);
    var minSem = (semDecimal - horasSem) * 60;

    tHorasLabel.innerHTML = horasLab + " Horas";
    tMinutosLabel.innerHTML = parseInt(minLab) + " Mins";
    pHorasCom.innerHTML = horasCom + " Horas";
    pMinsCom.innerHTML = parseInt(minCom) + " Mins";
    pHorasSem.innerHTML = horasSem + " Horas";
    pMinsSem.innerHTML = parseInt(minSem) + " Mins";
}


function obtenerHorariosDom(obj) {
    var horarioInicial;
    var horarioFinal;
    var id = $(obj).attr("id");
    var dia = parseInt($(obj).attr("valor-dia"));
    var tipo = parseInt($(obj).attr("valor-tipo"));
    var indexH = parseInt($(obj).attr("valor-index"));
    var indexM = parseInt($(obj).attr("valor-matriz"));
    horarioInicial = document.getElementById('HorarioTipo[' + indexH + '].HoraInicial').value;
    horarioFinal = document.getElementById('HorarioTipo[' + indexH + '].HoraFinal').value;
    //llamando a funcion que calcula horas:
    //for (i = 0; horariosGenerales[1].Horarios > i; i++) {
    //    if (horariosGenerales[1].Horarios[i].HoraInicial != "" && horariosGenerales[1].Horarios[i].HoraFinal != "") {
    //    calcularHoras(id, dia, horarioInicial, horarioFinal, tipo);
    //    }
    //}

    return { "HoraInicial": horarioInicial, "HoraFinal": horarioFinal, "id": id, "indexH": indexH, "dia": dia, "tipo": tipo, "indexM": indexM };
}

function agregarHorario(obj, perfil) {
    var obHorario;
    if (perfil === "a") {
        obHorario = obtenerHorariosDom(obj);
    } else {
        obHorario = obtenerHorariosDomEstudiante(obj);
    }
    var countElements = horariosGenerales[obHorario.tipo -1].Horarios.length;
    horariosGenerales[obHorario.tipo - 1].Horarios.push(
        { dia: obHorario.dia, HoraInicial: obHorario.HoraInicial, HoraFinal: obHorario.HoraFinal }
    );
    return countElements;
    
};

function HorarioComida(obj) {
    var horario = $(obj).val();
    if (horario !== undefined && horario !== "") {
        var AHorario = horario.split('-');
        document.getElementById("HorarioTipo[" + $(obj).attr("valor-index") + "].HoraInicial").value = AHorario[0];
        document.getElementById("HorarioTipo[" + $(obj).attr("valor-index") + "].HoraFinal").value = AHorario[1];
        horIni = AHorario[0];
        horFin = AHorario[1];
    }
    var obHorario = obtenerHorariosDom(obj);
    horariosGenerales[2].Horarios[obHorario.dia - 1].HoraInicial = horIni;
    horariosGenerales[2].Horarios[obHorario.dia -1].HoraFinal = horFin;
    console.log("despues", horariosGenerales);
    //llamar función calculo general:
    calculoGeneralHoras();
};

//funcion tarea que al cambiar los datos del input actualice en el array, tiene que llamar a valida Empalme
function cambiarHorario(obj) {
    var obHorario = obtenerHorariosDom(obj);
    horariosGenerales[obHorario.tipo -1].Horarios[obHorario.indexM].HoraInicial = obHorario.HoraInicial;
    horariosGenerales[obHorario.tipo - 1].Horarios[obHorario.indexM].HoraFinal = obHorario.HoraFinal;
    if (obHorario.HoraInicial !== "" && obHorario.HoraFinal !== "") {
        controladorCambio(obj);
    }
}

function controladorCambio(obj) {
    var horarios;
    var obHorario = obtenerHorariosDom(obj);
    var acumuladosTipo = [{ "dia": 1, "horarios": [] }, { "dia": 2, "horarios": [] }, { "dia": 3, "horarios": [] }, { "dia": 4, "horarios": [] }, { "dia": 5, "horarios": [] }];
    var Limpiar = true;
    for (iTipoHorario = 0; iTipoHorario < 2; iTipoHorario++) {
        horarios = horariosGenerales[iTipoHorario].Horarios;
        for (x = 0; x < horarios.length; x++) {
            var horaInicial = horarios[x].HoraInicial;
            var horaFinal = horarios[x].HoraFinal;
            if ((horariosGenerales[iTipoHorario].Tipo !== obHorario.tipo || (horariosGenerales[iTipoHorario].Tipo === obHorario.tipo && x !== obHorario.indexM)) && horarios[x].dia === obHorario.dia) {
                validaEmpalme(horaInicial, horaFinal, obHorario.HoraInicial, obHorario.HoraFinal, obj, Limpiar);
                Limpiar = false;
            }
            if (horariosGenerales[iTipoHorario].Tipo === 2) {
                var indexdia = acumuladosTipo.findIndex(function (element) { return element.dia === horarios[x].dia });
                acumuladosTipo[indexdia].horarios.push({ "HoraInicial": horaInicial, "HoraFinal": horaFinal });
                calcularHoras(acumuladosTipo, horarios[x].dia);
            }
        }
    }
    calculoGeneralHoras();
}

function controladorCambioEstudiante(obj) {
    var horarios;
    var obHorario = obtenerHorariosDomEstudiante(obj);
    var acumuladosTipo = [{ "dia": 1, "horarios": [] }, { "dia": 2, "horarios": [] }, { "dia": 3, "horarios": [] }, { "dia": 4, "horarios": [] }, { "dia": 5, "horarios": [] }];
    horarios = horariosGenerales[0].Horarios;
    var Limpiar = true;
    for (x = 0; x < horarios.length; x++) {
        var horaInicial = horarios[x].HoraInicial;
        var horaFinal = horarios[x].HoraFinal;
        if (x !== obHorario.indexH && horarios[x].dia === obHorario.dia) {
            validaEmpalmeEstudiante(horaInicial, horaFinal, obHorario.HoraInicial, obHorario.HoraFinal, obj, Limpiar);
            Limpiar = false;
        }
    }
}

function validaEmpalme(horaInicial, horaFinal, obHoraInicial, obHoraFinal, obj,Limpiar) {
    
    var divdia = $(obj).parent().parent().parent();
    var obError = $(divdia).children(".box-error-val").children(".valMsg");
    var errorMsg = validaEmpalmeError(horaInicial, horaFinal, obHoraInicial, obHoraFinal);
    if (errorMsg !== "") {
        $(obError).html(errorMsg);
        $("#btnAprobarHorario").attr("disabled", "disabled");
        $("#btnRechazarHorario").attr("disabled", "disabled");
    } else {
        if (Limpiar) {
            $(obError).html("");
            $("#btnAprobarHorario").removeAttr("disabled");
            $("#btnRechazarHorario").removeAttr("disabled");
        }
    }
}

function validaEmpalmeEstudiante(horaInicial, horaFinal, obHoraInicial, obHoraFinal, obj, Limpiar) {
    var divdia = $(obj).parent().parent().parent().parent();
    var obError = $(divdia).children(".msg-err");
    var errorMsg = validaEmpalmeError(horaInicial, horaFinal, obHoraInicial, obHoraFinal);
    if (errorMsg !== "") {
        $(obError).html(errorMsg);
    } else {
        if (Limpiar) {
            $(obError).html("");
        }
    }
}

function validaEmpalmeError(horaInicial, horaFinal, obHoraInicial, obHoraFinal) {
    var error = "";
    var horaEscIni = horaInicial;
    var horaEscFin = horaFinal;
    var horaLabIni = obHoraInicial;
    var horaLabFin = obHoraFinal;
    var aux1 = obHoraInicial.replace(':', '');
    var aux2 = obHoraFinal.replace(':', '');

    if (!$.isNumeric(aux1) || !$.isNumeric(aux2) || (horaLabIni.length !== 5 || horaLabIni.indexOf(':') < 0) || horaLabFin.length !== 5 || horaLabFin.indexOf(':') < 0) {
        error = "El formato es incorrecto. Utiliza el formato 24 horas Ejemplo: 14:00 ó 07:00";
    } else {
        if ((horaLabIni <= horaEscIni && horaLabFin >= horaEscFin) || (horaLabFin > horaEscIni && horaLabFin < horaEscFin) || (horaLabIni > horaEscIni && horaLabIni < horaEscFin)) {
            error = "Este horario se empalma con otro anterior. ";
        }
        if (horaLabIni >= horaLabFin) {
            error ="El horario inicial no puede ser menor que el final.";
        }
    } 

    return error;
}

function cambiarHorarioEstudiante(obj) {
    var obHorario = obtenerHorariosDomEstudiante(obj);
    horariosGenerales[0].Horarios[obHorario.indexH].HoraInicial = obHorario.HoraInicial;
    horariosGenerales[0].Horarios[obHorario.indexH].HoraFinal = obHorario.HoraFinal;
    if (obHorario.HoraInicial !== "" && obHorario.HoraFinal !== "") {
        controladorCambioEstudiante(obj);
    }
}

function obtenerHorariosDomEstudiante(obj) {
    var horarioInicial;
    var horarioFinal;
    var id = $(obj).attr("id");
    var tipo = parseInt($(obj).attr("valor-tipo"));
    var dia = parseInt($(obj).attr("valor-dia"));
    var indexH = parseInt($(obj).attr("valor-index"));
    horarioInicial = document.getElementById('Horarios[' + indexH + '].HoraInicial').value;
    horarioFinal = document.getElementById('Horarios[' + indexH + '].HoraFinal').value;

    return { "HoraInicial": horarioInicial, "HoraFinal": horarioFinal, "id": id, "indexH": indexH, "tipo": tipo, "dia": dia};
}

