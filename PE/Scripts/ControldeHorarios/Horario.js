﻿var currenid = 0;
var HorariosPersonal = [];
var model;
var hayCambios = false;

function InicializaSeleccionDias(pId) {
    $("#ddltipoHorario").attr("data-colorbg", "green");
    $("#DDLDiasSemana_" + pId).attr("multiple", "multiple");
    $("#DDLDiasSemana_" + pId).attr("data-actions-box", "true");
    $("#DDLDiasSemana_" + pId).attr("data-selected-text-format", "count");
    setTimeout(
        function () {
            
        }, 2000);
    $("#DDLDiasSemana_" + pId).selectpicker();
}

function salirEdicion() {
    if (hayCambios) {
        if (confirm("Hay cambios pendientes, ¿desea salir sin guardar?")) {
            hayCambios = false;
            $("#divHorarios").modal('hide');
        }
        return true;
    }
    hayCambios = false;
    $("#divHorarios").modal('hide');
    return true;
}

function agregaHorario(pId,pCode, pColor, pTipo, pStatus,pHorarios) {
    return HorariosPersonal.push({
        Id: pId, Status: pStatus, codeEmpleado: pCode, motivoRechazo: "", parametros: {
            indrag: false, colorActividad: pColor, colini: -1, colfin: -1, tablaini: "",
            rowIni: -1, TipoHorario: pTipo, horaIni:0, minutoIni:0}, Horarios: pHorarios
    });
}


function iniciarHorario(prowIni, ptablaini, pcolini, pcelda, phoraIni, pminutoIni) {
    HorariosPersonal[currenid].parametros.rowIni = prowIni;
    HorariosPersonal[currenid].parametros.tablaini = ptablaini;
    if (pcelda !== undefined) {
        $(pcelda).css('background', HorariosPersonal[currenid].parametros.colorActividad);
    }
    HorariosPersonal[currenid].parametros.colini = pcolini;
    HorariosPersonal[currenid].parametros.horaIni = phoraIni;
    HorariosPersonal[currenid].parametros.minutoIni = pminutoIni;
}

function aplicarHorario(sender, phInicial, phFinal, pLista, pCode) {
    buscarIndex(pCode);
    var pDiaini = 0;
    var pDiafin = 0;
    var brands = $('#' + pLista + ' option:selected');
    var obj;
    $(brands).each(function (index, brand) {
        obj = $(this);
        if (pDiaini === 0 && obj.val()!=="") {
            pDiaini = obj.index();
        }
        if (obj.val() !== "") {
            pDiafin = obj.index();
        }
    });
    if (pDiaini === 0 || pDiafin === 0) {
        alert("Debe indicar los días de la semana.");
        return false;
    }
    var pHoraIni = parseInt($("#iniValueH").val());
    var pMinutoIni = parseInt($("#iniValue").val());
    var pHoraFin = parseInt($("#finValueHF").val());
    var pMinutoFin = parseInt($("#finValue").val());
    if (pHoraFin < pHoraIni || (pHoraIni === pHoraFin && pMinutoFin < pMinutoIni)) {
        alert("Verifique el horario indicado.");
        return false;
    }
    if (pHoraIni < phInicial || pHoraFin > phFinal || pHoraIni > phFinal) {
        alert("Verifique que el horario sea hábil");
        return false;
    }
    if (selectHorarios(pDiaini, pDiafin, pHoraIni, pMinutoIni, pHoraFin, pMinutoFin, phInicial)) {
        hayCambios = true;
    }
}

function buscacolumna(pminuto) {
    minuto = parseInt(pminuto);
    if (minuto < 10) {
        return minuto - 1;
    }
    return parseInt(pminuto.substr(1,1))-1;
}

function selectHorarios(diaini, diafin, horaIni, minutoIni, horaFin, minutoFin, phInicial) {
    var msg = fillhorario(diaini, diafin, horaIni, minutoIni, horaFin, minutoFin, true, phInicial,false);
    if (msg !== "") {
        alert(msg);
        return false;
    }
    fillhorario(diaini, diafin, horaIni, minutoIni, horaFin, minutoFin, false, phInicial,false);
    //Agregar Horario
    var cons = HorariosPersonal[currenid].Horarios.length;
    var modelL = { Id: 0, Consecutivo: 0, VisOrder: 0, LogInst: 0, U_TIPOH: 0, U_DIAINI: 0, U_HORAINI: 0, U_MINUTOINI: 0, U_DIAFIN: 0, U_HORAFIN: 0, U_MINUTOFIN:0};
    modelL.Id = HorariosPersonal[currenid].Id;
    modelL.Consecutivo = cons + 1;
    modelL.VisOrder = 0;
    modelL.LogInst = 0;
    modelL.U_TIPOH = $("#ddltipoHorario").val();
    modelL.U_DIAINI = diaini;
    modelL.U_HORAINI = horaIni;
    modelL.U_MINUTOINI = minutoIni;
    modelL.U_DIAFIN = diafin;
    modelL.U_HORAFIN = horaFin;
    modelL.U_MINUTOFIN = minutoFin;
    HorariosPersonal[currenid].Horarios.push(modelL);
    return true;
}

function SeleccionHorarios(pIndex, pFIndex, pDataParent, pDataHora, pDataMinuto) {
    //Colorear
    if (!HorariosPersonal[currenid].parametros.indrag) {
        return false;
    }
    HorariosPersonal[currenid].parametros.indrag = false;
    HorariosPersonal[currenid].parametros.colfin = pIndex;
    var rowFin = pFIndex;
    var tablafin = pDataParent;
    var Arreglo1 = HorariosPersonal[currenid].parametros.tablaini.split("_");
    var Arreglo2 = tablafin.split("_");
    var numTabla1 = parseInt(Arreglo1[1]);
    var numTabla2 = parseInt(Arreglo2[1]);
    var diaini = parseInt(Arreglo1[2]);
    var diafin = parseInt(Arreglo2[2]);
    var paso = numTabla1;
    if (numTabla2 < numTabla1) {
        numTabla1 = numTabla2;
        numTabla2 = paso;
        paso = HorariosPersonal[currenid].parametros.colini;
        HorariosPersonal[currenid].parametros.colini = colfin;
        HorariosPersonal[currenid].parametros.colfin = paso;
        paso = dia1;
        diaini = diafin;
        diafin = paso;
        paso = HorariosPersonal[currenid].parametros.rowIni;
        HorariosPersonal[currenid].parametros.rowIni = rowFin;
        rowFin = paso;
    }
}

function fillhorario(pdiaIni, pdiafin, pHoraini, pminutoini, pHorafin, pminutofin, pvalidar, phInicial,plimpiar) {
    var valido = false;
    var curTab;
    pHorafin = pminutofin === 0 ? pHorafin - 1 : pHorafin;
    var pmi = 0;
    var pmf = 0;
    for (var x = pdiaIni; x <= pdiafin; x++) {
        for (var y = pHoraini; y <= pHorafin; y++) {
            valido = false;
            curTab = document.getElementById("TAB_" + (y - parseInt(phInicial) + 1) + "_" + x);
            pmi = y === pHoraini ? pminutoini : 0;
            pmf = y === pHorafin && pminutofin> 0 ? pminutofin - 1 : 59;
            valido = rellenaHora(curTab, pmi, pmf, pvalidar, plimpiar);
            if (pvalidar && ! valido) { return "El horario indicado se empalma con otros horarios.";}
        }
    }
    return "";
}

function rellenaHora(pTabla,pminutoini,pminutofin,pvalidar,limpiar){
    var nrows = 10;
    var ncols = 6;
    var colindex = 0;
    var col;
    var atrib;
    var minutoActual = 0;
    var jsonat;
    for (var rowIndex = 0; rowIndex < nrows; rowIndex++) {
        r = pTabla.rows[rowIndex];
        colindex = 0;
        for (colIndex = 0; colIndex < ncols; colIndex++) {
            col = r.cells[colIndex];
            if (minutoActual >= pminutoini && minutoActual <= pminutofin) {
                if (pvalidar) {
                    atrib = $(col).attr("data-asig");
                     //Ya se asigno algún horario en este intervalo.
                    if (atrib !== undefined && atrib!=="") {
                        jsonat = JSON.parse(atrib);
                        if (jsonat.def === 1) { return false; }
                    }
                } else {
                    if (limpiar) {
                        col.style.backgroundColor = "white";
                        $(col).removeAttr("data-asig");
                    } else {
                        col.style.backgroundColor = $("#ddltipoHorario").attr("data-colorbg");
                        $(col).attr("data-asig", '{"def":1, "TipoHorario":' + $("#ddltipoHorario").val() + "}");
                    }
                }
            }
            minutoActual += 1;
        }
    }
    return true;
}

function EliminarHorario(phInicial) {
    var index = 0;
    do {
        index = HorariosPersonal[currenid].Horarios.findIndex(function (element) {
            return element.U_TIPOH === parseInt($("#ddltipoHorario").val());
        });
        if (index >= 0) {
            var horario = HorariosPersonal[currenid].Horarios[index];
            fillhorario(horario.U_DIAINI, horario.U_DIAFIN, horario.U_HORAINI, horario.U_MINUTOINI, horario.U_HORAFIN, horario.U_MINUTOFIN, false, phInicial, true);
            HorariosPersonal[currenid].Horarios.splice(index, 1);
            $('#btnGuardarHorario').show();
            hayCambios = true;
        }
    } while (index>=0);
}

function VueSaveInit() {
    var VueSave = new Vue({
        el: "#GrabarHorario",
        data: {
            mensaje: "",
            urls: {
                urlGuardarHorarios: "../ControldeHorarios/GuardarHorarios"
            }
        },
        created: function () {
            
        },
        mounted: function () {
            
        },
        methods: {
            GuardarHorarios: function (event, fila) {
                if (HorariosPersonal.length === 0) {
                    alert("No se modificarón registros.");
                } else {
                    $('#divProgreso').modal('show');
                    var inst = this;
                    var Data = HorariosPersonal;
                    this.$http.post(this.urls.urlGuardarHorarios, Data).then(function (response) {
                        this.pausa(2000);
                        $('#divProgreso').modal('hide');
                        var result = response.body;
                        hayCambios = false;
                    }, function (a, b, c) {
                        this.mensaje = a.status + '-' + a.statusText;
                        $('#divProgreso').modal('hide');
                    });
                }
            },
            pausa: function (milisegundos) {
                var inst = this;
                setTimeout(
                    function () {
                        $('#divProgreso').modal('hide');
                    }, milisegundos);
            }
        }
    });
}

function UIDrop() {
   
}

function cambioTipoHorario(obj){
    var op = $(obj).children("option:selected");
    $(obj).css("background-color", $(op).attr("data-colorbg"));
    $(obj).attr("data-colorbg", $(op).attr("data-colorbg"));
    HorariosPersonal[currenid].parametros.colorActividad = $(op).attr("data-colorbg");
    HorariosPersonal[currenid].parametros.TipoHorario = $(op).val();
    $(obj).css("color", $(op).attr("data-color"));
}

function buscarIndex(pCode) {
    var indexr = HorariosPersonal.findIndex(function (element) {
        return element.codeEmpleado === pCode;
    });
    currenid = indexr;
}

function buscarHorario(pId, pStatus,pCode,pHorarios) {
    var indexr = HorariosPersonal.findIndex(function (element) {
        return element.Id === pId;
    });
    if (indexr < 0) {
        indexr = agregaHorario(pId, pCode, "green", 1, pStatus, pHorarios);
        indexr -= 1;
    }
    currenid = indexr;
}

function buscaEmpleadoHorario(emp, pHorarios) {
    var indexr = HorariosPersonal.findIndex(function (element) {
        return element.codeEmpleado === emp;
    });
    if (indexr >= 0) {
        HorariosPersonal[indexr].Horarios = pHorarios;
    }
}


//function validahora(horaInicialEsc, horaFinalEsc,  indexH) {
//    var idHoraInicialLab = 'HorarioTipo[' + indexH + '].HoraInicial';
//    var idHoraFinalLab = 'HorarioTipo[' + indexH + '].HoraFinal';
//    var isValid = true;
//    var mensaje = "";

//    var resFormIni = horaToFloat(document.getElementById(idHoraInicialLab).value)[1];
//    var resFormFin = horaToFloat(document.getElementById(idHoraFinalLab).value)[1];

//    var horaInicialLab = horaToFloat(document.getElementById(idHoraInicialLab).value, indexH)[0];
//    var horaFinalLab = horaToFloat(document.getElementById(idHoraFinalLab).value, indexH)[0];

//    if (resFormIni == true && resFormFin == true) {
//        if (horaFinalLab > horaInicialEsc && horaFinalLab < horaFinalEsc) {
//            isValid = false;
//            mensaje = "Hora final laboral no puede estar en el rango del horario escolar";
//            console.log(mensaje);
//        }
//        if (horaInicialLab > horaInicialEsc && horaInicialLab < horaFinalEsc) {
//            isValid = false;
//            mensaje = "Hora inicial laboral no puede estar dentro del rango del horario escolar";
//            console.log(mensaje);
//        }
//        if (horaInicialLab < horaInicialEsc && horaFinalLab > horaInicialEsc) {
//            isValid = false;
//            mensaje = "Hora final laboral se traslapa con el horario escolar";
//            console.log(mensaje);
//        }
//        if (horaInicialLab > horaFinalEsc && horaFinalLab < horaInicialLab) {
//            isValid = false;
//            mensaje = "Hora final laboral no puede ser anterior a la hora inicial laboral";
//            console.log(mensaje);
//        }
//        if (horaInicialLab > horaFinalLab) {
//            isValid = false;
//            mensaje = "Hora final laboral no puede ser anterior a la hora inicial laboral";
//            console.log(mensaje);
//        }
//        if (horaInicialLab == horaFinalLab) {
//            isValid = false;
//            mensaje = "Hora inicial laboral no puede ser igual a la hora final laboral hola Mich";
//            console.log(mensaje);
//        }
//        if (isValid) {
//            console.log("El horario es valido");
//        } else {
//            console.log("El horario no es valido");
//        }
//    } else {
//        mensaje = "Utiliza el formato en 24 horas Ejemplo: 14:00 ó 07:00"

//    }
       
//    document.getElementById("valMsg-" + indexH +"-laboral").innerHTML = mensaje;
//}
//function horaToFloat(horaStr) {
//    var result = 0;
//    var formato = false;
//    var mensaje = "";

//    if (horaStr.length > 0) {

//        if (horaStr.indexOf(":") != -1) {
//            var horaMinuto = horaStr.split(':');
//            var hora = horaMinuto[0] * 1;
//            var minuto = horaMinuto[1] * 1;

//            if (minuto > 0) {
//                minuto = minuto / 60;
//                result = hora + minuto;
//                formato = true;

//            } else {
//                result = hora;
//                formato = true;

//            }
//        } else {
//            formato = false;                
//        }
//        var resForm = [result, formato];
//    }
//    return resForm;
//}

