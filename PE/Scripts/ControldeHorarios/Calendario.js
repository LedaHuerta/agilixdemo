﻿var paramTabla;
var definicion;
var tableData;
var HorarioEditar;

$(document).ready(function () {
    $("#divHorariosEmpleado").hide();
    VueInitListado(paramTabla, definicion, tableData);
});

function cerrarEdicion() {
    $('#divHorariosEmpleado').hide();
    $("#boxListadoEmpleados").show();
}

function VueInitListado(parTabla, parDefinicion,  ptableData) {
    var VueTabla = new Vue({
        el: "#boxListadoEmpleados",
        data: {
            IdTabla: "",
            loading: true,
            mensaje: "",
            urls: {
                urlBusquedaDatos: "../ControldeHorarios/ConsultaDatos",
                urlEditarHorarioEmpleado: "../ControldeHorarios/EditarHorarioEmpleado",
                urlSupervisor: "../ControldeHorarios/SupervisorEmpleado",
                urlRH: "../ControldeHorarios/Administrador",
                urlRechazarHorarios: "../ControldeHorarios/RechazarHorarios",
                urlAutorizarHorarios: "../ControldeHorarios/AutorizarHorarios"
            },
            tabla: {
                paginacionCliente: true,
                registrosxpagina: 50,
                totalregistros: 0,
                paginaactual: 1,
                registroFinal: 0,
                totalpaginas: 0,
                Paginas: [],
                definicionCampos: []
            },
            filtro: {
                Empleado: "",
                FechaInicio: "",
                FechaFin: "",
                Status: "",
                Pagina: 1,
                NumeroRegistroPag: 10
             },
            Seleccion: { Registros: [] },
            DetalleRegistros: [],
            DetalleRegistrosBk: [],
            DetallePagina: [],
            EdicionRegistros: { Empleados: [] },
            MensajesRegistro: []
        },
        created: function () {
            var self = this;
            this.loading = true;
            this.IdTabla = parTabla;
            this.tabla.definicionCampos = parDefinicion;
            if ($.isArray(ptableData)) {
                var paginado = this.InicializarTabla(ptableData);
                this.paginarCliente(paginado.paginas, paginado.registros);
            }
        },
        mounted: function () {
            //$('[data-toggle="tooltip"]').tooltip();
        },
        methods: {
            PaginaDin: function (inc, dec) {
                var pag = 1;
                if (this.tabla.totalpaginas > 1) {
                    if (this.tabla.paginaactual <= 5) {
                        pag = 1 + inc;
                    } else {
                        if (this.tabla.paginaactual > this.tabla.totalpaginas - 5) {
                            pag = this.tabla.totalpaginas - dec;
                        } else {
                            if (inc === 4) {
                                pag = this.tabla.paginaactual + 1;
                            } else if (inc === 3) {
                                pag = this.tabla.paginaactual;
                            } else {
                                pag = this.tabla.paginaactual - dec - 2;
                            }
                        }
                    }
                }
                return pag;
            },
            InicializarTabla: function (paramData) {
                this.DetalleRegistros = paramData;
                this.DetalleRegistrosBk = this.DetalleRegistros;
                var paginado = this.preparaPaginado();
                return paginado;
            },
            CambioPaginacion: function () {
                if (this.tabla.paginacionCliente) {
                    var paginado = this.InicializarTabla(this.DetalleRegistros);
                    this.paginarCliente(paginado.paginas, paginado.registros);
                } else {
                    this.buscarDatos();
                }
            },
            CargarPaginas: function () {
                loading = true;
                this.tabla.Paginas = [];
                for (var j = 1; j <= this.tabla.totalpaginas; j++) {
                    this.tabla.Paginas.push({ pagina: j });
                }
                loading = false;
            },
            CambioListaPagina: function (event) {
                if (this.loading) {
                    return true;
                }
                var pag = event.target.value;
                this.SeleccionPagina(pag);
            },
            EstadoProgreso: function (activo) {
                if (activo) {
                    $('#divProgreso').modal('show');
                } else {
                    $('#divProgreso').modal('hide');
                }
            },
            LimpiarDatos: function () {
                this.Seleccion = { Registros: [] };
            },
            ConfiguraPaginacion: function () {
                this.tabla.paginaactual = 1;
                this.buscarDatos();
            },
            SeleccionPaginaDin: function (event) {
                var pag = parseInt(event.target.text);
                this.SeleccionPagina(pag);
            },
            VistaPaginas: function (pag) {
                var lis = $("ul.pagination > li");
                var obli;
                var oba;
                var y = 4;
                var z = pag - 2;
                for (var x = 3; x < 7; x++) {
                    obli = lis[x];
                    oba = $(obli).find("a");
                    if (pag < 5) {
                        $(oba).text(x - 1);
                    } else if (pag > this.tabla.totalpaginas - 4) {
                        $(oba).text(this.tabla.totalpaginas - y);
                    } else {
                        $(oba).text(z);
                    }
                    y -= 1;
                    z += 1;
                }
            },
            BuscaIndicesPagina: function (pagina) {
                var inicio = (pagina - 1) * parseInt(this.tabla.registrosxpagina);
                var fin = inicio + parseInt(this.tabla.registrosxpagina);
                var result = { inicial: inicio, final: fin };
                return result;
            },
            SeleccionPagina: function (pagina) {
                this.VistaPaginas(pagina);
                if (pagina !== this.tabla.paginaactual) {
                    $("ul.pagination > li").removeClass("active");
                    var lis = $("ul.pagination > li");
                    var obli;
                    var oba;
                    var text = "";
                    for (var x = 0; x < lis.length; x++) {
                        obli = lis[x];
                        oba = $(obli).find("a");
                        text = $(oba).text();
                        if (text === "" + pagina) {
                            $(obli).addClass("active");
                        }
                    }
                    this.tabla.paginaactual = pagina;
                    if (this.tabla.paginacionCliente) {
                        var indices = this.BuscaIndicesPagina(pagina);
                        this.DetallePagina = this.DetalleRegistros.slice(indices.inicial, indices.final);
                        if (this.tabla.totalpaginas === parseInt(this.tabla.paginaactual)) {
                            this.tabla.registroFinal = this.tabla.totalregistros;
                        } else {
                            this.tabla.registroFinal = (this.tabla.paginaactual - 1) * this.tabla.registrosxpagina + parseInt(this.tabla.registrosxpagina);
                        }
                    } else {
                        this.buscarDatos();
                    }
                }
            },
            eliminarFiltro: function () {
                var tbl = $(this.IdTabla);
                var row = $('tr:eq(0)', tbl);
                var child;
                $(row).children().each(function () {
                    cell = $(this);
                    child = $(cell).children();
                    if ($(child).length > 0) {
                        $(cell).empty();
                        cell.text(cell.attr("data-cap"));
                    }
                });
            },
            exportToExcel: function (columnaInicial) {
                var instancia = this;
                var table = "<table><tr>";
                var ncol = 0;
                for (ncol = 0; ncol < this.tabla.definicionCampos.length; ncol++) {
                    table += "<td>" + this.tabla.definicionCampos[ncol].Descripcion + "</td>";
                }

                table += "</tr>";
                var defcampo;
                for (var nrow = 0; nrow < this.DetalleRegistros.length; nrow++) {
                    table += "<tr>";
                    index = -1;
                    ncol = 0;
                    for (ncol = 0; ncol < this.tabla.definicionCampos.length; ncol++) {
                        dato = instancia.DetalleRegistros[nrow];
                        defcampo = instancia.obtenerCampo(dato, ncol, "DD/MM/YYYY HH:MM:SS");
                        table += "<td>" + defcampo.texto + "</td>";
                    }
                    table += "</tr>";
                }
                table += "</table>";
                window.open('data:application/vnd.ms-excel,' + table, 'Detalle');
                table = "";
            },
            obtenerCampo: function (dato, ncol, formato) {
                var key = this.tabla.definicionCampos[ncol].Nombre;
                var datavalue = dato[key] + "";
                var textdata = datavalue;
                if (datavalue.toLowerCase().indexOf("date") >= 0) {
                    textdata = moment(datavalue).format(formato);
                }
                return { valor: datavalue, texto: textdata };
            },
            autoFiltro: function (columnaInicial) {
                var tbl = $(this.IdTabla);
                var row = $('tr:eq(0)', tbl);
                var cell;
                var cellData;
                var index = -1;
                var child;
                var DataColumn = [];
                var found = -1;
                var ddl;
                var nrow = 0;
                var ncol = 0;
                var dataT = this.DetalleRegistros;
                var dato;
                var defcampo;
                var instancia = this;
                var filtrado = false;
                $(row).children().each(function () {
                    cell = $(this);
                    index += 1;
                    if (index >= columnaInicial - 1) {
                        child = $(cell).children();
                        if ($(child).length === 0) {
                            cell.attr("data-cap", cell.text());
                            DataColumn = [];
                            nrow = 0;
                            for (nrow = 0; nrow < dataT.length; nrow++) {
                                dato = dataT[nrow];
                                defcampo = instancia.obtenerCampo(dato, ncol, "DD/MM/YYYY");

                                found = DataColumn.findIndex(function (element) {
                                    return element.valor === defcampo.valor;
                                });

                                if (found < 0) {
                                    DataColumn.push({ valor: defcampo.valor, texto: defcampo.texto });
                                }
                            }
                            filtrado = true;
                            $(cell).append("<select class='selectpicker' multiple id='DDLFIL_" + ncol + "'></select>");
                            child = $(cell).children();
                            ddl = child[0];
                            $(ddl).selectpicker();
                            $(ddl).append("<option value='@ALL'>Todos</option>");
                            var op;
                            for (var x = 0; x < DataColumn.length; x++) {
                                op = DataColumn[x];
                                $(ddl).append("<option value='" + op.valor + "' >" + op.texto + "</option>");
                            }
                            $(ddl).change(function () {
                                instancia.filtrar(columnaInicial, row);
                            });
                        } else {
                            $(cell).empty();
                            cell.text(cell.attr("data-cap"));
                        }
                        ncol++;
                    }
                });
                if (!filtrado) {
                    this.DetalleRegistros = this.DetalleRegistrosBk;
                    var paginado = this.preparaPaginado();
                    this.paginarCliente(paginado.paginas, paginado.registros);
                }
            },
            filtrar: function (columnaInicial, row) {
                var index = -1;
                var ncol = 0;
                var cell;
                var child;
                var ddl;
                var valor = "";
                var Arr;
                var condiciones = [];
                var dataT = this.DetalleRegistrosBk;
                var dato = dataT[0];
                var todo = false;
                $(row).children().each(function () {
                    cell = $(this);
                    index += 1;
                    if (index >= columnaInicial - 1) {
                        child = $(cell).children();
                        ddl = $(child[0]).find("#DDLFIL_" + ncol);
                        valor = $(ddl).val();
                        Arr = [];
                        $(ddl).find('option:selected').each(function () {
                            valor = $(this).val();
                            Arr.push($(this).val());
                        });
                        if (Arr.length > 0) {
                            condiciones.push({ columna: Object.keys(dato)[ncol], filtro: Arr });
                        }
                        ncol += 1;
                    }
                });
                if (condiciones.length > 0) {
                    var elemData = "";

                    var Data = this.DetalleRegistrosBk.filter(function (element) {
                        var coincidencias = 0;
                        var pos = 0;
                        var numcondiciones = condiciones.length;
                        for (var cond = 0; cond < numcondiciones; cond++) {
                            pos = jQuery.inArray("@ALL", condiciones[cond].filtro);
                            if (pos < 0) {
                                elemData = eval("element." + condiciones[cond].columna) + "";
                                pos = jQuery.inArray(elemData, condiciones[cond].filtro);
                            }
                            coincidencias += pos < 0 ? 0 : 1;
                        }
                        return coincidencias === numcondiciones;
                    });

                    if (Data !== undefined) {
                        this.DetalleRegistros = Data;
                        var paginado = this.preparaPaginado();
                        this.paginarCliente(paginado.paginas, paginado.registros);
                    }
                }
            },
            preparaPaginado: function () {
                this.tabla.paginaactual = 1;
                var numregistros = this.DetalleRegistros.length;
                if (numregistros > 0) {
                    var indices = this.BuscaIndicesPagina(this.tabla.paginaactual);
                    this.DetallePagina = this.DetalleRegistros.slice(0, indices.final);
                } else {
                    this.DetallePagina = this.DetalleRegistros;
                }
                var numpags = Math.ceil(numregistros / this.tabla.registrosxpagina);
                return { paginas: numpags, registros: numregistros };
            },
            Confirmar: function (fila) {
                var codEmpleado = fila.NumEmpleado;
                this.EdicionRegistros.Empleados = [];
                HorariosPersonal = [];
                currenid = 0;
                buscarHorario(fila.DocEntry, 1, fila.NumEmpleado);
                HorariosPersonal[currenid].Status = 1;
                HorariosPersonal[currenid].motivoRechazo = "";
                fila.Aprobado = true;
                this.GuardarAutorizacion(true);
            },
            Rechazar: function (fila) {
                var codEmpleado = fila.NumEmpleado;
                this.EdicionRegistros.Empleados = [];
                HorariosPersonal = [];
                currenid = 0;
                buscarHorario(fila.DocEntry, 0, fila.NumEmpleado);
                HorariosPersonal[currenid].Status = 0;
                fila.Aprobado = false;
                var motivo=prompt("Motivo de Rechazo",""); 
                HorariosPersonal[currenid].motivoRechazo = motivo;
                fila.U_MOTIVOREC = motivo;
                this.GuardarAutorizacion(false);
            },
            GuardarAutorizacion: function (autorizar) {
                $('#divProgreso').modal('show');
                var inst = this;
                var Data = HorariosPersonal;
                var url = autorizar === false ? this.urls.urlRechazarHorarios : this.urls.urlAutorizarHorarios;
                this.$http.post(url, Data).then(function (response) {
                    this.pausa(2000);
                    $('#divProgreso').modal('hide');
                    var result = response.body;
                 }, function (a, b, c) {
                    this.mensaje = a.status + '-' + a.statusText;
                    $('#divProgreso').modal('hide');
                });
            },
            BuscarEdicion: function (Id) {
                var empleado = this.EdicionRegistros.Empleados.find(function (element) {
                    return element.Id === Id;
                });
                if (empleado === undefined) {
                    return true;
                } else {
                    return false;
                }
            },
            CerrarEdicion: function (pId, pClave) {
                var panel = $("#" + pClave);
                var row = $(panel).parent().parent().parent();
                var cell = $(row).find("td:first");
                $(cell).attr('colspan', 1);
                $(panel).html("");
                var indexEmp = this.EdicionRegistros.Empleados.findIndex(function (element) {
                    return element.Id === pId;
                });
                this.EdicionRegistros.Empleados.splice(indexEmp, 1);
            },
            EdicionRegistro: function (event, fila) {
                $('#divProgreso').modal('show');
                this.EdicionRegistros.Empleados = [];
                //var empleado = this.EdicionRegistros.Empleados.find(function (element) {
                //    return element.Id === fila.DocEntry;
                //});

                //if (empleado === undefined) {
                    var index = this.EdicionRegistros.Empleados.push({ Horarios: [], Id: fila.DocEntry });
                    empleado = this.EdicionRegistros.Empleados[index - 1];
                //}
                var inst = this;
                var codEmpleado = fila.NumEmpleado;
                this.$http.post(this.urls.urlEditarHorarioEmpleado, { Empleado: codEmpleado }).then(function (response) {
                    HorariosPersonal = [];
                    buscarHorario(empleado.Id, fila.Aprobado ? '1' : '0', codEmpleado, []);
                    this.pausa(1000);
                    $('#divProgreso').modal('hide');
                    var result = response.body;
                    //var row = event.target.parentNode.parentNode.parentNode;
                    //var cell = $(row).find("td:first");
                    var formeditar = result;
                    //$(cell).attr('colspan', 15);
                    //var panel = $("#" + fila.NumEmpleado);
                    $("#dvEdicionHorario").html(formeditar);
                    UIDrop();
                    InicializaSeleccionDias(fila.NumEmpleado);
                    $('#divHorariosEmpleado').show();
                    $("#boxListadoEmpleados").hide();
                    VueSaveInit();
                }, function (a, b, c) {
                    this.mensaje = a.status + '-' + a.statusText;
                    this.EstadoProgreso(false);
                });
            },
            rowClass: function (registro) {
                var clase = "";
                return clase;
            },
            cellFormat: function (registro, columna, tipo) {
                var data = registro[columna];
                if (tipo.toLowerCase().indexOf("date") >= 0) {
                    data = moment(data).format("DD/MM/YYYY");
                } else if (columna === "Aprobado") {
                    var aprobado = data;
                    data = aprobado ? "Si" : "No";
                } else if (columna === "U_HorarioESC") {
                    var subiohorario = data;
                    data = subiohorario ? "Si" : "No";
                }
                return data;
            },
            cellStyle: function (registro, columna, tipo) {
                var data = registro[columna];
                var estilo = "";
                if (columna.toLowerCase() === "statusvalidacion") {
                    var color = "";
                    if (data === 0) {
                        color = "green";
                    } else if (data === 1) {
                        color = "yellow";
                    } else {
                        color = "red";
                    }
                    estilo = "color:" + color + "; background-color: " + color + ";";
                }
                return estilo;
            },
            paginarCliente: function (totalpaginas, totalregistros) {
                this.tabla.totalpaginas = totalpaginas;
                this.tabla.totalregistros = totalregistros;
                if (this.tabla.totalpaginas === parseInt(this.tabla.paginaactual)) {
                    this.tabla.registroFinal = this.tabla.totalregistros;
                } else {
                    this.tabla.registroFinal = (this.tabla.paginaactual - 1) * this.tabla.registrosxpagina + parseInt(this.tabla.registrosxpagina);
                }
                this.CargarPaginas();
            },
            presentarDatos: function (model) {
                this.loading = false;
                this.EstadoProgreso(false);
                if (model.success) {
                    this.mensaje = model.mensaje;
                    this.DetallePagina = [];
                    this.DetalleRegistros = [];
                    this.DetalleRegistrosBk = [];
                    var paginado;
                    //Recupera todos los datos y asigna los registros a la página actúal.
                    if (this.tabla.paginacionCliente) {
                        paginado = this.InicializarTabla(model.results[0]);
                    } else {
                        this.DetallePagina = model.results[0];
                        paginado = { paginas: model.results[1], registros: model.results[2] };
                    }
                    this.paginarCliente(paginado.paginas, paginado.registros);
                    if (paginado.registros === 0) {
                        this.pausa(2000);
                    }
                } else {
                    this.mensaje = model.mensaje;
                }
            },
            buscarDatos: function (perfil) {
                this.EstadoProgreso(true);
                this.eliminarFiltro();
                this.EdicionRegistros = { Empleados: [] };
                //Se recuperan todas las páginas.
                if (this.tabla.paginacionCliente) {
                    this.filtro.Pagina = 0;
                } else {
                    this.filtro.Pagina = this.tabla.paginaactual;
                }
                this.filtro.NumeroRegistroPag = this.tabla.registrosxpagina;
                var url = this.urls.urlSupervisor;
                if (perfil === "ADM") {
                    url = this.urls.urlRH;
                }
                this.$http.post(url, this.filtro).then(function (response) {
                    this.pausa(2000);
                    this.EstadoProgreso(false);
                    model = response.body.model;
                    //this.presentarDatos(model);
                }, function (a, b, c) {
                    this.loading = false;
                    this.EstadoProgreso(false);
                    this.mensaje = a.status + '-' + a.statusText;
                });
            },
            GuardarDatos: function () {
                this.EstadoProgreso(true);
                this.eliminarFiltro();
                var Data = { Filtro: this.filtro, Datos: this.EdicionRegistros.Empleados };
                this.$http.post(this.urls.urlSave, Data).then(function (response) {
                    this.EdicionRegistros = { Empleados: [] };
                    model = response.body.model;
                    this.presentarDatos(model);
                }, function (a, b, c) {
                    this.loading = false;
                    this.EstadoProgreso(false);
                    this.mensaje = a.status + '-' + a.statusText;
                });
            },
            pausa: function (milisegundos) {
                var inst = this;
                setTimeout(
                    function () {
                        inst.EstadoProgreso(false);
                    }, milisegundos);
            }
        }
    });
}