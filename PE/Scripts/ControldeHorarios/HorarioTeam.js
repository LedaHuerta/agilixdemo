﻿//var horariosGenerales;
//console.log(horariosGenerales);



function setVueHorariosTeam(pmodel, psemana, phoras) {
    //var prueba = pmodel.map(elem => elem.HorariosSolicitud).map(escolar => escolar.filter(tipo => tipo.Tipo == 1).map(horario => horario.Horarios.map(objetos => objetos)));
    //console.log(prueba);
    var vueinst = new Vue({
        el: '#horarios-team-view',
        data: {
            horariosTeam: [],
            semana: [],
            horas: [],
            colors: [{ "fondo": "#56CCF2", "text": "whitesmoke" }, { "fondo": "#B2DFDB", "text": "#00464e" }, { "fondo": "#3949AB", "text": "whitesmoke" }, { "fondo": "#DCFF79", "text": "#00464e" }],
            empleadosSeleccionados: [],
            empleadosTeam:[],
            tipos: [],
            horarios: [],
            numeroEmpleados: 0,
            empleadosCards: [],
            popUpHorario: {titulo:"", Descripcion: ""}

        },
        created: function () {
            this.horariosTeam = pmodel;
            this.semana = psemana;
            this.horas = phoras;
            for (var i = 0; i < this.horariosTeam.length; i++) {
                var index = this.empleadosCards.findIndex(element => element.U_EMP === this.horariosTeam[i].U_EMP);
                if (index < 0) {
                    this.empleadosCards.push({ U_EMP: this.horariosTeam[i].U_EMP, nombre: this.horariosTeam[i].nombre, puesto: this.horariosTeam[i].Puesto });
                }
            };
            this.empleadosTeam = this.creaHorario();
            this.empleadosSeleccionados = this.empleadosTeam;
            this.numeroEmpleados = this.horariosTeam.length;
            
            //this.empleadosSeleccionados = pmodel.map(empleado => empleado);
            //this.tipos = pmodel.map(elem => elem.HorariosSolicitud).map(escolar => escolar.map(tipo => tipo.Tipo));
            //this.horarios = pmodel.map(elem => elem.HorariosSolicitud).map(escolar => escolar.filter(tipo => tipo.Tipo == 1).map(horario => horario.Horarios.map(objetos => objetos)));
        },
        mounted: function () {
            $('[data-toggle="popover"]').popover();
            
        },
        methods: {
            colorEmp: function (index) {
                var estilo = "";
                var cursor = "cursor: pointer;";
                if (index > this.colors.length -1) {
                    index = 0;
                }
                var fondo = this.colors[index].fondo;
                var texto = this.colors[index].text;
                estilo = "background-color:" + fondo + ";" + "color:" + texto + ";" + cursor;
                return estilo;
            },
            creaHorario: function () {
                var horariosemp = [];
                for (var i = 0; i < this.empleadosCards.length; i++) {
                    var index = this.horariosTeam.findIndex(element => element.U_EMP === this.empleadosCards[i].U_EMP);
                    var horarioSolicitud = this.horariosTeam[index].HorariosSolicitud;
                    for ( var h = 0; h < horarioSolicitud.length; h++) {
                        if (horarioSolicitud[h].Tipo === 1 || horarioSolicitud[h].Tipo === 2) {
                            for (var x = 0; x < horarioSolicitud[h].Horarios.length; x++) {
                                var horario = horarioSolicitud[h].Horarios[x];
                                if (horario.HoraInicial !== "") {
                                    horariosemp.push({ index: i, tipoHorario: horarioSolicitud[h].Tipo, dia: horario.dia, HoraInicial: horario.HoraInicial, HoraFinal: horario.HoraFinal})
                                }
                            }
                        }
                    }
                }
                return horariosemp;
            },
            anchoBarra: function () {
                return "25";
            },
            altoBarra: function (HoraInicial, HoraFinal) {
                return this.proporcionVertical(HoraInicial, HoraFinal);
            },
            proporcionVertical: function(HoraInicial, HoraFinal) {
                var horarioInicial = HoraInicial.split(":");
                var horarioFinal = HoraFinal.split(":");
                var horasIni = parseInt(horarioInicial[0]) + parseInt(horarioInicial[1]) / 60;
                var horasFin = parseInt(horarioFinal[0]) + parseInt(horarioFinal[1]) / 60;
                var resHoras = (horasFin - horasIni) * 2 * 30;
                return resHoras;
            },
            calculaPosY: function (HoraFinal) {
                return this.proporcionVertical("06:00",HoraFinal) + 30;
            },
            iconoBarra: function (empleadoHorario) {
                var tipo = empleadoHorario.tipoHorario;
                var clase = tipo === 2 ? "icon-barra icon s-4 icon-briefcase" : "icon-barra icon s-4 icon-school";
                return clase;
            },
            barraHorario: function (empleadoHorario) {
                var index = empleadoHorario.index;
                var estilo = "";
                var cursor = "cursor: pointer;";
                
                if (index > this.colors.lenght - 1) {
                    index = 0;
                }
                var fondo = this.colors[index].fondo;
                var texto = this.colors[index].text;
                estilo = "position: absolute; left: " + (135 * empleadoHorario.dia + (empleadoHorario.index * 30)) + "px; top: " + this.calculaPosY(empleadoHorario.HoraInicial) + "px; height: " + this.altoBarra(empleadoHorario.HoraInicial, empleadoHorario.HoraFinal) + "px; width:" + this.anchoBarra() + "px; background-color:" + fondo + ";" + "color:" + texto + ";" + cursor +"margin-left: 2px; border-radius: 5px;";
                //var obj = $('#barra' + empleadoHorario.index + 'tipo' + empleadoHorario.tipoHorario);
                //var icon = $(obj).find(".icon-barra");
                //$(icon).addClass(this.iconoBarra(empleadoHorario));
                return estilo;

            },
            seleccionEmpleado: function (index, event) {
                var seleccionado = event.target.checked;
                var empleados = this.empleadosTeam.filter(element => element.index == index);
                if (seleccionado) {
                    for (var i = 0; i < empleados.length; i++) {
                        this.empleadosSeleccionados.push(empleados[i]);
                    }
                } else {
                    for (var x = 0; x < empleados.length; x++) {
                        var toRemove = [];
                        toRemove.push(empleados[x]);
                        this.empleadosSeleccionados = $.grep(this.empleadosSeleccionados, function (value) {
                            return $.inArray(value, toRemove) < 0;
                        });
                    }
                }
            },
            tituloPopUp: function (horario, key) {
                return "'prueba titulo'";
            },
            descripcionPopUp: function (horario, key) {
                var tipoHorario = "";
                var tituloComida = "";
                var tiempoComida = "";
                if (horario.tipoHorario === 1) {
                    tipoHorario = "Escolar: ";
                } else if (horario.tipoHorario === 2) {
                    tipoHorario = "Laboral: ";
                    tituloComida = "Tiempo Comida: ";
                    var horariosEmpleado = this.horariosTeam[horario.index].HorariosSolicitud[2].Horarios;
                    var diaHorarioComida = horariosEmpleado.find(element => element.dia == horario.dia);
                    tiempoComida = this.calculaTiempoComida(diaHorarioComida);
                }

                var horarioActual = tipoHorario + "\n" + horario.HoraInicial + " - " + horario.HoraFinal + "\n" + tituloComida + tiempoComida;

                return horarioActual;
            },
            calculaTiempoComida: function (diaHorarioComida) {
                var tiempoComida = "";
                if (diaHorarioComida.HoraInicial === "00:00" && diaHorarioComida.HoraFinal === "00:00") {
                    tiempoComida = "No tiene tiempo de comida";
                } else if (diaHorarioComida.HoraInicial === "01:00" && diaHorarioComida.HoraFinal === "01:30") {
                    tiempoComida = "30 min";
                } else if (diaHorarioComida.HoraInicial === "01:00" && diaHorarioComida.HoraFinal === "02:00") {
                    tiempoComida = "1 hora";
                } else if (diaHorarioComida.HoraInicial === "01:00" && diaHorarioComida.HoraFinal === "02:30") {
                    tiempoComida = "1 hora 30 min";
                } else if (diaHorarioComida.HoraInicial === "01:00" && diaHorarioComida.HoraFinal === "03:00") {
                    tiempoComida = "2 horas";
                }
                return tiempoComida;
            }
        }
        
    });
}
