﻿function navMiPerfil(obj) {
    //Obtener el value de cada input(botón) del nav:
    var datosPer = document.getElementById("btn-dat-per").value;
    var datosAca = document.getElementById("btn-dat-acad").value;
    //Comparar el value del obj con el value de cada botón, para devolver la vista parcial deseada:
    if (obj.value == datosAca) {
        sendAjax("../Empleado/DatosAcademicos", viewDatos);
    } else if (obj.value == datosPer) {
        sendAjax("../Empleado/DatosPersonales", viewDatos);
    }
};

//función ok de sendAjax:
function viewDatos(Data) {
    $("#content-data").html(Data);
};



function editarInfoGen() {
    //switch entre los span de edicion y lectura:
    $("#p-tittle").addClass("tittle-reduce");
    $("#saveMode").removeClass("showSave");
    $("#saveMode").addClass("hide");
    $("#editMode").removeClass("hide");
    $("#editMode").addClass("showSave");
    //remover readonly a los campos editables:
    $("#edoCivil").removeAttr("readonly");
    $("#celPersonal").removeAttr("readonly");
    $("#fijoPersonal").removeAttr("readonly");
    $("#celLaboral").removeAttr("readonly");
    $("#fijoLaboral").removeAttr("readonly");
    $("#mailPersonal").removeAttr("readonly");
};

function backInfoGen() {
    //switch entre los span de edicion y lectura:
    $("#p-tittle").removeClass("tittle-reduce");
    $("#saveMode").removeClass("hide");
    $("#saveMode").addClass("showSave");
    $("#editMode").removeClass("showSave");
    $("#editMode").addClass("hide");
    //agregar readonly a los campos editables
    $("#edoCivil").attr("readonly", "readonly");
    $("#celPersonal").attr("readonly", "readonly");
    $("#fijoPersonal").attr("readonly", "readonly");
    $("#celLaboral").attr("readonly", "readonly");
    $("#fijoLaboral").attr("readonly", "readonly");
    $("#mailPersonal").attr("readonly", "readonly");

    //Ejecuta la función ajax para que regrese la data que estaba guardada originalmente.
    var obj = document.getElementById("btn-dat-per");
    navMiPerfil(obj);
};

function saveInfoGen() {
    //switch entre los span de edicion y lectura:
    $("#p-tittle").removeClass("tittle-reduce");
    $("#saveMode").removeClass("hide");
    $("#saveMode").addClass("showSave");
    $("#editMode").removeClass("showSave");
    $("#editMode").addClass("hide");

    //Aquí va la función ajax para que guarde la data en bd.
   saveGen();
   
    
};

function saveGen() {
    var edoCiv = $("#edoCivil").val();
    var celPers = $("#celPersonal").val();
    var telDom = $("#fijoPersonal").val();
    var telCel = $("#celLaboral").val();
    var telOfc = $("#fijoLaboral").val();
    var mail = $("#mailPersonal").val();
    Data = { "U_Eci": edoCiv, "U_Telrec": celPers, "U_Teldom": telDom, "U_Telcel": telCel, "U_Telofi": telOfc, "U_Email": mail };
    senDataAjaxJsonDefault(Data, "../Empleado/guardarDatosGenerales", resultSaveInfoGen);
}

function resultSaveInfoGen(Data) {
    if (Data === '') {

        $("#boxPMsg").addClass("margin-msg");
        $("#saveGenP").addClass("text-success");
        $("#saveGenP").removeClass("text-danger");
        $("#saveGenP").html("¡Datos actualizados correctamente!");
        //agregar readonly a los campos editables
        $("#edoCivil").attr("readonly", "readonly");
        $("#celPersonal").attr("readonly", "readonly");
        $("#fijoPersonal").attr("readonly", "readonly");
        $("#celLaboral").attr("readonly", "readonly");
        $("#fijoLaboral").attr("readonly", "readonly");
        $("#mailPersonal").attr("readonly", "readonly");
        setTimeout(function () { $("#saveGenP").html(""); }, 2000);
        $("#boxPMsg").removeClass("margin-msg");

    } else {
        $("#saveGenP").removeClass("text-success");
        $("#saveGenP").addClass("text-danger");
        $("#saveGenP").html("¡Tus datos no se actualizaron, da click en regresar e intenta de nuevo!");
    }
}

function editarInfoDom() {
    //switch entre los span de edicion y lectura:
    $("#p-tittledom").addClass("tittle-reduce");
    $("#saveModeDom").removeClass("showSave");
    $("#saveModeDom").addClass("hide");
    $("#editModeDom").removeClass("hide");
    $("#editModeDom").addClass("showSave");

    //remover readonly a los campos editables:
    $("#calle").removeAttr("readonly");
    $("#noExt").removeAttr("readonly");
    $("#noInt").removeAttr("readonly");
    $("#colonia").removeAttr("readonly");
    $("#codPostal").removeAttr("readonly");
    $("#alcaldia").removeAttr("readonly");

};

function backInfoDom() {
    //switch entre los span de edicion y lectura:
    $("#p-tittledom").removeClass("tittle-reduce");
    $("#saveModeDom").removeClass("hide");
    $("#saveModeDom").addClass("showSave");
    $("#editModeDom").removeClass("showSave");
    $("#editModeDom").addClass("hide");

    //agregar readonly a los campos editables
    $("#calle").attr("readonly", "readonly");
    $("#noExt").attr("readonly", "readonly");
    $("#noInt").attr("readonly", "readonly");
    $("#colonia").attr("readonly", "readonly");
    $("#codPostal").attr("readonly", "readonly");
    $("#alcaldia").attr("readonly", "readonly");

    //Ejecuta la función ajax para que regrese la data que estaba guardada originalmente.
    var obj = document.getElementById("btn-dat-per");
    navMiPerfil(obj);
    
};

function saveInfoDom() {
    //switch entre los span de edicion y lectura:
    $("#p-tittledom").removeClass("tittle-reduce");
    $("#saveModeDom").removeClass("hide");
    $("#saveModeDom").addClass("showSave");
    $("#editModeDom").removeClass("showSave");
    $("#editModeDom").addClass("hide");

    //Aquí va la función ajax para que guarde la data en bd.
    saveDom();
};

function saveDom() {
    var pais = $("#pais").val();
    var ciudad = $("#ciudad").val();
    var calle = $("#calle").val();
    var noExt = $("#noExt").val();
    var noInt = $("#noInt").val();
    var colonia = $("#colonia").val();
    var codPostal = $("#codPostal").val();
    var alcaldia = $("#alcaldia").val();

    Data = { "U_PAIS": pais, "U_EDO": ciudad, "U_DOM": calle, "U_NUMEXT": noExt, "U_NUMINT": noInt, "U_COL": colonia, "U_CODP": codPostal, "U_DEL": alcaldia};
    senDataAjaxJsonDefault(Data, "../Empleado/guardarDatosDomicilio", resultSaveInfoDom);
}

function resultSaveInfoDom(Data) {
    if (Data === '') {
        $("#boxPMsg2").addClass("margin-msg");
        $("#saveDomP").addClass("text-success");
        $("#saveDomP").removeClass("text-danger");
        $("#saveDomP").html("¡Datos actualizados correctamente!");

        //agregar readonly a los campos editables
        $("#calle").attr("readonly", "readonly");
        $("#noExt").attr("readonly", "readonly");
        $("#noInt").attr("readonly", "readonly");
        $("#colonia").attr("readonly", "readonly");
        $("#codPostal").attr("readonly", "readonly");
        $("#alcaldia").attr("readonly", "readonly");
        setTimeout(function () { $("#saveDomP").html(""); }, 2000);
        $("#boxPMsg2").removeClass("margin-msg");
    } else {
        $("#saveDomP").removeClass("text-success");
        $("#saveDomP").addClass("text-danger");
        $("#saveDomP").innerHTML = "¡Tus datos no se actualizaron, da click en regresar e intenta de nuevo!";

    }
}

function editarInfoEsco() {
    //switch entre los span de edicion y lectura:
    $("#p-tittledom").addClass("tittle-reduce");
    $("#saveModeEsco").addClass("hide");
    $("#saveModeEsco").removeClass("showSave");
    $("#editModeEsco").removeClass("hide");
    $("#editModeEsco").addClass("showSave");

    //remover disabled a los campos editables:
    $("#nivelEscolar").removeAttr("disabled");
    $("#situacionEscolar").removeAttr("disabled");
    $("#gradoEscolar").removeAttr("disabled");
    $("#periodoEscolar").removeAttr("disabled");
    $("#Carrera").removeAttr("disabled");
    $("#universidad").removeAttr("disabled");

};

function backInfoEsco() {
    //switch entre los span de edicion y lectura:
    $("#p-tittledom").removeClass("tittle-reduce");
    $("#saveModeEsco").removeClass("hide");
    $("#saveModeEsco").addClass("showSave");
    $("#editModeEsco").removeClass("showSave");
    $("#editModeEsco").addClass("hide");

    //agregar disabled a los campos editables
    $("#nivelEscolar").attr("disabled", "disabled");
    $("#situacionEscolar").attr("disabled", "disabled");
    $("#gradoEscolar").attr("disabled", "disabled");
    $("#periodoEscolar").attr("disabled", "disabled");
    $("#Carrera").attr("disabled", "disabled");
    $("#universidad").attr("disabled", "disabled");
    
    //Ejecuta la función ajax para que regrese la data que estaba guardada originalmente.
    var obj = document.getElementById("btn-dat-acad");
    navMiPerfil(obj);

};

function saveInfoEsco() {
    //switch entre los span de edicion y lectura:
    $("#p-tittledom").removeClass("tittle-reduce");
    $("#saveModeEsco").removeClass("hide");
    $("#saveModeEsco").addClass("showSave");
    $("#editModeEsco").removeClass("showSave");
    $("#editModeEsco").addClass("hide");

    //Aquí va la función ajax para que guarde la data en bd.
    SaveAcad();
};


function SaveAcad() {
    var nivEsc = $("#nivelEscolar").val();
    var sitAcad = $("#situacionEscolar").val();
    var gradEsc = $("#gradoEscolar").val();
    var perEsc = $("#periodoEscolar").val();
    var carrera = $("#Carrera").val();
    var universidad = $("#universidad").val();

    Data = { "nivelEscolar": nivEsc, "situacionEscolar": sitAcad, "gradoEscolar": gradEsc, "periodoEscolar": perEsc, "profesion": carrera, "claveUniversidad": universidad };
    senDataAjaxJsonDefault(Data, "../Empleado/guardarDatosAcademicos", resultSaveInfoAcad);
}

function resultSaveInfoAcad(Data) {
    if (Data === '') {
        $("#boxPMsg3").addClass("margin-msg");
        $("#saveAcadP").addClass("text-success");
        $("#saveAcadP").removeClass("text-danger");
        $("#saveAcadP").html("¡Datos actualizados correctamente!");


        //agregar disabled a los campos editables
        $("#nivelEscolar").attr("disabled", "disabled");
        $("#situacionEscolar").attr("disabled", "disabled");
        $("#gradoEscolar").attr("disabled", "disabled");
        $("#periodoEscolar").attr("disabled", "disabled");
        $("#Carrera").attr("disabled", "disabled");
        $("#universidad").attr("disabled", "disabled");
        setTimeout(function () { $("#saveGenP").html(""); }, 2000);
        $("#boxPMsg3").removeClass("margin-msg");
    } else {
        $("#saveAcadP").removeClass("text-success");
        $("#saveAcadP").addClass("text-danger");
        $("#saveAcadP").innerHTML = "¡Tus datos no se actualizaron, da click en regresar e intenta de nuevo!";
    }
}

