﻿
function aprobarSolPermiso() {
    Data = { "id": $("#idSolApr").val(), "empleado": $("#empleadoSolApr").val(), "Observaciones": $("#ObservacionesSolicitud").val() };
    $("#aprob-sol").attr("disabled", "disabled");
    senDataAjaxJsonDefault(Data, "../Permisos/aprobarSolicitud", resultAprobacionRechazo);
}

function rechazarSolPermiso() {
    MotivoRechazo = document.getElementById("mot-rechazo").value;
    var pValErr = document.getElementById("error-rechazo");
    if (MotivoRechazo !== "" && MotivoRechazo !== undefined) {
        $("#rechazar-solicitud").attr("disabled", "disabled");
        Data = { "id": $("#idSolApr").val(), "empleado": $("#empleadoSolApr").val(), "MotivoRechazo": MotivoRechazo, "Observaciones": $("#ObservacionesSolicitud").val() };
        senDataAjaxJsonDefault(Data, "../Permisos/rechazarSolicitud", resultAprobacionRechazo);
    } else {
        pValErr.innerHTML = "Debes indicar el motivo de rechazo";
    }
}

function resultAprobacionRechazo(Data) {
    if (Data === '') {
        $("#modal-confirm-aprob").modal("hide");
        $("#modal-confirm-recha").modal("hide");
        $("#btnAprobarSol").hide();
        $("#btnRechazarSol").hide();
        window.location.replace("../Permisos/AdminSolicitudesPermisos");
    } else {
        alert("Error: " + Data);
    }
}

function HistSolEmpleado(empleado) {
    Data = { "Empleado": empleado };
    senDataAjaxJsonDefault(Data, "../Permisos/HistSolicitudesEmpledo", consultaHist);
}

function consultaHist(Data){
    $("#HistoricoEmpleadoPermisos").html(Data);
    $("#modal-hist-permisos").modal("show");
}

function SolicitudDetalle(pid) {
    $("#id").val(pid);
    Data = { "id": $("#id").val(), "filtroPerfil": $("#filtroPerfil").val(), "Status": $("#Status").val() };
    senDataAjaxJsonDefault(Data, "../Permisos/BuscarSolicitudxId", consultaSol);
}
function SolicitudDetalleNotificaciones(pid) {
    $("#id").val(pid);
    Data = { "id": $("#id").val(), "filtroPerfil": $("#filtroPerfil").val(), "Status": $("#Status").val() };
    senDataAjaxJsonDefault(Data, "../Permisos/BuscarNotificacionxId", consultaSol);
}

function consultaSol(Data) {
    $("#DetalleSolicitudPermiso").html(Data);
}

function SolicitudDetalleHistorico(pid) {
    $("#id").val(pid);
    Data = { "id": $("#id").val(), "filtroPerfil": $("#filtroPerfil").val(), "Status": $("#Status").val() };
    senDataAjaxJsonDefault(Data, "../Permisos/BuscarSolicitudxId", consultaSolDetalleHistorico);
}

function consultaSolDetalleHistorico(Data) {
    $("#collapse-hist-permisos1").html(Data);
}

function filtroSolicitudesAnio(anio) {
    var form = $('<form action="../Permisos/FiltroAnio" method="post"></form>');
    var inputAnio = $("<input name='Anio' value='" + $("#filtroAnio").val() + "'/>");
    var inputStatus = $("<input name='Status' value='" + $("#StatusFiltroSol").val() + "'/>");
    $(inputAnio).appendTo($(form));
    $(inputStatus).appendTo($(form));
    $(form).appendTo('body').submit();
}

function filtroSolicitudesEmp() {
    var form = $('<form action="../Permisos/FiltroEmpleado" method="post"></form>');
    var inputEmp = $("<input name='Empleado' value='" + $("#filtroEmpleado").val() + "'/>");
    var inputStatus = $("<input name='Status' value='" + $("#StatusFiltroSol").val() + "'/>");
    $(inputEmp).appendTo($(form));
    $(inputStatus).appendTo($(form));
    $(form).appendTo('body').submit();
}

function cambiar() {
    var pdrs = document.getElementById('file').files[0].name;
    document.getElementById('info').innerHTML = pdrs;
}

function enviarSolicitud() {
    horasPremio();
    validacionEmpalmeFechas();
    
    //$("#form_permiso").submit();
}

function horasPremio() {
    var concepto = document.getElementById("tipo").value;
    var brGroup = document.getElementById("radioButtonGroup");
    var inputMed = document.getElementById("medioDia").value;
    var radioMd = document.getElementById("radioMd");
    var radioDc = document.getElementById("radioDc");

    if (concepto === "1105") {
        brGroup.classList.remove('hide-radio-button');
        brGroup.classList.add('radio-button-group');
        inputMed = "";
        validaCheck();
    } else {
        brGroup.classList.remove('radio-button-group');
        brGroup.classList.add('hide-radio-button');
        inputMed = "";
    }
}

function llenaHidden() {
    var concepto = document.getElementById("tipo").value;
    var inputMed = document.getElementById("medioDia").value;

    if (concepto === "1104") {
        inputMed = "1";
    } else if (concepto === "1106" || concepto === "1107") {
        inputMed = "1";
    } else {
        inputMed = "";
    }
}

function validaCheck() {
    var inputMed = document.getElementById("medioDia").value;
    var radioEmpty = document.getElementById("radioEmpty");
    var radioMd = document.getElementById("radioMd");
    var radioDc = document.getElementById("radioDc");
    var errMsg = document.getElementById("radio-err");
    if (radioEmpty.checked) {
        errMsg.innerHTML = "Debes seleccionar Medio día o Día Completo.";
        $("#solicitar").attr("disabled", "disabled");
    }
    if (radioMd.checked) {
        errMsg.innerHTML = "";
        inputMed = "1";
        $("#solicitar").removeAttr("disabled");

    }
    if (radioDc.checked) {
        errMsg.innerHTML = "";
        inputMed = "2";
        $("#solicitar").removeAttr("disabled");
    }
}

function validaFecha() {
    var fechaInicial = moment($("#fechaIni").val(), 'YYYY-MM-DD', true);
    var fechaFinal = moment($("#fechaFin").val(), 'YYYY-MM-DD', true);
    var fechaActual = moment().startOf('day');

    var diaInicial = fechaInicial.day();
    var diaFinal = fechaFinal.day();
    $("#solicitar").attr("disabled", "disabled");
    $("#tipo").attr("disabled", "disabled");
    document.getElementById("parrafo-mod-per").innerHTML = "";
    document.getElementById("parrafo-mod-per2").innerHTML = "";

    if (diaInicial === 0 || diaInicial === 6) {
        document.getElementById("parrafo-mod-per").innerHTML = "La fecha inicial no puede ser sabado o domingo";
        return;
    }

    if (diaFinal === 0 || diaFinal === 6) {
        document.getElementById("parrafo-mod-per2").innerHTML = "La fecha final no puede ser sabado o domingo";
        return;
    } 

    if (fechaInicial.isBefore(fechaActual)) {
        document.getElementById("parrafo-mod-per").innerHTML = "La fecha inicial no puede ser anterior a la fecha actual";
        return;
    }

    if (fechaInicial.isAfter(fechaFinal)) {
        document.getElementById("parrafo-mod-per2").innerHTML = "La fecha final no puede ser anterior a la fecha inicial";
        return;
    } 
    var validacionHorasEnfermedad = validaHorasEnfermedad();
    if (validacionHorasEnfermedad) {
        horasPremio();
        llenaHidden();
        $("#solicitar").removeAttr("disabled");
        $("#tipo").removeAttr("disabled");
    }
}

var TotalDias = null;

function diasReales(finicial, ffinal, concepto) {
    //Data = { "FechaInicial": moment(finicial).format('YYYY-MM-DD'), "FechaFinal": moment(ffinal).format('YYYY-MM-DD') };
    //senDataAjaxJsonDefault(Data, "../Permisos/validaFechasReales",null);
    Data = { "FechaInicial": moment(finicial).format('YYYY-MM-DD'), "FechaFinal": moment(ffinal).format('YYYY-MM-DD'), "concepto": concepto };

    //var TotalDias = null;

    $.ajax({
        async: false,
        url: '../Permisos/validaFechasReales',
        type: 'POST',
        data: Data,
        success: function (respuesta) {
            //console.log(respuesta);
            //console.log(respuesta.TotalDias);
            TotalDias = respuesta.TotalDias;
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
}

function validacionEmpalmeFechas() {
    var concepto = document.getElementById("tipo").value;
    var finicial = moment($("#fechaIni").val(), 'YYYY-MM-DD', true);
    var ffinal = moment($("#fechaFin").val(), 'YYYY-MM-DD', true);
    Data = { "FechaInicial": moment(finicial).format('YYYY-MM-DD'), "FechaFinal": moment(ffinal).format('YYYY-MM-DD'), "concepto": concepto };
    $.ajax({
        async: false,
        url: '../Permisos/validaEmpalmeFechas',
        type: 'POST',
        data: Data,
        success: function (respuesta) {
            console.log(respuesta.Resultado);
            var EmpalmeDias = respuesta.Resultado;
            if (EmpalmeDias === true) {
                document.getElementById("parrafo-mod-per").innerHTML = "Hay empalme de dias con alguna otra solicitud.";
                $("#solicitar").attr("disabled", "disabled");
                $("#tipo").removeAttr("disabled", "disabled");
                $("#fechaIni").removeAttr("disabled", "disabled");
                $("#fechaFin").removeAttr("disabled", "disabled");
            } else {
                disableForm();
                $("#form_permiso").submit();
            }
        },
        error: function () {
            console.log("error al validar el empalme de los dias");
            $("#solicitar").attr("disabled", "disabled");

        }
    });
    
}

function validaHorasEnfermedad() {
    var select = document.getElementById("tipo").value;
    var fechaInicial = moment($("#fechaIni").val(), 'YYYY-MM-DD', true);
    var fechaFinal = moment($("#fechaFin").val(), 'YYYY-MM-DD', true);
    var resultado = true;
    diasReales(fechaInicial, fechaFinal, select);
    document.getElementById("parrafo-mod-per").innerHTML = "";

    if (select === "1102" || select === "1105" || select === "1106" || select === "1107" || select === "1104") {
         if (fechaInicial.isSame(fechaFinal)) {
             document.getElementById("parrafo-mod-per").innerHTML = "";
         } else {
             document.getElementById("parrafo-mod-per").innerHTML = "Para solicitar este permiso la Fecha Inicial y la Fecha Final deben ser iguales. ";
             resultado = false;
        }
        
    }
    var tope = $("#tipo option:selected").attr("data-tope");
    if (tope !== undefined) {
        var itope = parseInt(tope);
        if (itope > 1 && itope < 5000) {
            // To calculate the no. of days between two dates 
            //var Difference_In_Days = moment.duration(fechaFinal.diff(fechaInicial)).asDays();

            //if (Difference_In_Days > itope) {
            //    document.getElementById("parrafo-mod-per").innerHTML = "Sobrepasa el tope del permiso. ";
            //    $("#solicitar").attr("disabled", "disabled");
            //}

            //se codifica segun lo obtenido en la variable TotalDia 
            //(NO CAMBIAR POR OTRA VARIABLE YA QUE ESTA CONTEMPLA EL CAMBIO PARA NO CONTAR LOS FINES DE SEMANA Y EN SU MOMENTO SE SETEARA PARA NO CONTEMPLAR LOS DIAS FESTIVOS)
            //ESTA VARIABLE HACE UNA LLAMADA A UN METODO DEL CONTROLADOR DE PERMISOS (ACTION RESULT VALIDAFECHASREALES)
            //*********NO CAMBIAR VARIABLE***********//
            if (TotalDias > itope) {
                document.getElementById("parrafo-mod-per").innerHTML = "Sobrepasa el tope del permiso. ";
                resultado = false;
            }

        }
    }
    return resultado;
    
}
