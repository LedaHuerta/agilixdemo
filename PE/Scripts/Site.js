﻿$(document).ready(function () {
    if (navigator.userAgent.indexOf("Firefox") > 0) {
        $(document).bind("keydown keypress", function (e) {
            if (e.which === 8) { if (!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly) { e.preventDefault(); } }
        });
    }
 });


function openDialog(pmodal) {
    $(pmodal).modal();
}

function senDataAjaxJsonDefault(Data,url,funok) {
    senDataAjaxJson(Data, url, funok, sendErrorDefault);
}

function senDataAjaxJson(Data, url, funok, funerr) {
    $("#loader-general").show();
    $.ajax({
        url: url,
        type: 'POST',
        data: Data
    }).done(function (data) {
        $("#loader-general").hide();
        funok(data);
    }).fail(function (xhr, textStatus, errorThrown) {
        $("#loader-general").hide();
        funerr(xhr, textStatus, errorThrown);
    });
}

function sendAjax(url, funok) {
    sendAjaxNoData(url, funok, sendErrorDefault);
}
function sendAjaxNoData(url, funok, funerr) {
    $("#loader-general").show();
    $.ajax({
        url: url,
        type: 'GET',
    }).done(function (data) {
        $("#loader-general").hide();
        funok(data);
    }).fail(function (xhr, textStatus, errorThrown) {
        $("#loader-general").hide();
        funerr(xhr, textStatus, errorThrown);
    });
}

function sendErrorDefault(xhr, textStatus, errorThrown) {
    alert(xhr.responseText);
}

function ConsultarSolicitud(url, idNotificacion, idProceso) {
    Data = { "Id": idNotificacion, "url": url, "idProceso": idProceso };
    senDataAjaxJsonDefault(Data, '../VistasParciales/LeerMensaje', IraSolicitud);
}
function ConsultarNotificacion(url, idNotificacion, idProceso) {
    Data = { "Id": idNotificacion, "url": url, "idProceso": idProceso };
    senDataAjaxJsonDefault(Data, '../VistasParciales/LeerNotificacion', IraSolicitud);
}
function IraSolicitud(Data) {
    var AData = Data.split(',');
    var url = AData[0] + "?Id=" + AData[1]; //url esta en 0 y el id de proceso en 1
    location.replace(url);
}

function back() {
    window.history.back();
}

function cargarPdf(url) {
    Data = { "pdf": url };
    senDataAjaxJsonDefault(Data, '../VistasParciales/VisorPDF', actualizarDivPdf);
}

function actualizarDivPdf(Data) {
    $("#visor-pdf-mis-docs").html(Data);
}