﻿using ControldeHorarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalEmpleadosV2
{
    public static class OwnHelpers
    {
        public static String EncontrarFotoEmpleado (String rootPath, String FolderKey)
        {
            String result = String.Empty;
            try
            {
                var docPath = rootPath + "\\" + "F" + FolderKey + ".jpg";
                //String File = String.Empty;
                if (System.IO.File.Exists(docPath))
                {
                    //regresa el html incluir variables de etiquetas
                    result = docPath;

                }
                else
                {
                    //avatar
                    var avatar = "silueta";
                    var docPath2 = rootPath + "\\" + avatar +".jpg";
                    result = docPath2;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static String EncontrarFotoEmpleado2(String rootPath, String FolderKey)
        {
            var result = "<img style='width: 3vw; border-radius: 100%;' src='";
            try
            {
                var docPath = rootPath + "\\" + "F" + FolderKey;
                String File = docPath + ".jpg";
                if (System.IO.File.Exists(File))
                {
                    //regresa el html incluir variables de etiquetas
                    result += docPath + string.Format(".jpg'/>");

                }
                else
                {
                    //avatar
                    var avatar = "silueta";
                    var docPath2 = rootPath + "\\" + avatar;
                    result += docPath2 + string.Format(".jpg'/>");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public static MvcHtmlString EncontrarFotografiaEmpleado(String rootPath, String FolderKey)
        {
            var result = "<img style='width: 3vw; border-radius: 100%;' src='"; 
            try
            {
                var docPath = rootPath + "\\" + "F" + FolderKey;
                String File = docPath + ".jpg";
                if (System.IO.File.Exists(File))
                {
                    //regresa el html incluir variables de etiquetas
                    result += docPath + string.Format(".jpg'/>");

                }
                else
                {
                    //avatar
                    var avatar = "silueta";
                    var docPath2 = rootPath + "\\" + avatar;
                    result += docPath2 + string.Format(".jpg'/>");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return new MvcHtmlString(result);
        }
    }
}