﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Generales;
using Aplics.Servicios.Modelos.Horarios;
using Aplics.Servicios.Modelos.Nominix;
using Aplics.Servicios.Modelos.Solicitudes;
using Aplics.Servicios.Negocio.Horarios;
using Aplics.Servicios.Negocio.Nominix;
using Aplics.Servicios.Negocio.Solicitudes;
using GRC.Servicios.Data.DAO.EF;
using GRC.Servicios.Utilidades.Genericos;
using PortalEmpleadosV2.Models.Nominix;

namespace PortalEmpleadosV2.Controllers
{
    public class EmpleadoController : Controller
    {

        private String urlAPINominix = ConfigurationManager.AppSettings["BaseURLServiciosNominix"];
        private String urlAPIAgilix = ConfigurationManager.AppSettings["BaseURLServiciosAgilix"];

        public ActionResult MiPerfil()
        {
            Empleados empleado = new Empleados();
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            try {
                empleado = NominixGenerales.ObtenerEmpleado(Session["NoEmp"].ToString(),0);
                ViewBag.Titulo = "Mi Perfil";
                List<Empleado> listaEmpleados = new NegocioHorarios().ListaEmpleados(Session["NoEmp"].ToString());
                ViewBag.listaEmpleados = listaEmpleados;
                List<CatalogoGeneral> listaDepartamento = NominixGenerales.GetCatalogoDepartamentos(empleado.U_Dep);
                ViewBag.listaDepartamento = listaDepartamento;
                List<CatalogoGeneral> listaPuesto = NominixGenerales.GetCatalogoPuestos(empleado.U_Pue);
                ViewBag.listaPuesto = listaPuesto;
                List<CatalogoGeneral> listaCategoria = NominixGenerales.GetCatalogoCategoria(empleado.U_Cat);
                ViewBag.listaCategoria = listaCategoria;
                List<CatalogoGeneral> listaEmpSup = NominixGenerales.GetEmpleadoSuperior(empleado.U_Empsup);
                ViewBag.listaEmpSup = listaEmpSup;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(empleado);
        }

        public ActionResult DatosPersonales()
        {
            Empleados empleado = new Empleados();
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                empleado = NominixGenerales.ObtenerEmpleado(Session["NoEmp"].ToString(), 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("~/Views/Shared/_DatosPersonales.cshtml", empleado);
        }

        public ActionResult DatosAcademicos()
        {
            Empleados empleado = new Empleados();
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                empleado = NominixGenerales.ObtenerEmpleado(Session["NoEmp"].ToString(), 0);
                List<CatalogoGeneral> listaNivelEsc = NominixGenerales.GetCatalogoNivelEsc();
                ViewBag.NivelEscolar = listaNivelEsc;

                List<CatalogoGeneral> listaSituacionEsc = NominixGenerales.GetCatalogoSituacionEsc();
                ViewBag.SituacionEsc = listaSituacionEsc;

                List<CatalogoGeneral> listaGradoEsc = NominixGenerales.GetCatalogoGradoEsc();
                ViewBag.GradoEsc = listaGradoEsc;

                List<CatalogoGeneral> listaPeriodoEsc = NominixGenerales.GetCatalogoPeriodoEsc();
                ViewBag.PeriodoEsc = listaPeriodoEsc;

                List<CatalogoGeneral> listaProfesiones = NominixGenerales.GetCatalogoProfesiones();
                ViewBag.Profesiones = listaProfesiones;

                List<CatalogoGeneral> listaUniversidades = NominixGenerales.GetCatalogoUniversidades();
                ViewBag.Universidades = listaUniversidades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("~/Views/Shared/_DatosAcademicos.cshtml", empleado);
        }
        

        public async Task<JsonResult> guardarDatosGenerales (DatosGenerales datosGen)
        {
            try
            {
                Empleados empleado = new Empleados();
                empleado = NominixGenerales.ObtenerEmpleado(Session["NoEmp"].ToString(), 0);
                //DatosGenerales infoGeneral = new DatosGenerales();
                
                empleado.U_Eci = datosGen.U_Eci;
                empleado.U_Telcel = datosGen.U_Telcel;
                empleado.U_Telrec = datosGen.U_Telrec;
                empleado.U_Teldom = datosGen.U_Teldom;
                empleado.U_Telofi = datosGen.U_Telofi;
                empleado.U_Email = datosGen.U_Email;
                //llamar funcion de entity que guarda en otra sesión
                NominixGenerales.GuardarEmpleado(empleado, 0);
                await guardarDatosNominix(empleado, urlAPINominix);
                return Json("");
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }

        public async Task<JsonResult> guardarDatosDomicilio(DatosDomicilio datosDom)
        {
            try
            {
                Empleados empleado = new Empleados();
                empleado = NominixGenerales.ObtenerEmpleado(Session["NoEmp"].ToString(), 0);
                //DatosGenerales infoGeneral = new DatosGenerales();

                empleado.U_Pais = datosDom.U_PAIS;
                empleado.U_Edo = datosDom.U_EDO;
                empleado.U_Dom = datosDom.U_DOM;
                empleado.U_Numext = datosDom.U_NUMEXT.ToString();
                empleado.U_Numint = datosDom.U_NUMINT.ToString();
                empleado.U_Col = datosDom.U_COL;
                empleado.U_Codp = datosDom.U_CODP;
                empleado.U_Del = datosDom.U_DEL;

                //llamar funcion de entity que guarda en otra sesión
                NominixGenerales.GuardarEmpleado(empleado, 0);
                await guardarDatosNominix(empleado, urlAPINominix);
                return Json("");
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }

        public async Task<JsonResult> guardarDatosAcademicos(DatosAcademicos datosAcad)
        {
            try
            {
                Empleados empleado = new Empleados();
                empleado = NominixGenerales.ObtenerEmpleado(Session["NoEmp"].ToString(), 0);
                DatosGenerales infoGeneral = new DatosGenerales();

                empleado.U_Nivesc = datosAcad.nivelEscolar;
                empleado.U_SITESC = datosAcad.situacionEscolar;
                empleado.U_GRAESC = datosAcad.gradoEscolar ;
                empleado.U_PERESC = datosAcad.periodoEscolar;
                empleado.U_Prof = datosAcad.profesion;
                empleado.U_UNIV = datosAcad.claveUniversidad;
                //llamar funcion de entity que guarda en otra sesión
                NominixGenerales.GuardarEmpleado(empleado, 0);
                await guardarDatosNominix(empleado, urlAPINominix);
                return Json("");
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }
        }
        private async Task<ResultService> guardarDatosNominix(Empleados empleado, String urlAPINominix = "")
        {
            ResultService result = new ResultService();
            try
            {
                NegocioSolicitudDashBoard negocio = new NegocioSolicitudDashBoard();
                await negocio.guardarDatosEmpleadoEnNominix(urlAPINominix, empleado);
                result.resultado = 1;

            }
            catch (Exception ex)
            {
                //To Do: hacer un log
                result.Mensaje = ex.Message.ToString();
            }
            return result;
        }
    }
}
