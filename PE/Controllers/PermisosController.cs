﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aplics.Servicios.Negocio.Solicitudes;
using Aplics.Servicios.Modelos.Solicitudes;
using GRC.Servicios.Utilidades;
using GRC.Servicios.Utilidades.Genericos;
using Aplics.Servicios.Negocio;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Negocio.Nominix;
using System.Configuration;
using Aplics.Servicios.Modelos.Nominix;
using System.IO;
using System.Web.Services.Description;
using PortalEmpleadosV2.Models;
using Aplics.Servicios.Entidades.DAO;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace PortalEmpleadosV2.Controllers
{
    
	public class PermisosController : Controller
	{
		private int _UserSign = 0;
		private String urlAPINominix = ConfigurationManager.AppSettings["BaseURLServiciosNominix"];
		private String urlAPIAgilix = ConfigurationManager.AppSettings["BaseURLServiciosAgilix"];
		public PermisosController()
		{
			
		}
		// GET: Permisos
		public ActionResult Index()
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
				return RedirectToAction("Index", "Home");
            }
			_UserSign = (int)Session["UserSign"];

            
			return View();
		}

		public ActionResult HistoricoDescansos()
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            ClaseAuditoria<String> pe = new ClaseAuditoria<String>();
			Dashboard resDashboard = new Dashboard();
            try
			{
				pe.UserName = this.Session["UserName"].ToString();
				pe.UserSign = _UserSign;
				pe.Clase = Session["NoEmp"].ToString();
				resDashboard = obtenerHistorialSolicitudes(pe);
			} catch (Exception ex)
			{
				throw ex;
			}

            return View(resDashboard);
        }

		public ActionResult ModalPermiso()
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

			List<ItemSolicitiud> concep = new List<ItemSolicitiud>();
			try
			{
				ClaseAuditoria<String> peticionDashBord = new ClaseAuditoria<String>();
				String claveEmpleado = this.Session["NoEmp"].ToString();
				peticionDashBord.UserName = this.Session["UserName"].ToString();
				peticionDashBord.UserSign = _UserSign;
				peticionDashBord.Clase = claveEmpleado;
				concep = obtenerConceptosSolicitudes(peticionDashBord);

				foreach (var item in concep)
				{
					Console.WriteLine(item.Descripcion);
				}
			} catch (Exception ex)
			{
				throw ex;
			}
			return View(concep);
		}
		public async Task<ActionResult> AdminSolicitudes()
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			Dashboard concep = new Dashboard();
			try
			{
				String id = this.Session["NoEmp"].ToString();
				ClaseAuditoria<String> peticionDashBord = new ClaseAuditoria<String>();
				QueryParameters<Empleados> parametrosE = new QueryParameters<Empleados>();
				parametrosE.where = x => x.UserSign.Equals(id);
				peticionDashBord.Clase = id;
				peticionDashBord.UserName = NominixGenerales.ObtenerEmpleado(id, 1).Name;
				peticionDashBord.UserSign = _UserSign;
				this.Session["UserName"] = peticionDashBord.UserName;
				ViewData["Name"] = peticionDashBord.UserName;

				ClaseAuditoria<String> pe = new ClaseAuditoria<String>();
				String claveEmpleado = this.Session["NoEmp"].ToString();
				pe.UserName = this.Session["UserName"].ToString();
				pe.UserSign = _UserSign;
				pe.Clase = claveEmpleado;
				concep = await obtenerSolicitudes(pe);
		    }
			catch (Exception ex)
			{
				throw ex;
			}
			return View(concep);
		}

		[HttpPost]
		public async Task<ActionResult> aprobarSolicitud(int id,String empleado,String Observaciones)
		{
			String mensaje = String.Empty;
			try
			{
				mensaje = await cambiarStatusSolicitud(id, empleado, this.Session["NoEmp"].ToString(),String.Empty, true,Observaciones, urlAPINominix);
			}
			catch(Exception ex)
			{
				mensaje = "Error en la aprobación: " + ex.Message.ToString();
				throw ex;
			}
			return Content(mensaje);
		}

		[HttpPost]
		public async Task<ActionResult> rechazarSolicitud(int id, String Empleado,String MotivoRechazo, String Observaciones)
		{
			String mensaje = String.Empty;
			try
			{
				mensaje = await cambiarStatusSolicitud(id, Empleado, this.Session["NoEmp"].ToString(), MotivoRechazo, false,Observaciones);
			}
			catch (Exception ex)
			{
				mensaje = "Error en la aprobación: " + ex.Message.ToString();
				throw ex;
			}
			return Content(mensaje);
		}

		private async Task<String> cambiarStatusSolicitud(int id,String empleado, String empleadoAutoriza, String MotivoRechazo,Boolean aprobado,String Observaciones, String urlAPINominix = "")
		{
			String result = String.Empty;
			try
			{
				ClaseAuditoria<SolicitudXAprobar> peticionAprobarSol = new ClaseAuditoria<SolicitudXAprobar>();
				SolicitudXAprobar solicitudXAprobar = new SolicitudXAprobar();
				solicitudXAprobar.Id = id;
				solicitudXAprobar.NumEmpleado = empleado;
				solicitudXAprobar.Aprobar = aprobado;
				solicitudXAprobar.EmpleadoAutoriza = empleadoAutoriza;
				solicitudXAprobar.MotivoRechazo = MotivoRechazo;
				solicitudXAprobar.Observaciones = Observaciones;
				peticionAprobarSol.UserName = this.Session["NoEmp"].ToString();
				peticionAprobarSol.UserSign = _UserSign;
				peticionAprobarSol.Clase = solicitudXAprobar;

				result=await aprobarRechazarSolicitud(peticionAprobarSol, urlAPINominix);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return result;
		}

		public async Task<ActionResult> ListaPermisos()
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			Dashboard concep = new Dashboard();
			try
			{
				String id = this.Session["NoEmp"].ToString();

                this.Session["UserName"] = Session["NombreEmpleado"];
                ViewData["Name"] = Session["NombreEmpleado"];

                ClaseAuditoria<String> pe = new ClaseAuditoria<String>();
				String claveEmpleado = this.Session["NoEmp"].ToString();

                pe.UserName = Session["NoEmp"].ToString();
                pe.UserSign = _UserSign;
				pe.Clase = claveEmpleado;
                concep = await obtenerSolicitudes(pe, urlAPINominix, urlAPIAgilix);
               
            }
            catch (Exception ex)
			{
				throw ex;

			}
			return View(concep);
		}


		/// <summary>
		/// Métdo que registra una solicitud.
		/// </summary>
		/// <param name="peticion">peticion del servicio</param>
		/// <returns>RespuestaData<Solicitud>Solicitud actualizada con el nuevo id</returns>
		public RespuestaData<Solicitud> registrarSolicitud(ClaseAuditoria<Solicitud> peticion,String mediosDias="0")
		{
			RespuestaData<Solicitud> oRespuesta = new RespuestaData<Solicitud>();
			try
			{
				SendMails sm = new Aplics.Servicios.Models.EmailConfig().getConfigMail(true);
				NegocioSolicitudRegistro negocio = new NegocioSolicitudRegistro(peticion.UserSign);
				String empleadoRH = ConfigurationManager.AppSettings["empleadoRH"];
				oRespuesta = negocio.registrarSolicitud(peticion.Clase, sm, empleadoRH,Convert.ToInt32(Session["TypeRolMenuSup"]), mediosDias);
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
            return oRespuesta;
		}

		/// <summary>
		/// Métdo para aprobar o rechazar una solicitud.
		/// </summary>
		/// <param name="peticion">peticion del servicio</param>
		/// <returns>RespuestaSimple con resultado de la operación</returns>
		public async Task<String> aprobarRechazarSolicitud(ClaseAuditoria<SolicitudXAprobar> peticion, String urlAPINominix = "")
		{
			String mensaje = String.Empty;
			RespuestaSimple oRespuesta = new RespuestaSimple();
			try
			{
				NegocioSolicitudAutorizacionRechazo negocio = new NegocioSolicitudAutorizacionRechazo(peticion.UserSign);

				SendMails sm = new Aplics.Servicios.Models.EmailConfig().getConfigMail(true);
				String empleadoRH = ConfigurationManager.AppSettings["empleadoRH"];
				oRespuesta = await negocio.actualizarAutorizacionRechazo(peticion.Clase, sm, empleadoRH,Convert.ToInt32(Session["TypeRolMenuSup"]), urlAPINominix);
			}
			catch (Exception ex)
			{
				oRespuesta.result = 0;
				oRespuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			if (oRespuesta.result != 1)
			{
				mensaje = oRespuesta.mensaje;
				ViewBag.error = mensaje;

			}
			return mensaje;
		}

		/// <summary>
		/// Métdo que devuelve las solicitudes que puede solicar el empleado con sus saldos.
		/// </summary>
		/// <param name="peticion">peticion del servicio</param>
		/// <returns>RespuestaData<Dashboard>Conceptos que puede perdir el empleado</returns>
		public async Task<Dashboard> obtenerSolicitudes(ClaseAuditoria<String> peticion, String urlAPINominix = "", String urlAPIAgilix = "")
		{
			RespuestaData<Dashboard> oRespuesta = new RespuestaData<Dashboard>();
			try
			{
				NegocioSolicitudDashBoard negocio = new NegocioSolicitudDashBoard();
				oRespuesta = await negocio.obtenerDashboard(peticion.Clase, 0, urlAPINominix, urlAPIAgilix);
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta.Datos;
		}

        /// <summary>
		/// Métdo que devuelve las solicitudes que puede solicar el empleado con sus saldos.
		/// </summary>
		/// <param name="peticion">peticion del servicio</param>
		/// <returns>RespuestaData<Dashboard>Conceptos que puede perdir el empleado</returns>
		public Dashboard obtenerHistorialSolicitudes(ClaseAuditoria<String> peticion)
        {
            RespuestaData<Dashboard> oRespuesta = new RespuestaData<Dashboard>();
            try
            {
                NegocioSolicitudDashBoard negocio = new NegocioSolicitudDashBoard();
                oRespuesta = negocio.obtenerHistorialDashboard(peticion.Clase);
            }
            catch (Exception ex)
            {
                oRespuesta.Respuesta.result = 0;
                oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
            return oRespuesta.Datos;
        }


        /// <summary>
        /// Métdo que devuelve los conceptos de ausentsimo que puede solicitar un empleado.
        /// </summary>
        /// <param name="peticion">peticion del servicio</param>
        /// <returns>List<ItemSolicitiud>Conceptos que puede perdir el empleado</returns>
        public List<ItemSolicitiud> obtenerConceptosSolicitudes(ClaseAuditoria<String> peticion)
		{
			RespuestaData<List<ItemSolicitiud>> oRespuesta = new RespuestaData<List<ItemSolicitiud>>();
			try
			{
				NegocioSolicitudDashBoard negocio = new NegocioSolicitudDashBoard(peticion.UserSign);
				oRespuesta = negocio.ListaConceptos(peticion.Clase);
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta.Datos;

		}


		/// <summary>
		/// Método que devuelve los correos de empleados en base a algún criterio.
		/// </summary>
		/// <param name="parametros">peticion con parametros generico</param>
		/// Ejemplo:
		/// QueryParameters<Empleados> parametrosE = new QueryParameters<Empleados>();
		/// parametrosE.where = x => x.U_Cat.Equals("XXXX");
		/// <returns>Lista de correos de empleados</returns>
		public List<CorreoEmpleado> obtenerCorreosEmpleados(ClaseAuditoria<QueryParameters<Empleados>> peticion)
		{
			List<CorreoEmpleado> correoEmpleados = new List<CorreoEmpleado>();
			try
			{
				List<Empleados> empleados = NominixGenerales.ObtenerEmpleados(peticion.Clase, peticion.UserSign);
				CorreoEmpleado correo = new CorreoEmpleado();
				foreach (Empleados empleado in empleados)
				{
					if (!String.IsNullOrEmpty(empleado.U_Email))
					{
						correo = new CorreoEmpleado();
						empleado.U_Nomp = String.IsNullOrEmpty(empleado.U_Nomp) ? "" : empleado.U_Nomp;
						empleado.U_Apepat = String.IsNullOrEmpty(empleado.U_Apepat) ? "" : empleado.U_Apepat;
						empleado.U_Apemat = String.IsNullOrEmpty(empleado.U_Apemat) ? "" : empleado.U_Apemat;
						correo.Nombre = empleado.U_Nomp + " " + empleado.U_Apepat + " " + empleado.U_Apemat;
						correo.NumEmpleado = empleado.Code;
						correo.eMail = empleado.U_Email;
						correoEmpleados.Add(correo);
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return correoEmpleados;
		}

		public List<Notificaciones> BandejaMensajes(ClaseAuditoria<String> peticion, String perfil = "")
		{
			try
			{
				List<Notificaciones> notificaciones = NominixGenerales.consultarNotificaciones(peticion.UserSign, peticion.Clase, perfil);
				return notificaciones;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        [HttpPost]
        public ActionResult SolicitarElPermiso(FormCollection fc, HttpPostedFileBase postedFile)
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

			Solicitud sol = new Solicitud();
            ClaseAuditoria<Solicitud> peticion = new ClaseAuditoria<Solicitud>();
            RespuestaData<Solicitud> res = new RespuestaData<Solicitud>();
			try
			{

				sol.FechaInicial = DateTime.Parse(fc["FechaIni"]);
				sol.FechaFinal = DateTime.Parse(fc["FechaFin"]);
				sol.NumEmpleado = this.Session["NoEmp"].ToString(); 
				sol.TipoSolicitud = fc["tipo"];
                String mediosDias = fc["medioDia"];
				peticion.UserName = this.Session["UserName"].ToString();
				peticion.UserSign = _UserSign;
				peticion.Clase = sol;
				res = registrarSolicitud(peticion, mediosDias);
				if (postedFile != null)
				{
					string path = Server.MapPath("~/EmpFiles/Comprobantes/");
					path += sol.NumEmpleado.ToString() + "\\";
					if (!Directory.Exists(path))
					{
						Directory.CreateDirectory(path);
					}

					postedFile.SaveAs(path + Path.GetFileName(res.Datos.Id.ToString() + "_" + Session["UserName"].ToString() + "_Comprobante.pdf"));

				}

				Session["idSolicitud"] = res.Datos.Id.ToString();
			} catch (Exception ex)
			{
				throw ex;
			}
            return RedirectToAction("ListaPermisos", "Permisos");
		}

        [HttpPost]
		public ActionResult MisPermisos(string rConcep, string rId, string modulo, string valor1, string conceptos, FormCollection fc)
		{
            

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            
			List<DatosSolicitud> solicitudes = new List<DatosSolicitud>();
			try
			{
				ViewData["Valor1"] = valor1;
				ViewData["Modulo"] = modulo;
				ViewData["concep"] = (fc["name"] != null ? fc["name"] : rConcep);
				ViewData["id"] = (fc["id"] != null ? fc["id"] : rId);
				solicitudes = null;

				if (modulo == "ListaPermisos")
				{
					ClaseAuditoria<String> peticionDashBord = new ClaseAuditoria<String>();
					peticionDashBord.Clase = (fc["id"] != null ? fc["id"] : rId);
					solicitudes = SolicitudesEmpleados(peticionDashBord, Session["NoEmp"].ToString());
				}
				else if (modulo == "ListaDescansosAnual")
				{
					ClaseAuditoria<String> peticionDashBord = new ClaseAuditoria<String>();
					peticionDashBord.Clase = ViewData["Valor1"].ToString();
					peticionDashBord.UserName = Session["NoEmp"].ToString();

                    solicitudes = HistorialSolicitudesEmpleados(peticionDashBord,"",conceptos);
                }


				if (!String.IsNullOrEmpty(fc["ddStatusSup"]) || !String.IsNullOrEmpty(fc["ddStatusRH"]))
				{
					if (fc["ddStatusSup"] != "N" && fc["ddStatusRH"] == "N")
					{
						var res1 = solicitudes
							.Where(c => c.StatusSup == fc["ddStatusSup"]).ToList<DatosSolicitud>();
						return View(res1);
					}
					else if (fc["ddStatusSup"] == "N" && fc["ddStatusRH"] != "N")
					{
						var res1 = solicitudes
							.Where(c => c.StatusRH == fc["ddStatusRH"]).ToList<DatosSolicitud>();
						return View(res1);
					}
					else if (fc["ddStatusSup"] != "N" && fc["ddStatusRH"] != "N")
					{
						var res1 = solicitudes
							.Where(c => c.StatusRH == fc["ddStatusRH"] && c.StatusSup == fc["ddStatusSup"]).ToList<DatosSolicitud>();
						return View(res1);
					}

				}
			} catch (Exception ex)
			{
				throw ex;
			}

			return View(solicitudes);
		}


        [HttpPost]
        public ActionResult ListaDescansosAnual(int anio,string flagVacaciones,string flagPermisos)
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

			ClaseAuditoria<String> peticionDashBord = new ClaseAuditoria<String>();
            string conceptos = null;
            List<DatosSolicitud> solicitudes = null;            
			try
			{
                ViewData["Valor1"] = anio;
				ViewData["Modulo"] = "ListaDescansosAnual";
				ViewData["concep"] = anio.ToString();
				ViewData["color"] = "color";

				peticionDashBord.Clase = anio.ToString();
				peticionDashBord.UserName = Session["NoEmp"].ToString();

                if (flagVacaciones == "1" && flagPermisos == "0")
                {
                    conceptos = "S.U_CON IN ('201','1110')";
                } else if (flagVacaciones == "0" && flagPermisos == "1")
                {
                    conceptos = "S.U_CON NOT IN ('201','1110')";
                }

                ViewData["conceptos"] = conceptos;

                solicitudes = HistorialSolicitudesEmpleados(peticionDashBord, "", conceptos);

            } catch (Exception ex)
			{
				throw ex;
			}

			return View("MisPermisos",solicitudes);
		}

        /// <summary>
		/// Método que devuelve el historico de las solicitudes de permisos de los empleados 
		/// </summary>
		/// <param name="peticion"></param>
		/// Filtra por año y por empleado
		/// <returns>List<DatosSolicitud></returns>
		public List<DatosSolicitud> HistorialSolicitudesEmpleados(ClaseAuditoria<String> peticion,String Anio="",string conceptos="")
        {
            try
            {
                List<DatosSolicitud> solicitudes = new NegocioSolicitudDashBoard(peticion.UserSign).HistorialSolicitdesEmpleados(peticion.UserName, peticion.Clase,conceptos);
                return solicitudes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Método que devuelve las solicitudes de permisos de los empleados
        /// </summary>
        /// <param name="peticion"></param>
        /// Filtra por categoría o general con cadena vacía
        /// <returns>List<DatosSolicitud></returns>
        public List<DatosSolicitud> SolicitudesEmpleados(ClaseAuditoria<String> peticion, String cveEmp ="")
		{
			try
			{
				List<DatosSolicitud> solicitudes = new NegocioSolicitudDashBoard(peticion.UserSign).SolicitdesEmpleados(peticion.Clase, cveEmp);
				return solicitudes;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Método que devuelve las solicitudes de permisos de los empleados
		/// </summary>
		/// <param name="peticion"></param>
		/// Filtra por categoría o general con cadena vacía
		/// <returns>List<DatosSolicitud></returns>
		public List<DatosSolicitud> SolicitudesEmpleadosGeneral(ClaseAuditoria<String> peticion,int Anio=0)
		{
			try
			{
				List<DatosSolicitud> solicitudes = new NegocioSolicitudDashBoard(peticion.UserSign).SolicitdesEmpleadosGeneral(peticion.Clase,Anio);
				return solicitudes;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		//public List<DatosSolicitud> SolicitudesEmpleadosHistGeneral(ClaseAuditoria<String> peticion, int Anio = 0)
		//{
		//	try
		//	{
		//		List<DatosSolicitud> solicitudes = new NegocioSolicitudDashBoard(peticion.UserSign).SolicitdesEmpleadosGeneral(peticion.Clase, Anio);
		//		return solicitudes;
		//	}
		//	catch (Exception ex)
		//	{
		//		throw ex;
		//	}
		//}



		/// <summary>
		/// Método que devuelve las solicitudes de permisos de los empleados
		/// </summary>
		/// <param name="peticion"></param>
		/// Filtra solicitud
		/// <returns>List<DatosSolicitud></returns>
		public DatosSolicitud SolicitudEmpleado(ClaseAuditoria<int> peticion)
		{
			try
			{
				DatosSolicitud solicitud = new NegocioSolicitudDashBoard(peticion.UserSign).SolicitdEmpleado(peticion.Clase);
				return solicitud;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        [HttpPost]
        public ActionResult validaFechasReales(DateTime FechaInicial, DateTime FechaFinal, string concepto)
        {
			//hacer query para saber que tiene utdias si es n mando a llamar naturales si es h mando allamar diashabiles
			NegocioSolicitudRegistro negocio = new NegocioSolicitudRegistro();
			var totalDias = 0;
			String tipoDia = String.Empty;

			///tiene que venir de una variable de sesión, pero hay que convertirlo a int
			int UserSign = _UserSign;
			CriteriosConceptosH critConcepto = negocio.obtenerCriterioConcepto(concepto);
			tipoDia = critConcepto.U_TDIAS;
			if (tipoDia.Equals("H"))
            {
				totalDias = (negocio.DiasHabilesContador(FechaInicial, FechaFinal))+1;
            } else
            {
				totalDias = (negocio.DiasNaturalesContador(FechaInicial, FechaFinal));

			}

			return Json(new { TotalDias=totalDias }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult validaEmpalmeFechas(DateTime FechaInicial, DateTime FechaFinal, string concepto)
        {
            //hacer query para saber que tiene utdias si es n mando a llamar naturales si es h mando allamar diashabiles
            NegocioSolicitudRegistro negocio = new NegocioSolicitudRegistro();
            bool resultado = false;

            resultado = negocio.EmpalmeDiasValidacion(FechaInicial, FechaFinal, concepto, Session["NoEmp"].ToString());

            //String tipoDia = String.Empty;

            /////tiene que venir de una variable de sesión, pero hay que convertirlo a int
            //int UserSign = 0;
            //CriteriosConceptosH critConcepto = negocio.obtenerCriterioConcepto(concepto);
            //tipoDia = critConcepto.U_TDIAS;
            //if (tipoDia.Equals("H"))
            //{
            //    totalDias = (negocio.DiasHabilesContador(FechaInicial, FechaFinal)) + 1;
            //}
            //else
            //{
            //    totalDias = (negocio.DiasNaturalesContador(FechaInicial, FechaFinal));

            //}

            //return Json(new { TotalDias = totalDias }, JsonRequestBehavior.AllowGet);

            return Json(new { Resultado = resultado }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LlenaComboTipo(string fecha1)
        {

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            ClaseAuditoria<String> peticionDashBord = new ClaseAuditoria<String>();
            List<ItemSolicitiud> res = null;
			try
			{

                peticionDashBord.UserName = Session["NombreEmpleado"].ToString();
                peticionDashBord.UserSign = _UserSign;
                peticionDashBord.Clase = Session["NoEmp"].ToString();

                res = obtenerConceptosSolicitudes(peticionDashBord);
            }
            catch (Exception ex)
			{
				throw ex;
			}           

            return Json(res, JsonRequestBehavior.AllowGet);
        }

		[HttpGet]
		public ActionResult AdminSolicitudesPermisosId(int Id)
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			DatosSolicitud ds = new DatosSolicitud();
			try
			{
				ds = AdminSolicitudPermiso(Id);
			}catch (Exception ex)
			{
				throw ex;
			}
			
			return View("SolicitudesPermisos", ds);
		}

		public ActionResult AdminSolicitudesPermisos()
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			DatosSolicitud ds = new DatosSolicitud();
			try
			{
				ds = AdminSolicitudPermiso();
			}catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesPermisos",ds);
		}

		private DatosSolicitud AdminSolicitudPermiso(int Id=0)
		{
			DatosSolicitud ds = new DatosSolicitud();
			//aqui
			NegocioSolicitudRegistro negocio = new NegocioSolicitudRegistro();
			try
			{
				
				string userType = this.Session["TypeRol"].ToString();
				string userCode = this.Session["NoEmp"].ToString();

				List<DatosSolicitud> solicitudes = consultarSolicitudes("P", System.DateTime.Today.Year);
				solicitudes = solicitudes.OrderByDescending(y => y.Id).ToList();
				List<DatosSolicitud> aprueba = new List<DatosSolicitud>();

				if (solicitudes.Count() > 0)
				{
					aprueba = filtrarSolicitudPorPerfil(solicitudes, userCode, userType); ;


					if (aprueba.Count() > 0)
                    {

						int idsol = Id;
						if (idsol == 0)
						{
							idsol = aprueba[0].Id;
						}

						ds = consultarSolicitudPermisoxId(idsol);
						List<Documento> documentosEmpleado = NominixGenerales.permisosDocumentos(this.Url.Action("Index", "Documentos"), Server.MapPath("~/EmpFiles/"), "1", ds.NumEmpleado, "Comprobantes\\" + ds.NumEmpleado);
						String nombreArchivo = String.Empty;
						String[] arrayArchivo;
						String relativePath = String.Empty;

						if (documentosEmpleado.Count() > 0)
						{
							foreach (Documento doc in documentosEmpleado)
							{
								nombreArchivo = doc.Nombre;
								arrayArchivo = nombreArchivo.Split('_');
								if (idsol.ToString().Equals(arrayArchivo[0]))
								{
									relativePath = "EmpFiles/Comprobantes/" + ds.NumEmpleado + "/" + doc.Nombre;
									break;
								}
							}
							if (!String.IsNullOrEmpty(relativePath))
							{
								var url = this.Url.Action("Index", "Documentos");
								url = url.Replace("Documentos", "");
								url += relativePath;
								ViewBag.ComprobanteSolicitud = url;
							}
						}
					}
					
				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(aprueba);
				ViewBag.Titulo = "Administrador de Vacaciones y Permisos";
				ViewBag.Perfil = "Admin";
				ViewBag.Status = "P";
			} catch(Exception ex)
			{
				throw ex;
			}
			return ds;
		}

		private List<DatosSolicitud> filtrarSolicitudPorPerfil(List<DatosSolicitud> solicitudes, String userCode, String userType)
        {
			List<DatosSolicitud> aprueba = new List<DatosSolicitud>();
			NegocioSolicitudRegistro negocio = new NegocioSolicitudRegistro();
			try
			{
				if (userType.Equals("1"))
				{
					aprueba = negocio.ObtenerConceptosNotif(solicitudes, "ADMIN");
				}
				else if (userType.Equals("2"))
				{
					aprueba = negocio.ObtenerConceptosNotif(solicitudes, "SUP", userCode);
				}
			}
			catch(Exception ex)
            {
				throw ex;
            }
			return aprueba;
        }

		private List<DatosSolicitud> consultarSolicitudes(String Status,int Anio=0)
		{
			List<DatosSolicitud> ListaSolicitudes= new List<DatosSolicitud>();
			try
			{
				ClaseAuditoria<String> peticion = new ClaseAuditoria<String>();
				peticion.UserName = Session["NoEmp"].ToString();
				peticion.Clase = String.Empty;
				ListaSolicitudes = SolicitudesEmpleadosGeneral(peticion,Anio);
				if (!String.IsNullOrEmpty(Status))
				{
					ListaSolicitudes = ListaSolicitudes.Where(x=> x.StatusRH.Equals(Status) && x.StatusSup.Equals(Status)).ToList();
				}
			}
			catch(Exception ex)
			{
				throw ex; 
			}
			return ListaSolicitudes;
		}

		private List<DatosSolicitud> consultaSolicitudes(String Status, int Anio = 0)
		{
			List<DatosSolicitud> ListaSolicitudes = new List<DatosSolicitud>();
			try
			{
				ClaseAuditoria<String> peticion = new ClaseAuditoria<String>();
				peticion.UserName = Session["NoEmp"].ToString();
				peticion.Clase = String.Empty;
				ListaSolicitudes = SolicitudesEmpleadosGeneral(peticion, Anio);
            }
			catch (Exception ex)
			{
				throw ex;
			}
			return ListaSolicitudes;
		}

		[HttpPost]
		public ActionResult FiltroEmpleado(String Empleado,String Status)
		{
			if (Session["TypeRol"] == null || Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}

			DatosSolicitud ds = new DatosSolicitud();
			try
			{
				List<DatosSolicitud> solicitudes = consultarSolicitudes(Status);
				solicitudes = solicitudes.Where(y => y.NumEmpleado.Equals(Empleado)).ToList();
				if (solicitudes.Count() > 0)
				{
					ds = consultarSolicitudPermisoxId(solicitudes[0].Id);
				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(solicitudes);
				ViewBag.Titulo = "Administrador de Vacaciones y Permisos";
				ViewBag.Perfil = "Admin";
				ViewBag.Status = Status;
			}catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesPermisos", ds);
		}

		[HttpPost]
		public ActionResult FiltroAnio(int Anio, String Status)
		{
			if (Session["TypeRol"] == null || Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			DatosSolicitud ds = new DatosSolicitud();
			try
			{
				List<DatosSolicitud> solicitudes = consultarSolicitudes(Status, Anio);
				if (solicitudes.Count() > 0)
				{
					ds = consultarSolicitudPermisoxId(solicitudes[0].Id);
				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(solicitudes);
				ViewBag.Titulo = "Resultado de Búsqueda por Filtro";
				ViewBag.Perfil = "Admin";
				ViewBag.Status = Status;
			} catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesPermisos", ds);
		}

		[HttpPost]
		public ActionResult HistSolicitudesEmpledo(String Empleado)
		{
			ClaseAuditoria<String> peticionsolicitud = new ClaseAuditoria<String>();
			try
			{
                peticionsolicitud.UserName = Empleado;
                peticionsolicitud.UserSign = _UserSign;
                peticionsolicitud.Clase = System.DateTime.Today.Year.ToString();
				List<DatosSolicitud> solicitudes = HistorialSolicitudesEmpleados(peticionsolicitud);
				solicitudes = solicitudes.Where(y => y.StatusSup != "P" && y.StatusRH != "P").OrderByDescending( z => z.Id).ToList();
				ViewBag.ListaSolicitudesDetalle = solicitudes;
				ViewBag.EmpleadoSeleccionado = Empleado;
				DatosSolicitud ds = new DatosSolicitud();
				for (var i = 0; solicitudes.Count() > i; i++)
                {
					ds = consultarSolicitudPermisoxId(solicitudes[0].Id);
					List<Documento> documentosEmpleado = NominixGenerales.permisosDocumentos(this.Url.Action("Index", "Documentos"), Server.MapPath("~/EmpFiles/"), "1", ds.NumEmpleado, "Comprobantes\\" + ds.NumEmpleado);
					String nombreArchivo = String.Empty;
					String[] arrayArchivo;
					String relativePath = String.Empty;

					if (documentosEmpleado.Count() > 0)
					{
						foreach (Documento doc in documentosEmpleado)
						{
							nombreArchivo = doc.Nombre;
							arrayArchivo = nombreArchivo.Split('_');
							if (solicitudes[0].Id.ToString().Equals(arrayArchivo[0]))
							{
								relativePath = "EmpFiles/Comprobantes/" + ds.NumEmpleado + "/" + doc.Nombre;
								break;
							}
						}
						if (!String.IsNullOrEmpty(relativePath))
						{
							var url = this.Url.Action("Index", "Documentos");
							url = url.Replace("Documentos", "");
							url += relativePath;
							ViewBag.ComprobanteSolicitud = url;
						}
					}
				}

				
			}
			catch (Exception ex)
			{
				throw ex;
			}
			
			return PartialView("~/Views/Shared/_modalHistPermisos.cshtml");
		}

		private List<ResumenSolicitud> ListaSolicitudes(List<DatosSolicitud> ListaSolicitudesEmpleado)
		{

			List<ResumenSolicitud> Solicitudes = new List<ResumenSolicitud>();
			try
			{
				if (ListaSolicitudesEmpleado != null)
				{
					foreach (DatosSolicitud solempleado in ListaSolicitudesEmpleado)
					{
						Solicitudes.Add(new ResumenSolicitud() { Id = solempleado.Id, Fecha = solempleado.FechaSolicitud, TipoSolicitud = "Permiso", ClaveTipoSolicitud = "P" });
					}
				}
			} catch(Exception ex)
			{
				throw ex;
			}
			return Solicitudes.OrderByDescending(x => x.Id).ToList();
		}

		private DatosSolicitud consultarSolicitudPermisoxId(int id)
		{
			DatosSolicitud solicitud = new DatosSolicitud();
			try
			{
				ClaseAuditoria<int> peticionsolicitud = new ClaseAuditoria<int>();
				peticionsolicitud.UserName = "aplics";
				peticionsolicitud.UserSign = _UserSign;
				peticionsolicitud.Clase = id;
				//Se trae una sola solicitud de permiso por docentry
				solicitud = SolicitudEmpleado(peticionsolicitud);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return solicitud;
		}
		private DatosSolicitud consultarSolicitudXId(int id)
		{
			DatosSolicitud solicitud = new DatosSolicitud();
			try
			{
				ClaseAuditoria<int> peticionsolicitud = new ClaseAuditoria<int>();
				peticionsolicitud.UserName = "aplics";
				peticionsolicitud.UserSign = _UserSign;
				peticionsolicitud.Clase = id;
				//Se trae una sola solicitud de permiso por docentry
				solicitud = SolicitudEmpleado(peticionsolicitud);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return solicitud;
		}

		[HttpPost]
		public ActionResult BuscarSolicitudxId(String filtroPerfil, int id,String Status)
		{

			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}

			DatosSolicitud solicitud = new DatosSolicitud();
			try
			{
				ViewBag.Perfil = filtroPerfil;
				solicitud = consultarSolicitudPermisoxId(id);
				ViewBag.Status = Status;
				List<Documento> documentosEmpleado = NominixGenerales.permisosDocumentos(this.Url.Action("Index", "Documentos"), Server.MapPath("~/EmpFiles/"), "1", solicitud.NumEmpleado, "Comprobantes\\" + solicitud.NumEmpleado);
				String nombreArchivo = String.Empty;
				String[] arrayArchivo;
				String relativePath = String.Empty;

				if (documentosEmpleado.Count() > 0)
				{
					foreach (Documento doc in documentosEmpleado)
					{
						nombreArchivo = doc.Nombre;
						arrayArchivo = nombreArchivo.Split('_');
						if (id.ToString().Equals(arrayArchivo[0]))
						{
							relativePath = "EmpFiles/Comprobantes/" + solicitud.NumEmpleado + "/" + doc.Nombre;
							break;
						}
					}
					if (!String.IsNullOrEmpty(relativePath))
					{
						var url = this.Url.Action("Index", "Documentos");
						url = url.Replace("Documentos", "");
						url += relativePath;
						ViewBag.ComprobanteSolicitud = url;
					}
				}

			} catch(Exception ex)
			{
				throw ex;
			}
			
			return PartialView("~/Views/Shared/_solicitudPermiso.cshtml", solicitud);
		}
		[HttpPost]
		public ActionResult BuscarNotificacionxId(String filtroPerfil, int id, String Status)
		{

			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}

			DatosSolicitud solicitud = new DatosSolicitud();
			try
			{
				ViewBag.Perfil = filtroPerfil;
				solicitud = consultarSolicitudPermisoxId(id);
				ViewBag.Status = Status;
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return PartialView("~/Views/Shared/_fileSolPermisoEmp.cshtml", solicitud);
		}

		public ActionResult BusqHistoricoPermisos ()
        {
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			DatosSolicitud ds = new DatosSolicitud();
			try
            {
				String empSup = this.Session["NoEmp"].ToString();
				List<DatosSolicitud> solicitudes = consultaSolicitudes(String.Empty, 0);
				if (solicitudes.Count() > 0)
				{
					string userType = this.Session["TypeRol"].ToString();
					string userCode = this.Session["NoEmp"].ToString();
					//solicitudes = filtrarSolicitudPorPerfil(solicitudes, userCode, userType); ;
					if (solicitudes.Count > 0)
					{
						ds = consultarSolicitudPermisoxId(solicitudes[0].Id);
						List<Documento> documentosEmpleado = NominixGenerales.permisosDocumentos(this.Url.Action("Index", "Documentos"), Server.MapPath("~/EmpFiles/"), "1", ds.NumEmpleado, "Comprobantes\\" + ds.NumEmpleado);
						String nombreArchivo = String.Empty;
						String[] arrayArchivo;
						String relativePath = String.Empty;

						if (documentosEmpleado.Count() > 0)
						{
							foreach (Documento doc in documentosEmpleado)
							{
								nombreArchivo = doc.Nombre;
								arrayArchivo = nombreArchivo.Split('_');
								if (solicitudes[0].Id.ToString().Equals(arrayArchivo[0]))
								{
									relativePath = "EmpFiles/Comprobantes/" + ds.NumEmpleado + "/" + doc.Nombre;
									break;
								}
							}
							if (!String.IsNullOrEmpty(relativePath))
							{
								var url = this.Url.Action("Index", "Documentos");
								url = url.Replace("Documentos", "");
								url += relativePath;
								ViewBag.ComprobanteSolicitud = url;
							}
						}
					}
				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(solicitudes);
				ViewBag.Titulo = "Histórico de Permisos";
				ViewBag.Perfil = "Admin";
				ViewBag.Status = String.Empty;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesPermisos", ds);
		}


		public ActionResult BusqSolicitudesPermisos()
		{

			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			DatosSolicitud ds = new DatosSolicitud();
			try
			{
			    String empSup = this.Session["NoEmp"].ToString();
				List<DatosSolicitud> solicitudes = consultarSolicitudes(String.Empty, 0);
				solicitudes = solicitudes.Where( y => y.StatusRH != "P" && y.StatusSup != "P").OrderByDescending(z => z.Id).ToList();
				if (solicitudes.Count() > 0)
				{
					string userType = this.Session["TypeRol"].ToString();
					string userCode = this.Session["NoEmp"].ToString();
					solicitudes = filtrarSolicitudPorPerfil(solicitudes, userCode, userType); ;
					if (solicitudes.Count > 0)
					{
						ds = consultarSolicitudPermisoxId(solicitudes[0].Id);
						List<Documento> documentosEmpleado = NominixGenerales.permisosDocumentos(this.Url.Action("Index", "Documentos"), Server.MapPath("~/EmpFiles/"), "1", ds.NumEmpleado, "Comprobantes\\" + ds.NumEmpleado);
						String nombreArchivo = String.Empty;
						String[] arrayArchivo;
						String relativePath = String.Empty;

						if (documentosEmpleado.Count() > 0)
						{
							foreach (Documento doc in documentosEmpleado)
							{
								nombreArchivo = doc.Nombre;
								arrayArchivo = nombreArchivo.Split('_');
								if (solicitudes[0].Id.ToString().Equals(arrayArchivo[0]))
								{
									relativePath = "EmpFiles/Comprobantes/" + ds.NumEmpleado + "/" + doc.Nombre;
									break;
								}
							}
							if (!String.IsNullOrEmpty(relativePath))
							{
								var url = this.Url.Action("Index", "Documentos");
								url = url.Replace("Documentos", "");
								url += relativePath;
								ViewBag.ComprobanteSolicitud = url;
							}
						}
					}
				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(solicitudes);
				ViewBag.Titulo = "Buscar Solicitudes de Permisos";
				ViewBag.Perfil = "Admin";
				ViewBag.Status = String.Empty;
			}catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesPermisos", ds);
		}

		public ActionResult BusqSolicitudesPermisosEmpxId(int Id)
		{

			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			DatosSolicitud ds = new DatosSolicitud();
			try
			{
				ds = busqSolicitudEmp(Id);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesPermisos", ds);
		}

		public ActionResult BusqSolicitudesPermisosEmp()
		{

			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			DatosSolicitud ds = new DatosSolicitud();
			try
			{
				ds = busqSolicitudEmp();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesPermisos", ds);
		}

		private DatosSolicitud busqSolicitudEmp( int id = 0)
		{
			DatosSolicitud ds = new DatosSolicitud();
			try
			{
				List<DatosSolicitud> solicitudes = consultarSolicitudes(String.Empty, 0);
				solicitudes = solicitudes.Where(y => y.StatusRH != "P" && y.StatusSup != "P" && y.NumEmpleado == this.Session["NoEmp"].ToString()).OrderByDescending(z => z.Id).ToList();
				if (solicitudes.Count() > 0)
				{
					if (id == 0)
					{
						id = solicitudes[0].Id;
					}
					ds = consultarSolicitudPermisoxId(id);
				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(solicitudes);
				ViewBag.Titulo = "Notificaciones de Permisos";
				ViewBag.Perfil = "Empleado";
				ViewBag.Status = String.Empty;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return ds;
		}


        [HttpPost]
        public JsonResult CalendarioListaEventos()
        {
            CalendarioJson calRes2 = new CalendarioJson();
            try
			{
				Boolean isManager = Session["TypeRol"].ToString()== "2" ? true : false;
				string filtroEmpleado = Session["TypeRol"].ToString() == "1" ? string.Empty : Session["NoEmp"].ToString();
				Calendario calRes = obtenerItemsCalendario(filtroEmpleado, isManager, true);
				CalendarioItemJson calResJson = new CalendarioItemJson();
			
				foreach (var item in calRes.ItemsCalendario)
				{
					calResJson = new CalendarioItemJson();
					calResJson.color = item.color;
					calResJson.description = item.descripcion2;
					calResJson.end = item.fin;
					calResJson.start = item.inicio;
					calResJson.textColor = item.colorTexto;
					calResJson.title = item.titulo;
					calRes2.ItemsCalendario.Add(calResJson);
				}
			} catch (Exception ex)
			{
				throw ex;
			}

            return Json(calRes2.ItemsCalendario, JsonRequestBehavior.AllowGet);
        }

		private List<String> Subordinados(String empleado)
		{
			List<String> filtroEmpleados = new List<String>();
			try
			{
				ClaseAuditoria<QueryParameters<Empleados>> peticion = new ClaseAuditoria<QueryParameters<Empleados>>();
				QueryParameters<Empleados> parametrosE = new QueryParameters<Empleados>();
				parametrosE.where = x => x.U_Empsup.Equals(empleado);
				peticion.Clase = parametrosE;
				peticion.UserName = "aplics";
				peticion.UserSign = _UserSign;
				List<Empleados> empleados = NominixGenerales.ObtenerEmpleados(peticion.Clase, peticion.UserSign);
				if (empleados.Count > 0)
				{
					filtroEmpleados = empleados.Select(x => x.Code).ToList();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return filtroEmpleados;
		}

		public ActionResult CalendarioEmp()
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			try
			{
				//se utiliza para cambiar la parte visual y activar ciertos codigos html, java y css para el uso del calendario
				Session["CalendarioActivo"] = true;
			} catch (Exception ex)
			{
				throw ex;
			}
            return View();
		}

        public Calendario obtenerItemsCalendario(string empleado, Boolean isManager = false, Boolean Confirmados = false)
        {
            Calendario item = new Calendario();
            try
            {
				string filtro = empleado;
				if (isManager)
				{
					List<String> filtroEmpleados = Subordinados(Session["NoEmp"].ToString());
					if(filtroEmpleados.Count() > 0)
					{
						filtro = string.Join(",", filtroEmpleados);
						filtro = "'" + filtro.Replace(",", "','") + "'";
					}
				}
				NegocioSolicitudCalendario negocio = new NegocioSolicitudCalendario();
                item = negocio.buscarListadoEventosCalendario(filtro, isManager, Confirmados);
            } catch (Exception ex)
            {
				throw ex;
			}
            return item;
        }

        
        public ActionResult MenuSupAdmin()
        {
			try
			{
				Session["TypeRol"] = 1;
			} catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("AdminSolicitudesPermisos", "Permisos");
        }

        public ActionResult MenuSupManager()
        {
			try
			{
				Session["TypeRol"] = 2;

			}catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("AdminSolicitudesPermisos", "Permisos");
        }

        public ActionResult MenuSupEstudiante()
        {
			try
			{
				Session["TypeRol"] = 3;

			}catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("ListaPermisos", "Permisos");
        }

        public ActionResult MenuSupEmpleado()
        {
			try
			{
				Session["TypeRol"] = 4;
			}catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("ListaPermisos", "Permisos");
        }

        public ActionResult MenuQuit()
        {

			try
			{
				Session.RemoveAll();
				Session.Abandon();
			} catch (Exception ex)
			{
				throw ex;
			}

            return RedirectToAction("Index", "Home");
        }

	}

}
