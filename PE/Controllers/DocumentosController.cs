﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Aplics.Servicios.Modelos.Nominix;
using GRC.Servicios.Utilidades.Genericos;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Solicitudes;
using Aplics.Servicios.Negocio.Nominix;
using PortalEmpleadosV2.Models;

namespace PortalEmpleadosV2.Controllers
{
    public class DocumentosController : Controller
    {
        // GET: Documentos
        public ActionResult Index()
        {
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            List<Documento> Documentos = new List<Documento>();
			try
			{
				Documentos = NominixGenerales.permisosDocumentos(this.Url.Action("Index", "Documentos"), Server.MapPath("~/EmpFiles/"), Session["TypeRol"].ToString(), Session["NoEmp"].ToString());
			}
			catch (Exception ex)
			{
				throw ex;
			}
			ViewBag.Titulo = "Mis Documentos";

			return View(Documentos);
        }

		[HttpPost]
		public ActionResult ContenidoCarpeta(String Ruta)
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }


            List<Documento> Documentos = new List<Documento>();
			try
			{
				Documentos = NominixGenerales.permisosDocumentos(this.Url.Action("Index", "Documentos"), Server.MapPath("~/EmpFiles/"), Session["TypeRol"].ToString(), Session["NoEmp"].ToString(), Ruta);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("Index",Documentos);
		}
	}
}