﻿using Aplics.Servicios.Modelos.Solicitudes;
using PortalEmpleadosV2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace PortalEmpleadosV2.Controllers
{
    public class solicitudesController : Controller
    {
        [HttpGet]
        public ActionResult Index(FormCollection formCollection)
        {
            String fechaSol = formCollection["fechaSol"];
            String fechaIn = formCollection["fechaIn"];
            String fechaFin = formCollection["fechaFin"];
            String NumEmp = formCollection["NumEmp"];
            String tipo = formCollection["tipo"];
            List<Solicitud> _solicitudes;
            Nominix _Nominix;
            using (_Nominix = new Nominix())
            {
                 
                // Recuperamos el 'DbSet' completo
                _solicitudes = Nominix.Solicitud.ToList();

                // Filtramos el resultado por el 'texto de búqueda'
                if (!string.IsNullOrEmpty(fechaIn))
                {
                        _solicitudes = _solicitudes.Where(x => x.FechaInicial.Equals(fechaIn)).ToList();

                }
                if (!string.IsNullOrEmpty(fechaFin))
                {
                    _solicitudes = _solicitudes.Where(x => x.FechaFinal.Equals(fechaFin)).ToList();

                }
                if (!string.IsNullOrEmpty(fechaSol))
                {
                    _solicitudes = _solicitudes.Where(x => x.FechaSolicitud.Equals(fechaSol)).ToList();

                }
                if (!string.IsNullOrEmpty(NumEmp))
                {
                    _solicitudes = _solicitudes.Where(x => x.NumEmpleado.Contains(NumEmp)).ToList();

                }
                if (!string.IsNullOrEmpty(tipo))
                {
                    _solicitudes = _solicitudes.Where(x => x.TipoSolicitud.Contains(tipo)).ToList();

                }

            }
            return View(_solicitudes);
        }

        [HttpGet]
        public ActionResult Descansos(FormCollection formCollection)
        {
            String fechaSol=formCollection["fechaSol"];
            String fechaIn = formCollection["fechaIn"];
            String fechaFin = formCollection["fechaFin"];
            String NumEmp = formCollection["NumEmp"];
            String tipo = formCollection["tipo"];
            List <Solicitud> _solicitudes;
            Nominix _Nominix;
            using (_Nominix = new Nominix())
            {

                // Recuperamos el 'DbSet' completo
                _solicitudes = Nominix.Solicitud.ToList();

                // Filtramos el resultado por el 'texto de búqueda'
                if (!string.IsNullOrEmpty(fechaIn))
                {
                    _solicitudes = _solicitudes.Where(x => x.FechaInicial.Equals(fechaIn)).ToList();

                }
                if (!string.IsNullOrEmpty(fechaFin))
                {
                    _solicitudes = _solicitudes.Where(x => x.FechaFinal.Equals(fechaFin)).ToList();

                }
                if (!string.IsNullOrEmpty(fechaSol))
                {
                    _solicitudes = _solicitudes.Where(x => x.FechaSolicitud.Equals(fechaSol)).ToList();

                }
                if (!string.IsNullOrEmpty(NumEmp))
                {
                    _solicitudes = _solicitudes.Where(x => x.NumEmpleado.Contains(NumEmp)).ToList();

                }
                if (!string.IsNullOrEmpty(tipo))
                {
                    _solicitudes = _solicitudes.Where(x => x.TipoSolicitud.Contains(tipo)).ToList();

                }

            }
            return View(_solicitudes);
        }

        [HttpGet]
        public ActionResult Mis_solicitudes()
        {
            List<Dashboard> Dashboard;
            List<DashboardSolicitud> _Ds = new List<DashboardSolicitud>();
            Nominix _Nominix;
            using (_Nominix = new Nominix())
            {

                // Recuperamos el 'DbSet' completo
                Dashboard = Nominix.Dashboard.ToList();
                Dashboard = Dashboard.Where(x => x.NumEmpleado.Contains(Session.SessionID)).ToList();
                Dashboard db = new Dashboard();
                foreach (var dss in Dashboard[0].Solicitudes)
                {
                    List<DashboardSolicitud> _Dss;
                    _Dss = Nominix.DashboardSolicitud.ToList();
                    _Dss = _Dss.Where(x => x.Id.Contains(dss.Id)).ToList();
                    _Ds.AddRange(_Dss);
                }

                




            }
            return View(_Ds);
        }

        [HttpGet]
        public ActionResult Mis_descansos()
        {
            List<Dashboard> Dashboard;
            List<DashboardSolicitud> _Ds = new List<DashboardSolicitud>();
            Nominix _Nominix;
            using (_Nominix = new Nominix())
            {
                // Recuperamos el 'DbSet' completo
                Dashboard = Nominix.Dashboard.ToList();
                Dashboard = Dashboard.Where(x => x.NumEmpleado.Contains(Session.SessionID)).ToList();
                Dashboard db = new Dashboard();
                foreach (var dss in Dashboard[0].Solicitudes)
                {
                    List<DashboardSolicitud> _Dss;
                    _Dss = Nominix.DashboardSolicitud.ToList();
                    _Dss = _Dss.Where(x => x.Id.Contains(dss.Id)).ToList();
                    _Ds.AddRange(_Dss);
                }
            }
            return View(_Ds);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewSolicitud([Bind(Include = "TipoSolicitud, FechaSolicitud, FechaInicial, FechaFinal")]Solicitud soli)
        {
            Nominix db = new Nominix();
            try
            {
                if (ModelState.IsValid)
                {
                    db.Solicitud.Add(soli);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(soli);
        }
    }
}