﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Aplics.Servicios.Modelos.Nominix;
using GRC.Servicios.Utilidades.Genericos;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Solicitudes;
using Aplics.Servicios.Negocio.Nominix;

namespace PortalEmpleadosV2.Controllers
{
    public class CustomErrorController : Controller
    {
        // GET: /CustomError/
        [HandleError]
        public ActionResult Index()
        {
            return View("~/Views/Shared/CustomError.cshtml");
        }

    }
}