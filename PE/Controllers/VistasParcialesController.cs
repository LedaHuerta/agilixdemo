﻿using Aplics.Servicios.Modelos.Nominix;
using Aplics.Servicios.Negocio.Nominix;
using GRC.Servicios.Utilidades.Genericos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortalEmpleadosV2.Controllers
{
    public class VistasParcialesController : Controller
    {
       
        public ActionResult MensajesHeader()
        {
            List<Notificaciones> msgs = new List<Notificaciones>();
            List<Notificaciones> resFilter = new List<Notificaciones>();
            try
            {
                 if (Session["TypeRol"] == null && Session["NoEmp"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }
                ClaseAuditoria<String> peticionBandeja = new ClaseAuditoria<String>();
                //peticionBandeja.Clase = "0195";
                peticionBandeja.Clase = Session["NoEmp"].ToString();
                //Muestra la bandeja de mensajes por destinatario
                msgs = BandejaMensajes(peticionBandeja, Session["TypeRol"].ToString());

                //var resFilter = (from q in msgs
                //                 orderby q.fechaEntrega descending
                //                 where q.rol == Convert.ToInt32(Session["TypeRol"])
                //                 select q
                //    ).Take(10).ToList<Notificaciones>();

                //var resFilter = (from q in msgs
                //                 orderby q.fechaEntrega descending
                //                 where q.rol == Convert.ToInt32(Session["TypeRol"])
                //                 select q
                //    ).ToList<Notificaciones>();

                resFilter = msgs;

                //ViewBag.TotalNotificaciones = msgs.Count();
                ViewBag.TotalNotificaciones = resFilter.Count();

                //return PartialView("_Notificaciones", msgs);


                //ViewBag.NotificacionHorarios = 
            } catch( Exception ex)
            {
                throw ex;
            }


            return PartialView("_Notificaciones", resFilter);
        }

		public ActionResult LeerMensaje(int Id,String url, int idProceso)
		{
            ClaseAuditoria<String> peticionBandeja = new ClaseAuditoria<String>();
            try
            {
                peticionBandeja.Clase = Session["NoEmp"].ToString();
                actualizarMensaje(peticionBandeja, Id);
            } catch (Exception ex)
            {
                throw ex;
            }
			return Content(url + "," + idProceso);
		}

        public ActionResult LeerNotificacion(int Id, String url, int idProceso)
        {
            ClaseAuditoria<String> peticionBandeja = new ClaseAuditoria<String>();
            try
            {
                peticionBandeja.Clase = Session["NoEmp"].ToString();
                actualizarMensaje(peticionBandeja, Id);
            } catch (Exception ex)
            {
                throw ex;
            }
            return Content(url + "," + idProceso);
        }
        public List<Notificaciones> BandejaMensajes(ClaseAuditoria<String> peticion, String perfil = "")
        {
            try
            {
                List<Notificaciones> notificaciones = NominixGenerales.consultarNotificaciones(peticion.UserSign, peticion.Clase, perfil);
                return notificaciones.OrderByDescending(x => x.Id).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		public Boolean actualizarMensaje(ClaseAuditoria<String> peticion, int Id)
		{
			try
			{
                Notificaciones notificacionxId = NominixGenerales.consultarNotificacionXid(peticion.UserSign, Id);
                notificacionxId.fechaLectura = (DateTime?)System.DateTime.Now;
                Notificaciones notificar = NominixGenerales.Notificar(notificacionxId,false,peticion.UserSign);
                //NominixGenerales.consultarNotificaciones(peticion.UserSign, peticion.Clase, perfil);
                return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        [HttpPost]
        public ActionResult VisorPDF(String pdf)
        {

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

            String mensaje = String.Empty;
            try
            {
                obtienePdf(pdf);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            ViewBag.Error = mensaje;
            return PartialView("_Constancia");
        }

        private void obtienePdf(string pdf)
        {
            try
            {
                    string embed = "<object data='{0}' type='application/pdf' id='constancia-horario'>";
                    embed += "<a href = '{0}'>" + pdf + "</a>";
                    embed += "</object>";
                    String sEmbed = string.Format(embed, pdf);
                    TempData["Embed"] = sEmbed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
