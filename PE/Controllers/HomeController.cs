﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Aplics.Servicios.Modelos.Nominix;
using GRC.Servicios.ClienteRestNF;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using PortalEmpleadosV2.DataQuerys;

namespace PortalEmpleadosV2.Controllers
{
    public class HomeController : Controller
    {
        private String urlAPINominix = ConfigurationManager.AppSettings["BaseURLServiciosNominix"];
        private String urlAPIAgilix = ConfigurationManager.AppSettings["BaseURLServiciosAgilix"];
        // GET: Home
        public  async Task<ActionResult> Index()
        {
            String TipoAutenticacion = ConfigurationManager.AppSettings["TipoAutenticacion"];
            if (TipoAutenticacion.Equals("FORMS"))
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                //AZURE AD
                if (Request.IsAuthenticated)
                {
                    var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;
                    var email = userClaims?.FindFirst("preferred_username")?.Value;
                    Boolean isValidUser = await autenticacionUsuario(email);
                    
                    if (isValidUser)
                    {
                        if (Convert.ToInt32(Session["TypeRol"]) == 1 || Convert.ToInt32(Session["TypeRol"]) == 2)
                        {
                            return RedirectToAction("AdminSolicitudesPermisos", "Permisos");
                        }
                        else
                        {
                            return RedirectToAction("ListaPermisos", "Permisos");
                        }
                    }
                    
                    
                }
            }
            return View();
        }

       
        /// <summary>
        /// Send an OpenID Connect sign-in request.
        /// Alternatively, you can just decorate the SignIn method with the [Authorize] attribute
        /// </summary>
        public void SignIn()
        {
            var url = this.Url.Action("Index", "Home");
            HttpContext.GetOwinContext().Authentication.Challenge(
                new AuthenticationProperties { RedirectUri = url },
                OpenIdConnectAuthenticationDefaults.AuthenticationType);
        }

        /// <summary>
        /// Send an OpenID Connect sign-out request.
        /// </summary>
        //public void SignOut()
        public ActionResult SignOut()
        {
            String TipoAutenticacion = ConfigurationManager.AppSettings["TipoAutenticacion"];
            if (TipoAutenticacion.Equals("FORMS"))
            {
                Session.Clear();
                Session.Abandon();
                return RedirectToAction("Index", "Login");
            }
            else
            {
                Session["NoEmp"] = null;
                Session["TypeRol"] = null;
                HttpContext.GetOwinContext().Authentication.SignOut(
                        OpenIdConnectAuthenticationDefaults.AuthenticationType,
                        CookieAuthenticationDefaults.AuthenticationType);
                return RedirectToAction("Index", "Login");
            }
            
        }

        /// <summary>
        /// Validar usuario
        /// </summary>
        /// <param name="eMail"></param>
        /// <returns>Boolean</returns>
        private async Task<Boolean> autenticacionUsuario(String eMail)
        {
            Boolean result = false;
            try
            {
                //OBTENEMOS LA DATA
                //DataInsert di = new DataInsert();
                //var dataGeneral = di.sp_ObtenDataLoginAD(eMail).FirstOrDefault<LoginAcceso>();

                //aqui se construye el token

                //aqui se llama el API, para traer los datos de acceso GET
                //Crear un cliente del tipo del objeto devuelto:
                ClienteRest<LoginAcceso> cliente = new ClienteRest<LoginAcceso>();
                //Hacer GET al Api Agilix:
                LoginAcceso dataGeneral = await cliente.LLamarServicioSimple<String>(urlAPIAgilix, "AccesoAgilix/", eMail);

                if (dataGeneral != null)
                {
                    result = true;
                    Session["NoEmp"] = dataGeneral.NoEmp;
                    Session["Notificaciones"] = "15";
                    Session["NombreEmpleado"] = dataGeneral.NombreEmpleado == null ? "No se tiene nombre del empleado" : dataGeneral.NombreEmpleado;
                    Session["AnioIngreso"] = dataGeneral.AnioIngreso;
                    Session["UserSign"] = dataGeneral.UserSign;


                    //se agrega para cuando se utiliza el calendario activar ciertas funciones
                    Session["CalendarioActivo"] = false;

                    //variable de sesion para indicar el tipo de rol
                    //1 = Admin,2 = Manager, 3 = Empleado
                    Session["TypeRol"] = dataGeneral.TipoRol;
                    if (dataGeneral.TipoRol == null)
                    {
                        result = false;
                        Session["NoEmp"] = null;
                        ViewBag.Error = "El tipo de usuario no está configurado, por favor verificarlo con sistemas.";
                    } else
                    {
                        Session["TypeRolMenuSup"] = dataGeneral.TipoRol;
                        Session["idSolicitud"] = null;
                        Session["UserSign"] = dataGeneral.UserSign;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}