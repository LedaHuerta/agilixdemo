﻿using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Modelos.Nominix;
using GRC.Servicios.ClienteRestNF;
using PortalEmpleadosV2.DataQuerys;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PortalEmpleadosV2.Controllers
{
    public class LoginController : Controller
    {
        private String urlAPINominix = ConfigurationManager.AppSettings["BaseURLServiciosNominix"];
        private String urlAPIAgilix = ConfigurationManager.AppSettings["BaseURLServiciosAgilix"];
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(string a, FormCollection fc)
        {
            var isValidUser = false;

            try
            {
                //OBTENEMOS LA DATA
                LoginAcceso dataGeneral = new LoginAcceso();
                if (Debugger.IsAttached)
                {     
                    DataInsert di = new DataInsert();
                    dataGeneral = di.sp_ObtenDataLogin(fc["user"], fc["password"]).FirstOrDefault<LoginAcceso>();
                }else
                {
                    //aqui se construye el token

                    //aqui se llama el API, para traer los datos de acceso GET
                    //Crear un cliente del tipo del objeto devuelto:
                    ClienteRest<LoginAcceso> cliente = new ClienteRest<LoginAcceso>();
                    //Hacer GET al Api Agilix:
                    dataGeneral = await cliente.LLamarServicioPutGeneral<String>(urlAPIAgilix, "AccesoAgilix/" + fc["user"], fc["password"]);
                }

                if (dataGeneral != null)
                {
                    isValidUser = true;
                    Session["NoEmp"] = fc["user"];
                    Session["Notificaciones"] = "15";
                    Session["NombreEmpleado"] = dataGeneral.NombreEmpleado == null ? "No se tiene nombre del empleado" : dataGeneral.NombreEmpleado;
                    Session["AnioIngreso"] = dataGeneral.AnioIngreso;
                    //una vez que agregue el campo reemplazar en todos los lugares donde se le pasa el user sign por esta variable de sesión.
                    Session["UserSign"] = dataGeneral.UserSign;

                    //se agrega para cuando se utiliza el calendario activar ciertas funciones
                    Session["CalendarioActivo"] = false;

                    //variable de sesion para indicar el tipo de rol
                    //1 = Admin,2 = Manager, 3 = Empleado
                    Session["TypeRol"] = dataGeneral.TipoRol;
                    if(dataGeneral.TipoRol == null)
                    {
                        Session["NoEmp"] = null;
                        ViewData["invalidPass"] = "El tipo de usuario no está configurado, por favor verificarlo con sistemas.";
                    } else
                    {
                        Session["TypeRolMenuSup"] = dataGeneral.TipoRol;
                        Session["idSolicitud"] = null;
                    }
                }
                else
                {
                    ViewData["invalidPass"] = "Contraseña inválida";
                }

                //Session["NoEmp"] = fc["user"];
                //Session["Notificaciones"] = "15";

                //Session["NombreEmpleado"] = "Juan Pérez";
                //Session["AnioIngreso"] = "2017";

                ////variable de sesion para indicar el tipo de rol
                ////1 = Admin,2 = Ma    nager, 3 = Estudiante, 4 = Empleado
                //Session["TypeRol"] = 1;
                //Session["TypeRolMenuSup"] = 1;
            } catch (Exception ex)
            {
                throw ex;
            }

            if (isValidUser)
            {
                if (Convert.ToInt32(Session["TypeRol"]) == 1 || Convert.ToInt32(Session["TypeRol"]) == 2)
                {
                    return RedirectToAction("AdminSolicitudesPermisos", "Permisos");
                }
                else
                {
                    return RedirectToAction("ListaPermisos", "Permisos");
                }

            } else {
                return View();
            }
            //return RedirectToAction("test", "Home");
        }

        
    }
}