﻿using Aplics.Servicios.Modelos.Generales;
using Aplics.Servicios.Modelos.Horarios;
using Aplics.Servicios.Negocio;
using Aplics.Servicios.Negocio.Horarios;
using GRC.Servicios.Utilidades;
using GRC.Servicios.Utilidades.Genericos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Aplics.Servicios.Modelos.Nominix;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Negocio.Nominix;
using Aplics.Servicios.Entidades.DAO.Nominix;

namespace Aplics.Servicios.Horarios.Controllers
{
     public class HorariosController : Controller
	{
		int _UserSign = 0;
		public HorariosController()
        {
		}

		// GET: Permisos
		public ActionResult Index()
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			_UserSign = (int)Session["UserSign"];
			return View();
		}

		public ActionResult HorariosStatus(string flagNotificaSolicitudPendiente = null)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				ViewBag.NotificacionSolicitudPendiente = flagNotificaSolicitudPendiente;
				ViewBag.Perfil = "EMP";
				List<EmpleadoEstudiante> ListaSolicitudesEmpleado = SolicitudesEstudiantesXStatus("T");
				ListaSolicitudesEmpleado = ListaSolicitudesEmpleado.Where(x => x.U_EMP.Equals(this.Session["NoEmp"].ToString())).ToList();
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleado);
				ViewBag.HorariosEmpleado = new List<VMHorarioLista>();

				ViewBag.Estatus = "P";
				if (ListaSolicitudesEmpleado.Count > 0)
				{
					ListaSolicitudesEmpleado = ListaSolicitudesEmpleado.OrderByDescending(x => x.Id).ToList();
					int id = ListaSolicitudesEmpleado[0].Id;
					empleado = BuscaEmpleadoAsignacionXId(id);
					obtieneConstancia(empleado.U_EMP, id);
					ViewBag.HorariosEmpleado = horariosEmp(id, 0);

				}
		    }
			catch (Exception ex)
			{
				throw ex;
			}

			//return View("HorariosStatus", empleado);
			return View("HorariosStatus2", empleado);
		}

		private List<ResumenSolicitud> ListaSolicitudes(List<EmpleadoEstudiante> ListaSolicitudesEmpleado)
		{
			List<ResumenSolicitud> Solicitudes = new List<ResumenSolicitud>();
			try
			{
				if (ListaSolicitudesEmpleado != null)
				{
					foreach (EmpleadoEstudiante solempleado in ListaSolicitudesEmpleado)
					{
						Solicitudes.Add(new ResumenSolicitud() { Id = solempleado.Id, Fecha = solempleado.FechaSolicitud, TipoSolicitud = "Horario", ClaveTipoSolicitud = "H" });
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return Solicitudes.OrderByDescending(x => x.Id).ToList();
		}

		public ActionResult BusqSolicitudesHorarios()
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				ViewBag.Titulo = "Buscar Solicitudes de Horarios";
				ViewBag.Perfil = "Admin";
				List<EmpleadoEstudiante> ListaSolicitudesEmpleado = SolicitudesEstudiantesXStatus("AR").OrderByDescending(y => y.Id).ToList();
				List<VMHorarioLista> HorariosEmpleado = new List<VMHorarioLista>();
				if (ListaSolicitudesEmpleado.Count() > 0)
				{
					int idSol = ListaSolicitudesEmpleado[0].Id;
					empleado = BuscaEmpleadoAsignacionXId(idSol);
					HorariosEmpleado = horariosEmp(idSol, 0);
					obtieneConstancia(empleado.U_EMP, idSol);
				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleado);
				ViewBag.HorariosEmpleado = HorariosEmpleado;
				List<EmpleadoEstudiante> HistorialEmpleadoSeleccionado = SolicitudesHistoricoEstudiantesXEmp(empleado.U_EMP);
				ViewBag.HistorialSeleccionado = HistorialEmpleadoSeleccionado;
				ViewBag.EmpleadoSeleccionado = empleado.U_EMP;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesHorarios", empleado);
		}

		public ActionResult BusqSolicitudesHorariosEstxId(int Id)
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				empleado = busqSolHorariosEst(Id);

			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesHorarios", empleado);
		}

		public ActionResult BusqSolicitudesHorariosEst()
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				empleado = busqSolHorariosEst();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesHorarios", empleado);
		}

		private EmpleadoEstudiante busqSolHorariosEst(int id = 0)
		{
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();

			try
			{
				ViewBag.Titulo = "Notificaciones de Horarios";
				ViewBag.Perfil = "Estudiante";
				List<EmpleadoEstudiante> ListaSolicitudesEmpleado = SolicitudesEstudiantesXStatus("AR");
				ListaSolicitudesEmpleado = ListaSolicitudesEmpleado.Where(y => y.U_EMP == this.Session["NoEmp"].ToString()).OrderByDescending(z => z.Id).ToList();
				List<VMHorarioLista> horariosEmpleado = new List<VMHorarioLista>();
				if (ListaSolicitudesEmpleado.Count() > 0)
				{
					int index = 0;
					if (id > 0)
					{
						index = ListaSolicitudesEmpleado.FindIndex(z => z.Id == id);
					}
					if (index >= 0)
					{
						int idsol = ListaSolicitudesEmpleado[index].Id;
						empleado = BuscaEmpleadoAsignacionXId(idsol);
						horariosEmpleado = horariosEmp(idsol, 0);
						obtieneConstancia(empleado.U_EMP, idsol);
					}

				}
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleado);
				ViewBag.HorariosEmpleado = horariosEmpleado;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleado;
		}

		[HttpGet]
		public ActionResult HorariosAdminStatusId(int Id)
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				empleado = AdminSolicitud(Id);

			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesHorarios", empleado);
		}

		public ActionResult HorariosAdminStatus()
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				empleado = AdminSolicitud();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesHorarios", empleado);
		}

		private EmpleadoEstudiante AdminSolicitud(int Id = 0)
		{
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				ViewBag.Perfil = "Admin";
				List<EmpleadoEstudiante> ListaSolicitudesEmpleados = SolicitudesEstudiantesXStatus("P");
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleados);
				if (ListaSolicitudesEmpleados.Count() > 0)
				{
					ListaSolicitudesEmpleados = ListaSolicitudesEmpleados.OrderByDescending(x => x.Id).ToList();
					int idsol = Id;
					if (idsol == 0)
					{
						idsol = ListaSolicitudesEmpleados[0].Id;
					}
					empleado = BuscaEmpleadoAsignacionXId(idsol);
					ViewBag.HorariosEmpleado = horariosEmp(idsol, 0);
					obtieneConstancia(empleado.U_EMP, idsol);
					List<EmpleadoEstudiante> HistorialEmpleadoSeleccionado = SolicitudesHistoricoEstudiantesXEmp(empleado.U_EMP);
					ViewBag.HistorialSeleccionado = HistorialEmpleadoSeleccionado;
					ViewBag.EmpleadoSeleccionado = empleado.U_EMP;
				}
				ViewBag.Titulo = "Administrador de Horarios";
				ViewBag.Status = "P";
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleado;
		}

		[HttpPost]
		public ActionResult FiltroAnio(int Anio, String Status)
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				ViewBag.Perfil = "Admin";
				List<EmpleadoEstudiante> ListaSolicitudesEmpleados = SolicitudesEstudiantesXStatus(Status);
				ListaSolicitudesEmpleados = ListaSolicitudesEmpleados.Where(x => x.U_FINI.Year == Anio || x.U_FFIN.Year == Anio).ToList();
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleados);
				List<VMHorarioLista> HorariosEmpleado = new List<VMHorarioLista>();
				if (ListaSolicitudesEmpleados.Count() > 0)
				{
					int id = ListaSolicitudesEmpleados[0].Id;
					empleado = BuscaEmpleadoAsignacionXId(id);
					HorariosEmpleado = obtenerHorariosLista(id, 0);
					ViewBag.HorariosEmpleado = horariosEmp(id, 0);
					obtieneConstancia(empleado.U_EMP, id);
				}
				ViewBag.HorariosEmpleado = HorariosEmpleado;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesHorarios", empleado);
		}

		[HttpPost]
		public ActionResult FiltroEmpleado(String Empleado, String Status)
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				ViewBag.Perfil = "Admin";
				List<EmpleadoEstudiante> ListaSolicitudesEmpleados = SolicitudesEstudiantesXStatus(Status);
				ListaSolicitudesEmpleados = ListaSolicitudesEmpleados.Where(x => x.U_EMP.Equals(Empleado)).ToList();
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleados);
				List<VMHorarioLista> HorariosEmpleado = new List<VMHorarioLista>();
				if (ListaSolicitudesEmpleados.Count() > 0)
				{
					int id = ListaSolicitudesEmpleados[0].Id;
					empleado = BuscaEmpleadoAsignacionXId(id);
					HorariosEmpleado = obtenerHorariosLista(id, 0);
					ViewBag.HorariosEmpleado = horariosEmp(id, 0);
					obtieneConstancia(empleado.U_EMP, id);
				}
				ViewBag.HorariosEmpleado = HorariosEmpleado;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("SolicitudesHorarios", empleado);
		}

		[HttpPost]
		public ActionResult BuscarSolicitudxId(String filtroPerfil, int id, String Status)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			EmpleadoEstudiante empleado = buscarSolHorario(filtroPerfil,id, Status);
			return PartialView("~/Views/Shared/_solicitudHorario.cshtml", empleado);
		}

        private EmpleadoEstudiante buscarSolHorario(String filtroPerfil, int id, String Status)
        {
            EmpleadoEstudiante empleado = new EmpleadoEstudiante();
            try
            {
                ViewBag.Perfil = filtroPerfil;
                empleado = BuscaEmpleadoAsignacionXId(id);

                List<EmpleadoEstudiante> ListaSolicitudesEmpleado = SolicitudesEstudiantesXStatus(Status);
                if (filtroPerfil == "EMP")
                {
                    ListaSolicitudesEmpleado = ListaSolicitudesEmpleado.Where(x => x.U_EMP.Equals(this.Session["NoEmp"].ToString())).ToList();
                }
                ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleado);
                List<VMHorarioLista> HorariosEmpleado = obtenerHorariosLista(id, 0);
                ViewBag.HorariosEmpleado = horariosEmp(id, 0);
                obtieneConstancia(empleado.U_EMP, id);
                List<EmpleadoEstudiante> HistorialEmpleadoSeleccionado = SolicitudesHistoricoEstudiantesXEmp(empleado.U_EMP);
                ViewBag.HistorialSeleccionado = HistorialEmpleadoSeleccionado;
                ViewBag.EmpleadoSeleccionado = empleado.U_EMP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return empleado;
        }

        public ActionResult GetSolicitudxId(int id, String Status)
        {
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            String filtroPerfil = "Admin";
            EmpleadoEstudiante empleado = buscarSolHorario(filtroPerfil, id, Status);
            return View("HorarioHist", empleado);
        }

        public List<EmpleadoEstudiante> SolicitudesHistoricoEstudiantesXEmp(string emp)
		{
			List<EmpleadoEstudiante> Solicitudes = new List<EmpleadoEstudiante>();
			try
			{
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.U_EMP == emp && (x.Status == "R" || x.Status == "A");
				Solicitudes = SolicitudesEstudiantesFiltro(parametros);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Solicitudes;
		}

		List<String> ActualizarStatusSolicitud(EmpleadoEstudiante empleadoEstudiante, Boolean aprobar, ref short numRechazos)
		{
			List<String> respuesta = new List<String>();
			ClaseAuditoria<EmpleadoEstudiante> peticion = new ClaseAuditoria<EmpleadoEstudiante>();
			RespuestaData<List<String>> respuestaData = new RespuestaData<List<String>>();
			try
			{
				peticion.Clase = empleadoEstudiante;
				peticion.UserSign = 1;
				peticion.UserName = "aplics";

				if (aprobar)
				{
					numRechazos = 0;
					respuestaData = AceptarAsignacion(peticion);
				}
				else
				{
					respuestaData = RechazarAsignacion(peticion, ref numRechazos);

				}
			}
			catch (Exception ex)
			{
				respuesta.Add(ex.Message.ToString());
				throw ex;
			}

			respuesta = respuestaData.Datos;
			return respuesta;
		}

		[HttpPost]
		public ActionResult RechazarHorario(String motivoRechazo, int id, String SolComentariosRec, List<HorarioTipo> horarios)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			String resultado = String.Empty;
			try
			{
				ViewBag.Perfil = "Admin";
				empleado = BuscaEmpleadoAsignacionXId(id);
				List<EmpleadoEstudiante> ListaSolicitudesEmpleado = SolicitudesEstudiantesXStatus("P");
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleado);
				ViewBag.HorariosEmpleado = horariosEmp(id, 0);
				obtieneConstancia(empleado.U_EMP, id);
				short numrec = 0;
				RespuestaSimple resp = new RespuestaSimple();
				resp.result = 1;
				if (horarios != null)
				{
					resp = agregaEmpleadoHorario(id, horarios);
				}
				if (resp.result == 1)
				{
					empleado.motivoRechazo = motivoRechazo;
					List<String> Mensajes = ActualizarStatusSolicitud(empleado, false, ref numrec);
					empleado.U_NUMREC = numrec;
					if (Mensajes.Count() > 0)
					{
						resultado = string.Join(",", Mensajes);
					}
				}
				else
				{
					resultado = resp.mensaje;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Content(resultado);
		}

		[HttpPost]
		public ActionResult AprobarHorario(int id, String SolComentariosRec, List<HorarioTipo> horarios)
		{

			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}

			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			String resultado = String.Empty;
			try
			{
				ViewBag.Perfil = "Admin";
				empleado = BuscaEmpleadoAsignacionXId(id);
				empleado.Observaciones = SolComentariosRec;
				List<EmpleadoEstudiante> ListaSolicitudesEmpleado = SolicitudesEstudiantesXStatus("P");
				ViewBag.ListaSolicitudes = ListaSolicitudes(ListaSolicitudesEmpleado);
				ViewBag.HorariosEmpleado = horariosEmp(id, 0);
				obtieneConstancia(empleado.U_EMP, id);
				short numrec = 0;
                byte Escolar = 1;
                List<HorarioTipo> horariosAdmin = horarios.Where(x => x.TipoHorario>Escolar).OrderBy(x => x.TipoHorario).ThenBy(y => y.dia).ThenBy(z => z.HoraInicial).ToList();
                RespuestaSimple resp = agregaEmpleadoHorario(id, horariosAdmin);
				if (resp.result == 1)
				{
					List<String> Mensajes = ActualizarStatusSolicitud(empleado, true, ref numrec);

					if (Mensajes.Count() > 0)
					{
						resultado = string.Join(",", Mensajes);
					}
				}
				else
				{
					resultado = resp.mensaje;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Content(resultado);
		}


		public ActionResult ControHorarios()

		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			EmpleadoEstudiante empleadoEstudiante = new EmpleadoEstudiante();
			try
			{
				empleadoEstudiante = ultimaSolicitudEmpleado();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View(empleadoEstudiante);
		}

		EmpleadoEstudiante ultimaSolicitudEmpleado()
		{
			EmpleadoEstudiante empleadoEstudiante = new EmpleadoEstudiante();
			try
			{
				List<EmpleadoEstudiante> SolicitudesEmpleado = SolicitudesEstudiantesXEmpleado(this.Session["NoEmp"].ToString());
				if (SolicitudesEmpleado.Count() > 0)
				{
					SolicitudesEmpleado = SolicitudesEmpleado.Where(x => x.Status == "I").OrderByDescending(x => x.Id).ToList();
				}
				if (SolicitudesEmpleado.Count() > 0)
				{
					empleadoEstudiante = SolicitudesEmpleado[0];
					short periodo = CatalogosSolicitud(false);
				}
				else
				{
					empleadoEstudiante.Id = 0;
					empleadoEstudiante.U_EMP = this.Session["NoEmp"].ToString();
					empleadoEstudiante.U_CVEUNI = String.Empty;
					short periodo = CatalogosSolicitud(true, true);
					empleadoEstudiante.U_PER = periodo;
				}
				empleadoEstudiante.nombre = this.Session["NombreEmpleado"].ToString();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleadoEstudiante;
		}

		[HttpPost]
		public ActionResult EliminarSolHorario(int IdSol)
		{
			EmpleadoEstudiante empleadoEstudiante = new EmpleadoEstudiante();
			try
			{
				String result = borrarSolicitud(IdSol);
				if (!String.IsNullOrEmpty(result))
				{
					ViewBag.Error = result;
				}
				eliminarArchivoConstancia(this.Session["NoEmp"].ToString(), IdSol);
				empleadoEstudiante = ultimaSolicitudEmpleado();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("ControHorarios", empleadoEstudiante);
		}

		private short CatalogosSolicitud(Boolean alta, Boolean descartarRechazados = false)
		{
			short periodo = 0;
			try
			{
				ClaseAuditoria<String> peticion = new ClaseAuditoria<String>();
				peticion.UserSign = 1;
				peticion.UserName = this.Session["NoEmp"].ToString();
				peticion.Clase = this.Session["NoEmp"].ToString();
				periodo = periodosEmpleadoEstudiante(peticion, descartarRechazados);
				ViewBag.Perfil = "EMP";
				peticion.Clase = "";
				List<CatalogoGeneral> Periodos = new List<CatalogoGeneral>();
				Periodos.Add(new CatalogoGeneral() { Id = "", Descripcion = "Seleccione el Período" });
				Periodos.Add(new CatalogoGeneral() { Id = periodo.ToString(), Descripcion = periodo.ToString() });
				ViewBag.Periodos = Periodos;
				//List<CatalogoGeneral> Universidades = new List<CatalogoGeneral>();
				//Universidades = CatalogodeUniversidades(peticion).Datos;
				//Universidades.Add(new CatalogoGeneral() { Id = "", Descripcion = "Seleccione la Universidad" });
				//ViewBag.CatalogoUniversidades = Universidades;

				List<CatalogoGeneral> listaUniversidades = NominixGenerales.GetCatalogoUniversidades();
				ViewBag.CatalogoUniversidades = listaUniversidades;
				
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return periodo;
		}

		[HttpPost]
		public ActionResult Documento(FormCollection fc)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			try
			{
				EmpleadoEstudiante empEst = new EmpleadoEstudiante();
				empEst.Id = Convert.ToInt16(fc["Id"]);
				empEst.nombre = fc["nombre"];
				empEst.motivoRechazo = fc["mot"];
				empEst.U_ANIO = Convert.ToInt16(fc["anio"]);
				empEst.U_CVEUNI = fc["univer"];
				empEst.U_DUR = Convert.ToInt16(fc["dur"]);
				empEst.U_EMP = fc["noEmp"];
				empEst.U_FFIN = Convert.ToDateTime(fc["ffin"]);
				empEst.U_FINI = Convert.ToDateTime(fc["fini"]);
				empEst.U_PER = Convert.ToInt16(fc["per"]);
				empEst.Status = "I";
				ClaseAuditoria<EmpleadoEstudiante> CA = new ClaseAuditoria<EmpleadoEstudiante>();
				CA.Clase = empEst;
				CA.UserName = this.Session["NoEmp"].ToString();
				CA.UserSign = 1;
				RespuestaData<EmpleadoEstudiante> oRespuesta = new RespuestaData<EmpleadoEstudiante>();
				int solid = empEst.Id;
				if (empEst.Id > 0)
				{
					ViewBag.DocEntry = empEst.Id;
					ActualizarAsignacion(CA);
				}
				else
				{
					oRespuesta = AgregarAsignacion(CA, true);
					if (oRespuesta.Respuesta.result != 1)
					{
						ViewBag.Error = oRespuesta.Respuesta.mensaje;
					}
					else
					{
						ViewBag.DocEntry = oRespuesta.Datos.Id;
						solid = oRespuesta.Datos.Id;
					}
				}
				//inicio
				Empleados empleadoObj = new Empleados();
				empleadoObj = NominixGenerales.ObtenerEmpleado(Session["NoEmp"].ToString(), 0);
				empleadoObj.U_UNIV = empEst.U_CVEUNI;
				NominixGenerales.GuardarEmpleado(empleadoObj, 0);
				//fin
				obtieneConstancia(empEst.U_EMP, solid);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View();
		}

		[HttpPost]
		public ActionResult DocTransfer(string FolderKey, String keyProcess)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			String Errors = "";
			try
			{
				foreach (string file in Request.Files)
				{
					var fileContent = Request.Files[file];
					if (fileContent != null && fileContent.ContentLength > 0)
					{
						var fileName = fileContent.FileName;
						if (!String.IsNullOrEmpty(keyProcess))
						{
							fileName = keyProcess + "-" + fileName;
						}
						var ext = Path.GetExtension(fileName).ToLower();
						fileName = Path.GetFileName(fileName);
						if (fileContent.ContentType.Equals("application/octet-stream") || ext.Equals(".exe") || ext.Equals(".dll"))
						{
							Errors += "- File type wrong ";
						}
						else
						{
							var rootPath = Server.MapPath("~/EmpFiles/Constancias/");
							var docPath = rootPath + "\\" + FolderKey;
							if (CheckFoldesStruct(rootPath, docPath))
							{
								// get a stream
								var stream = fileContent.InputStream;
								var path = Path.Combine(docPath, fileName);
								using (var fileStream = System.IO.File.Create(path))
								{
									stream.CopyTo(fileStream);
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json("Upload failed - " + ex.Message.ToString());
				throw ex;
			}
			if (!Errors.Equals(""))
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(Errors);
			}
			else
			{
				return Json("File uploaded successfully");
			}
		}

        [HttpPost]
        public ActionResult UpLoadConstancia(int SolId, HttpPostedFileBase files)
        {
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ViewBag.DocEntry = SolId;
                // Verify that the user selected a file
                if (files != null && files.ContentLength > 0)
                {
                    string FolderKey = Session["NoEmp"].ToString();
                    var rootPath = Server.MapPath("~/EmpFiles/Constancias/");
                    if (files.ContentType.Equals("application/octet-stream"))
                    {
                        throw new Exception("Formato Incorrecto.");
                    }
                    else
                    {
                        // extract only the filename
                        var fileName = Path.GetFileName(files.FileName);
                        // store the file inside ~/App_Data/uploads folder
                        var docPath = rootPath + "\\" + FolderKey;
                        if (CheckFoldesStruct(rootPath, docPath))
                        {
                            fileName = SolId.ToString() + "-" + fileName;
                            var path = Path.Combine(docPath, fileName);
                            files.SaveAs(path);
                            obtieneConstancia(FolderKey, SolId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View("Documento");
        }

        [HttpPost]
		public ActionResult VisorConstanciasPDF(String claveEmpleado, int Id)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			String mensaje = String.Empty;
			try
			{
				ViewBag.DocEntry = Id;
				if (Session["NoEmp"] == null)
				{
					return RedirectToAction("Index", "Home");
				}
				obtieneConstancia(claveEmpleado, Id);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			ViewBag.Error = mensaje;
			return View("Documento");
		}

		private void eliminarArchivoConstancia(String claveEmpleado, int Id)
		{
			List<TipoConstancia> constancias = new List<TipoConstancia>();
			try
			{
				constancias = getConstancias(claveEmpleado, Id);
				if (constancias.Count() > 0)
				{
					var rootPath = Server.MapPath("~/");
					var docPath = rootPath + "\\EmpFiles\\Constancias\\" + claveEmpleado + "\\";
					System.IO.File.Delete(docPath + constancias[0].archivo);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		[HttpPost]
		public ActionResult EliminarConstancia(String claveEmpleado, int Id)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			String mensaje = String.Empty;
			try
			{
				ViewBag.DocEntry = Id;
				if (Session["NoEmp"] == null)
				{
					return RedirectToAction("Index", "Home");
				}
				eliminarArchivoConstancia(claveEmpleado, Id);
			}
			catch (Exception ex)
			{
				mensaje = ex.Message.ToString();
				throw ex;
			}
			ViewBag.Error = mensaje;
			return View("Documento");
		}

		private void obtieneConstancia(string claveEmpleado, int idSolicitud = 0)
		{
			List<TipoConstancia> constancias = new List<TipoConstancia>();
			try
			{
				constancias = getConstancias(claveEmpleado, idSolicitud);
				if (constancias.Count() > 0)
				{
					string embed = "<object data='{0}' type='application/pdf' id='constancia-horario'>";
					embed += "<a href = '{0}'>" + constancias[0].archivo + "</a>";
					embed += "</object>";
					String sEmbed = string.Format(embed, VirtualPathUtility.ToAbsolute("~/" + constancias[0].url));
					TempData["Embed"] = sEmbed;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private List<TipoConstancia> getConstancias(String emp, int idSolicitud = 0)
		{
			List<TipoConstancia> tc = new List<TipoConstancia>();
			try
			{
				var rootPath = Server.MapPath("~/");
				var docPath = rootPath + "\\EmpFiles\\Constancias\\" + emp + "\\";
				List<String> Files = new List<String>();
				if (System.IO.Directory.Exists(docPath))
				{
					Files = System.IO.Directory.GetFiles(docPath).ToList();
					string relativePath = String.Empty;
					Boolean esimagen = false;
					String archivo = String.Empty;
					String[] AVerificaSolicitud;
					Boolean agregar = true;
					foreach (String sfile in Files)
					{
						archivo = sfile.Replace(docPath, "");
						agregar = true;
						if (idSolicitud > 0)
						{
							agregar = false;
							if (archivo.IndexOf("-") > 0)
							{
								AVerificaSolicitud = archivo.Split('-');
								if (AVerificaSolicitud[0].Equals(idSolicitud.ToString())) { agregar = true; }
							}
						}
						if (agregar)
						{
							if (sfile.ToLower().IndexOf(".jpg") > 0 || sfile.ToLower().IndexOf(".jpeg") > 0 || sfile.ToLower().IndexOf(".bmp") > 0 || sfile.ToLower().IndexOf(".png") > 0 || sfile.ToLower().IndexOf(".tiff") > 0)
							{
								esimagen = true;
							}
							relativePath = "EmpFiles/Constancias/" + emp + "/" + archivo;
							tc.Add(new TipoConstancia(relativePath, esimagen, archivo));
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return tc;
		}


		Boolean CheckFoldesStruct(String ruta1, String ruta2)
		{
			Boolean ret = false;
			try
			{
				if (!Directory.Exists(ruta1))
				{
					Directory.CreateDirectory(ruta1);
				}
				if (!ruta2.Equals(""))
				{
					if (!Directory.Exists(ruta2))
					{
						Directory.CreateDirectory(ruta2);
					}
				}
				ret = true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return ret;
		}

		private List<VMHorarioLista> horariosEmp(int Id, Byte tipoHorario)
		{
			List<VMHorarioLista> HorariosEmpleado = new List<VMHorarioLista>();
			List<VMHorarioLista> horarioLista = new List<VMHorarioLista>();
			try
			{
				HorariosEmpleado = obtenerHorariosLista(Id, tipoHorario);
				if (HorariosEmpleado.Count > 0)
				{
					horarioLista = HorariosEmpleado;
				}
				else
				{
					if (tipoHorario != 0)
					{
						VMHorarioLista vMHorarioLista = new VMHorarioLista();
						vMHorarioLista.Id = Id;
						vMHorarioLista.Tipo = tipoHorario;
						List<Horario> horario = new List<Horario>();
						vMHorarioLista.Horarios = new List<Horario>();
						for (byte idia = 1; idia <= 5; idia++)
						{
							horario.Add(new Horario() { dia = idia, HoraInicial = String.Empty, HoraFinal = String.Empty });
						}
						vMHorarioLista.Horarios = horario;
						horarioLista.Add(vMHorarioLista);
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return horarioLista;
		}

		[HttpGet]
		public ActionResult Horarios(int Id)
		{

            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			try
			{
				ViewBag.DocEntry = Id;
				Byte tipoHorario = 1; //Escolar
				ViewBag.TipoHorario = tipoHorario;
				ViewBag.Perfil = "EMP";
				List<VMHorarioLista> horarioLista = horariosEmp(Id, tipoHorario);
				ViewBag.Horarios = horarioLista[0];
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return View();
		}


		[HttpPost]
		public ActionResult EditaSolicitud(int id)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			RespuestaSimple respuesta = new RespuestaSimple();
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				empleado = BuscaEmpleadoAsignacionXId(Session["NoEmp"].ToString(), id);
				empleado.nombre = this.Session["NombreEmpleado"].ToString();
				short periodo = CatalogosSolicitud(false);
				ViewBag.Consulta = false;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return View("ControHorarios", empleado);
		}

		[HttpPost]
		public ActionResult VerificaEmpleado(VMHorarioLista horarioEscolar)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }

			RespuestaSimple respuesta = new RespuestaSimple();
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				Byte tipoHorario = 1; //Escolar
				ViewBag.TipoHorario = tipoHorario;
				horarioEscolar.Tipo = tipoHorario;
				horarioEscolar.Horarios = horarioEscolar.Horarios.Where(x => x.HoraInicial != null).OrderBy(y => y.dia).ThenBy(z => z.HoraInicial).ToList();
				respuesta = agregaEmpleadoHorario(horarioEscolar);
				empleado = BuscaEmpleadoAsignacionXId(Session["NoEmp"].ToString(), horarioEscolar.Id);
				short periodo = CatalogosSolicitud(false);
				ViewBag.Horarios = horarioEscolar;
				obtieneConstancia(Session["NoEmp"].ToString(), horarioEscolar.Id);
				ViewBag.Perfil = "EMP";
				ViewBag.DocEntry = horarioEscolar.Id;
				ViewBag.ModoConsulta = true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			if (String.IsNullOrEmpty(empleado.nombre))
			{
				empleado.nombre = Session["NombreEmpleado"].ToString();
			}
			return View("ConfirmacionEmpleado", empleado);
		}


		[HttpPost]
		public ActionResult NotificarRH(int id)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			RespuestaSimple respuesta = new RespuestaSimple();
			String pMensaje = "Recibirás una notificación cuando RH revise tu solicitud.";
			String presultadoEnvio = "¡Tu solicitud ha sido enviada!";
			Boolean pError = false;
			Byte tipoHorario = 1;
			ViewBag.TipoHorario = tipoHorario;
			ViewBag.DocEntry = id;
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			try
			{
				VMHorarioLista horarioEscolar = new VMHorarioLista();
				String codEmpleado = Session["NoEmp"].ToString();
				empleado = BuscaEmpleadoAsignacionXId(codEmpleado, id);
				short periodo = CatalogosSolicitud(false);
				horarioEscolar = obtenerHorariosLista(id, tipoHorario)[0];
				ViewBag.Horarios = horarioEscolar;
				ViewBag.ModoConsulta = true;
				obtieneConstancia(codEmpleado, id);
				
				respuesta = notificarHorarioCargado(codEmpleado, id);
				if (respuesta.result != 1)
				{
					pMensaje = "Verifica tu conexión e inténtalo nuevamente error:" + respuesta.mensaje;
					presultadoEnvio = "La solicitud no se ha enviado";
					pError = true;
				}
			}
			catch (Exception ex)
			{
				pMensaje = "Verifica tu conexión e inténtalo nuevamente error:" + ViewBag.Error;
				presultadoEnvio = "La solicitud no se ha enviado";
				pError = true;
				throw ex;
			}
			ViewBag.NotificacionHorario = new NotificacionSolicitud() { Id = id, Mensaje = pMensaje, resultadoEnvio = presultadoEnvio, Error = pError, url = String.Empty, controlador = "Horarios", accion = "HorariosStatus" };
			return View("ConfirmacionEmpleado", empleado);
		}

		[HttpPost]
		public ActionResult Finall(FormCollection fc)
		{
            if (Session["TypeRol"] == null && Session["NoEmp"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
			try
			{
				List<String> dias = new List<string>();
				List<String> hhi = new List<string>();
				List<String> hhf = new List<string>();
				dias = fc["d"].Split(',').ToList();
				hhi = fc["s"].Split(',').ToList();
				hhf = fc["d"].Split(',').ToList();
				int i = dias.Count;
				for (int y = 0; y < i; y++)
				{
					MVHorario h = new MVHorario();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return View();
		}

		public void ListarEmpleados()
		{
			RespuestaData<List<EmpleadoHorario>> oRespuesta = new RespuestaData<List<EmpleadoHorario>>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				oRespuesta = negocio.ListaEmpleadosActivos();
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
		}

		public void ListarEmpleadosSuperior(String codSuperior)
		{
			RespuestaData<List<EmpleadoHorario>> oRespuesta = new RespuestaData<List<EmpleadoHorario>>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				oRespuesta = negocio.ListaEmpleadosXSuperior(codSuperior);
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
		}

		private List<VMHorarioLista> obtenerHorariosLista(int Id, Byte TipoHorario)
		{
			List<VMHorarioLista> Horarios = new List<VMHorarioLista>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				Horarios = negocio.obtenerHorariosLista(Id, TipoHorario);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Horarios;
		}

		public EmpleadoEstudiante BuscaEmpleadoAsignacionXId(int Id)
		{
			EmpleadoEstudiante empleadoEstudiante = new EmpleadoEstudiante();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				empleadoEstudiante = negocio.buscarDatosAsignacionXId(Id);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleadoEstudiante;
		}

		public EmpleadoEstudiante BuscaEmpleadoAsignacionXId(String codEmpleado, int Id)
		{
			EmpleadoEstudiante empleadoEstudiante = new EmpleadoEstudiante();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				empleadoEstudiante = negocio.buscarDatosAsignacion(codEmpleado, Id);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleadoEstudiante;
		}

		public EmpleadoEstudiante BuscaEmpleadoAsignacion(String codEmpleado, short periodo)
		{
			EmpleadoEstudiante empleadoEstudiante = new EmpleadoEstudiante();
			RespuestaData<EmpleadoEstudiante> oRespuesta = new RespuestaData<EmpleadoEstudiante>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				oRespuesta = negocio.buscarEmpleadoAsignacion(codEmpleado, periodo);
				empleadoEstudiante = oRespuesta.Datos;
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return empleadoEstudiante;
		}


		public String borrarSolicitud(int Id)
		{
			String result = String.Empty;
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				Boolean Admin = true;
				result = negocio.borrarEmpleadoHorariosLista(Id, Admin);
				if (String.IsNullOrEmpty(result))
				{
					result = negocio.borrarSolicitudHorario(Id);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return result;
		}

		public RespuestaSimple agregaEmpleadoHorario(int Id, List<HorarioTipo> Horarios)
		{
			RespuestaSimple oRespuesta = new RespuestaSimple();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				Boolean Admin = true;
				String borrarHorario = negocio.borrarEmpleadoHorariosLista(Id, Admin);
				if (String.IsNullOrEmpty(borrarHorario))
				{
					oRespuesta = negocio.agregaEmpleadoHorario(Id, Horarios);
				}
			}
			catch (Exception ex)
			{
				oRespuesta.result = 0;
				oRespuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta;
		}

		public RespuestaSimple agregaEmpleadoHorario(VMHorarioLista Horario)
		{
			RespuestaSimple oRespuesta = new RespuestaSimple();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				String borrarHorario = negocio.borrarEmpleadoHorariosLista(Horario.Id);
				if (String.IsNullOrEmpty(borrarHorario))
				{
					oRespuesta = negocio.agregaEmpleadoHorario(Horario);
				}
			}
			catch (Exception ex)
			{
				oRespuesta.result = 0;
				oRespuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta;
		}

		public void actualizaEmpleadoHorario(VMHorarioLista Horario)
		{
			RespuestaSimple oRespuesta = new RespuestaSimple();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				oRespuesta = negocio.agregaEmpleadoHorario(Horario);
			}
			catch (Exception ex)
			{
				oRespuesta.result = 0;
				oRespuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
		}


		public short periodosEmpleadoEstudiante(ClaseAuditoria<String> peticion, Boolean descartarRechazados = false)
		{
			short ultimoPeriodo = 0;
			RespuestaData<List<short>> oRespuesta = new RespuestaData<List<short>>();
			oRespuesta.Respuesta.result = 0;
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				List<short> periodos = negocio.periodosEmpleadoEstudiante(peticion.Clase, descartarRechazados);
				oRespuesta.Respuesta.result = 1;
				oRespuesta.Datos = periodos;
				if (periodos.Count() > 0)
				{

					ultimoPeriodo = periodos[periodos.Count() - 1];
				}
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return ultimoPeriodo;
		}

		public RespuestaData<EmpleadoEstudiante> AgregarAsignacion(ClaseAuditoria<EmpleadoEstudiante> asignacion, Boolean descartarRechazados = false)
		{
			RespuestaData<EmpleadoEstudiante> oRespuesta = new RespuestaData<EmpleadoEstudiante>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				oRespuesta = negocio.agregaEmpleadoAsignacion(asignacion.Clase, descartarRechazados);
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta;
		}

		public void ActualizarAsignacion(ClaseAuditoria<EmpleadoEstudiante> asignacion)
		{
			RespuestaData<EmpleadoEstudiante> oRespuesta = new RespuestaData<EmpleadoEstudiante>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(asignacion.UserSign);
				oRespuesta = negocio.actualizarEmpleadoAsignacion(asignacion.Clase);
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
		}

		public RespuestaData<List<String>> RechazarAsignacion(ClaseAuditoria<EmpleadoEstudiante> asignacion, ref short numRechazos)
		{
			RespuestaData<List<String>> oRespuesta = new RespuestaData<List<String>>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(asignacion.UserSign);
				RespuestaData<EmpleadoEstudiante> remp = new RespuestaData<EmpleadoEstudiante>();

				EmpleadoEstudiante empAsig = asignacion.Clase;
				EmpleadoEstudiante empAsigDatos = negocio.buscarDatosAsignacion(empAsig.U_EMP, empAsig.Id);

				empAsig.U_Aprobado = false;
				empAsig.Status = "R";
				empAsig.U_NUMREC = empAsigDatos.U_NUMREC;
				empAsig.U_NUMREC += 1;
				numRechazos = empAsig.U_NUMREC;
				remp = negocio.actualizarEmpleadoAsignacion(empAsig);
				if (remp.Respuesta.result != 1)
				{
					oRespuesta.Datos.Add(remp.Respuesta.mensaje);
				}
				SendMails sm = new Aplics.Servicios.Models.EmailConfig().getConfigMail(true);
				String empleadoRH = ConfigurationManager.AppSettings["empleadoRH"];

				List<VMAutorizacionesHorarios> ListavMAutorizacionesHorarios = new List<VMAutorizacionesHorarios>();
				VMAutorizacionesHorarios vMAutorizacionesHorarios = new VMAutorizacionesHorarios() { Id = empAsig.Id, Autorizado = empAsig.U_Aprobado, codeEmpleado = empAsig.U_EMP, idEmpleado = 0, motivoRechazo = empAsig.motivoRechazo, numRechazos = empAsig.U_NUMREC, horarios = new List<MVHorario>() };
				ListavMAutorizacionesHorarios.Add(vMAutorizacionesHorarios);
				oRespuesta.Datos = negocio.notificarAutorizaciones(ListavMAutorizacionesHorarios, sm, empleadoRH, Convert.ToInt32(Session["TypeRolMenuSup"]));
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				oRespuesta.Datos.Add(ex.Message.ToString());
				throw ex;
			}
			return oRespuesta;
		}

		public void RechazarAsignacion(ClaseAuditoria<List<VMAutorizacionesHorarios>> asignaciones)
		{
			RespuestaData<List<String>> oRespuesta = new RespuestaData<List<String>>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(asignaciones.UserSign);
				RespuestaData<EmpleadoEstudiante> remp = new RespuestaData<EmpleadoEstudiante>();
				foreach (VMAutorizacionesHorarios vma in asignaciones.Clase)
				{
					EmpleadoEstudiante empAsig = negocio.buscarDatosAsignacion(vma.codeEmpleado, vma.horarios[0].Id);
					empAsig.U_Aprobado = false;
					empAsig.U_NUMREC += 1;
					empAsig.motivoRechazo = vma.motivoRechazo;
					remp = negocio.actualizarEmpleadoAsignacion(empAsig);
					if (remp.Respuesta.result != 1)
					{
						oRespuesta.Datos.Add(remp.Respuesta.mensaje);
					}
				}
				SendMails sm = new Aplics.Servicios.Models.EmailConfig().getConfigMail(true);
				String empleadoRH = ConfigurationManager.AppSettings["empleadoRH"];
				oRespuesta.Datos = negocio.notificarAutorizaciones(asignaciones.Clase, sm, empleadoRH, Convert.ToInt32(Session["TypeRolMenuSup"]));
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				oRespuesta.Datos.Add(ex.Message.ToString());
				throw ex;
			}
		}

		public void AceptarAsignacion(ClaseAuditoria<List<VMAutorizacionesHorarios>> asignaciones)
		{
			RespuestaData<List<String>> oRespuesta = new RespuestaData<List<String>>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(asignaciones.UserSign);
				RespuestaData<EmpleadoEstudiante> remp = new RespuestaData<EmpleadoEstudiante>();
				foreach (VMAutorizacionesHorarios vma in asignaciones.Clase)
				{
					EmpleadoEstudiante empAsig = negocio.buscarDatosAsignacion(vma.codeEmpleado, vma.horarios[0].Id);
					empAsig.U_Aprobado = true;
					empAsig.motivoRechazo = string.Empty;
					remp = negocio.actualizarEmpleadoAsignacion(empAsig);
					if (remp.Respuesta.result != 1)
					{
						oRespuesta.Datos.Add(remp.Respuesta.mensaje);
					}
				}
				SendMails sm = new Aplics.Servicios.Models.EmailConfig().getConfigMail(true);
				String empleadoRH = ConfigurationManager.AppSettings["empleadoRH"];
				oRespuesta.Datos = negocio.notificarAutorizaciones(asignaciones.Clase, sm, empleadoRH, Convert.ToInt32(Session["TypeRolMenuSup"]));
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				oRespuesta.Datos.Add(ex.Message.ToString());
				throw ex;
			}
		}

		public RespuestaData<List<String>> AceptarAsignacion(ClaseAuditoria<EmpleadoEstudiante> asignacion)
		{
			RespuestaData<List<String>> oRespuesta = new RespuestaData<List<String>>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(asignacion.UserSign);
				RespuestaData<EmpleadoEstudiante> remp = new RespuestaData<EmpleadoEstudiante>();
				EmpleadoEstudiante empAsig = asignacion.Clase;
				empAsig.U_Aprobado = true;
				empAsig.Status = "A";
				empAsig.motivoRechazo = string.Empty;
				remp = negocio.actualizarEmpleadoAsignacion(empAsig);
				if (remp.Respuesta.result != 1)
				{
					oRespuesta.Datos.Add(remp.Respuesta.mensaje);
				}
				SendMails sm = new Aplics.Servicios.Models.EmailConfig().getConfigMail(true);
				String empleadoRH = ConfigurationManager.AppSettings["empleadoRH"];

				List<VMAutorizacionesHorarios> ListavMAutorizacionesHorarios = new List<VMAutorizacionesHorarios>();
				VMAutorizacionesHorarios vMAutorizacionesHorarios = new VMAutorizacionesHorarios() { Id = empAsig.Id, Autorizado = true, codeEmpleado = empAsig.U_EMP, idEmpleado = 0, motivoRechazo = empAsig.motivoRechazo, numRechazos = 0, horarios = new List<MVHorario>() };
				ListavMAutorizacionesHorarios.Add(vMAutorizacionesHorarios);
				oRespuesta.Datos = negocio.notificarAutorizaciones(ListavMAutorizacionesHorarios, sm, empleadoRH, Convert.ToInt32(Session["TypeRolMenuSup"]));
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				oRespuesta.Datos.Add(ex.Message.ToString());
				throw ex;
			}
			return oRespuesta;
		}


		public RespuestaSimple notificarHorarioCargado(String codEmpleado, int solicitudId = 0)
		{
			RespuestaSimple oRespuesta = new RespuestaSimple();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				String correoRH = ConfigurationManager.AppSettings["emailRH"];
				String empleadoRH = ConfigurationManager.AppSettings["empleadoRH"];
				SendMails sm = new Aplics.Servicios.Models.EmailConfig().getConfigMail(true);
				oRespuesta = negocio.notificarHorarioCargado(codEmpleado, correoRH, sm, empleadoRH, Convert.ToInt32(Session["TypeRolMenuSup"]), solicitudId);
			}
			catch (Exception ex)
			{
				oRespuesta.result = 0;
				oRespuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta;
		}


		//public RespuestaData<List<CatalogoGeneral>> CatalogodeUniversidades(ClaseAuditoria<String> peticion)
		//{
		//	RespuestaData<List<CatalogoGeneral>> oRespuesta = new RespuestaData<List<CatalogoGeneral>>();
		//	oRespuesta.Respuesta.result = 0;
		//	try
		//	{
		//		NegocioHorarios negocio = new NegocioHorarios(_UserSign);
		//		oRespuesta.Datos = negocio.CatalogodeUniversidades();
		//		oRespuesta.Respuesta.result = 0;
		//	}
		//	catch (Exception ex)
		//	{
		//		oRespuesta.Respuesta.mensaje = ex.Message.ToString();
		//		throw ex;
		//	}
		//	return oRespuesta;
		//}

		public List<EmpleadoEstudiante> SolicitudesEstudiantesXStatus(String Status)
		{
			List<EmpleadoEstudiante> Solicitudes = new List<EmpleadoEstudiante>();
			QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
			try
			{
				if (Status == "T")
				{
					parametros.where = x => x.DocEntry > 0;
				}
				else if (Status == "AR")
				{
					parametros.where = x => x.Status == "A" || x.Status == "R";
				}
				else
				{
					parametros.where = x => x.Status == Status;
				}
				Solicitudes = SolicitudesEstudiantesFiltro(parametros);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Solicitudes;
		}


		public List<EmpleadoEstudiante> SolicitudesEstudiantesXAnio(int anio)
		{
			List<EmpleadoEstudiante> Solicitudes = new List<EmpleadoEstudiante>();
			try
			{
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.U_FINI.Year == anio || x.U_FFIN.Year == anio;
				Solicitudes = SolicitudesEstudiantesFiltro(parametros);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Solicitudes;
		}

		public List<EmpleadoEstudiante> SolicitudesEstudiantesXEmpleado(String Emp)
		{
			List<EmpleadoEstudiante> Solicitudes = new List<EmpleadoEstudiante>();
			try
			{
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.U_EMP.Equals(Emp);
				Solicitudes = SolicitudesEstudiantesFiltro(parametros);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Solicitudes;
		}

		public List<EmpleadoEstudiante> SolicitudesEstudiantesFiltro(QueryParameters<EmpleadoAsignacion> parametros)
		{
			List<EmpleadoEstudiante> Solicitudes = new List<EmpleadoEstudiante>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				Solicitudes = negocio.SolicitudesEstudiantesFiltro(parametros);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Solicitudes;
		}

		public RespuestaData<List<EmpleadoEstudiante>> SolicitudesEstudiantesEstatus(ClaseAuditoria<Boolean> peticion)
		{
			RespuestaData<List<EmpleadoEstudiante>> oRespuesta = new RespuestaData<List<EmpleadoEstudiante>>();
			oRespuesta.Respuesta.result = 0;
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				oRespuesta.Datos = negocio.SolicitudesEstudiantesEstatus(peticion.Clase);
				oRespuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta;
		}

		public RespuestaData<List<EmpleadoEstudiante>> SolicitudesEstudiantes(ClaseAuditoria<String> peticion)
		{
			RespuestaData<List<EmpleadoEstudiante>> oRespuesta = new RespuestaData<List<EmpleadoEstudiante>>();
			oRespuesta.Respuesta.result = 0;
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(_UserSign);
				oRespuesta.Datos = negocio.SolicitudesEstudiantes();
				oRespuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
			return oRespuesta;
		}

		public void ActualizarAsignaciones(ClaseAuditoria<List<VMAutorizacionesHorarios>> asignaciones)
		{
			RespuestaData<List<String>> oRespuesta = new RespuestaData<List<String>>();
			try
			{
				NegocioHorarios negocio = new NegocioHorarios(asignaciones.UserSign);
				oRespuesta = negocio.actualizarEmpleadoAsignaciones(asignaciones.Clase);
			}
			catch (Exception ex)
			{
				oRespuesta.Respuesta.result = 0;
				oRespuesta.Respuesta.mensaje = ex.Message.ToString();
				throw ex;
			}
		}

		public ActionResult HorariosTeam()
		{
			if (Session["TypeRol"] == null && Session["NoEmp"] == null)
			{
				return RedirectToAction("Index", "Home");
			}
			EmpleadoEstudiante empleado = new EmpleadoEstudiante();
			List<EmpleadoEstudiante> ListaSolicitudesTeam = new List<EmpleadoEstudiante>();
			List<EmpleadoEstudiante> ListaSolicitudesEmpleado = new List<EmpleadoEstudiante>();
			try
			{
				ViewBag.Titulo = "Horarios Team";
				ViewBag.Perfil = "Admin";
				ListaSolicitudesEmpleado = SolicitudesEstudiantesXStatus("A").OrderByDescending(y => y.Id).ToList();
				if (ListaSolicitudesEmpleado.Count() > 0)
				{
					List<Empleado> listaEmpleados = new NegocioHorarios(_UserSign).ListaEmpleados(Session["NoEmp"].ToString());
					int index = 0;
					int indexList = 0;

					foreach (EmpleadoEstudiante emp in ListaSolicitudesEmpleado)
					{
						index = listaEmpleados.FindIndex(x => x.codigo.Equals(emp.U_EMP));
						indexList = ListaSolicitudesTeam.FindIndex(x => x.U_EMP.Equals(emp.U_EMP));
						if (index >= 0 && indexList < 0)
						{
							emp.nombre = listaEmpleados[index].nombre;
							emp.Puesto = listaEmpleados[index].puesto;
							emp.HorariosSolicitud = horariosEmp(emp.Id, 0);
							ListaSolicitudesTeam.Add(emp);
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			if (ListaSolicitudesTeam.Count() > 0)
			{
				ListaSolicitudesTeam = ListaSolicitudesTeam.OrderBy(x => x.U_EMP).ToList();

			}
			return View("HorariosTeam", ListaSolicitudesTeam);
		}
	}
}
