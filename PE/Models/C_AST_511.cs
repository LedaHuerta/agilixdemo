namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@AST_511")]
    public partial class C_AST_511
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public int? Period { get; set; }

        public short? Instance { get; set; }

        public int? Series { get; set; }

        [StringLength(1)]
        public string Handwrtten { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public string U_EMP { get; set; }

        public short? U_EVAC { get; set; }

        public short? U_ANTA { get; set; }

        public DateTime? U_FAV { get; set; }

        public string U_IFAVAC { get; set; }

        public string U_CIA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_DVAC { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_CA6VAC { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_DVDI { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_DVPER { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_DVPE { get; set; }

        public string U_CAT { get; set; }

        public string U_LOC { get; set; }

        public short? U_NTB { get; set; }

        public string U_EDOCTA { get; set; }
    }
}
