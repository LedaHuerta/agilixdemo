﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControldeHorarios.Models
{
	public class Result
	{
		public int Status { get; set; }
		private String _Mensaje = String.Empty;
		public String Mensaje { get => _Mensaje; set => _Mensaje = value; }
	}
}