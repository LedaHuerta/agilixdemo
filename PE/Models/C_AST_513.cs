namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@AST_513")]
    public partial class C_AST_513
    {          

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public int? Period { get; set; }

        public short? Instance { get; set; }

        public int? Series { get; set; }

        [StringLength(1)]
        public string Handwrtten { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public string U_EMP { get; set; }

        public DateTime? U_FECSOL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_DSOL { get; set; }

        public string U_STSV { get; set; }

        public string U_CON { get; set; }

        [StringLength(8)]
        public string U_CIA { get; set; }

        [StringLength(8)]
        public string U_LOC { get; set; }

        public DateTime? U_FIV { get; set; }

        public DateTime? U_FTV { get; set; }

        [StringLength(1)]
        public string U_SSUP { get; set; }

        [StringLength(1)]
        public string U_SRH { get; set; }

        [StringLength(512)]
        public string U_MOTR { get; set; }

        [StringLength(8)]
        public string U_EMPSUP { get; set; }

        public string U_MOTRE { get; set; }

        //public virtual C_AST_031 Conceptos { get; set; }

    }
}
