﻿namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class EvetoCalendario : DbContext
    {
        public EvetoCalendario()
            : base("name=EvetoCalendario")
        {
        }

        public string U_Name { get; set; }
        public string C_Name { get; set; }
        public DateTime I_Date { get; set; }
        public DateTime F_Date { get; set; }
    }
}
