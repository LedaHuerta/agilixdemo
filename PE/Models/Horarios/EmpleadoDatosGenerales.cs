﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Aplics.Servicios.Modelos.Horarios
{
	public class EmpleadoDatosGenerales
	{
		private int _DocEntry = 0;
		[Display(Description = "Clave")]
		public int DocEntry { get => _DocEntry; set => _DocEntry = value; }
		private String _NoEmpleado = String.Empty;
		[Display(Description = "No. Empleado")]
		public String NumEmpleado { get => _NoEmpleado; set => _NoEmpleado = value; }
		private String _nombre = String.Empty;
		[Display(Description = "Nombre")]
		public String Nombre { get => _nombre; set => _nombre = value; }
		[Display(Description = "Superior")]
		public String NombreSuperior { get; set; }
	}
}
