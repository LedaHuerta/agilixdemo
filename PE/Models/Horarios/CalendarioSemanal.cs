﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Horarios
{


    public enum diasSemana : byte { Lunes = 1, Martes = 2, Miércoles = 3, Jueves = 4, Viernes = 5, Sábado = 6, Domingo = 7 }

    public class Tiempo
    {
        private byte _hora = 0;
        public byte hora { get => _hora; set => _hora = value; }
        private byte _minuto = 0;
        public byte minuto { get => _minuto; set => _minuto = value; }
        private byte _segundo = 0;
        public byte segundo { get => _segundo; set => _segundo = value; }

        public Tiempo()
        {

        }

        public Tiempo(byte phora)
        {
            _hora = phora;
        }


        public Tiempo(byte phora,byte pminuto, byte psegundo)
        {
            _hora = phora;
            _minuto = pminuto;
            _segundo = psegundo;
        }

    }

    public class CalendarioSemanal
    {
		private String _padre = String.Empty;
		public String padre { get => _padre; set => _padre = value; }
		public Boolean editable { get; set; }
		private diasSemana _diaInicial = diasSemana.Lunes;
        public diasSemana diaInicial { get => _diaInicial; set => _diaInicial = value; }
        private diasSemana _diaFinal = diasSemana.Viernes;
        public diasSemana diaFinal { get => _diaFinal; set => _diaFinal = value; }
		private diasSemana _diaActual = diasSemana.Lunes;
		public diasSemana diaActual { get => _diaActual; set => _diaActual = value; }
		private List<MVHorario> _mVHorarios = new List<MVHorario>();
		public List<MVHorario> mVHorarios { get => _mVHorarios; set => _mVHorarios = value; }
		private List<Dia> _dias = new List<Dia>();
        public List<Dia> dias { get => _dias; set => _dias = value; }
        private List<Dia> _actividadSemanal = new List<Dia>();
        public List<Dia> actividadSemanal { get => _actividadSemanal; set => _actividadSemanal=value; }
        private List<Tiempo> _horasHabiles = new List<Tiempo>();
        public List<Tiempo> horasHabiles { get => _horasHabiles; set => _horasHabiles = value; }
        private DateTime? _fechaSemana = null;
        public DateTime? fechaSemana
        {
            get
            {
                return _fechaSemana;
            }
            set
            {
                _fechaSemana = value;
                DateTime fecha = (DateTime)_fechaSemana;
                int thisWeekNumber = GetIso8601WeekOfYear(fecha);
                _inicioSemana = FirstDateOfWeek(fecha.Year, thisWeekNumber, CultureInfo.InvariantCulture);
            }
        }
        private DateTime?  _inicioSemana = null;
        public DateTime? inicioSemana { get => _inicioSemana; }

        public CalendarioSemanal()
        {

        }

        public CalendarioSemanal(diasSemana pdiaInicial, diasSemana pdiaFinal)
        {
            _diaInicial = pdiaInicial;
            _diaFinal = pdiaFinal;
        }

       

        public void crearCalendario(Tiempo pinicioActividades, Tiempo pFinActividades)
        {
			byte horafin = pFinActividades.hora;
			horafin += pFinActividades.minuto > 0 ? (byte)1 : (byte)0;
			CalendarioSemanalfn.horasDia(pinicioActividades.hora, horafin, ref _horasHabiles);
            diasSemana d;
            for (byte dia=(byte) _diaInicial; dia<=(byte) _diaFinal; dia++)
            {
                d = (diasSemana)dia;
                _dias.Add(new Dia(d, Enum.GetName(typeof(diasSemana), dia), pinicioActividades, pFinActividades));
            }
        }

        public void crearCalendario(Tiempo pinicioActividades, Tiempo pFinActividades,List<Dia> pactividadSemanal)
        {
            _actividadSemanal = pactividadSemanal;
			byte horafin = pFinActividades.hora;
			horafin += pFinActividades.minuto > 0 ? (byte)1 : (byte)0;
			CalendarioSemanalfn.horasDia(pinicioActividades.hora, horafin, ref _horasHabiles);
            diasSemana d;
            Dia nDia = new Dia();
            for (byte dia = (byte)_diaInicial; dia <= (byte)_diaFinal; dia++)
            {
                d = (diasSemana)dia;
                nDia = new Dia(d, Enum.GetName(typeof(diasSemana), dia), pinicioActividades, pFinActividades);
                foreach(Dia xdia in _actividadSemanal)
                {
                    if ((byte)xdia.numero == dia) {
                        nDia.horario = xdia.horario;
                        break;
                    }
                }
                _dias.Add(nDia);
            }
        }

        private int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }



        private DateTime FirstDateOfWeek(int year, int weekOfYear, CultureInfo ci)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = (int)ci.DateTimeFormat.FirstDayOfWeek - (int)jan1.DayOfWeek;
            DateTime firstWeekDay = jan1.AddDays(daysOffset);
            int firstWeek = ci.Calendar.GetWeekOfYear(jan1, ci.DateTimeFormat.CalendarWeekRule, ci.DateTimeFormat.FirstDayOfWeek);
            if ((firstWeek <= 1 || firstWeek >= 52) && daysOffset >= -3)
            {
                weekOfYear -= 1;
            }
            return firstWeekDay.AddDays(weekOfYear * 7);
        }

        public class Dia
        {
            private diasSemana _numero=diasSemana.Lunes;
            public diasSemana numero { get => _numero; set => _numero=value; }
            private String _nombre = Enum.GetName(typeof(diasSemana), 1);
            public String nombre {
				get {
					_nombre = Enum.GetName(typeof(diasSemana), _numero);
					return _nombre;
				}
			}
            private List<Horario> _horario = new List<Horario>();
            public List<Horario> horario { get => _horario; set => _horario = value; }
            private List<Tiempo> _horas = new List<Tiempo>();
            public List<Tiempo> horas { get => _horas; set => _horas=value; }
            private List<Tiempo> _horasHabiles = new List<Tiempo>();
            public List<Tiempo> horasHabiles { get => _horasHabiles; set => _horasHabiles = value; }
            private Tiempo _inicioActividades = new Tiempo();
            public Tiempo inicioActividades { get => _inicioActividades; set => _inicioActividades=value; }
            private Tiempo _finActividades = new Tiempo();
            public Tiempo finActividades { get => _finActividades; set => _finActividades = value; }
            private DateTime _ahora = System.DateTime.Now;
            public DateTime ahora { get => _ahora; set => _ahora = value; }

            public Dia()
            {
                CalendarioSemanalfn.horasDia(1,24,ref _horas);
            }

            public Dia(diasSemana numdia,String pnombre, Tiempo pinicioActividades, Tiempo pFinActividades)
            {
                _numero = numdia;
                _nombre = pnombre;
                _inicioActividades = pinicioActividades;
                _finActividades = pFinActividades;
                CalendarioSemanalfn.horasDia(1, 24, ref _horas);
                CalendarioSemanalfn.horasDia(_inicioActividades.hora, _finActividades.hora + _finActividades.minuto>0 ? (byte) 1 : (byte) 0, ref _horasHabiles);
            }

			public Dia(diasSemana numdia,  Tiempo pinicioActividades, Tiempo pFinActividades)
			{
				_numero = numdia;
				_inicioActividades = pinicioActividades;
				_finActividades = pFinActividades;
				CalendarioSemanalfn.horasDia(1, 24, ref _horas);
				CalendarioSemanalfn.horasDia(_inicioActividades.hora, _finActividades.hora + _finActividades.minuto > 0 ? (byte)1 : (byte)0, ref _horasHabiles);
			}

			public Dia(diasSemana numdia, Tiempo pinicioActividades, Tiempo pFinActividades,List<Horario> pHorario)
            {
                _numero = numdia;
                 _inicioActividades = pinicioActividades;
                _finActividades = pFinActividades;
                _horario = pHorario;
                CalendarioSemanalfn.horasDia(1, 24, ref _horas);
                CalendarioSemanalfn.horasDia(_inicioActividades.hora, _finActividades.hora + _finActividades.minuto > 0 ? (byte)1 : (byte)0, ref _horasHabiles);
            }

           

            public class Horario
            {
                private Tiempo _inicioActividad = new Tiempo();
                public Tiempo inicioActividad { get => _inicioActividad ; set => _inicioActividad = value; }
                private Tiempo _finActividad = new Tiempo();
                public Tiempo finActividad { get => _finActividad; set => _finActividad=value; }
                int _claveActividad = 0;
                public int claveActividad { get => _claveActividad; set => _claveActividad = value; }
                private String _actividad = String.Empty;
                public String actividad { get => _actividad; set => _actividad = value; }
				private String _colorFondo = String.Empty;
				public String colorFondo { get => _colorFondo; set => _colorFondo = value; }
				private String _colorTexto = String.Empty;
                public String colorTexto { get => _colorTexto; set => _colorTexto = value; }
                private String _icono = String.Empty;
                public String icono { get => _icono; set => _icono = value; }

                public Horario()
                {

                }

                public Horario(Tiempo inicio, Tiempo fin,int pclaveActividad, String pactividad,String pcolorbg,String pcolor,String picono)
                {
                    _inicioActividad.hora = inicio.hora;
                    _inicioActividad.minuto = inicio.minuto;
                    _inicioActividad.segundo = inicio.segundo;
                    _finActividad.hora = fin.hora;
                    _finActividad.minuto = fin.minuto;
                    _finActividad.segundo = fin.segundo;
                    _claveActividad = pclaveActividad;
                    _actividad = pactividad;
					_colorFondo = pcolorbg;

					_colorTexto = pcolor;
                    _icono = picono;
                }
            }
        }
    }
}