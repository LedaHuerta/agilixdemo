﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Aplics.Servicios.Modelos.Horarios
{
	public class EmpleadoHorario : EmpleadoDatosGenerales
	{
		public DateTime Inicio { get; set; }
		public DateTime Fin { get; set; }
		public Boolean Aprobado { get; set; }
		[Display(Description = "Horario académico")]
		public Boolean U_HorarioESC { get; set; }
		private String _motivoRechazo = String.Empty;
		[Display(Description = "Motivo de Rechazo")]
		public String U_MOTIVOREC { get => _motivoRechazo; set => _motivoRechazo = value; }
		private short _U_NUMREC = 0;
		[Display(Description = "No. Rechazos")]
		public short U_NUMREC { get => _U_NUMREC; set => _U_NUMREC = value; }
	}
}
