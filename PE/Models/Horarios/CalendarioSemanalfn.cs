﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Horarios
{
    public static class CalendarioSemanalfn
    {
        public static void horasDia(byte inicio, byte fin, ref List<Tiempo> lista)
        {
            for (byte x = inicio; x <= fin; x++)
            {
                lista.Add(new Tiempo(x));
            }
        }
    }
}