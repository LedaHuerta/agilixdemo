﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Horarios
{
	public class HorarioTipo
	{
		public Byte TipoHorario { get; set; }
		public Byte dia { get; set; }
		public String HoraInicial { get; set; }
		public String HoraFinal { get; set; }
	}
}