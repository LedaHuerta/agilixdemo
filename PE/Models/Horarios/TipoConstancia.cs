﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Modelos.Horarios
{
	public class TipoConstancia
	{
		private String _url = String.Empty;
		public String url { get => _url; set => _url = value; }
		private Boolean _esimagen=false;
		public Boolean esimagen { get => _esimagen ; set => _esimagen = value; }
		private String _archivo = String.Empty;
		public String archivo { get => _archivo; set => _archivo = value; }
		public TipoConstancia()
		{

		}

		public TipoConstancia(String purl,Boolean pEsimagen,String parchivo)
		{
			_url = purl;
			_esimagen = pEsimagen;
			_archivo = parchivo;
		}
	}
}
