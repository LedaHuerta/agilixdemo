﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace Aplics.Servicios.Modelos.Horarios
{
	public class Empleado
	{
		private int _Id = 0;
		[Display(Description ="Clave")]
		public int Id { get => _Id; set => _Id=value; }
		private String _nombre = String.Empty;
		[Display(Description ="Nombre")]
		public String nombre { get => _nombre; set => _nombre = value; }

        private String _puesto = String.Empty;
        [Display(Description = "Puesto")]
        public String puesto { get => _puesto; set => _puesto = value; }

        private String _codigo = String.Empty;
        [Display(Description = "Código")]
        public String codigo { get => _codigo; set => _codigo = value; }

        public Empleado()
		{

		}

		public Empleado(int pId, String pnombre)
		{
			_Id = pId;
			_nombre = pnombre;
		}
	}
}