﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Aplics.Servicios.Modelos.Horarios
{
	public class EmpleadoEstudiante
	{
		private int _Id = 0;
		[Display(Description = "Clave")]
		public int Id { get => _Id; set => _Id = value; }
		private String _nombre = String.Empty;
		[Display(Description = "Nombre")]
		public String nombre { get => _nombre; set => _nombre = value; }

		[Required]
		[MaxLength(8)]
		[Display(Description = "Cod. Emp.")]
		public String U_EMP { get; set; }
		private short _U_ANIO = (short)System.DateTime.Today.Year;
		[Required]
		[Display(Description = "Año")]
		public short U_ANIO { get => _U_ANIO; set => _U_ANIO = value; }
		[Required]
		[Display(Description = "Período")]
		public short U_PER { get; set; }
		[Required]
		[Display(Description = "Duración")]
		public short U_DUR { get; set; }
		[Required]
		[Display(Description = "Fecha Inicial")]
		public DateTime U_FINI { get; set; }
		[Required]
		[Display(Description = "Fecha Final")]
		public DateTime U_FFIN { get; set; }
		[Display(Description = "Aprobado")]
		[Required]
		public Boolean U_Aprobado { get; set; }
		[Display(Description = "Horario académico")]
		[Required]
		public Boolean U_HorarioESC { get; set; }

		private DateTime _FechaSolicitud = System.DateTime.Now;
		[Required]
		[Display(Description = "Fecha de Solicitud")]
		public DateTime FechaSolicitud { get => _FechaSolicitud; set => _FechaSolicitud = value; }

		private String _motivoRechazo = String.Empty;
		public String motivoRechazo { get => _motivoRechazo; set => _motivoRechazo = value; }
		private short _U_NUMREC = 0;
		[Display(Description = "No. Rechazos")]
		public short U_NUMREC { get => _U_NUMREC; set => _U_NUMREC = value; }
		private String _U_CVEUNI = String.Empty;
		[Display(Description = "Cve. Universidad")]
		public String U_CVEUNI { get => _U_CVEUNI; set => _U_CVEUNI = value; }
		[Display(Description = "Status")]
		public String Status { get; set; }

		private String _Duracion = String.Empty;
		[Display(Description = "Duración")]
		public String Duracion { get => _Duracion; set => _Duracion = value; }
		private String _Universidad = String.Empty;
		[Display(Description = "Universidad")]
		public String Universidad { get => _Universidad; set => _Universidad = value; }
		private String _Observaciones = String.Empty;
		[Display(Description = "Observaciones")]
		public String Observaciones { get => _Observaciones; set => _Observaciones = value; }
		public CalendarioSemanal ocalendario { get; set; }
		public List<TipoConstancia> constancias { get; set; } = new List<TipoConstancia>();

        private String _puesto = String.Empty;
        [Display(Description = "Puesto")]
        public String Puesto { get => _puesto; set => _puesto = value; }

        public List<VMHorarioLista> HorariosSolicitud { get; set; } = new List<VMHorarioLista>();

    }
}
