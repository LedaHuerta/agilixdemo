﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Modelos.Horarios
{
	public class MVHorario
	{
		public int Id { get; set; }
		public int Consecutivo { get; set; }
		public int? VisOrder { get; set; }
		public int LogInst { get; set; }
		public byte U_TIPOH { get; set; }
		public byte U_DIAINI { get; set; }
		public byte U_HORAINI { get; set; }
		public byte U_MINUTOINI { get; set; }
		public byte U_DIAFIN { get; set; }
		public byte U_HORAFIN { get; set; }
		public byte U_MINUTOFIN { get; set; }
	}
}