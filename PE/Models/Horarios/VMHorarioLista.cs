﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Horarios
{
	public class VMHorarioLista
	{
		public int Id { get; set; }
		public byte Tipo { get; set; }
		public List<Horario> Horarios { get; set; }
	}
}