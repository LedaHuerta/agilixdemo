﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Modelos.Horarios
{
	public class VMAutorizacionesHorarios
	{
		private int _Id = 0;
		public int Id { get=> _Id; set => _Id = value; }
		public int idEmpleado { get; set; }
		public String codeEmpleado { get; set; }
		public Boolean Autorizado { get; set; }
		private String _motivoRechazo = String.Empty;
		public String motivoRechazo { get => _motivoRechazo; set => _motivoRechazo = value ; }
		private short _numRechazos = 0;
		public short numRechazos { get => _numRechazos; set => _numRechazos = value; }
		private List<MVHorario> _horarios = new List<MVHorario>();
		public List<MVHorario> horarios { get => _horarios; set => _horarios = value; }
	}
}
