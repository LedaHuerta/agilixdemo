﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aplics.Servicios.Horarios.Modelos;
namespace ControldeHorarios.Models
{
	public class HorariosPersonal
	{
		public int Id { get; set; }
		private Parametros _parametros = new Parametros();
		public byte Status { get; set; }
		public String codeEmpleado { get; set; }
		private String _motivoRechazo = String.Empty;
		public String motivoRechazo { get => _motivoRechazo; set => _motivoRechazo = value; }
		public Parametros parametros { get => _parametros; set => _parametros = value; }
		private List<MVHorario> _Horarios = new List<MVHorario>();
		public List<MVHorario> Horarios { get => _Horarios; set => _Horarios = value; }
		public class Parametros
		{
			public Boolean indrag { get; set; }
			private String _colorActividad = String.Empty;
			public String colorActividad { get => _colorActividad; set => _colorActividad = value; }
			public int colini { get; set; }
			public int colfin { get; set; }
			private String _tablaini = String.Empty;
			public String tablaini { get => _tablaini; set => _tablaini = value; }
			public int rowIni { get; set; }
			public int TipoHorario { get; set; }
			public int horaIni { get; set; }
			public int minutoIni { get; set; }
		}
	}
}