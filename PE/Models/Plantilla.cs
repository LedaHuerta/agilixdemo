﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ControldeHorarios.Models
{
	public class Plantilla
	{
		public Plantilla()
		{

		}

		[Display(Description = "Clave")]
		public int Id { get; set; }
		[Display(Description = "Nombre")]
		public string nombre { get; set; }
		[Display(Description = "Cod. Emp.")]
		[MaxLength(8)]
		[Required]
		public string U_EMP { get; set; }
		[Display(Description = "Año")]
		[Required]
		public short U_ANIO { get; set; }
		[Display(Description = "Período")]
		[Required]
		public short U_PER { get; set; }
		[Display(Description = "Duración")]
		[Required]
		public short U_DUR { get; set; }
		[Display(Description = "Fecha Inicial")]
		[Required]
		public DateTime U_FINI { get; set; }
		[Display(Description = "Fecha Final")]
		[Required]
		public DateTime U_FFIN { get; set; }
	}
}