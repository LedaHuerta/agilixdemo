﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Generales
{
	public class CatalogoGeneral
	{
		private String _Id = String.Empty;
		public String Id { get => _Id; set => _Id = value; }
		private String _Descripcion = String.Empty;
		public String Descripcion { get => _Descripcion; set => _Descripcion = value; }

		public CatalogoGeneral()
		{

		}

		public CatalogoGeneral(String pId,String pDescripcion)
		{
			_Id = pId;
			_Descripcion = pDescripcion;
		}
	}
}