namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@AST_161")]
    public partial class C_AST_161
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public int? Period { get; set; }

        public short? Instance { get; set; }

        public int? Series { get; set; }

        [StringLength(1)]
        public string Handwrtten { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public DateTime U_FFI { get; set; }

        public DateTime U_FFF { get; set; }

        public short? U_DIA { get; set; }

        public short? U_MES { get; set; }

        public short? U_AF { get; set; }

    }
}
