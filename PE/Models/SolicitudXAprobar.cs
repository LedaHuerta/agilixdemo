﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Modelos.Solicitudes
{
	public class SolicitudXAprobar
	{
		public int Id { get; set; }
		public String NumEmpleado { get; set; }
		public String EmpleadoAutoriza { get; set; }
		public Boolean Aprobar { get; set; }
		private String _motivoRechazo = String.Empty;
		public String MotivoRechazo { get => _motivoRechazo; set => _motivoRechazo = value; }
	}
}
