﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Aplics.Servicios.Modelos.Solicitudes
{
   public  class DatosSolicitudCardsHistorico
    {

        public string U_EMP { get; set; }
        public decimal DSOL_TOTAL { get; set; }
        public string U_CIA { get; set; }
        public int ANIO { get; set; }
        public decimal DCorrespondientes { get; set; }
        public decimal DDisfrutados { get; set; }
        public decimal DDisponibles { get; set; }

    }
}
