﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Aplics.Servicios.Modelos.Solicitudes
{
	public class DashboardSolicitud
	{
		public String Id { get; set; }
		public String Descripcion { get; set; }
		public String ColordeFondo { get; set; }
		public String ColordeTexto { get; set; }

		[Display(Description = "Te corresponden: ")]
		public decimal DiasCorrespondientes { get; set; }

		[Display(Description = "Tienes disponibles: ")]
		public decimal DiasDisponibles { get; set; }

		[Display(Description = "Has disfrutado: ")]
		public decimal DiasDisfrutados { get; set; }

		[Display(Description = "Por aprobar: ")]
		public decimal DiasPorAprobar { get; set; }

        [Display(Description = "Proceso de Nomina: ")]
        public decimal DiasPorProcesarNominix { get; set; }

        private String _Comentario = String.Empty;
		public String Comentario { get => _Comentario; set => _Comentario = value; }

		private String _UMED = String.Empty;

        public String UMED { get => _UMED; set => _UMED = value; }
    }
}
