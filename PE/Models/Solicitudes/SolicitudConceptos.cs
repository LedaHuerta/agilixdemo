﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Horarios.Modelos.Solicitudes
{
   public class SolicitudConceptos
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string Canceled { get; set; }
        public string Object { get; set; }
        public int? LogInst { get; set; }
        public int? UserSign { get; set; }
        public string Transfered { get; set; }
        public DateTime? CreateDate { get; set; }
        public short? CreateTime { get; set; }
        public DateTime? UpdateDate { get; set; }
        public short? UpdateTime { get; set; }
        public string DataSource { get; set; }
        public string U_TCON { get; set; }
        public string U_CALAGR { get; set; }
        public short? U_PRI { get; set; }
        public string U_UME { get; set; }
        public string U_IDC { get; set; }
        public string U_LCON { get; set; }
        public string U_NATCON { get; set; }
        public string U_CEC { get; set; }
        public string U_CNTC { get; set; }
        public string U_CCON { get; set; }
        public string U_ANSM { get; set; }
        public string U_CAP { get; set; }
        public string U_CALC { get; set; }
        public string U_IMPRC { get; set; }
        public string U_RC { get; set; }
        public string U_RAMASUA { get; set; }
        public string U_TIPRIE { get; set; }
        public string U_CTLINC { get; set; }
        public string U_CVAA { get; set; }
        public string U_EINCAP { get; set; }
        public string U_CMPEDC { get; set; }
        public string U_EKDX { get; set; }
    }
}
