﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Solicitudes
{
    public class DashboardxEjercicio
    {
       public class DescansosAnual
        {
            public int Anio { get; set; }
            public DashboardSolicitud Descansos { get; set; }
        }
       public List<DescansosAnual> TableroDescanso { get; set; }
    }
}