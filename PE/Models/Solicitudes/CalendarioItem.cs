﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Aplics.Servicios.Modelos.Solicitudes
{
	public class CalendarioItem
	{
	
		[Display(Description = "title")]
		public string titulo { get; set; }

		[Display(Description = "description")]
		public string descripcion { get; set; }

		[Display(Description = "start")]
		public string inicio { get; set; }

		[Display(Description = "end")]
		public string fin { get; set; }

        [Display(Description = "color")]
        public string color { get; set; }

        [Display(Description = "textColor")]
        public string colorTexto { get; set; }

        [Display(Description = "description2")]
        public string descripcion2 { get; set; }

		private string _claveEmpleado = string.Empty;

		[Display(Description = "clave Empleado")]
		public string claveEmpleado { get => _claveEmpleado; set => _claveEmpleado = value; }

	}
}
