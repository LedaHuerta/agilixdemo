﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Aplics.Servicios.Modelos.Solicitudes
{
    public class CalendarioJson
    {
        private List<CalendarioItemJson> _ItemsCalendario = new List<CalendarioItemJson>();
        public List<CalendarioItemJson> ItemsCalendario { get => _ItemsCalendario; set => _ItemsCalendario = value; }
    }
}
