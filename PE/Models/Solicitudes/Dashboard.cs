﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Aplics.Servicios.Modelos.Solicitudes
{
    public class Dashboard
    {
		public String  NumEmpleado { get; set; }
		private List<DashboardSolicitud> _Solicitudes = new List<DashboardSolicitud>();
		public List<DashboardSolicitud> Solicitudes { get => _Solicitudes; set => _Solicitudes = value; }
	}
}
