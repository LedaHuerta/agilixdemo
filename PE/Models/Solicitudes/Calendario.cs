﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Aplics.Servicios.Modelos.Solicitudes
{
    public class Calendario
    {
        private List<CalendarioItem> _ItemsCalendario = new List<CalendarioItem>();
        public List<CalendarioItem> ItemsCalendario { get => _ItemsCalendario; set => _ItemsCalendario = value; }
    }
}
