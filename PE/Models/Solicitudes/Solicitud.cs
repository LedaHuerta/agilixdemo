﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Aplics.Servicios.Modelos.Solicitudes
{
   public  class Solicitud
    {

		[Display(Description = "Id ")]
		public int Id { get; set; }

		[Display(Description = "Empleado ")]
        public String NumEmpleado { get; set; }

        [Display(Description = "Tipo de Solicitud")]
		public String TipoSolicitud { get; set; }

		private DateTime _FechaSolicitud =System.DateTime.Now;
		[Display(Description = "Fecha Solicitud")]
		public DateTime FechaSolicitud { get => _FechaSolicitud; set => _FechaSolicitud = value; }

		[Display(Description = "Fecha Inicial")]
        public DateTime FechaInicial { get; set; }

        [Display(Description = "Fecha Final")]
        public DateTime FechaFinal { get; set; }



    }
}
