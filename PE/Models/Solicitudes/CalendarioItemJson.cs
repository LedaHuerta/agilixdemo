﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Aplics.Servicios.Modelos.Solicitudes
{
	public class CalendarioItemJson
	{
	
		[Display(Description = "title")]
		public string title { get; set; }

		[Display(Description = "description")]
		public string description { get; set; }

		[Display(Description = "start")]
		public string start { get; set; }

		[Display(Description = "end")]
		public string end { get; set; }

        [Display(Description = "color")]
        public string color { get; set; }

        [Display(Description = "textColor")]
        public string textColor { get; set; }


    }
}
