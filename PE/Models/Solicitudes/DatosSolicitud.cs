﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Solicitudes
{
	public class DatosSolicitud:Solicitud
	{
		[Display(Description = "Nombre Empleado")]
		public String NombreEmpleado { get; set; }
		[Display(Description = "Descripción")]
		public String Descripcion { get; set; }
		[Display(Description = "Cve. Categoría")]
		public String Categoria { get; set; }
        [Display(Description = "StatusSup")]
        public String StatusSup { get; set; }
        [Display(Description = "StatusRH")]
        public String StatusRH { get; set; }
		[Display(Description = "Puesto")]
		public String Puesto { get; set; }
		[Display(Description = "Superior")]
		public String Superior { get; set; }
		[Display(Description = "Observaciones")]
		public String Observaciones { get; set; }
        [Display(Description = "Dias")]
        public decimal Dias { get; set; }

		private String _uMed = String.Empty;
        public String UMed { get => _uMed; set => _uMed = value; }
    }
}