﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Modelos.Solicitudes
{
	public class ItemSolicitiud
	{
		public String Id { get; set; }
		public String Descripcion { get; set; }
		public Boolean Politica { get; set; }
		private Decimal _Tope = 0.00M;
		public Decimal Tope { get => _Tope; set => _Tope = value; }
	}
}
