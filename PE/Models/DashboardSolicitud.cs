﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Aplics.Servicios.Modelos.Solicitudes
{
	public class DashboardSolicitud
	{
		public String Id { get; set; }
		public String Descripcion { get; set; }

		[Display(Description = "Te corresponden: ")]
		public int DiasCorrespondientes { get; set; }

		[Display(Description = "Tienes disponibles: ")]
		public int DiasDisponibles { get; set; }

		[Display(Description = "Has disfrutado: ")]
		public int DiasDisfrutados { get; set; }

		[Display(Description = "Por aprobar: ")]
		public int DiasPorAprobar { get; set; }

		private String _Comentario = String.Empty;
		public String Comentario { get => _Comentario; set => _Comentario = value; }

	}
}
