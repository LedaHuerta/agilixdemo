﻿namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@AST_522")]
    public partial class C_AST_522
    {

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Code { get; set; }

        [Key, Column(Order = 1),DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal U_DIAS { get; set; }

        [Column(TypeName = "numeric")]
        public decimal U_SALD { get; set; }

        [StringLength(8)]
        public string U_CON { get; set; }

    }
}