﻿namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@AST_T91")]
    public partial class C_AST_T91
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(50)]
        public string U_EMP { get; set; }

        public string U_DESC { get; set; }

        [StringLength(200)]
        public string U_CTRL { get; set; }

        [StringLength(200)]
        public string U_ACTION { get; set; }

        [StringLength(1)]
        public string U_STAT { get; set; }

    }
}