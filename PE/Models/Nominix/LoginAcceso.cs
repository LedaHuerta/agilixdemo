﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Nominix
{
	public class LoginAcceso
	{
		public String NoEmp { get; set; }
		public String NombreEmpleado { get; set; }
		public int AnioIngreso { get; set; }
        public String TipoRol { get; set; }
        public int UserSign { get; set; }

    }
}