﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Nominix
{
	public class Notificaciones
	{
		public int Id { get; set; }
		public String empleadoNotifica { get; set; }
		public String empleadoRecibe { get; set; }
		public String notificacion { get; set; }
		public DateTime? fechaEntrega { get; set; }
		public DateTime? fechaLectura { get; set; }
		public String status { get; set; }
		public String parametros { get; set; }
		public String identificadorProceso { get; set; }
        public int rol { get; set; }
		public int rolDestino { get; set; }
		public String idProcesoNotificacion { get; set; }


	}
}