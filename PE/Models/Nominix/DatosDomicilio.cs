﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace PortalEmpleadosV2.Models.Nominix
{
    public class DatosDomicilio
    {
        [StringLength(8)]
        public string Code { get; set; }

        public string U_PAIS { get; set; }

        public string U_EDO { get; set; }

        public string U_DOM { get; set; }

        public int U_NUMEXT { get; set; }

        public int U_NUMINT { get; set; }

        public string U_COL { get; set; }

        public string U_CODP { get; set; }

        public string U_DEL { get; set; }

    }
}