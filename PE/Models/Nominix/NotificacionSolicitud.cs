﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Nominix
{
	public class NotificacionSolicitud
	{
		public int Id { get; set; }
		public String Mensaje { get; set; }
		private String _NumEmpleado = String.Empty;
		public String NumEmpleado { get=> _NumEmpleado; set => _NumEmpleado= value; }
		public String resultadoEnvio { get; set; }
		public Boolean Error { get; set; }
		private String _url = String.Empty;
		public String url { get => _url; set => _url = value; }
		private String _controlador = String.Empty;
		public String controlador { get => _controlador; set => _controlador = value; }
		private String _accion = String.Empty;
		public String accion { get => _accion; set => _accion = value; }
	}
}