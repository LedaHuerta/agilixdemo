﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Aplics.Servicios.Modelos.Horarios
{
	public class DatosEmpleadoGeneral
	{
		private String _Id = String.Empty;
		public String Id { get => _Id; set => _Id = value; }
		private String _Descripcion = String.Empty;
		public String Descripcion { get => _Descripcion; set => _Descripcion = value; }

		public DatosEmpleadoGeneral()
		{

		}

		public DatosEmpleadoGeneral(String pId, String pDescripcion)
		{
			_Id = pId;
			_Descripcion = pDescripcion;
		}
	}
}
