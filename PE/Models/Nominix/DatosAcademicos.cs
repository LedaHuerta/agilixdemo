﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace PortalEmpleadosV2.Models.Nominix
{
    public class DatosAcademicos
    {
        [StringLength(8)]
        public string Code { get; set; }

        [MaxLength(8)]
        public string nivelEscolar { get; set; }

        public string situacionEscolar { get; set; }

        public string gradoEscolar { get; set; }

        public string periodoEscolar { get; set; }

        [MaxLength(8)]
        public string profesion { get; set; }

        public string claveUniversidad { get; set; }

    }
}