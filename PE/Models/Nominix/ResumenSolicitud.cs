﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Nominix
{
	public class ResumenSolicitud
	{
		public int Id { get; set; }
		public DateTime Fecha { get; set; }
		public String TipoSolicitud { get; set; }
		private String _claveTipoSolicitud = String.Empty;
		public String ClaveTipoSolicitud { get => _claveTipoSolicitud; set => _claveTipoSolicitud = value; }
	}
}