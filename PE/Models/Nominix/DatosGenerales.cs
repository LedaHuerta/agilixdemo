﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace PortalEmpleadosV2.Models.Nominix
{
    public class DatosGenerales
    {
        [StringLength(8)]
        public string Code { get; set; }

        public string U_Eci { get; set; }

        public string U_Telcel { get; set; }

        public string U_Teldom { get; set; }

        public string U_Telofi { get; set; }

        public string U_Telrec { get; set; }

        public string U_Email { get; set; }

    }
}