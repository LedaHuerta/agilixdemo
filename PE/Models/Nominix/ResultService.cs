﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Nominix
{
    public class ResultService
    {
        private int _resultado = 0;
        public int resultado { get => _resultado; set => _resultado=value ; }
        public String Mensaje { get; set; }
    }
}