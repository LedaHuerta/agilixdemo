﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Modelos.Nominix
{
	public class Documento
	{
		public String Propietario { get; set; }
		public String Nombre { get; set; }
		public String Tipo { get; set; }
		public DateTime Fecha { get; set; }
		public String Raiz { get; set; }
		public String URL { get; set; }
	}
}