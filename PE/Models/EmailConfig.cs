﻿using Aplics.Servicios.Negocio;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Aplics.Servicios.Models
{
	public class EmailConfig
	{
		public SendMails getConfigMail(Boolean conplantilla)
		{
			try
			{
				String usuariosec = ConfigurationManager.AppSettings["usuariosec"];
				String _correoSalida = ConfigurationManager.AppSettings["correoSalida"];
				String _alias = ConfigurationManager.AppSettings["alias"];
				String pSecureSSL = ConfigurationManager.AppSettings["SecureSSL"];
				Boolean _SecureSSL = pSecureSSL.Equals("1") ? true : false;
				String _servicioSMTP = ConfigurationManager.AppSettings["ServicioSMTP"];
				int _puertoSMTP = int.Parse(ConfigurationManager.AppSettings["puertoSMTP"].ToString());
				String _usuario = ConfigurationManager.AppSettings["usuarioMail"];
				String _password = ConfigurationManager.AppSettings["passMail"];
				String _rutaCorreos = ConfigurationManager.AppSettings["rutaMail"];
				String _plantilla = String.Empty;
				if (conplantilla)
				{
					String archivoPlantilla = System.Web.HttpContext.Current.Server.MapPath("~/Plantillas/");
					archivoPlantilla += "\\" + ConfigurationManager.AppSettings["PlantillaDefault"];
					_plantilla = System.IO.File.ReadAllText(archivoPlantilla);
				}
				SendMails sm = new SendMails(usuariosec, _correoSalida, _alias, _servicioSMTP, _SecureSSL, _puertoSMTP, _usuario, _password, _rutaCorreos, _plantilla);

				return sm;
			}
			catch (Exception ex)
			{
				throw (ex);
			}
		}
	}
}