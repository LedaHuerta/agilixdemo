namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Aplics.Servicios.Modelos.Nominix;

    public partial class NominixContext : DbContext
    {
        public NominixContext()
            : base("name=Nominix")
        {
        }

        public virtual DbSet<C_AST_011> C_AST_011 { get; set; }
        public virtual DbSet<C_AST_511> C_AST_511 { get; set; }
        public virtual DbSet<C_AST_031> C_AST_031 { get; set; }
        public virtual DbSet<C_AST_513> C_AST_513 { get; set; }
        public virtual DbSet<C_AST_T91> C_AST_T91 { get; set; }
        public virtual DbSet<C_AST_522> C_AST_522 { get; set; }
        public virtual DbSet<C_AST_161> C_AST_161 { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //TABLA 011
            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.Canceled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.Transfered)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.DataSource)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_STAT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SUE)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SDI)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SIST)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SFIJ)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SVAR)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_INTF)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_IRE1)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_IRE2)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_BONO)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SN)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SPRO)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_DBANT)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_VBANT)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_PTJAUM)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_SUED)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_IFV)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_011>()
                .Property(e => e.U_CALFN)
                .HasPrecision(19, 6);

            //TABLA 511
            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.Handwrtten)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.Canceled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.Transfered)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.DataSource)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.U_DVAC)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.U_CA6VAC)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.U_DVDI)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.U_DVPER)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_511>()
                .Property(e => e.U_DVPE)
                .HasPrecision(19, 6);

            //TABLA 031
            modelBuilder.Entity<C_AST_031>()
                .Property(e => e.Canceled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_031>()
                .Property(e => e.Transfered)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_031>()
                .Property(e => e.DataSource)
                .IsFixedLength()
                .IsUnicode(false);

            //TABLA 513
            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.Handwrtten)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.Canceled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.Transfered)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.DataSource)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.U_DSOL)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.U_SSUP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_513>()
                .Property(e => e.U_SRH)
                .IsFixedLength()
                .IsUnicode(false);

            // TABLA AST_522
            modelBuilder.Entity<C_AST_522>()
                .Property(e => e.U_DIAS)
                .HasPrecision(19, 6);

            modelBuilder.Entity<C_AST_522>()
                .Property(e => e.U_SALD)
                .HasPrecision(19, 6);

            // TABLA AST_161
            modelBuilder.Entity<C_AST_161>()
                .Property(e => e.Handwrtten)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_161>()
                .Property(e => e.Canceled)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_161>()
                .Property(e => e.Transfered)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_161>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<C_AST_161>()
                .Property(e => e.DataSource)
                .IsFixedLength()
                .IsUnicode(false);


        }
    }
}
