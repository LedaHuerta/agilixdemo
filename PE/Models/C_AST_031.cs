﻿namespace PortalEmpleadosV2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@AST_031")]
    public partial class C_AST_031
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Code { get; set; }

        public string Name { get; set; }

        public int? Docentry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(1)]
        public string U_TCON { get; set; }

        [StringLength(1)]
        public string U_CALAGR { get; set; }

        public short? U_PRI { get; set; }

        [StringLength(3)]
        public string U_UME { get; set; }

        [StringLength(1)]
        public string U_IDC { get; set; }

        public string U_LCON { get; set; }

        [StringLength(8)]
        public string U_NATCON { get; set; }

        public string U_CEC { get; set; }

        [StringLength(1)]
        public string U_CNTC { get; set; }

        public string U_CCON { get; set; }

        public string U_ANSM { get; set; }

        [StringLength(8)]
        public string U_CAP { get; set; }

        public string U_CALC { get; set; }

        public string U_IMPRC { get; set; }

        public string U_RC { get; set; }

        public string U_RAMASUA { get; set; }

        public string U_TIPRIE { get; set; }

        public string U_CTLINC { get; set; }

        public string U_CVAA { get; set; }

        public string U_EINCAP { get; set; }

        public string U_CMPEDC { get; set; }

        public string U_EKDX { get; set; }

    }
}