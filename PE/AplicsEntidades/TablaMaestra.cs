﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Data.Entidades.Generales
{
    public class TablaMaestra
    {
        private Boolean _CodigoConsecutivo = true;
        [NotMapped]
        [ScaffoldColumn(false)]
        public Boolean CodigoConsecutivo { get => _CodigoConsecutivo; set => _CodigoConsecutivo = value; }
        private Byte _RellenarCodeA = 4;
        [NotMapped]
        [ScaffoldColumn(false)]
        public Byte RellenarCodeA { get => _RellenarCodeA; set => _RellenarCodeA = value; }
        [Key]
        [Display(Description = "Clave")]
        public String Code { get; set; }
        public String Name { get; set; }
        public int DocEntry { get; set; }
        private char _Canceled = 'N';
        public char Canceled { get => _Canceled; set => _Canceled = value; }
        [StringLength(20)]
        [ScaffoldColumn(false)]
        public String Object { get; set; }
        private int? _LogInst = 0;
        [ScaffoldColumn(false)]
        public int? LogInst { get => _LogInst; set => _LogInst = value; }
        [ScaffoldColumn(false)]
        public int? UserSign { get; set; }
        private char _Transfered = 'N';
        [ScaffoldColumn(false)]
        public char Transfered { get => _Transfered; set => _Transfered = value; }
        [ScaffoldColumn(false)]
        public DateTime? CreateDate { get; set; }
        [ScaffoldColumn(false)]
        public short? CreateTime { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? UpdateDate { get; set; }
        [ScaffoldColumn(false)]
        public short? UpdateTime { get; set; }
        [ScaffoldColumn(false)]
        public char DataSource { get; set; }
    }
}
