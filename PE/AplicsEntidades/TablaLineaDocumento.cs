﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Data.Entidades.Generales
{
    public class TablaLineaDocumento
    {
        [Key, Column(Order = 0)]
        [Display(Description = "Clave")]
        public int DocEntry { get; set; }
        [Key, Column(Order = 1)]
        [Display(Description = "Consecutivo")]
        public int LineId { get; set; }
        public int? VisOrder { get; set; }
        [StringLength(20)]
        public String Object { get; set; }
        private int? _LogInst = 0;
        [ScaffoldColumn(false)]
        public int? LogInst { get => _LogInst; set => _LogInst = value; }
    }
}
