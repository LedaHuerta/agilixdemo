﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Data.Entidades.Generales
{
    public class TablaDocumento
    {
        [Display(Description="Clave")]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		[Key, Column("DocEntry",  Order = 0)]
		public int DocEntry { get; set; }
        public int? DocNum { get; set; }
        private int? _Period = 0;
        public int? Period { get => _Period; set => _Period = value; }
        private short? _Instance = 0;
        public short? Instance { get => _Instance; set => _Instance=value; }
        private int? _Series = 34;
        public int? Series { get => _Series; set => _Series = value; }
        private char _Handwrtten = 'N';
        public char Handwrtten { get => _Handwrtten; set => _Handwrtten = value; }
        private char _Canceled = 'N';
        public char Canceled { get => _Canceled; set => _Canceled=value; }
        [StringLength(20)]
        [ScaffoldColumn(false)]
        public String Object { get; set; }
        private int? _LogInst = 0;
        [ScaffoldColumn(false)]
        public int? LogInst { get => _LogInst; set => _LogInst = value; }
        [ScaffoldColumn(false)]
        public int? UserSign { get; set; }
        private char _Transfered = 'N';
        [ScaffoldColumn(false)]
        public char Transfered { get => _Transfered; set => _Transfered=value; }
        private char _Status = 'O';
        [ScaffoldColumn(false)]
        public char Status { get => _Status; set => _Status = value; }
        [ScaffoldColumn(false)]
        public DateTime? CreateDate { get; set; }
        [ScaffoldColumn(false)]
        public short? CreateTime { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? UpdateDate { get; set; }
        [ScaffoldColumn(false)]
        public short? UpdateTime { get; set; }
        private char _DataSource = '1';
        [ScaffoldColumn(false)]
        public char DataSource { get => _DataSource; set => _DataSource = value; }
    }
}
