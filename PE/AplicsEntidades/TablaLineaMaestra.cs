﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aplics.Servicios.Data.Entidades.Generales
{
    public class TablaLineaMaestra
    {
        [Key, Column(Order = 0)]
        [Display(Description = "Clave")]
        public String Code { get; set; }
        [Key, Column(Order = 1)]
        [Display(Description = "Consecutivo")]
        public int LineId { get; set; }
        [ScaffoldColumn(false)]
        public String Object { get; set; }
        private int? _LogInst = 0;
        [ScaffoldColumn(false)]
        public int? LogInst { get => _LogInst; set => _LogInst = value; }
    }
}
