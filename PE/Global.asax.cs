﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace PortalEmpleadosV2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        void Application_Error(object sender, EventArgs e)
        {     // Code that runs when an unhandled error occurs     
            Exception ex = Server.GetLastError();
            String detalle = String.Empty;
            if(ex !=null && ex.InnerException != null)
            {
                detalle = ex.InnerException.Message.ToString();
            }
            HttpContext httpContext = ((MvcApplication)sender).Context;
            String urlError = httpContext.Request.AppRelativeCurrentExecutionFilePath.ToString();
            //var senderInfo = (ASP.global_asax)sender;
            if(ex !=null)
            {
                this.Session["LastErrorDetail"] = ex.StackTrace.ToString();
                this.Session["LastError"] = ex.Message.ToString() + Environment.NewLine + detalle + Environment.NewLine + urlError; //store the error for later     
            }else
            {
                this.Session["LastError"] = "Error Indeterminado";
            }

            
            Server.ClearError(); //clear the error so we can continue onwards     
            Response.Redirect("~/CustomError/Index"); //direct user to error page }
        }
    }
}