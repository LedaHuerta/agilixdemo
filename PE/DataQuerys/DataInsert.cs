﻿using Aplics.Servicios.Modelos.Nominix;
using PortalEmpleadosV2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PortalEmpleadosV2.DataQuerys
{
    public class DataInsert
    {

        private NominixContext db = new NominixContext();

        //CREAMOS EL REGISTRO DE SOLICITUD A TRAVES DE UN SP (tabla 513)
        public int CrearSolicitud(string Empleado, DateTime fechaSolicitud, int diasSolicitud, DateTime fechaInicial, DateTime fechaFinal, string TipoSolicitud, DateTime fechaRegreso)
        {
            var parametro3 = new SqlParameter();
            parametro3.ParameterName = "@parametro3";
            parametro3.Direction = ParameterDirection.Output;
            parametro3.SqlDbType = SqlDbType.Int;

            string finicial = fechaInicial.ToString("yyyyMMdd");
            string ffinal = fechaFinal.ToString("yyyyMMdd");
            string fsolicitud = fechaSolicitud.ToString("yyyyMMdd");
            string fregreso = fechaRegreso.ToString("yyyyMMdd");



            var solicitudC = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                "sp_NuevaSolicitud @Empleado, @fechaSolicitud, @diasSolicitud, @fechaInicial, @fechaFinal, @TipoSolicitud, @fechaRegreso, @parametro3 OUT",
                new SqlParameter("@Empleado", Empleado),
                new SqlParameter("@fechaSolicitud", fsolicitud),
                new SqlParameter("@diasSolicitud", diasSolicitud),
                new SqlParameter("@fechaInicial", finicial),
                new SqlParameter("@fechaFinal", ffinal),
                new SqlParameter("@TipoSolicitud", TipoSolicitud),
                new SqlParameter("@fechaRegreso", fechaRegreso),
                parametro3);

            int resultado = int.Parse(parametro3.Value.ToString());

            return resultado;
        }

        //APROBAMOS LA SOLICITUD EJECUTANDO UN SP QUE TAMBIEN ACTUALIZA MAS TABLAS.
        public int sp_AprobarSolicitud(int idRegistro)
        {
            var parametro3 = new SqlParameter();
            parametro3.ParameterName = "@parametro3";
            parametro3.Direction = ParameterDirection.Output;
            parametro3.SqlDbType = SqlDbType.Int;

            var solicitudC = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                "sp_AprobarSolicitud @idSolicitud, @parametro3 OUT",
                new SqlParameter("@idSolicitud", idRegistro),
                parametro3);

            int resultado = int.Parse(parametro3.Value.ToString());

            return resultado;
        }

        //RECHAZAMOS LA SOLICITUD EJECUTANDO UN SP QUE TAMBIEN ACTUALIZA MAS TABLAS.
        public int sp_RechazarSolicitud(int idRegistro,string motivorechazo = "Null")
        {
            var parametro3 = new SqlParameter();
            parametro3.ParameterName = "@parametro3";
            parametro3.Direction = ParameterDirection.Output;
            parametro3.SqlDbType = SqlDbType.Int;

            var solicitudC = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                "sp_RechazarSolicitud @idSolicitud, @motivoRechazo, @parametro3 OUT",
                new SqlParameter("@idSolicitud", idRegistro),
                new SqlParameter("@motivoRechazo", motivorechazo),
                parametro3);

            int resultado = int.Parse(parametro3.Value.ToString());

            return resultado;
        }

        //OBTENEMOS DATA GENERAL DEL LOGIN
        public List<LoginAcceso> sp_ObtenDataLogin(string empleado, string password)
        {

            String sql = @"select e.Code as NoEmp, 
            e.U_NOMP + ' ' + e.U_APEPAT + ' ' + e.U_APEMAT as NombreEmpleado,
            year(convert(datetime,e.U_FIN)) as AnioIngreso,
            (case when d.U_TIPUSU is not null then d.U_TIPUSU else '0' end) as TipoRol, d.U_PWD as Password, d.DocEntry as UserSign
            from [@AST_011] e
            left join [@AST_D21] d on d.U_EMP = e.Code
            where e.Code = '" + empleado + "' and U_STAT = 'A' and d.U_PWD like '" + password + "'";

            try
            {
                var Listado = db.Database.SqlQuery<LoginAcceso>(sql).ToList();
                return Listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            
            //return Listado;
        }

        //OBTENEMOS DATA GENERAL DEL LOGIN
        public List<LoginAcceso> sp_ObtenDataLoginAD(string eMail)
        {

            String sql = @"select e.Code as NoEmp, 
            e.U_NOMP + ' ' + e.U_APEPAT + ' ' + e.U_APEMAT as NombreEmpleado,
            year(convert(datetime,e.U_FIN)) as AnioIngreso,
            (case when d.U_TIPUSU is not null then d.U_TIPUSU else '0' end) as TipoRol, d.U_PWD as Password, d.DocEntry as UserSign
            from [@AST_011] e
            inner join [@AST_D21] d on d.U_EMP = e.Code
            where d.U_EMAIL = '" + eMail + "' and U_STAT = 'A'";
            try
            {
                var Listado = db.Database.SqlQuery<LoginAcceso>(sql).ToList();
                return Listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}