﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GRC.Servicios.Data.DAO.EF;
using GRC.Servicios.Utilidades.Genericos;
using Aplics.Servicios.Modelos.Solicitudes;
using Aplics.Servicios.Entidades.Solicitudes;

using Aplics.Servicios.Entidades.DAO;
using AutoMapper;
using GRC.Servicios.Utilidades;
using Aplics.Servicios.Entidades.DAO.Nominix;
using GRC.Mensajeria.Modelos;
using Aplics.Servicios.Negocio.Nominix;
using Aplics.Servicios.Modelos.Nominix;
using GRC.Servicios.ClienteRestNF;
using ControldeHorarios.Models;
//using System.Threading.Tasks;
using Aplics.Servicios.Modelos;

namespace Aplics.Servicios.Negocio.Solicitudes
{
   public class NegocioSolicitudAutorizacionRechazo
    {

		private int _UserSign = 0;
		//Crear Mapeos
		public NegocioSolicitudAutorizacionRechazo()
		{

		}

		public NegocioSolicitudAutorizacionRechazo(int pUserSign)
		{
			_UserSign = pUserSign;
		}



		public async Task<RespuestaSimple> actualizarAutorizacionRechazo(SolicitudXAprobar solicitudXAprobar, SendMails sm,String empleadoRH,int rol, String urlAPINominix = "") 
		{
			RespuestaSimple respuesta = new RespuestaSimple();
			//Error por default.
			respuesta.result = 0;
			respuesta.mensaje = String.Empty;

			try
			{
				DAOCRUDNominixTD<TablaSolicitudes> _repositorio = new CreateDAO().GeneraDAOSolicitudes(_UserSign);

				TablaSolicitudes solicitud = new TablaSolicitudes();
				QueryParameters<TablaSolicitudes> parametros = new QueryParameters<TablaSolicitudes>();
				parametros.where = x => x.DocEntry.Equals(solicitudXAprobar.Id);

				solicitud = _repositorio.EncontrarPor(parametros).FirstOrDefault();

				DAOCRUDNominixTM<Usuarios> _repositorioU = new CreateDAO().GeneraDAOUsuarios(_UserSign);

				QueryParameters<Usuarios> parametrosu = new QueryParameters<Usuarios>();
				parametrosu.where = x => x.U_EMP.Equals(solicitudXAprobar.EmpleadoAutoriza);
				Usuarios usuario= _repositorioU.EncontrarPor(parametrosu).FirstOrDefault();

				Boolean aprobado = solicitudXAprobar.Aprobar;
				solicitud.U_SRH = aprobado ? "A" : "R";
				solicitud.U_SSUP = aprobado ? "A" : "R";
				solicitud.U_STSV = aprobado ? "A" : "R";
                if (aprobado == true)
                {
                    solicitud.U_SNOMINIX = "S";
                }

				_repositorioU.Dispose();

				solicitud.U_MOTR = solicitudXAprobar.MotivoRechazo;
				solicitud.U_OBS = solicitudXAprobar.Observaciones;
				_repositorio.Actualizar(solicitud);
				_repositorio.Dispose();
				
				if(aprobado)
                {
                    //await registrarAprobacionSolicitud(urlAPINominix, solicitud);

                }

				Entidades.DAO.Nominix.Empleados curEmpleado = NominixGenerales.ObtenerEmpleado(solicitud.U_EMP, _UserSign);
				String nombreEmpleado = curEmpleado.Code + "-" + curEmpleado.U_Nomp + " " + curEmpleado.U_Apepat + " " + curEmpleado.U_Apemat;

				respuesta.result = 1;
				
				String emailsNotificacion = String.Empty;
				String empleadosfiltro = NominixGenerales.empleadoRelaciones(solicitud.U_EMP, _UserSign);
				if (empleadosfiltro.Equals(String.Empty))
				{
					empleadosfiltro = solicitud.U_EMP;
				} else
				{
					empleadosfiltro = "'" + solicitud.U_EMP + "'," + empleadosfiltro;
				}

				emailsNotificacion = NominixGenerales.ObtenerCorreosEmpleados(empleadosfiltro, _UserSign);

				//String empleadoNotificar = NominixGenerales.empleadoNotificar(solicitud.U_CON, solicitudXAprobar.EmpleadoAutoriza, empleadoRH, _UserSign);

				String statusSol = aprobado ? "aprobada" : "rechazada";
				
				respuesta = NominixGenerales.notificar(_UserSign ,sm, "Aprobación/Rechazo de Solicitudes.",
					"Este mensaje ha sido envíado de forma automática y no requiere una respuesta.",
					solicitudXAprobar.EmpleadoAutoriza, solicitudXAprobar.NumEmpleado,
					"La solicitud con folio " + solicitud.DocEntry.ToString() + " de " + nombreEmpleado + " ha sido " + statusSol + ".", 
					String.Empty, empleadosfiltro, emailsNotificacion,"PERMISOS",rol, solicitud.DocEntry.ToString());

			}
			catch (Exception ex)
			{
				respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		async Task<ResultService> registrarAprobacionSolicitud (String urlAPINominix, TablaSolicitudes solicitud)
        {
			//String result = String.Empty;
			ResultService result = new ResultService();
			try
            {
				//aqui se construye el token
				//aqui se llama el API, para actualizar la tabla de solicitudesNominix POST
				//Crear un cliente del tipo del objeto :
				ClienteRest<ResultService> cliente = new ClienteRest<ResultService>();
				//Hacer POST al Api:
				ResultService respuestaServicio = await cliente.LLamarServicioPostGeneral(urlAPINominix, "SolicitudesNominix/", solicitud);
				result.resultado = 1;
			}
			catch(Exception ex)
            {
				result.Mensaje = ex.Message.ToString();
			}
			return result;
		}
	}
}
