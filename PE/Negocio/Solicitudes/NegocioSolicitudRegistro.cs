﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRC.Servicios.Data.DAO.EF;
using GRC.Servicios.Utilidades.Genericos;
using Aplics.Servicios.Entidades.Solicitudes;
using Aplics.Servicios.Modelos.Solicitudes;

using Aplics.Servicios.Entidades.DAO;
using AutoMapper;
using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Negocio.Nominix;
using Aplics.Servicios.Entidades.DAO.Nominix;
using GRC.Servicios.Utilidades;

namespace Aplics.Servicios.Negocio.Solicitudes
{

    public class NegocioSolicitudRegistro
	{

		private int _UserSign = 0;
		//Crear Mapeos
		IMapper mapper = new Mapeado().config.CreateMapper();
		public NegocioSolicitudRegistro()
		{

		}

		public NegocioSolicitudRegistro(int pUserSign)
		{
			_UserSign = pUserSign;
		}

        public int DiasNaturalesContador(DateTime fechaInicio, DateTime fechaFin)
        {
            System.TimeSpan diffResult = fechaFin.Subtract(fechaInicio);
            int difDays = diffResult.Days + 1;
            return difDays;
        }

        public int DiasHabilesContador(DateTime fechaInicio, DateTime fechaFin)
        {
            var diasHabiles = new[]
            {
                DayOfWeek.Monday,
                DayOfWeek.Tuesday,
                DayOfWeek.Wednesday,
                DayOfWeek.Thursday,
                DayOfWeek.Friday,
            };

            IEnumerable<DateTime> RangoDeFechas()
            {
                var currentDate = fechaInicio;
                while (currentDate < fechaFin)
                {

                    //BUSCAMOS EL CURRENT DATE EN LA TABLA 161 QUE ES LA DE LOS DIAS FESTIVOS
                    //SI ES FESTIVO NO SE CONTEMPLA

                    //var total = db.C_AST_161.Where(x => x.U_FFI <= currentDate && x.U_FFF >= currentDate).Count();

                    //if (total == 0)
                    //{
                    //    yield return currentDate;
                    //}

                    yield return currentDate;

                    currentDate = currentDate.AddDays(1);
                }
            }

            return RangoDeFechas().Count(x => diasHabiles.Contains(x.DayOfWeek));
        }

        public bool EmpalmeDiasValidacion(DateTime fechaInicio, DateTime fechaFin, string concepto,string empleado)
        {
            bool res = false;

            DAOSolicitudDashBoard _repo = new CreateDAO().EmpalmeFechas(_UserSign);
            List<EmpalmeFechas> Empalme = _repo.ValidacionEmpalmeFechas(fechaInicio, fechaFin, concepto,empleado);
            
            if (Empalme[0].TotalResultado > 0)
            {
                res = true;
            }

            return res;
        }

        //public RespuestaData<Solicitud> registrarSolicitud(Solicitud solicitud, SendMails sm,String empleadoRH) 
        public RespuestaData<Solicitud> registrarSolicitud(Solicitud solicitud, SendMails sm, String empleadoRH,int rol, String mediosDias = "0")
        {
			RespuestaData<Solicitud> respuesta = new RespuestaData<Solicitud>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;

			try
			{
				DAOCRUDNominixTD<TablaSolicitudes> _repositorio = new CreateDAO().GeneraDAOSolicitudes(_UserSign);
				TablaSolicitudes tablaSolicitudes = mapper.Map<TablaSolicitudes>(solicitud);

				Empleados empleado = NominixGenerales.ObtenerEmpleado(solicitud.NumEmpleado, _UserSign);
				tablaSolicitudes.U_EMPSUP = String.IsNullOrEmpty(empleado.U_Empsup) ? "" : empleado.U_Empsup;
				tablaSolicitudes.U_CIA = String.IsNullOrEmpty(empleado.U_Cia) ? ""  : empleado.U_Cia;
				tablaSolicitudes.U_LOC = String.IsNullOrEmpty(empleado.U_Loc) ? "" : empleado.U_Loc;

                //no tomar en cuenta fines de semana y dias festivos
                if (solicitud.TipoSolicitud == "404")
                {
                    tablaSolicitudes.U_DSOL = (DiasNaturalesContador(Convert.ToDateTime(tablaSolicitudes.U_FIV), Convert.ToDateTime(tablaSolicitudes.U_FTV)));
                    if (tablaSolicitudes.U_DSOL < 6)
                    {
                        tablaSolicitudes.U_DSOL = (DiasHabilesContador(Convert.ToDateTime(tablaSolicitudes.U_FIV), Convert.ToDateTime(tablaSolicitudes.U_FTV))) + 1;
                    }
                } else
                {
                    String tipoDia = String.Empty;
                    //hacer consulta a la tabla query parameters
                    CriteriosConceptosH criteriosH = new CriteriosConceptosH();
                   
                    DAOCRUDNominixTLM<CriteriosConceptosH> _repositorioCriteriosH = new CreateDAO().GeneraDAOCriteriosConceptosHeader(_UserSign);
                    QueryParameters<CriteriosConceptosH> parametrosch = new QueryParameters<CriteriosConceptosH>();
                    parametrosch.where = x => x.U_ACTIVO == true && x.Code.Equals(solicitud.TipoSolicitud);
                    parametrosch.orderBy = x => x.U_IFVAC;
                    criteriosH = _repositorioCriteriosH.EncontrarPor(parametrosch).FirstOrDefault();
                    _repositorioCriteriosH.Dispose();
                    tipoDia = criteriosH.U_TDIAS;

                    if (tipoDia.Equals("H"))
                    {
                        tablaSolicitudes.U_DSOL = (DiasHabilesContador(Convert.ToDateTime(tablaSolicitudes.U_FIV), Convert.ToDateTime(tablaSolicitudes.U_FTV))) + 1;

                    }
                    else
                    {
                        tablaSolicitudes.U_DSOL = (DiasNaturalesContador(Convert.ToDateTime(tablaSolicitudes.U_FIV), Convert.ToDateTime(tablaSolicitudes.U_FTV)));

                    }
                }


                if (mediosDias.Equals("1"))
                {
                    tablaSolicitudes.U_DSOL = 1;
                }else if (mediosDias.Equals("2"))
                {
                    tablaSolicitudes.U_DSOL = 2;
                }

                tablaSolicitudes =_repositorio.AgregarIdentity(tablaSolicitudes);
				_repositorio.Dispose();
				solicitud.Id = tablaSolicitudes.DocEntry;

				Entidades.DAO.Nominix.Empleados curEmpleado = NominixGenerales.ObtenerEmpleado(solicitud.NumEmpleado, _UserSign);
				String nombreEmpleado = curEmpleado.Code + "-" + curEmpleado.U_Nomp + " " + curEmpleado.U_Apepat + " " + curEmpleado.U_Apemat;

				String emailsNotificacion = String.Empty;
				String empleadosfiltro = NominixGenerales.empleadoRelaciones(solicitud.NumEmpleado, _UserSign);
				if (empleadosfiltro.IndexOf(curEmpleado.U_Empsup) < 0)
				{
                    empleadosfiltro += empleadosfiltro.Length > 0 ? "," : "";
					empleadosfiltro += "'" + curEmpleado.U_Empsup + "'";
				}

                if (empleadosfiltro.Length > 0)
                {
                    emailsNotificacion = NominixGenerales.ObtenerCorreosEmpleados(empleadosfiltro, _UserSign);
                }

				//emailsNotificacion = NominixGenerales.ObtenerCorreosEmpleados(empleadosfiltro, _UserSign);

				String empleadoNotificar = NominixGenerales.empleadoNotificar(solicitud.TipoSolicitud,curEmpleado.U_Empsup, empleadoRH, _UserSign);
                //RespuestaSimple respuestaNot = NominixGenerales.notificar(_UserSign, sm, "Solicitud de permisos.",
                //	"Este mensaje ha sido envíado de forma automática y no requiere una respuesta.",
                //	solicitud.NumEmpleado, empleadoNotificar, nombreEmpleado + " ha enviado una solicitud con folio " + solicitud.Id.ToString()  ,
                //	String.Empty, empleadosfiltro, emailsNotificacion,"PERMISOS");
                RespuestaSimple respuestaNot = NominixGenerales.notificar(_UserSign, sm, "Solicitud de permisos.",
                    "Este mensaje ha sido envíado de forma automática y no requiere una respuesta.",
                    solicitud.NumEmpleado, empleadoNotificar, nombreEmpleado + " ha enviado una solicitud con folio " + solicitud.Id.ToString(),
                    String.Empty, empleadosfiltro, emailsNotificacion, "PERMISOS",rol, solicitud.Id.ToString());

                respuesta.Datos = solicitud;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}



        public List<DatosSolicitud> ObtenerConceptosNotif(List<DatosSolicitud> ListaSolicitudes, String usuarioNotifica, String empSup ="")
        {
            List<DatosSolicitud> notifica = new List<DatosSolicitud>();
            List<CriteriosConceptosH> critConceptosH = new List<CriteriosConceptosH>();
            critConceptosH = obtenerConceptos();
            try
            {
                var listaNotifica = (from x in ListaSolicitudes
                                     join conceptos in critConceptosH
                                     //tipo de concepto
                                     on x.TipoSolicitud equals conceptos.Code
                                     where (conceptos.U_NOTIF.Equals(usuarioNotifica))
                                     select new DatosSolicitud()
                                     {
                                         Categoria = x.Categoria,
                                         Descripcion = x.Descripcion,
                                         Dias = x.Dias,
                                         Id = x.Id,
                                         NombreEmpleado = x.NombreEmpleado,
                                         NumEmpleado = x.NumEmpleado,
                                         Puesto = x.Puesto,
                                         StatusSup = x.StatusSup,
                                         StatusRH = x.StatusRH,
                                         Superior = x.Superior,
                                         TipoSolicitud = x.TipoSolicitud,
                                         Observaciones = x.Observaciones
                                     }
                                     ).ToList();
                if(usuarioNotifica.Equals("SUP"))
                {
                    List<Empleados> listaSubordinados = NominixGenerales.ObtenerSubordinados(empSup,0);
                    listaNotifica = (from x in listaNotifica
                                     join subordinado in listaSubordinados
                                     //subornidados
                                     on x.NumEmpleado equals subordinado.Code
                                     select new DatosSolicitud()
                                     {
                                         Categoria = x.Categoria,
                                         Descripcion = x.Descripcion,
                                         Dias = x.Dias,
                                         Id = x.Id,
                                         NombreEmpleado = x.NombreEmpleado,
                                         NumEmpleado = x.NumEmpleado,
                                         Puesto = x.Puesto,
                                         StatusSup = x.StatusSup,
                                         StatusRH = x.StatusRH,
                                         Superior = x.Superior,
                                         TipoSolicitud = x.TipoSolicitud,
                                         Observaciones = x.Observaciones
                                     }
                                     ).ToList();
                }
                notifica = listaNotifica;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return notifica;
        }



        public List<CriteriosConceptosH> obtenerConceptos()
        {
            List<CriteriosConceptosH> criteriosH = new List<CriteriosConceptosH>();
            try
            {
                //Tabla de criterios Header.
                DAOCRUDNominixTLM<CriteriosConceptosH> _repositorioCriteriosH = new CreateDAO().GeneraDAOCriteriosConceptosHeader(_UserSign);
                QueryParameters<CriteriosConceptosH> parametrosch = new QueryParameters<CriteriosConceptosH>();
                parametrosch.where = x => x.U_ACTIVO == true;
                criteriosH = _repositorioCriteriosH.EncontrarPor(parametrosch).ToList();
                _repositorioCriteriosH.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return criteriosH;

        }

        public CriteriosConceptosH obtenerCriterioConcepto(String concepto)
        {
            CriteriosConceptosH criteriosH = new CriteriosConceptosH();
            try
            {
                //Tabla de criterios Header.
                DAOCRUDNominixTLM<CriteriosConceptosH> _repositorioCriteriosH = new CreateDAO().GeneraDAOCriteriosConceptosHeader(_UserSign);
                QueryParameters<CriteriosConceptosH> parametrosch = new QueryParameters<CriteriosConceptosH>();
                parametrosch.where = x => x.U_ACTIVO == true && x.Code.Equals(concepto);
                criteriosH = _repositorioCriteriosH.EncontrarPor(parametrosch).FirstOrDefault();
                _repositorioCriteriosH.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return criteriosH;

        }
    }
}
