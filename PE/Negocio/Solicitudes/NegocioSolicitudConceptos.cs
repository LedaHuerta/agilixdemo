﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Aplics.Servicios.Entidades.DAO;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Entidades.Solicitudes;
using Aplics.Servicios.Modelos.Solicitudes;
using GRC.Servicios.Data.DAO.EF;
using GRC.Servicios.Utilidades.Genericos;

namespace Aplics.Servicios.Negocio.Solicitudes
{
   public class NegocioSolicitudConceptos
    {

		private int _UserSign = 0;
		public NegocioSolicitudConceptos()
		{

		}

		public NegocioSolicitudConceptos(int pUserSign)
		{
			_UserSign = pUserSign;
		}

		public RespuestaData<List<ItemSolicitiud>> ListaConceptos(String NumEmpleado)
		{
			RespuestaData<List<ItemSolicitiud>> respuesta = new RespuestaData<List<ItemSolicitiud>>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
				List<ItemSolicitiud> Listado = new List<ItemSolicitiud>();
				DAOConceptos _repositorio = new CreateDAO().GeneraDAOSolicitudConceptos(_UserSign);
				Listado = _repositorio.ListaConceptosAusentismo();

				///TODO: FILTRAR CON LAS POLITICAS Y DERECHOS DEL EMPLEADO
				_repositorio.Dispose();
				respuesta.Datos = Listado;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}


		private void verificarConcepto(String concepto, List<Empleados> empleados, List<AusentismoNominaTotales> ListaAusentismoNominaTotales)
		{

		}

		public RespuestaData<List<ItemSolicitiud>> ActualizarSaldosConceptos()
		{
			RespuestaData<List<ItemSolicitiud>> respuesta = new RespuestaData<List<ItemSolicitiud>>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{

				//Cataálogo de Conceptos.
				DAOCRUDNominixTM<Conceptos> _repoConceptos= new CreateDAO().GeneraDAOConceptos(_UserSign);
				QueryParameters<Conceptos> parametroscn = new QueryParameters<Conceptos>();
				parametroscn.where = x => x.Canceled!='Y';
				List<Conceptos> catConceptos= _repoConceptos.EncontrarPor(parametroscn).ToList();

				//Catálogo de perstaciones.
				DAOCRUDNominixTM<GrupoPrestaciones> _repogp = new CreateDAO().GeneraDAOGrupoPrestaciones(_UserSign);
				QueryParameters<GrupoPrestaciones> parametrosgp = new QueryParameters<GrupoPrestaciones>();
				parametrosgp.where = x => x.U_IFVAC != "";
				List<GrupoPrestaciones> catGruposPrestaciones = _repogp.EncontrarPor(parametrosgp).ToList();

				//Tabla de criterios Header.
				DAOCRUDNominixTLM<CriteriosConceptosH> _repositorioCriteriosH = new CreateDAO().GeneraDAOCriteriosConceptosHeader(_UserSign);
				QueryParameters<CriteriosConceptosH> parametrosch = new QueryParameters<CriteriosConceptosH>();
				parametrosch.where = x => x.U_ACTIVO == true;
				List<CriteriosConceptosH> criteriosH = _repositorioCriteriosH.EncontrarPor(parametrosch).ToList();
				_repositorioCriteriosH.Dispose();

				//Tabla de criterios Detalle.
				DAOCRUDNominixTLM<CriteriosConceptos> _repositorioCriterios = new CreateDAO().GeneraDAOCriteriosConceptos(_UserSign);
				QueryParameters<CriteriosConceptos> parametros = new QueryParameters<CriteriosConceptos>();
				parametros.where = x => x.U_ACTIVO==true;
				List<CriteriosConceptos> criterios = _repositorioCriterios.EncontrarPor(parametros).ToList();
				_repositorioCriterios.Dispose();

				//Tabla de empleados.
				DAOCRUDNominixTM<Empleados> repoempleados = new CreateDAO().GeneraDAOEmpleados(_UserSign);
				QueryParameters<Empleados> parametrosEmp = new QueryParameters<Empleados>();
				parametrosEmp.where = x => x.U_Stat.Equals("A");
				List<Empleados> empleados = repoempleados.EncontrarPor(parametrosEmp).ToList();

				//Tabla de Saldos
				DAOCRUDNominixTLM <SaldosConceptos> _repositorio = new CreateDAO().GeneraDAOSaldosConceptos(_UserSign);
				QueryParameters<SaldosConceptos> parametrosEmpConcepto = new QueryParameters<SaldosConceptos>();

				//Tabla de ausentismo.
				DAOAusentismo dAOAusentismo=new CreateDAO().GeneraDAOAusentismo(_UserSign);
				List<AusentismoNominaTotales> ListaAusentismoNominaTotales = dAOAusentismo.Ausentismo();
				AusentismoNominaTotales ausentismoNominaTotales = new AusentismoNominaTotales();

				String valor = String.Empty;
				Boolean tieneDrecho = false;
				DateTime? fechaAntiguedad;
				List<String> valores = new List<String>();
				String concepto = String.Empty;
				decimal? saldo = 0.00M;
				short? disfrutados = 0;


				List<CriteriosConceptos> criteriosGrupo = new List<CriteriosConceptos>();
				foreach (Empleados empleado in empleados)
				{
					tieneDrecho = true;
					fechaAntiguedad = empleado.U_Fan;
					foreach (CriteriosConceptosH criterioheader in criteriosH)
					{
						concepto = criterioheader.Code;
						criteriosGrupo = criterios.Where(x => x.U_IFVAC == criterioheader.U_IFVAC).ToList();

						if (empleado.U_Ifavac.Equals(criterioheader.U_IFVAC))
						{
							foreach (CriteriosConceptos criterio in criteriosGrupo)
							{
								valor = GetColumn(empleado, criterio.U_CAMPO);
								if (criterio.U_MULTIPLE)
								{
									valores = criterio.U_VALOR.Split(',').ToList();
									if (!valores.Contains(valor))
									{
										tieneDrecho = false;
										break;
									}
								}
								else
								{
									if (!valor.Equals(criterio.U_VALOR))
									{
										tieneDrecho = false;
										break;
									}
								}
							}

							if (tieneDrecho)
							{
								ausentismoNominaTotales = ListaAusentismoNominaTotales.Where(x => x.U_CON.Equals(concepto) && x.U_EMP.Equals(empleado.Code)).FirstOrDefault();
								if (ausentismoNominaTotales != null)
								{
									disfrutados = ausentismoNominaTotales.U_DAPL;
								}
								if (String.IsNullOrEmpty(criterioheader.U_CLAVEANT))
								{
									saldo = criterioheader.U_UNID - (decimal)disfrutados;
								}
								else
								{
									//calcular
								}

								_repositorio = new CreateDAO().GeneraDAOSaldosConceptos(_UserSign);
								parametrosEmpConcepto.where = x => x.U_CON.Equals(concepto) && x.Code.Equals(empleado.Code);
								SaldosConceptos sc = _repositorio.EncontrarPor(parametrosEmpConcepto).FirstOrDefault();

								_repositorio = new CreateDAO().GeneraDAOSaldosConceptos(_UserSign);
								if (sc != null)
								{
									//calcular
									sc.U_SALDO = saldo;
									_repositorio.Actualizar(sc);
								}
								else
								{
									sc = new SaldosConceptos();
									sc.Code = empleado.Code;
									sc.U_CON = concepto;
									sc.U_SALDO = saldo;
									sc = _repositorio.AgregarIdentity(sc);
								}
							}
						}
					}
				}
				_repositorio.Dispose();
				//respuesta.Datos = Listado;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}


		private String GetColumn(Empleados empleado, string columnName)
		{
			String result = String.Empty;
			object value = empleado.GetType().GetProperty(columnName).GetValue(empleado);
			if (value != null) { result = value.ToString(); }
			return result;
		}


	}
}
