﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aplics.Servicios.Entidades.DAO;
using Aplics.Servicios.Entidades.Solicitudes;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Solicitudes;
using Aplics.Servicios.Modelos.Nominix;
using GRC.Servicios.Utilidades.Genericos;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Negocio.Nominix;
using GRC.Servicios.ClienteRestNF;
using ControldeHorarios.Models;
using Aplics.Servicios.Entidades.DAO;

namespace Aplics.Servicios.Negocio.Solicitudes
{
    public class NegocioSolicitudDashBoard
    {

        private int _UserSign = 0;
        //Crear Mapeos
        public NegocioSolicitudDashBoard()
        {

        }

        public NegocioSolicitudDashBoard(int pUserSign)
        {
            _UserSign = pUserSign;
        }


        public RespuestaData<List<ItemSolicitiud>> ListaConceptos(String NumEmpleado)
        {
            RespuestaData<List<ItemSolicitiud>> respuesta = new RespuestaData<List<ItemSolicitiud>>();
            //Error por default.
            respuesta.Respuesta.result = 0;
            respuesta.Respuesta.mensaje = String.Empty;
            try
            {
                List<ItemSolicitiud> Listado = new List<ItemSolicitiud>();

                Dashboard dasboard = buscarDerechosEmpleado(NumEmpleado);
                foreach (DashboardSolicitud permiso in dasboard.Solicitudes)
                {
                    Listado.Add(new ItemSolicitiud() { Id = permiso.Id, Descripcion = permiso.Descripcion, Politica = true, Tope = permiso.DiasCorrespondientes });
                }
                respuesta.Datos = Listado;
                respuesta.Respuesta.result = 1;
            }
            catch (Exception ex)
            {
                respuesta.Respuesta.mensaje = ex.Message.ToString();
            }
            return respuesta;
        }

		public List<DatosSolicitud> SolicitdesEmpleadosGeneral(String Concepto, int Anio=0)
		{
			List<DatosSolicitud> solicitudes = new List<DatosSolicitud>();

            try
			{
                //Crear repo para obtener una lista de solicitudes generales:
				DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOSolicitudDashBoard(_UserSign);
				solicitudes = _repo.ListaSolicitudesGeneral(Concepto, 0,Anio);
                //Crear DAO para obtener usuarioNotifica, filtrando por parametros:
                //CreateDAO DAO = new CreateDAO();
                //DAOCRUDNominixTLM<CriteriosConceptosH> _repoCriteriosH = DAO.GeneraDAOCriteriosConceptosHeader(_UserSign);
                //QueryParameters<CriteriosConceptosH> parameters = new QueryParameters<CriteriosConceptosH>();
                //parameters.where = x => x.U_NOTIF != "";
                //List<CriteriosConceptosH> usuarioNotifica = _repoCriteriosH.EncontrarPor(parameters).ToList();
                //Se instancia la clase de negocio y si usuarioNotifica es mayor que 0,
                //se recorre la lista para obtener cada parametro y llamar el metodo que obtiene la lista de
                //solicitudes de los subordinados
                //NegocioSolicitudRegistro negocioSolicitud = new NegocioSolicitudRegistro();
                //if(usuarioNotifica.Count() > 0)
                //{
                //    foreach (var item in usuarioNotifica)
                //    {
                //        solicitudes = negocioSolicitud.ObtenerConceptosNotif(solicitudes, item.U_NOTIF, empSup);
//
  //                  }
    //            }
                //_repoCriteriosH.Dispose();
            }
			catch (Exception ex)
			{
				throw ex;
			}
			return solicitudes;
		}

		public List<DatosSolicitud> SolicitdesEmpleados(String Concepto, String cveEmp = "")
        {
            List<DatosSolicitud> solicitudes = new List<DatosSolicitud>();
            try
            {
                DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOSolicitudDashBoard(_UserSign);
                solicitudes = _repo.ListaSolicitudes(Concepto, 0, cveEmp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return solicitudes;
        }
        //NUEVO CPHA
        public List<DatosSolicitud> HistorialSolicitdesEmpleados(String Empleado, String Anio="",string conceptos="")
        {
            List<DatosSolicitud> solicitudes = new List<DatosSolicitud>();
            try
            {
                DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOHistorialSolicitudDashBoard(_UserSign);
                solicitudes = _repo.ListaHistorialSolicitudes(Empleado, Anio,conceptos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return solicitudes;
        }
        public DatosSolicitud SolicitdEmpleado(int numSolicitud)
        {
            DatosSolicitud solicitud = new DatosSolicitud();
            try
            {
                List<DatosSolicitud> solicitudes = new List<DatosSolicitud>();
                DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOSolicitudDashBoard(_UserSign);
                solicitudes = _repo.ListaSolicitudes(String.Empty, numSolicitud);
                if (solicitudes.Count > 0)
                {
                    solicitud = solicitudes[0];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return solicitud;
        }

        //NUEVO CPHA
        private Dashboard buscarHistorialEmpleado(String NumEmpleado)
        {
            Dashboard dashboard = new Dashboard();
            dashboard.NumEmpleado = NumEmpleado;
            try
            {
                List<DashboardSolicitud> dashboardSolicitudes = new List<DashboardSolicitud>();
                List<TablaSolicitudes> Listado = new List<TablaSolicitudes>();

                DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOCardsHistorial(_UserSign);
                List<DatosSolicitudCardsHistorico> solicitudesCardsHistorico = _repo.ListaSolicitudesCardsHistorico(NumEmpleado);

                DashboardSolicitud dashboardSolicitud = new DashboardSolicitud();

				DAOCRUDNominixTD<EdoCuentaVacaciones> repoEstadoCuentaVacaciones = new CreateDAO().GeneraDAOEdoCuentaVacaciones(_UserSign);
				QueryParameters<EdoCuentaVacaciones> parametros = new QueryParameters<EdoCuentaVacaciones>();
				parametros.where = x => x.U_EMP.Equals(NumEmpleado);
				parametros.orderByDesc = x => x.U_FAV;
				List<EdoCuentaVacaciones> EstadoCuentaEmpleado = repoEstadoCuentaVacaciones.EncontrarPor(parametros).ToList();
				int anioAnt = 0;
				DatosSolicitudCardsHistorico item = new DatosSolicitudCardsHistorico();
				decimal DiasDisfrutados = 0.00M;
				decimal DiasPorAprobar = 0.00M;
				decimal DiasDisponibles = 0.00M;
				repoEstadoCuentaVacaciones.Dispose();
				foreach (EdoCuentaVacaciones cuentaVacaciones in EstadoCuentaEmpleado)
				{
					if (anioAnt!= cuentaVacaciones.U_FAV.Value.Year)
					{
						DiasDisfrutados = 0.00M;
						DiasPorAprobar = 0.00M;
						DiasDisponibles = 0.00M;
						anioAnt = cuentaVacaciones.U_FAV.Value.Year;
						item = solicitudesCardsHistorico.Where(x => x.ANIO == anioAnt).FirstOrDefault();
						if (item != null)
						{
							DiasDisfrutados = item.DDisfrutados;
							DiasPorAprobar = item.DSOL_TOTAL;
							if (cuentaVacaciones.U_FAV.Value.AddYears(1).Year>=System.DateTime.Today.Year)
							{
								DiasDisponibles = (decimal)cuentaVacaciones.U_DVAC - DiasDisfrutados;
							}
						}
						dashboardSolicitud = new DashboardSolicitud();
						dashboardSolicitud.ColordeFondo = "White";
						dashboardSolicitud.ColordeTexto = "Black";
						dashboardSolicitud.Comentario = String.Empty;
						dashboardSolicitud.Descripcion = "Vacaciones";
						dashboardSolicitud.DiasCorrespondientes = (decimal)cuentaVacaciones.U_DVAC;
						dashboardSolicitud.DiasDisfrutados = DiasDisfrutados;
						dashboardSolicitud.DiasDisponibles = DiasDisponibles;
						dashboardSolicitud.DiasPorAprobar = DiasPorAprobar;
						dashboardSolicitud.Id = anioAnt.ToString();
						dashboardSolicitudes.Add(dashboardSolicitud);
					}
				}

                int indexAnio = 0;
                Empleados curEmpleado = NominixGenerales.ObtenerEmpleado(NumEmpleado, _UserSign);
                for (int anioIng= curEmpleado.U_Fan.Value.Year; anioIng<=System.DateTime.Now.Year; anioIng++)
                {
                    indexAnio = dashboardSolicitudes.FindIndex(x=> x.Id.Equals(anioIng.ToString()));
                    if (indexAnio < 0)
                    {
                        dashboardSolicitud = new DashboardSolicitud();
                        dashboardSolicitud.ColordeFondo = "White";
                        dashboardSolicitud.ColordeTexto = "Black";
                        dashboardSolicitud.Comentario = String.Empty;
                        dashboardSolicitud.Descripcion = "Vacaciones";
                        dashboardSolicitud.Id = anioIng.ToString();
                        dashboardSolicitudes.Add(dashboardSolicitud);
                    }
                }

                //foreach (DatosSolicitudCardsHistorico item in solicitudesCardsHistorico)
                //            {

                //                dashboardSolicitud = new DashboardSolicitud();
                //                dashboardSolicitud.ColordeFondo = "White";
                //                dashboardSolicitud.ColordeTexto = "Black";
                //                dashboardSolicitud.Comentario = String.Empty;
                //                dashboardSolicitud.Descripcion = "Vacaciones";
                //                dashboardSolicitud.DiasCorrespondientes = item.DCorrespondientes;
                //                dashboardSolicitud.DiasDisfrutados = item.DDisfrutados;
                //                dashboardSolicitud.DiasDisponibles = item.DDisponibles;
                //                dashboardSolicitud.DiasPorAprobar = item.DSOL_TOTAL;
                //                dashboardSolicitud.Id = item.ANIO.ToString();
                //                dashboardSolicitudes.Add(dashboardSolicitud);
                //            }

                dashboard.Solicitudes = dashboardSolicitudes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dashboard;
        }

        private Dashboard buscarDerechosEmpleado(String NumEmpleado, int Anio=0)
		{
			Dashboard dashboard = new Dashboard();
			dashboard.NumEmpleado = NumEmpleado;
			Boolean tieneDerecho = false;
			DateTime? fechaAntiguedad;
            Anio = Anio == 0 ? System.DateTime.Today.Year : Anio;
            try
			{
				List<DashboardSolicitud> dashboardSolicitudes = new List<DashboardSolicitud>();
				List<TablaSolicitudes> Listado = new List<TablaSolicitudes>();

				DAOCRUDNominixTD<TablaSolicitudes> _repositorio = new CreateDAO().GeneraDAOSolicitudes(_UserSign);
				QueryParameters<TablaSolicitudes> parametrosds = new QueryParameters<TablaSolicitudes>();
				parametrosds.where = x => x.U_EMP.Equals(NumEmpleado);
				parametrosds.orderBy = x => x.U_EMP;
				List<TablaSolicitudes> solicitudes = _repositorio.EncontrarPor(parametrosds).ToList();

                //OBTENEMOS TODO EL AUSENTISMOS DE NOMINIX FILTRADO POR EL EMPLEADO
                DAOCRUDNominixTD<AusentismoNomina> _ausentismoNomina = new CreateDAO().GeneraDAOAusentismoNomina(_UserSign);
                QueryParameters<AusentismoNomina> parametroAN = new QueryParameters<AusentismoNomina>();
                parametroAN.where = x => x.U_EMP.Equals(NumEmpleado);
                List<AusentismoNomina> ausentismoNominix = _ausentismoNomina.EncontrarPor(parametroAN).ToList();

                //Catálogo de Conceptos.
                DAOCRUDNominixTM<Conceptos> _repoConceptos = new CreateDAO().GeneraDAOConceptos(_UserSign);
				QueryParameters<Conceptos> parametroscn = new QueryParameters<Conceptos>();
				parametroscn.where = x => x.Name != String.Empty;
				parametroscn.orderBy = x => x.Code;
				List<Conceptos> catConceptos = _repoConceptos.EncontrarPor(parametroscn).ToList();

				//Tabla de criterios Header.
				DAOCRUDNominixTLM<CriteriosConceptosH> _repositorioCriteriosH = new CreateDAO().GeneraDAOCriteriosConceptosHeader(_UserSign);
				QueryParameters<CriteriosConceptosH> parametrosch = new QueryParameters<CriteriosConceptosH>();
				parametrosch.where = x => x.U_ACTIVO == true;
				parametrosch.orderBy = x => x.U_IFVAC;
				List<CriteriosConceptosH> criteriosH = _repositorioCriteriosH.EncontrarPor(parametrosch).ToList();
				_repositorioCriteriosH.Dispose();


				//Tabla de criterios Detalle.
				DAOCRUDNominixTLM<CriteriosConceptos> _repositorioCriterios = new CreateDAO().GeneraDAOCriteriosConceptos(_UserSign);
				QueryParameters<CriteriosConceptos> parametros = new QueryParameters<CriteriosConceptos>();
				parametros.where = x => x.U_ACTIVO == true;
				parametros.orderBy = x => x.U_IFVAC;
				List<CriteriosConceptos> criterios = _repositorioCriterios.EncontrarPor(parametros).ToList();
				_repositorioCriterios.Dispose();

				Empleados curEmpleado = NominixGenerales.ObtenerEmpleado(NumEmpleado, _UserSign);
				List<Empleados> ListaEmpleados = new List<Empleados>();
				ListaEmpleados.Add(curEmpleado);

				List<CriteriosConceptos> criteriosGrupo = new List<CriteriosConceptos>();
				String concepto = String.Empty;
				String valor = String.Empty;
				List<String> valores = new List<String>();
				DashboardSolicitud dashboardSolicitud = new DashboardSolicitud();
				Conceptos curConcepto = new Conceptos();
                decimal diasDisfrutados = 0.00M;

                DAOCRUDNominixTD<EdoCuentaVacaciones> repoEstadoCuentaVacaciones = new CreateDAO().GeneraDAOEdoCuentaVacaciones(_UserSign);
				QueryParameters<EdoCuentaVacaciones> parametrosec = new QueryParameters<EdoCuentaVacaciones>();
				parametrosec.where = x => x.U_EMP.Equals(NumEmpleado) && x.U_FAV.Value.Year==Anio;
				parametrosec.orderByDesc = x => x.U_FAV;
				List<EdoCuentaVacaciones> EstadoCuentaEmpleado = repoEstadoCuentaVacaciones.EncontrarPor(parametrosec).ToList();

                DAOCRUDNominixTD<AusentismoNomina> repoAusentismo = new CreateDAO().GeneraDAOAusentismoGenerico(_UserSign);
                QueryParameters<AusentismoNomina> parametrosausentismo = new QueryParameters<AusentismoNomina>();
                parametrosausentismo.where = x => x.U_EMP.Equals(NumEmpleado);
                parametrosausentismo.orderBy = x => x.U_FAU;
                List<AusentismoNomina> AusentismoEmpleado = repoAusentismo.EncontrarPor(parametrosausentismo).ToList();
                List<AusentismoNomina> AusentismoConcepto = new List<AusentismoNomina>();
                foreach (Empleados empleado in ListaEmpleados)
				{
					fechaAntiguedad = empleado.U_Fan;
					foreach (CriteriosConceptosH criterioheader in criteriosH)
					{
						concepto = criterioheader.Code;
						empleado.U_Ifavac = String.IsNullOrEmpty(empleado.U_Ifavac) ? "" : empleado.U_Ifavac;
						
						criteriosGrupo = criterios.Where(x => x.U_AGRUPACION == criterioheader.U_AGRUPACION && x.Code.Equals(criterioheader.Code)).ToList();
						tieneDerecho = true;
						foreach (CriteriosConceptos criterio in criteriosGrupo)
						{
							valor = GetColumn(empleado, criterio.U_CAMPO).ToLower();
							criterio.U_VALOR = criterio.U_VALOR.ToLower();
							if (criterio.U_MULTIPLE)
							{
								valores = criterio.U_VALOR.Split(',').ToList();
								if (!valores.Contains(valor))
								{
									tieneDerecho = false;
									break;
								}
							}
							else
							{
								if (!valor.Equals(criterio.U_VALOR))
								{
									tieneDerecho = false;
									break;
								}
							}
						}
						curConcepto = catConceptos.Where(x => x.Code.Equals(concepto)).FirstOrDefault();
                        var resSolicitudes = solicitudes.Where(x => x.U_EMP == NumEmpleado && (x.U_CON == concepto) && (x.U_SRH == "P" || x.U_SSUP == "P")).ToList<TablaSolicitudes>().Sum(x => x.U_DSOL);

                        var AnoActual = DateTime.Now.Year;
                        var MesActual = DateTime.Now.Month - 1;
                        var resNominixPorProcesar = solicitudes.Where(x => x.U_EMP == NumEmpleado && x.U_CON == concepto && x.U_SNOMINIX == "S" && (x.U_FIV.Value.Year == AnoActual && x.U_FIV.Value.Month >= MesActual)).ToList<TablaSolicitudes>();
                        int Search = 0;

                        foreach (TablaSolicitudes item in resNominixPorProcesar)
                        {
                            DateTime fecha1 = item.U_FIV.Value;
                            DateTime fecha2 = item.U_FTV.Value;
                            TimeSpan ts = fecha2.Subtract(fecha1);
                            int totalDias = ts.Days + 1;
                            //var totalDias = (fecha1 - fecha2).TotalDays;

                            var Search2 = ausentismoNominix.Where(x => x.U_CON == item.U_CON && x.U_FAU == item.U_FIV && x.U_DAUS == totalDias && x.U_DAPL == x.U_DAUS).ToList<AusentismoNomina>().Sum(x => x.U_DAUS);
                            if (Search2 != null)
                            {
                                Search = Search + Convert.ToInt32(Search2);
                            }
                        }

                        diasDisfrutados = 0.00M;
						if (tieneDerecho)
						{
							dashboardSolicitud = new DashboardSolicitud();
                            dashboardSolicitud.UMED = curConcepto.U_UME;
                            dashboardSolicitud.ColordeFondo = String.IsNullOrEmpty(criterioheader.U_BCOLOR) ? "White" : criterioheader.U_BCOLOR;
							dashboardSolicitud.ColordeTexto = String.IsNullOrEmpty(criterioheader.U_COLOR) ? "Black" : criterioheader.U_COLOR;
							dashboardSolicitud.Comentario = String.Empty;
							dashboardSolicitud.Descripcion = curConcepto.Name;
							if (concepto.Equals("201")) //Vacaciones
                            {
								if (EstadoCuentaEmpleado.Count > 0)
								{
									dashboardSolicitud.DiasCorrespondientes = (decimal)EstadoCuentaEmpleado[0].U_DVAC;
                                    dashboardSolicitud.DiasDisfrutados = (decimal)EstadoCuentaEmpleado[0].U_DVDI;
                                }
							}
							else
							{
								dashboardSolicitud.DiasCorrespondientes = (Decimal)criterioheader.U_UNID;
                                if (AusentismoEmpleado.Count() > 0)
                                {
                                    if (AusentismoEmpleado.FindIndex(x => x.U_CON.Equals(concepto))>0)
                                    {
                                        if (curConcepto.U_CVETOP.Equals("U")) //Única Vez
                                        {
                                            AusentismoConcepto = AusentismoEmpleado.Where(x => x.U_CON.Equals(concepto)).ToList();
                                        }
                                        else if (curConcepto.U_CVETOP.Equals("A")) //Anual
                                        {
                                            AusentismoConcepto = AusentismoEmpleado.Where(x => x.U_CON.Equals(concepto) && x.U_FAU.Value.Year==Anio).ToList();
                                        }
                                        diasDisfrutados = (Decimal)AusentismoConcepto.Sum(x => x.U_DCAPL);
                                    }
                                }
							}
							dashboardSolicitud.DiasDisfrutados = diasDisfrutados;
							dashboardSolicitud.DiasDisponibles = dashboardSolicitud.DiasCorrespondientes - dashboardSolicitud.DiasDisfrutados;
                            dashboardSolicitud.DiasPorAprobar = (Decimal)resSolicitudes;
                            dashboardSolicitud.DiasPorProcesarNominix = Search;
							dashboardSolicitud.Id = concepto;
							dashboardSolicitudes.Add(dashboardSolicitud);
						}
					}
				}
				dashboard.Solicitudes = dashboardSolicitudes;
				_repositorio.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return dashboard;
		}

        

        public async Task<RespuestaData<Dashboard>> obtenerDashboard(String NumEmpleado, int Anio = 0, String urlAPINominix = "", String urlAPIAgilix ="")
		{
			RespuestaData<Dashboard> respuesta = new RespuestaData<Dashboard>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
                //await actualizarDatosEmpleado(urlAPINominix, urlAPIAgilix, NumEmpleado);
                //await actualizarSaldosAusentismo(urlAPINominix, urlAPIAgilix, NumEmpleado);
                //await actualizarSaldos(urlAPINominix, urlAPIAgilix, NumEmpleado);
                Dashboard dashboard = buscarDerechosEmpleado(NumEmpleado);
				respuesta.Datos = dashboard;
				respuesta.Respuesta.result = 1;
            }
            catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

        private async Task<String> actualizarSaldosAusentismo(String urlAPINominix, String urlAPIAgilix, String NumEmpleado)
        {
            //String result = String.Empty;
            String result = String.Empty;

            try
            {
                //Se llama el API, para traer los datos de la tabla de AusentismoAgilix POST:
                //Crear un cliente del tipo del objeto:
                ClienteRest<ResultService> cliente2 = new ClienteRest<ResultService>();
                //Hacer POST al Api Nominix:
                ResultService registrarAusentismoAgilix = await cliente2.LLamarServicioPostGeneral(urlAPIAgilix, "AusentismoAgilix/", NumEmpleado);
            }
            catch (Exception ex)
            {
                result = ex.Message.ToString();
            }
            return result; 
        }

        private async Task<ResultService> actualizarDatosEmpleado(String urlAPINominix, String urlAPIAgilix, String NumEmpleado)
        {
            //String result = String.Empty;
            ResultService result = new ResultService();
            try
            {
                if (!String.IsNullOrEmpty(urlAPINominix) && !String.IsNullOrEmpty(urlAPIAgilix))
                {
                    //aqui se construye el token
                    //aqui se llama el API, para traer los datos de la tabla de EmpleadosNominix GET
                    //Crear un cliente del tipo del objeto:
                    ClienteRest<Empleados> cliente = new ClienteRest<Empleados>();
                    //Hacer GET al Api Nominix:
                    Empleados empleadoNominix = await cliente.LLamarServicioSimple(urlAPINominix, "EmpleadoNominix/" + NumEmpleado);
                    if (empleadoNominix != null)
                    {
                        //aqui se construye el token
                        //aqui se llama el API, para actualizar los datos de la tabla de EmpleadosAgilix PUT
                        //Crear un cliente del tipo del objeto:
                        ClienteRest<ResultService> cliente2 = new ClienteRest<ResultService>();
                        //Hacer PUT al Api Agilix:
                        ResultService respuestaServicio = await cliente2.LLamarServicioPutGeneral<Empleados>(urlAPIAgilix, "EmpleadoAgilix/" + NumEmpleado, empleadoNominix);
                        result.resultado = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                //To Do: hacer un log
                result.Mensaje = ex.Message.ToString();
            }
            return result;
        }

        public async Task<ResultService> guardarDatosEmpleadoEnNominix(String urlAPINominix, Empleados Empleado)
        {
            ResultService result = new ResultService();
            try
            {
                if (Empleado != null)
                {
                    //aqui se construye el token
                    //aqui se llama el API, para actualizar los datos de la tabla de EmpleadosNominix PUT
                    //Crear un cliente del tipo del objeto:
                    ClienteRest<ResultService> cliente = new ClienteRest<ResultService>();
                    //Hacer PUT al Api Agilix:
                    ResultService respuestaServicio = await cliente.LLamarServicioPutGeneral<Empleados>(urlAPINominix, "EmpleadoNominix/" + Empleado.Code, Empleado);
                    result.resultado = 1;
                }
            }
            catch (Exception ex)
            {
                //To Do: hacer un log
                result.Mensaje = ex.Message.ToString();
            }
            return result;
        }

        private async Task<String> actualizarSaldos(String urlAPINominix, String urlAPIAgilix, String NumEmpleado)
        {
            //String result = String.Empty;
            String result = String.Empty;
            try
            {
                if (!String.IsNullOrEmpty(urlAPINominix) && !String.IsNullOrEmpty(urlAPIAgilix))
                {
                    //aqui se construye el token
                    //aqui se llama el API, para actualizar los datos de la tabla de saldosAgilix PUT
                    //Crear un cliente del tipo del objeto:
                    ClienteRest<ResultService> cliente2 = new ClienteRest<ResultService>();
                    //Hacer PUT al Api Agilix:
                    ResultService respuestaServicio = await cliente2.LLamarServicioPostGeneral<String>(urlAPIAgilix, "SaldosAgilix/", NumEmpleado);
                        
                    
                }
            }
            catch (Exception ex)
            {
                //To Do: hacer un log
				result = ex.Message.ToString();
            }
            return result;
        }

        //NUEVO CPHA
        public RespuestaData<Dashboard> obtenerHistorialDashboard(String NumEmpleado)
        {
            RespuestaData<Dashboard> respuesta = new RespuestaData<Dashboard>();
            //Error por default.
            respuesta.Respuesta.result = 0;
            respuesta.Respuesta.mensaje = String.Empty;
            try
            {
                Dashboard dashboard = buscarHistorialEmpleado(NumEmpleado);
                respuesta.Datos = dashboard;
                respuesta.Respuesta.result = 1;
            }
            catch (Exception ex)
            {
                respuesta.Respuesta.mensaje = ex.Message.ToString();
            }
            return respuesta;
        }

        private decimal parteDecimalHorario(int horasJornada, decimal horas)
		{
			decimal result = 0.00M;
			String valor = horas.ToString();
			if (valor.IndexOf(".") < 0)
			{
				valor += ".00";
			}
			String[] AValor = valor.Split('.');
			decimal partedecimal = decimal.Parse("0." + AValor[1]);
			result = partedecimal * (24.00M / horasJornada);
			return result;
		}

		private String GetColumn(Empleados empleado, string columnName)
		{
			String result = String.Empty;
			object value = empleado.GetType().GetProperty(columnName).GetValue(empleado);
			if (value != null) { result = value.ToString(); }
			return result;
		}

	}
}
