﻿using GRC.Mensajeria;
using GRC.Mensajeria.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServUtils = GRC.Servicios.Utilidades;
using Seguridad = Utilidades;
namespace Aplics.Servicios.Negocio
{
	public class SendMails
	{
		#region EMail
		private String usuariosec = String.Empty;
		private String _correoSalida = String.Empty;
		private String _alias = String.Empty;
		private Boolean _SecureSSL = false;
		private String _servicioSMTP = String.Empty;
		private int _puertoSMTP = 0;
		private String _usuario = String.Empty;
		private String _password = String.Empty;
		private string from = String.Empty;
		private String Token = String.Empty;
		private String _rutaCorreos = String.Empty;
		private String _plantilla = String.Empty;
		public SendMails()
		{
			
		}

		public SendMails(String pusuariosec,String pcorreoSalida,String pAlias,String pServicioSMTP,Boolean pSecureSSL, int ppuertoSMTP,String usuarioMail,String passMail,String prutaMail,String pPlantilla)
		{
			usuariosec = pusuariosec;
			_correoSalida = Seguridad.Encriptacion.Desencriptar(Convert.FromBase64String(pcorreoSalida), usuariosec);
			_alias = Seguridad.Encriptacion.Desencriptar(Convert.FromBase64String(pAlias), usuariosec);
			_SecureSSL = pSecureSSL;
			_servicioSMTP = Seguridad.Encriptacion.Desencriptar(Convert.FromBase64String(pServicioSMTP), usuariosec);
			_puertoSMTP = ppuertoSMTP;
			_usuario = Seguridad.Encriptacion.Desencriptar(Convert.FromBase64String(usuarioMail), usuariosec);
			_password = Seguridad.Encriptacion.Desencriptar(Convert.FromBase64String(passMail), usuariosec);
			_rutaCorreos = prutaMail;
			_plantilla = pPlantilla;
		}


		public String mensajePlantilla(String asuntoDetalle, String detalle, String accion)
		{
			String textoPlantilla = _plantilla;
			textoPlantilla = textoPlantilla.Replace("@asuntodetalle", asuntoDetalle);
			textoPlantilla = textoPlantilla.Replace("@detalle", detalle);
			if (String.IsNullOrEmpty(accion))
			{
				accion = "<a href='#' style='color: #192d66; text-decoration: none; font-size: 16px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; display: block; padding: 1px 8px;'>Confirmar de Enterado</a>";
			}
			textoPlantilla = textoPlantilla.Replace("@accion", accion);
			return textoPlantilla;
		}

		/// <summary>
		/// Envía correos con o sin archivos añadidos que esten previamente en el servidor.
		/// </summary>
		/// <param name="to"></param>
		/// <param name="asunto"></param>
		/// <param name="mensaje"></param>
		/// <param name="Files"></param>
		/// <param name="ruta"></param>
		/// <returns>En caso de error regresa el texto de la excepción.</returns>
		public ServUtils.RespuestaSimple SendMail(VMMailMessage mail)
		{
			ServUtils.RespuestaSimple oRespuesta = new ServUtils.RespuestaSimple();
			try
			{
				//Se inicializa con error
				oRespuesta.result = 0;
				oRespuesta.mensaje = String.Empty;
				if (mail.Files != null && mail.Files.Count() > 0) { mail.ruta = _rutaCorreos; }
				EMailMessage eMailMessage = new EMailMessage(_correoSalida, _alias, _SecureSSL, _servicioSMTP, _puertoSMTP, _usuario, _password, mail.ruta);
				oRespuesta.mensaje = eMailMessage.SendMail(mail.to, mail.asunto, mail.mensaje, mail.Files);
				if (String.IsNullOrEmpty(oRespuesta.mensaje))
				{
					oRespuesta.result = 1;
				}
			}
			catch (Exception ex)
			{
				oRespuesta.mensaje = ex.Message.ToString();
			}
			return oRespuesta;
		}

		

		/// <summary>
		/// Envía correos con o sin archivos es un array bidimensional con el nombre de los archivos 
		/// y su contenido en base 64
		/// </summary>
		/// <param name="to"></param>
		/// <param name="asunto"></param>
		/// <param name="mensaje"></param>
		/// <param name="base64Files"></param>
		/// <returns>En caso de error regresa el texto de la excepción.</returns>
		public ServUtils.RespuestaSimple SendMailBase64(VMMailMessage mail)
		{
			ServUtils.RespuestaSimple oRespuesta = new ServUtils.RespuestaSimple();
			try
			{
				//Se inicializa con error
				oRespuesta.result = 0;
				oRespuesta.mensaje = String.Empty;
				if (mail.ListaArchivosB64 != null && mail.ListaArchivosB64.Count() > 0) { mail.ruta = _rutaCorreos; }
				EMailMessage eMailMessage = new EMailMessage(_correoSalida, _alias, _SecureSSL, _servicioSMTP, _puertoSMTP, _usuario, _password, mail.ruta);
				oRespuesta.mensaje = eMailMessage.SendMail(mail.to, mail.asunto, mail.mensaje, mail.ListaArchivosB64);
				if (String.IsNullOrEmpty(oRespuesta.mensaje))
				{
					oRespuesta.result = 1;
				}
			}
			catch (Exception ex)
			{
				oRespuesta.mensaje = ex.Message.ToString();
			}
			return oRespuesta;
		}
		#endregion
	}
}
