﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Aplics.Servicios.Entidades.DAO;
using Aplics.Servicios.Entidades.Solicitudes;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Solicitudes;
using GRC.Servicios.Utilidades.Genericos;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Negocio.Nominix;

namespace Aplics.Servicios.Negocio.Solicitudes
{
    public class NegocioSolicitudCalendario
    {
        private int _UserSign = 0;
        public List<DatosSolicitud> SolicitdesEmpleados(String Concepto)
        {
            List<DatosSolicitud> solicitudes = new List<DatosSolicitud>();
            try
            {
                DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOSolicitudDashBoard(_UserSign);
                solicitudes = _repo.ListaSolicitudes(Concepto, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return solicitudes;
        }

        //NUEVO CPHA
        public List<DatosSolicitud> HistorialSolicitdesEmpleados(String Empleado, String Anio)
        {
            List<DatosSolicitud> solicitudes = new List<DatosSolicitud>();
            try
            {
                DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOHistorialSolicitudDashBoard(_UserSign);
                solicitudes = _repo.ListaHistorialSolicitudes(Empleado, Anio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return solicitudes;
        }

        //NUEVO CPHA
        public Calendario buscarListadoEventosCalendario(String NumEmpleado, Boolean isManager = false, Boolean Confirmados  = false)
        {
            Calendario calendario = new Calendario();
            try
            {

                DAOSolicitudDashBoard _repo = new CreateDAO().GeneraDAOEventosCalendario(_UserSign);
                List<CalendarioItem> queryCalendario = new List<CalendarioItem>();
                if (isManager )
                {
                    queryCalendario = _repo.ListaEventosCalendarioManager(NumEmpleado, Confirmados);

                }
                else
                {
                    queryCalendario = _repo.ListaEventosCalendario(NumEmpleado,Confirmados);
                }
                
                //DashboardSolicitud dashboardSolicitud = new DashboardSolicitud();

                //foreach (DatosSolicitudCardsHistorico item in solicitudesCardsHistorico)
                //{

                //    dashboardSolicitud = new DashboardSolicitud();
                //    dashboardSolicitud.ColordeFondo = "White";
                //    dashboardSolicitud.ColordeTexto = "Black";
                //    dashboardSolicitud.Comentario = String.Empty;
                //    dashboardSolicitud.Descripcion = "Vacaciones";
                //    dashboardSolicitud.DiasCorrespondientes = item.DCorrespondientes;
                //    dashboardSolicitud.DiasDisfrutados = item.DDisfrutados;
                //    dashboardSolicitud.DiasDisponibles = item.DDisponibles;
                //    dashboardSolicitud.Id = item.ANIO.ToString();
                //    dashboardSolicitudes.Add(dashboardSolicitud);
                //}

                //dashboard.Solicitudes = dashboardSolicitudes;

                calendario.ItemsCalendario = queryCalendario;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return calendario;
        }

	}
}
