﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Modelos.Horarios;
using GRC.Servicios.Utilidades;
using GRC.Servicios.Utilidades.Genericos;
using Aplics.Servicios.Entidades.DAO;
using AutoMapper;
using GRC.Mensajeria.Modelos;
using Aplics.Servicios.Negocio;
using Aplics.Servicios.Negocio.Nominix;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Generales;

namespace Aplics.Servicios.Negocio.Horarios
{
    public class NegocioHorarios
    {
		private int _UserSign=0;
		//Crear Mapeos
		IMapper mapper = new Mapeado().config.CreateMapper();
		public NegocioHorarios()
		{

		}

		public NegocioHorarios(int pUserSign)
		{
			_UserSign = pUserSign;
		}

			   
		private CalendarioSemanal buscaCalendario(String codEmpleado,int pDocEntry,Boolean nuevo)
		{
			//TODO: Verificar y obtener de (archivo de configuración o tabla) días hábiles del calendario.
			CalendarioSemanal calendarioSemanal = new CalendarioSemanal(diasSemana.Lunes, diasSemana.Viernes);
			try
			{
				//TODO: Verificar y obtener de (archivo de configuración o tabla) horas hábiles.
				Tiempo horaInicial = new Tiempo(7, 0, 0);
				Tiempo horaFinal = new Tiempo(21, 0, 0);

				if (!nuevo)
				{
					//Obtener Horarios
					List<EmpleadoEstudianteHorario> estudianteHorarios = obtenerHorarios(pDocEntry);
					calendarioSemanal.mVHorarios= mapper.Map<List<EmpleadoEstudianteHorario>,List<MVHorario>>(estudianteHorarios);

					List<CalendarioSemanal.Dia> ListaActividades = new List<CalendarioSemanal.Dia>();
					List<EmpleadoEstudianteHorario> filtroHorarios = new List<EmpleadoEstudianteHorario>();
					List<CalendarioSemanal.Dia.Horario> horario = new List<CalendarioSemanal.Dia.Horario>();
					Byte btdia = 0;
					for (diasSemana bdia = calendarioSemanal.diaInicial; bdia <= calendarioSemanal.diaFinal; bdia++)
					{
						btdia = (byte)bdia;
						filtroHorarios = estudianteHorarios.Where(x => x.U_DIAINI <= btdia && x.U_DIAFIN >= btdia).ToList();
						if (filtroHorarios.Count() > 0)
						{
							horario = new List<CalendarioSemanal.Dia.Horario>();
							foreach (EmpleadoEstudianteHorario eh in filtroHorarios)
							{
								horario.Add(new CalendarioSemanal.Dia.Horario(new Tiempo(eh.U_HORAINI, eh.U_MINUTOINI, 0), new Tiempo(eh.U_HORAFIN, eh.U_MINUTOFIN, 0),
									(int)eh.U_TIPOH, obtenerTipoHorario((int)eh.U_TIPOH), ColorHorario((int)eh.U_TIPOH, true),
									ColorHorario((int)eh.U_TIPOH, false), String.Empty));
							}
							ListaActividades.Add(new CalendarioSemanal.Dia(bdia, horaInicial, horaFinal, horario));
						}
						else
						{
							ListaActividades.Add(new CalendarioSemanal.Dia(bdia, horaInicial, horaFinal));
						}
					}
					calendarioSemanal.crearCalendario(horaInicial, horaFinal, ListaActividades);
				}
				else
				{
					calendarioSemanal.crearCalendario(horaInicial, horaFinal);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			calendarioSemanal.fechaSemana = System.DateTime.Today;
			return calendarioSemanal;
		}


		public List<EmpleadoEstudiante> SolicitudesEstudiantesFiltro(QueryParameters<EmpleadoAsignacion> parametros)
		{
			List<EmpleadoEstudiante> solicitudes = new List<EmpleadoEstudiante>();
			try
			{
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				List<EmpleadoAsignacion> LEmpleadoAsignacion = _repositorio.EncontrarPor(parametros).ToList();
				solicitudes = mapper.Map<List<EmpleadoAsignacion>, List<EmpleadoEstudiante>>(LEmpleadoAsignacion);
				_repositorio.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return solicitudes;
		}

		public List<EmpleadoEstudiante> SolicitudesEstudiantesEstatus(Boolean aprobada)
		{
			List<EmpleadoEstudiante> solicitudes = new List<EmpleadoEstudiante>();
			try
			{
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.U_Aprobado == aprobada;
				List<EmpleadoAsignacion> LEmpleadoAsignacion = _repositorio.EncontrarPor(parametros).ToList();
				solicitudes = mapper.Map<List<EmpleadoAsignacion>, List<EmpleadoEstudiante>>(LEmpleadoAsignacion);
				_repositorio.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return solicitudes;
		}

		public List<EmpleadoEstudiante> SolicitudesEstudiantes()
		{
			List<EmpleadoEstudiante> solicitudes = new List<EmpleadoEstudiante>();
			try
			{
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.U_PER>0;
				List<EmpleadoAsignacion> LEmpleadoAsignacion = _repositorio.EncontrarPor(parametros).ToList();
				solicitudes = mapper.Map<List<EmpleadoAsignacion>, List<EmpleadoEstudiante>>(LEmpleadoAsignacion);
				_repositorio.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return solicitudes;
		}

		public List<CatalogoGeneral> CatalogodeUniversidades()
		{
			List<CatalogoGeneral> CatUniversidades=new List<CatalogoGeneral>(); 
			try
			{
				DAOCRUDNominixTM<Universidades> _repositorio = new CreateDAO().GeneraDAOUniversidades(_UserSign);
				QueryParameters<Universidades> parametros = new QueryParameters<Universidades>();
				parametros.where = x => x.Name != String.Empty;
				List<Universidades> LUniversidades = _repositorio.EncontrarPor(parametros).ToList();
				foreach(Universidades universidad in LUniversidades)
				{
					CatUniversidades.Add(new CatalogoGeneral(universidad.Code, universidad.Name));
				}
				_repositorio.Dispose();
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return CatUniversidades;
		}

		public RespuestaSimple notificarHorarioCargado(String codEmpleado,String correoRH, SendMails sm,String empleadoRH,int rol,int solicitudId= 0)
		{
			RespuestaSimple respuesta = new RespuestaSimple();
			respuesta.result = 0;
			try
			{
				EmpleadoAsignacion empleadoAsignacion = new EmpleadoAsignacion();
				List<EmpleadoAsignacion> Lasignaciones = new List<EmpleadoAsignacion>();
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
                if (solicitudId > 0)
                {
                    parametros.where = x => x.U_EMP.Equals(codEmpleado) && x.Status == "I" && x.DocEntry== solicitudId;
                }
                else
                {
                    parametros.where = x => x.U_EMP.Equals(codEmpleado) && x.Status == "I";
                }
				Lasignaciones = _repositorio.EncontrarPor(parametros).ToList();
				_repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				if (Lasignaciones.Count() > 0)
				{
					Lasignaciones = Lasignaciones.OrderByDescending(x => x.DocEntry).ToList();
					empleadoAsignacion = Lasignaciones[0];
					empleadoAsignacion.U_HorarioESC = true;
					empleadoAsignacion.Status = "P";
					_repositorio.Actualizar(empleadoAsignacion);
					_repositorio.Dispose();
					//Obtener datos del empleado.
					Empleados empleado = NominixGenerales.ObtenerEmpleado(codEmpleado, _UserSign);
					String mensaje = "El empleado " + empleado.Code + "-" + empleado.U_Nomp + " " + empleado.U_Apepat + " " + empleado.U_Apemat + " ha subido su horario con No. de Solicitud " + solicitudId.ToString();

					NominixGenerales.notificacionPortal(_UserSign, empleado.Code, empleadoRH, mensaje, String.Empty,"HORARIOS",rol, solicitudId.ToString());
					String NOTIFICARHORARIOSXMAILRH = "1";
					respuesta.result = 1;
					respuesta.mensaje = String.Empty;
					//VERIFICAR SI SE NOTIFICA X MAIL A RH
					DAOCRUDNominixTD<ParametrosAPP> _repoParam = new CreateDAO().GeneraDAOParametrosAPP(_UserSign);
					QueryParameters<ParametrosAPP> parametrosApp = new QueryParameters<ParametrosAPP>();
					parametrosApp.where = x => x.U_MOD.Equals("RH") && x.U_SUBMOD.Equals("HORARIOS") && x.U_PARAM.Equals("NOTIFICARHORARIOSXMAILRH");
					List<ParametrosAPP> Lparams = _repoParam.EncontrarPor(parametrosApp).ToList();
					_repoParam.Dispose();
					if (Lparams.Count > 0)
					{
						NOTIFICARHORARIOSXMAILRH = Lparams[0].U_VALOR.Trim();
					}

					if (NOTIFICARHORARIOSXMAILRH.Equals("1"))
					{
						VMMailMessage mail = new VMMailMessage();
						mail.asuntoDetalle = "Este mensaje ha sido envíado de forma automática y no requiere una respuesta.";
						mail.asunto = "Notificación de envío de horario.";
						mail.mensaje = sm.mensajePlantilla(mail.asuntoDetalle, mensaje, mail.asunto);
						mail.to = correoRH;
						respuesta = sm.SendMail(mail);
					}
				}
				else
				{
					respuesta.mensaje = "No se encontro la información del empleado.";
				}
			}
			catch (Exception ex)
			{
				respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		public EmpleadoEstudiante buscarDatosAsignacionXId(int Id)
		{
			try
			{
				List<EmpleadoAsignacion> LempleadoAsignacion = new List<EmpleadoAsignacion>();
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.DocEntry == Id;
				LempleadoAsignacion = _repositorio.EncontrarPor(parametros).ToList();
				_repositorio.Dispose();
				EmpleadoEstudiante empleadoEstudiante = mapper.Map<EmpleadoAsignacion, EmpleadoEstudiante>(LempleadoAsignacion[0]);
				empleadoEstudiante = DatosCompSolicitud(empleadoEstudiante);
				return empleadoEstudiante;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private EmpleadoEstudiante DatosCompSolicitud(EmpleadoEstudiante empleadoEstudiante)
		{
			try
			{
				short clavedur = empleadoEstudiante.U_DUR;
				String[] CatDuracion = { "", "Mes", "Bimestre", "Trimestre", "Cuatrimestre", "","Semestre" };
				empleadoEstudiante.Duracion = CatDuracion[clavedur];
				Empleados empleado = NominixGenerales.ObtenerEmpleado(empleadoEstudiante.U_EMP, _UserSign);
				empleadoEstudiante.nombre = empleado.Name;
				List<CatalogoGeneral> catUniversidad = CatalogodeUniversidades();
				int index = catUniversidad.FindIndex(x => x.Id.Equals(empleadoEstudiante.U_CVEUNI));
				if (index >= 0)
				{
					empleadoEstudiante.Universidad = catUniversidad[index].Descripcion;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleadoEstudiante;
		}

		public EmpleadoEstudiante buscarDatosAsignacion(String codEmpleado,int Id)
		{
			try
			{
				List<EmpleadoAsignacion> LempleadoAsignacion = new List<EmpleadoAsignacion>();
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.U_EMP.Equals(codEmpleado) && x.DocEntry== Id;
				LempleadoAsignacion = _repositorio.EncontrarPor(parametros).ToList();
				_repositorio.Dispose();
				EmpleadoEstudiante empleadoEstudiante = mapper.Map<EmpleadoAsignacion, EmpleadoEstudiante>(LempleadoAsignacion[0]);
				return empleadoEstudiante;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<Empleado> ListaEmpleados(String codSuperior)
        {
            List<Empleado> empleados =new List<Empleado>();
            try
            {
                DAOEmpleadoAsignacion repo= new CreateDAO().GeneraDAOEmpleadoAsignacionCustom(_UserSign);
                empleados = repo.ListaEmpleados(codSuperior);
            }
            catch (Exception ex)
            {
                throw(ex);
            }
            return empleados;
        }

        public RespuestaData<EmpleadoEstudiante> buscarEmpleadoAsignacion(String codEmpleado,short periodo)
		{
			RespuestaData<EmpleadoEstudiante> respuesta = new RespuestaData<EmpleadoEstudiante>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
				List<EmpleadoAsignacion> LempleadoAsignacion= new List<EmpleadoAsignacion>();
				EmpleadoEstudiante empleadoEstudiante = new EmpleadoEstudiante();
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				parametros.where = x => x.U_EMP.Equals(codEmpleado) && x.U_PER==periodo;
				LempleadoAsignacion = _repositorio.EncontrarPor(parametros).ToList();
				_repositorio.Dispose();
				EmpleadoAsignacion empleadoAsignacion = new EmpleadoAsignacion();

				CalendarioSemanal calendarioSemanal ;
				if (LempleadoAsignacion.Count > 0)
				{
					empleadoAsignacion = LempleadoAsignacion[0];
					//Obtener Horarios
					calendarioSemanal = buscaCalendario(codEmpleado, empleadoAsignacion.DocEntry, false);
				}
				else
				{
					empleadoAsignacion.U_EMP = codEmpleado;
					empleadoAsignacion.U_ANIO = (short)System.DateTime.Today.Year;
					empleadoAsignacion.U_FINI = System.DateTime.Today;
					empleadoAsignacion.U_FFIN = System.DateTime.Today.AddMonths(6);
					calendarioSemanal = buscaCalendario(codEmpleado, 0, true);
				}

				calendarioSemanal.editable = (empleadoAsignacion.U_Aprobado) || (! empleadoAsignacion.U_Aprobado && empleadoAsignacion.U_NUMREC>1) ? false : true;
				empleadoEstudiante = mapper.Map<EmpleadoAsignacion, EmpleadoEstudiante>(empleadoAsignacion);

				empleadoEstudiante.ocalendario = calendarioSemanal;

				//Obtener nombre del empleado.
				Empleados empleado= NominixGenerales.ObtenerEmpleado(codEmpleado,_UserSign);
				empleadoEstudiante.nombre = empleado.U_Nomp + " " + empleado.U_Apepat + " " + empleado.U_Apemat;

				respuesta.Datos = empleadoEstudiante;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}


		private CalendarioSemanal nuevoCalendario()
		{
			try
			{
				//TODO: Verificar y obtener de (archivo de configuración o tabla) días hábiles del calendario.
				CalendarioSemanal calendarioSemanal = new CalendarioSemanal(diasSemana.Lunes, diasSemana.Viernes);

				//TODO: Verificar y obtener de (archivo de configuración o tabla) horas hábiles.
				Tiempo horaInicial = new Tiempo(7, 0, 0);
				Tiempo horaFinal = new Tiempo(21, 0, 0);
				calendarioSemanal.crearCalendario(horaInicial, horaFinal);
				calendarioSemanal.fechaSemana = System.DateTime.Today;
				return calendarioSemanal;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		//TODO: Extraer Nombre de Actividades de archivo de configuración o tabla
		String obtenerTipoHorario(int tipoHorario)
		{
			String result = String.Empty;
			if (tipoHorario == 1)
			{
				result = "Escolar";
			}
			else if (tipoHorario == 2)
			{
				result = "Transporte";
			}
			else if (tipoHorario == 3)
			{
				result = "Laboral";
			}
			else
			{
				result = "Comida";
			}
			return result;
		}

		//TODO extraer Colores de archivo de configuración o tabla
		String ColorHorario(int tipoHorario,Boolean fondo)
		{
			String color = String.Empty;
			if (fondo)
			{
				if (tipoHorario == 1)
				{
					color = "green";
				}
				else if (tipoHorario == 2)
				{
					color = "red";
				}
				else if (tipoHorario == 3) {
					color = "#6AA093";
				}
				else
				{
					color = "orange";
				}
			}
			else
			{
				if (tipoHorario == 1)
				{
					color = "white";
				}
				else if (tipoHorario == 2)
				{
					color = "white";
				}
				else if (tipoHorario == 3)
				{
					color = "black";
				}
				else
				{
					color = "white";
				}
			}
			return color;
		}


		private List<EmpleadoEstudianteHorario> obtenerHorarios(int Id)
		{
			try
			{
				DAOCRUDNominixTLD<EmpleadoEstudianteHorario> _repositorioE = new CreateDAO().GeneraDAOEmpleadoEstudianteHorario(_UserSign);
				QueryParameters <EmpleadoEstudianteHorario> parametrosE = new QueryParameters<EmpleadoEstudianteHorario>();
				parametrosE.where = x => x.DocEntry==Id;
				parametrosE.orderBy = x => x.LineId;
				List<EmpleadoEstudianteHorario> Listado = _repositorioE.EncontrarPor(parametrosE).ToList();
				_repositorioE.Dispose();
				if (Listado.Count == 0) { return Listado; }
				return Listado.OrderBy(x => x.U_DIAINI).ThenBy(n => n.U_TIPOH).ThenBy(p => p.U_HORAINI).ToList();
			}
			catch (Exception ex)
			{
				throw ex; ;
			}
		}

		public List<VMHorarioLista> obtenerHorariosLista(int Id,Byte TipoHorario)
		{
			List<VMHorarioLista> Horarios = new List<VMHorarioLista>();
			VMHorarioLista horarioLista = new VMHorarioLista();
			List<Horario> Horario = new List<Horario>();
			horarioLista.Id = Id;
			int index = 0;
			try
			{
				DAOCRUDNominixTLD<EmpleadoEstudianteHorario> _repositorioE = new CreateDAO().GeneraDAOEmpleadoEstudianteHorario(_UserSign);
				QueryParameters<EmpleadoEstudianteHorario> parametrosE = new QueryParameters<EmpleadoEstudianteHorario>();
				horarioLista.Tipo = TipoHorario;
				if (TipoHorario!=0)
				{
					parametrosE.where = x => x.DocEntry == Id && x.U_TIPOH == TipoHorario;
				}
				else
				{
					horarioLista.Horarios = new List<Horario>();
					parametrosE.where = x => x.DocEntry == Id;
				}
				Byte[] LTipoHorario = {1,2,3 }; 
				parametrosE.orderBy = x => x.U_TIPOH ;
				List<EmpleadoEstudianteHorario> Listado = _repositorioE.EncontrarPor(parametrosE).ToList();
				byte idia = 0;
                int indexTipoH = 0;
				if (Listado.Count() > 0)
				{
					foreach(EmpleadoEstudianteHorario eh in Listado)
					{
                        indexTipoH = Horarios.FindIndex(x => x.Tipo == eh.U_TIPOH);
                        if (indexTipoH<0)//eh.U_TIPOH != TipoHorario
                        {
							horarioLista = new VMHorarioLista();
							Horario = new List<Horario>();
							horarioLista.Id = Id;
							horarioLista.Tipo = eh.U_TIPOH;
							horarioLista.Horarios = new List<Horario>();
                        }
                        else
                        {
                            horarioLista = Horarios[indexTipoH];
                        }
						idia = eh.U_DIAINI;
						for(idia = eh.U_DIAINI; idia<=eh.U_DIAFIN; idia++)
						{
							Horario.Add(new Horario() { dia=idia, HoraInicial= formatoHora(eh.U_HORAINI,eh.U_MINUTOINI), HoraFinal= formatoHora(eh.U_HORAFIN, eh.U_MINUTOFIN) });
						}
						Horario = Horario.OrderBy(x=> x.dia).ThenBy(y=> y.HoraInicial).ToList();
						horarioLista.Horarios = Horario;
                        if (indexTipoH < 0)
                        {
                            Horarios.Add(horarioLista);
                        }
 					}
				}
				int indexth = 0;
				for (Byte bth = 1; bth < 4; bth++)
				{
					indexth = Horarios.FindIndex(x => x.Tipo == bth);
					if (indexth < 0)
					{

						horarioLista = new VMHorarioLista();
						Horario = new List<Horario>();
						horarioLista.Id = Id;
						horarioLista.Tipo = bth;
						horarioLista.Horarios = new List<Horario>();
						Horarios.Add(horarioLista);
						indexth = Horarios.FindIndex(x => x.Tipo == bth);
					}
					else
					{
						Horario = Horarios[indexth].Horarios;
					}
					idia = 1;
					for (idia = 1; idia <= 5; idia++)
					{
						index = Horario.FindIndex(x => x.dia == idia);
						if (index < 0)
						{
							Horario.Add(new Horario() { dia = idia, HoraInicial = String.Empty, HoraFinal = String.Empty });
						}
					}
					Horario = Horario.OrderBy(x => x.dia).ThenBy(y => y.HoraInicial).ToList();
					Horarios[indexth].Horarios = Horario;
				}
				_repositorioE.Dispose();
			}
			catch (Exception ex)
			{
				throw ex; ;
			}
			return Horarios.OrderBy(x => x.Tipo).ToList();
		}

		String formatoHora(byte horas, byte minutos)
		{
			return horas.ToString().PadLeft(2, '0') + ":" + minutos.ToString().PadLeft(2, '0');
		}

		public List<short> periodosEmpleadoEstudiante(String numEmpleado, Boolean descartarRechazados = false)
		{
			List<short> periodos = new List<short>();
			try
			{
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorioPeriodo = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				QueryParameters<EmpleadoAsignacion> parametros = new QueryParameters<EmpleadoAsignacion>();
				if (descartarRechazados)
				{
					parametros.where = x => x.U_EMP == numEmpleado && x.Status != "R";

				} else
				{
					parametros.where = x => x.U_EMP == numEmpleado;
				}
				parametros.orderBy = x => x.U_PER;
				List<EmpleadoAsignacion> asignaciones = _repositorioPeriodo.EncontrarPor(parametros).ToList();
				if (asignaciones.Count > 0)
				{
					periodos = asignaciones.Select(x => x.U_PER).ToList();
				}
				_repositorioPeriodo.Dispose();
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return periodos;
		}

		public RespuestaData<EmpleadoEstudiante> agregaEmpleadoAsignacion(EmpleadoEstudiante empleadoEstudiante, Boolean descartarRechazados = false)
		{
			RespuestaData<EmpleadoEstudiante> respuesta = new RespuestaData<EmpleadoEstudiante>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);

				List<short> periodos = periodosEmpleadoEstudiante(empleadoEstudiante.U_EMP, descartarRechazados);
				empleadoEstudiante.U_PER = 1;
				if (periodos.Count > 0)
				{
					empleadoEstudiante.U_PER = periodos[periodos.Count - 1];
					empleadoEstudiante.U_PER += 1;
				}

				EmpleadoAsignacion empleadoAsignacion = mapper.Map<EmpleadoEstudiante, EmpleadoAsignacion>(empleadoEstudiante);
				if (String.IsNullOrEmpty(empleadoAsignacion.U_MOTIVOREC)) { empleadoAsignacion.U_MOTIVOREC = String.Empty; }
				empleadoAsignacion = _repositorio.AgregarIdentity(empleadoAsignacion);
				Empleados empleado = NominixGenerales.ObtenerEmpleado(empleadoEstudiante.U_EMP,_UserSign);
				empleadoEstudiante.nombre = empleado.U_Nomp +  " " + empleado.U_Apepat + " " + empleado.U_Apemat ;
				empleadoEstudiante.Id = empleadoAsignacion.DocEntry;
				empleadoEstudiante.ocalendario = nuevoCalendario();
				respuesta.Datos = empleadoEstudiante;
				respuesta.Respuesta.result = 1;
				_repositorio.Dispose();
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}



		public RespuestaSimple agregaEmpleadoHorario(int Id,List<HorarioTipo> Horarios)
		{
			RespuestaSimple respuesta = new RespuestaSimple();
			//Error por default.
			respuesta.result = 0;
			respuesta.mensaje = String.Empty;
            try
			{
				int cons = 0;
				List<EmpleadoEstudianteHorario> horariosEmp= obtenerHorarios(Id);
				if (horariosEmp.Count > 0)
				{
					cons = horariosEmp[horariosEmp.Count()-1].LineId;
				}
				List<MVHorario> empleadoHorarios = new List<MVHorario>();
				MVHorario mVHorario = new MVHorario();
				String[] horaInicial;
				String[] horaFinal;
				byte hi = 0;
				byte hf = 0;
				byte mini = 0;
				byte minf = 0;
				foreach (HorarioTipo horario in Horarios)
				{
					if (!String.IsNullOrEmpty(horario.HoraInicial))
					{
						horaInicial = horario.HoraInicial.Split(':');
						horaFinal = horario.HoraFinal.Split(':');
						hi = byte.Parse(horaInicial[0]);
						mini = byte.Parse(horaInicial[1]);
						hf = byte.Parse(horaFinal[0]);
						minf = byte.Parse(horaFinal[1]);
                        mVHorario = new MVHorario();
                        mVHorario.Id = Id;
                        mVHorario.LogInst = 0;
                        mVHorario.VisOrder = 0;
                        mVHorario.U_TIPOH = horario.TipoHorario;
                        mVHorario.U_DIAFIN = horario.dia;
                        mVHorario.U_DIAINI = horario.dia;
                        mVHorario.U_HORAFIN = hf;
                        mVHorario.U_HORAINI = hi;
                        mVHorario.U_MINUTOFIN = minf;
                        mVHorario.U_MINUTOINI = mini;
                        empleadoHorarios.Add(mVHorario);
                    }
				}
				respuesta = agregaEmpleadoHorario(empleadoHorarios,cons);
			}
			catch (Exception ex)
			{
				respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		public RespuestaSimple agregaEmpleadoHorario(VMHorarioLista Horario)
		{
			RespuestaSimple respuesta = new RespuestaSimple();
			//Error por default.
			respuesta.result = 0;
			respuesta.mensaje = String.Empty;
			try
			{
				List<MVHorario> empleadoHorarios= new List<MVHorario>();
				MVHorario mVHorarioFind = new MVHorario();
				MVHorario mVHorario = new MVHorario();
				List<Horario> DetalleHorario = Horario.Horarios.OrderBy(x => x.dia).ThenBy(y => y.HoraInicial).ThenBy(z => z.HoraFinal).ToList();
				String[] horaInicial;
				String[] horaFinal;
				byte hi = 0;
				byte hf = 0;
				byte mini = 0;
				byte minf = 0;
				int index = 0;
				int cons = 0;
				Boolean nuevo = false;
				foreach(Horario h in DetalleHorario)
				{
					if (!String.IsNullOrEmpty(h.HoraInicial))
					{
						horaInicial = h.HoraInicial.Split(':');
						horaFinal = h.HoraFinal.Split(':');
						hi = byte.Parse(horaInicial[0]);
						mini = byte.Parse(horaInicial[1]);
						hf = byte.Parse(horaFinal[0]);
						minf = byte.Parse(horaFinal[1]);
						index = empleadoHorarios.FindIndex(x => x.U_HORAINI == hi && x.U_MINUTOINI == mini && x.U_HORAFIN == hf && x.U_MINUTOFIN == minf);
						nuevo = true;
						if (index >= 0)
						{
							if (empleadoHorarios[index].U_DIAFIN == h.dia-1)
							{
								empleadoHorarios[index].U_DIAFIN = h.dia;
								nuevo = false;
							}
						}
						if (nuevo)
						{
							cons += 1;
							mVHorario.Consecutivo = cons;
							mVHorario = new MVHorario();
							mVHorario.Id = Horario.Id;
							mVHorario.LogInst = 0;
							mVHorario.VisOrder = 0;
							mVHorario.U_TIPOH = Horario.Tipo;
							mVHorario.U_DIAFIN = h.dia;
							mVHorario.U_DIAINI = h.dia;
							mVHorario.U_HORAFIN = hf;
							mVHorario.U_HORAINI = hi;
							mVHorario.U_MINUTOFIN = minf;
							mVHorario.U_MINUTOINI = mini;
							empleadoHorarios.Add(mVHorario);
						}
					}
				}
				respuesta = agregaEmpleadoHorario(empleadoHorarios);
			}
			catch (Exception ex)
			{
				respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		public RespuestaSimple agregaEmpleadoHorario(List<MVHorario> empleadoHorarios,int ultimoConsecutivo=0)
		{
			RespuestaSimple respuesta = new RespuestaSimple();
			//Error por default.
			respuesta.result = 0;
			respuesta.mensaje = String.Empty;
			try
			{
				DAOCRUDNominixTLD<EmpleadoEstudianteHorario> _repositorio = new CreateDAO().GeneraDAOEmpleadoEstudianteHorario(_UserSign);
				List<EmpleadoEstudianteHorario> ListaHorario= mapper.Map<List<MVHorario>, List<EmpleadoEstudianteHorario>>(empleadoHorarios);
				int consecutivo = ultimoConsecutivo;
				foreach (EmpleadoEstudianteHorario empleadoEstudianteHorario in ListaHorario)
				{
					consecutivo += 1;
					_repositorio.AgregarBatch(empleadoEstudianteHorario, consecutivo);
				}
				_repositorio.Save();
				_repositorio.Dispose();
				respuesta.result = 1;
			}
			catch(Exception ex)
			{
				respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		public RespuestaData<EmpleadoEstudiante> actualizarEmpleadoAsignacion(EmpleadoEstudiante empleadoEstudiante)
		{
			RespuestaData<EmpleadoEstudiante> respuesta = new RespuestaData<EmpleadoEstudiante>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				EmpleadoAsignacion empleadoAsignacion = mapper.Map<EmpleadoEstudiante, EmpleadoAsignacion>(empleadoEstudiante);
				_repositorio.Actualizar(empleadoAsignacion);
				_repositorio.Dispose();
				empleadoEstudiante.ocalendario = buscaCalendario(empleadoEstudiante.U_EMP, empleadoEstudiante.Id, false);
				respuesta.Datos = empleadoEstudiante;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		public List<String> notificarAutorizaciones(List<VMAutorizacionesHorarios> ListaEmpleados,SendMails sm,String empleadoRH,int rol)
		{
			List<String> mensajes = new List<String>();
			try
			{
				//obtener los datos de los empleados modificados.
				List<String> CodigosEmpleados = ListaEmpleados.Select(x => x.codeEmpleado).ToList();
				List<Empleados> EmpleadosSeleccion = new List<Empleados>();
				DAOCRUDNominixTM<Empleados> _repositorioE = new CreateDAO().GeneraDAOEmpleados(_UserSign);
				EmpleadosSeleccion = _repositorioE.ObtenerPorListCode(CodigosEmpleados).ToList();
				_repositorioE.Dispose();

				//Obtener los superiores de los empleados modificados.
				_repositorioE = new CreateDAO().GeneraDAOEmpleados(_UserSign);
				List<Empleados> SuperioresSeleccion = new List<Empleados>();
				CodigosEmpleados = EmpleadosSeleccion.Select(x => x.U_Empsup).ToList();
				SuperioresSeleccion = _repositorioE.ObtenerPorListCode(CodigosEmpleados).ToList();
				_repositorioE.Dispose();

				String correo = String.Empty;
				String correoSupervisor = String.Empty;
				Empleados curEmpleado = new Empleados();
				RespuestaSimple respuestaenvio = new RespuestaSimple();
				VMMailMessage mail = new VMMailMessage();
				mail.asunto = "Aprobación/Rechazo de Horarios";
				String nombreEmpleado = String.Empty;
				String estatusAprobacion = String.Empty;
				String mensaje = String.Empty;
				foreach (VMAutorizacionesHorarios autorizacion in ListaEmpleados)
				{
					//Enviar Correo de aprobación/rechazo.
					curEmpleado = EmpleadosSeleccion.FirstOrDefault(x => x.Code.Equals(autorizacion.codeEmpleado));
					nombreEmpleado = curEmpleado.Code + "-" + curEmpleado.U_Nomp + " " + curEmpleado.U_Apepat + " " + curEmpleado.U_Apemat;
					correo = String.Empty;
					if (!String.IsNullOrEmpty(curEmpleado.U_Email))
					{
						correo = curEmpleado.U_Email;
					}
					if (autorizacion.Autorizado && SuperioresSeleccion.Count() > 0)
					{
						curEmpleado = SuperioresSeleccion.FirstOrDefault(x => x.Code.Equals(curEmpleado.U_Empsup));
						if (!String.IsNullOrEmpty(curEmpleado.U_Email))
						{
							correoSupervisor = curEmpleado.U_Email;
							if (!String.IsNullOrEmpty(correo)) { correo += ","; }
							correo += correoSupervisor;
						}
					}

					estatusAprobacion = autorizacion.Autorizado ? "autorizado" : "rechazado";
					mensaje = "El horario de " + nombreEmpleado + " ha sido " + estatusAprobacion;

					NominixGenerales.notificacionPortal(_UserSign, empleadoRH, autorizacion.codeEmpleado, mensaje, String.Empty,"HORARIOS",rol,autorizacion.Id.ToString());

					respuestaenvio = new RespuestaSimple();
					
					if (!String.IsNullOrEmpty(autorizacion.motivoRechazo))
					{
						estatusAprobacion += " por " + autorizacion.motivoRechazo;
					}
					mail.mensaje = mensaje;
					mail.to = correo;

					mail.asuntoDetalle = "Este mensaje ha sido envíado de forma automática y no requiere una respuesta.";
					mail.asunto = "Notificación de aprobación/rechazo de horarios.";
					mail.mensaje = sm.mensajePlantilla(mail.asuntoDetalle, mail.mensaje, mail.asunto);
					respuestaenvio = sm.SendMail(mail);
					if (respuestaenvio.result != 1)
					{
						mensajes.Add(respuestaenvio.mensaje);
					}
				}
			}
			catch(Exception ex)
			{
				mensajes.Add(ex.Message.ToString());
			}
			return mensajes;
		}

		public RespuestaData<List<String>> actualizarEmpleadoAsignaciones(List<VMAutorizacionesHorarios> ListaEmpleados)
		{
			RespuestaData<List<String>> respuesta = new RespuestaData<List<String>>();
			List<String> mensajes = new List<String>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
				String borrarEmp = String.Empty;
				foreach (VMAutorizacionesHorarios autorizacion in ListaEmpleados)
				{
					if (autorizacion.horarios.Count > 0)
					{
						borrarEmp=borrarEmpleadoHorariosLista(autorizacion.horarios[0].Id);
						if (!borrarEmp.Equals(""))
						{
							respuesta.Respuesta.mensaje = borrarEmp;
							return respuesta;
						}
						agregaEmpleadoHorario(autorizacion.horarios);
					}
				}
				respuesta.Datos = mensajes;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		public String borrarSolicitudHorario(int id)
		{
			String result = String.Empty;
			try
			{
				DAOCRUDNominixTD<EmpleadoAsignacion> _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacion(_UserSign);
				_repositorio.Eliminar(id);
			}
			catch (Exception ex)
			{
				result = ex.Message.ToString();
			}
			return result;
		} 

		public String borrarEmpleadoHorariosLista(int id,Boolean Admin=false)
		{
			String result = String.Empty;
			try
			{
				DAOCRUDNominixTLD<EmpleadoEstudianteHorario> _repositorio = new CreateDAO().GeneraDAOEmpleadoEstudianteHorario(_UserSign);
				List<EmpleadoEstudianteHorario> horario =_repositorio.ObtenerListaPorId(id).ToList();
				_repositorio.Dispose();
				if (horario.Count() > 0)
				{
					if (Admin)
					{
						horario = horario.Where(x => x.U_TIPOH > 1).ToList();
					}
					foreach (EmpleadoEstudianteHorario h in horario)
					{
						_repositorio = new CreateDAO().GeneraDAOEmpleadoEstudianteHorario(_UserSign);
						_repositorio.EliminarDocEntryLineId(h.DocEntry, h.LineId);
						_repositorio.Dispose();
					}
				}
			}
			catch (Exception ex)
			{
				result = ex.Message.ToString();
			}
			return result;
		}

		public RespuestaData<List<EmpleadoHorario>> ListaEmpleadosXSuperior(String codSuperior)
		{
			RespuestaData<List<EmpleadoHorario>> respuesta= new RespuestaData<List<EmpleadoHorario>>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
				List<EmpleadoHorario> Listado = new List<EmpleadoHorario>();
				DAOEmpleadoAsignacion _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacionCustom(_UserSign);
				Listado = _repositorio.ListaEmpleadosXSuperior(codSuperior);
				_repositorio.Dispose();
				respuesta.Datos = Listado;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}

		public RespuestaData<List<EmpleadoHorario>> ListaEmpleadosActivos()
		{

			RespuestaData<List<EmpleadoHorario>> respuesta = new RespuestaData<List<EmpleadoHorario>>();
			//Error por default.
			respuesta.Respuesta.result = 0;
			respuesta.Respuesta.mensaje = String.Empty;
			try
			{
				List<EmpleadoHorario> Listado = new List<EmpleadoHorario>();
				DAOEmpleadoAsignacion _repositorio = new CreateDAO().GeneraDAOEmpleadoAsignacionCustom(_UserSign);
				Listado = _repositorio.ListaEmpleadosActivos();
				_repositorio.Dispose();
				respuesta.Datos = Listado;
				respuesta.Respuesta.result = 1;
			}
			catch (Exception ex)
			{
				respuesta.Respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}
	}
}
