﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using GRC.Servicios.Data.DAO.EF;
using Aplics.Servicios.Data.Entidades.Generales;
using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Modelos.Horarios;
using Aplics.Servicios.Entidades.Solicitudes;
using Aplics.Servicios.Modelos.Solicitudes;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Modelos.Nominix;

namespace Aplics.Servicios.Negocio
{
	public class Mapeado
	{
		private MapperConfiguration _config;
		public MapperConfiguration config { get => _config ; set => _config = value; }

		public Mapeado()
		{
			_config = new MapperConfiguration(cfg => {
				cfg.CreateMap<EmpleadoAsignacion, EmpleadoEstudiante>()
				.ForMember(x => x.Id, x => x.MapFrom(y => y.DocEntry))
				.ForMember(x => x.constancias, x => x.Ignore())
				.ForMember(x => x.ocalendario, x => x.Ignore())
				.ForMember(x => x.FechaSolicitud, x => x.MapFrom(y => y.CreateDate))
				.ForMember(x => x.nombre, x => x.Ignore())
                .ForMember(x => x.Puesto, x => x.Ignore())
                .ForMember(x => x.U_ANIO, x => x.MapFrom(y => y.U_ANIO))
				.ForMember(x => x.U_Aprobado, x => x.MapFrom(y => y.U_Aprobado))
				.ForMember(x => x.U_CVEUNI, x => x.MapFrom(y => y.U_CVEUNI))
				.ForMember(x => x.U_DUR, x => x.MapFrom(y => y.U_DUR))
				.ForMember(x => x.U_EMP, x => x.MapFrom(y => y.U_EMP))
				.ForMember(x => x.U_FFIN, x => x.MapFrom(y => y.U_FFIN))
				.ForMember(x => x.U_FINI, x => x.MapFrom(y => y.U_FINI))
				.ForMember(x => x.U_HorarioESC, x => x.MapFrom(y => y.U_HorarioESC))
				.ForMember(x => x.motivoRechazo, x => x.MapFrom(y => y.U_MOTIVOREC))
				.ForMember(x => x.U_NUMREC, x => x.MapFrom(y => y.U_NUMREC))
				.ForMember(x => x.Observaciones, x => x.MapFrom(y => y.U_OBS))
				.ForMember(x => x.Status, x => x.MapFrom(y => y.Status))
				.ForMember(x => x.U_PER, x => x.MapFrom(y => y.U_PER));

				cfg.CreateMap<EmpleadoEstudiante, EmpleadoAsignacion>()
				.ForMember(x => x.Canceled, x => x.Ignore())
				.ForMember(x => x.CreateDate, x => x.MapFrom(y => y.FechaSolicitud))
				.ForMember(x => x.CreateTime, x => x.Ignore())
				.ForMember(x => x.DataSource, x => x.Ignore())
				.ForMember(x => x.DocEntry, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.DocNum, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.Handwrtten, x => x.Ignore())
				.ForMember(x => x.Instance, x => x.Ignore())
				.ForMember(x => x.LogInst, x => x.Ignore())
				.ForMember(x => x.Object, x => x.Ignore())
				.ForMember(x => x.Period, x => x.Ignore())
				.ForMember(x => x.Series, x => x.Ignore())
				.ForMember(x => x.Status, x => x.Ignore())
				.ForMember(x => x.Transfered, x => x.Ignore())
				.ForMember(x => x.UpdateDate, x => x.Ignore())
				.ForMember(x => x.UpdateTime, x => x.Ignore())
				.ForMember(x => x.UserSign, x => x.Ignore())
				.ForMember(x => x.U_ANIO, x => x.MapFrom(y => y.U_ANIO))
				.ForMember(x => x.U_Aprobado, x => x.MapFrom(y => y.U_Aprobado))
				.ForMember(x => x.U_CVEUNI, x => x.MapFrom(y => y.U_CVEUNI))
				.ForMember(x => x.U_DUR, x => x.MapFrom(y => y.U_DUR))
				.ForMember(x => x.U_EMP, x => x.MapFrom(y => y.U_EMP))
				.ForMember(x => x.U_FFIN, x => x.MapFrom(y => y.U_FFIN))
				.ForMember(x => x.U_FINI, x => x.MapFrom(y => y.U_FINI))
				.ForMember(x => x.U_HorarioESC, x => x.MapFrom(y => y.U_HorarioESC))
				.ForMember(x => x.U_MOTIVOREC, x => x.MapFrom(y => y.motivoRechazo))
				.ForMember(x => x.U_NUMREC, x => x.MapFrom(y => y.U_NUMREC))
				.ForMember(x => x.U_OBS, x => x.MapFrom(y => y.Observaciones))
				.ForMember(x => x.U_PER, x => x.MapFrom(y => y.U_PER))
				.ForMember(x => x.Status, x => x.MapFrom(y => y.Status));

				cfg.CreateMap<EmpleadoEstudianteHorario, MVHorario>()
				.ForMember(x => x.Id, x => x.MapFrom(y => y.DocEntry))
				.ForMember(x => x.Consecutivo, x => x.MapFrom(y => y.LineId))
				.ForMember(x => x.U_TIPOH, x => x.MapFrom(y => y.U_TIPOH))
				.ForMember(x => x.U_DIAINI, x => x.MapFrom(y => y.U_DIAINI))
				.ForMember(x => x.U_HORAINI, x => x.MapFrom(y => y.U_HORAINI))
				.ForMember(x => x.U_MINUTOINI, x => x.MapFrom(y => y.U_MINUTOINI))
				.ForMember(x => x.U_DIAFIN, x => x.MapFrom(y => y.U_DIAFIN))
				.ForMember(x => x.U_HORAFIN, x => x.MapFrom(y => y.U_HORAFIN))
				.ForMember(x => x.U_MINUTOFIN, x => x.MapFrom(y => y.U_MINUTOFIN));

				cfg.CreateMap<MVHorario,EmpleadoEstudianteHorario >()
				.ForMember(x => x.DocEntry, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.LineId, x => x.MapFrom(y => y.Consecutivo))
				.ForMember(x => x.VisOrder, x => x.MapFrom(y => y.VisOrder))
				.ForMember(x => x.Object, x => x.Ignore())
				.ForMember(x => x.LogInst, x => x.MapFrom(y => y.LogInst))
				.ForMember(x => x.U_TIPOH, x => x.MapFrom(y => y.U_TIPOH))
				.ForMember(x => x.U_DIAINI, x => x.MapFrom(y => y.U_DIAINI))
				.ForMember(x => x.U_HORAINI, x => x.MapFrom(y => y.U_HORAINI))
				.ForMember(x => x.U_MINUTOINI, x => x.MapFrom(y => y.U_MINUTOINI))
				.ForMember(x => x.U_DIAFIN, x => x.MapFrom(y => y.U_DIAFIN))
				.ForMember(x => x.U_HORAFIN, x => x.MapFrom(y => y.U_HORAFIN))
				.ForMember(x => x.U_MINUTOFIN, x => x.MapFrom(y => y.U_MINUTOFIN));

				cfg.CreateMap<Solicitud,TablaSolicitudes>()
				.ForMember(x => x.Canceled, x => x.Ignore())
				.ForMember(x => x.CreateDate, x => x.Ignore())
				.ForMember(x => x.CreateTime, x => x.Ignore())
				.ForMember(x => x.DataSource, x => x.Ignore())
				.ForMember(x => x.DocEntry, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.DocNum, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.Handwrtten, x => x.Ignore())
				.ForMember(x => x.Instance, x => x.Ignore())
				.ForMember(x => x.LogInst, x => x.Ignore())
				.ForMember(x => x.Object, x => x.Ignore())
				.ForMember(x => x.Period, x => x.Ignore())
				.ForMember(x => x.Series, x => x.Ignore())
				.ForMember(x => x.Status, x => x.Ignore())
				.ForMember(x => x.Transfered, x => x.Ignore())
				.ForMember(x => x.UpdateDate, x => x.Ignore())
				.ForMember(x => x.UpdateTime, x => x.Ignore())
				.ForMember(x => x.UserSign, x => x.Ignore())
				.ForMember(x => x.U_CIA, x => x.MapFrom(y => ""))
				.ForMember(x => x.U_CON, x => x.MapFrom(y => y.TipoSolicitud))
				.ForMember(x => x.U_DSOL, x => x.MapFrom(y => (((TimeSpan)(y.FechaFinal - y.FechaInicial)).Days)+1))
				.ForMember(x => x.U_EMP, x => x.MapFrom(y => y.NumEmpleado))
				.ForMember(x => x.U_EMPSUP, x => x.MapFrom(y => ""))
				.ForMember(x => x.U_FECSOL, x => x.MapFrom(y => y.FechaSolicitud))
				.ForMember(x => x.U_FIV, x => x.MapFrom(y => y.FechaInicial))
				.ForMember(x => x.U_FTV, x => x.MapFrom(y => y.FechaFinal))
				.ForMember(x => x.U_LOC, x => x.MapFrom(y => ""))
				.ForMember(x => x.U_MOTR, x => x.MapFrom(y => ""))
				.ForMember(x => x.U_SRH, x => x.MapFrom(y => "P"))
				.ForMember(x => x.U_SSUP, x => x.MapFrom(y => "P"))
				.ForMember(x => x.U_STSV, x => x.MapFrom(y => "P"))
                .ForMember(x => x.U_SNOMINIX, x => x.Ignore());

                cfg.CreateMap<Mensajes, Notificaciones>()
				.ForMember(x => x.empleadoNotifica, x => x.MapFrom(y => y.U_EMPORI))
				.ForMember(x => x.empleadoRecibe, x => x.MapFrom(y => y.U_EMPDEST))
				.ForMember(x => x.fechaEntrega, x => x.MapFrom(y => y.U_FFM))
				.ForMember(x => x.fechaLectura, x => x.MapFrom(y => y.U_FRM))
				.ForMember(x => x.Id, x => x.MapFrom(y => y.DocEntry))
				.ForMember(x => x.identificadorProceso, x => x.MapFrom(y => y.U_IDPROC))
				.ForMember(x => x.notificacion, x => x.MapFrom(y => y.U_MENSAJE))
				.ForMember(x => x.idProcesoNotificacion, x => x.MapFrom(y => y.U_IDNOT))
				.ForMember(x => x.parametros, x => x.MapFrom(y => y.U_JSONPR))
                .ForMember(x => x.rol, x => x.MapFrom(y => y.Instance))
				.ForMember(x => x.rol, x => x.MapFrom(y => y.U_ROLORG))
				.ForMember(x => x.rolDestino, x => x.MapFrom(y => y.U_ROLDEST))
				.ForMember(x => x.status, x => x.MapFrom(y => y.U_STAT));

                cfg.CreateMap<Notificaciones,Mensajes>()
				.ForMember(x => x.U_EMPORI, x => x.MapFrom(y => y.empleadoNotifica))
				.ForMember(x => x.U_EMPDEST, x => x.MapFrom(y => y.empleadoRecibe))
				.ForMember(x => x.U_FFM, x => x.MapFrom(y => y.fechaEntrega))
				.ForMember(x => x.U_FRM, x => x.MapFrom(y => y.fechaLectura))
				.ForMember(x => x.DocEntry, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.U_IDPROC, x => x.MapFrom(y => y.identificadorProceso))
				.ForMember(x => x.U_MENSAJE, x => x.MapFrom(y => y.notificacion))
				.ForMember(x => x.U_IDNOT, x => x.MapFrom(y => y.idProcesoNotificacion))
				.ForMember(x => x.U_JSONPR, x => x.MapFrom(y => y.parametros))
				.ForMember(x => x.U_STAT, x => x.MapFrom(y => y.status))
				.ForMember(x => x.Canceled, x => x.Ignore())
				.ForMember(x => x.CreateDate, x => x.Ignore())
				.ForMember(x => x.CreateTime, x => x.Ignore())
				.ForMember(x => x.DataSource, x => x.Ignore())
				.ForMember(x => x.DocEntry, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.DocNum, x => x.MapFrom(y => y.Id))
				.ForMember(x => x.Handwrtten, x => x.Ignore())
                //.ForMember(x => x.Instance, x => x.Ignore())
                .ForMember(x => x.Instance, x => x.MapFrom(y => y.rol))
				.ForMember(x => x.U_ROLORG, x => x.MapFrom(y => y.rol))
				.ForMember(x => x.U_ROLDEST, x => x.MapFrom(y => y.rolDestino))
				.ForMember(x => x.LogInst, x => x.Ignore())
				.ForMember(x => x.Object, x => x.Ignore())
				.ForMember(x => x.Period, x => x.Ignore())
				.ForMember(x => x.Series, x => x.Ignore())
				.ForMember(x => x.Status, x => x.Ignore())
				.ForMember(x => x.Transfered, x => x.Ignore())
				.ForMember(x => x.UpdateDate, x => x.Ignore())
				.ForMember(x => x.UpdateTime, x => x.Ignore())
				.ForMember(x => x.UserSign, x => x.Ignore());
			});
		}
	}
}
