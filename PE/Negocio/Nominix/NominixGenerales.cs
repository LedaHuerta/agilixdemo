﻿using Aplics.Servicios.Entidades.DAO;
using Aplics.Servicios.Entidades.DAO.Nominix;
using Aplics.Servicios.Entidades.Horarios;
using Aplics.Servicios.Modelos.Generales;
using Aplics.Servicios.Modelos.Nominix;
using Aplics.Servicios.Modelos.Solicitudes;
using AutoMapper;
using GRC.Mensajeria.Modelos;
using GRC.Servicios.Data.DAO.EF;
using GRC.Servicios.Utilidades;
using GRC.Servicios.Utilidades.Genericos;
using PortalEmpleadosV2.Entidades.DAO.Nominix;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplics.Servicios.Negocio.Nominix
{
	public static class NominixGenerales
	{
		static IMapper mapper = new Mapeado().config.CreateMapper();
		public static Empleados ObtenerEmpleado(String codEmpleado,int _UserSign)
		{
			try
			{
				DAOCRUDNominixTM<Empleados> _repositorioE = new CreateDAO().GeneraDAOEmpleados(_UserSign);
				QueryParameters<Empleados> parametrosE = new QueryParameters<Empleados>();
				parametrosE.where = x => x.Code.Equals(codEmpleado);
				Empleados empleado = _repositorioE.EncontrarPor(parametrosE).FirstOrDefault();
				_repositorioE.Dispose();
				return empleado;
			}
			catch (Exception ex)
			{
				throw ex; ;
			}
		}

		public static List<Empleados> ObtenerSubordinados(String codEmpleado, int _UserSign)
		{
			try
			{
				DAOCRUDNominixTM<Empleados> _repositorioE = new CreateDAO().GeneraDAOEmpleados(_UserSign);
				QueryParameters<Empleados> parametrosE = new QueryParameters<Empleados>();
				parametrosE.where = x => x.U_Empsup.Equals(codEmpleado);
				List<Empleados> subordinados = _repositorioE.EncontrarPor(parametrosE).ToList();
				_repositorioE.Dispose();
				return subordinados;
			}
			catch (Exception ex)
			{
				throw ex; ;
			}
		}

		public static void GuardarEmpleado(Empleados empleado, int _UserSign)
		{
			try
			{
				DAOCRUDNominixTM<Empleados> _repositorioE = new CreateDAO().GeneraDAOEmpleados(_UserSign);
				_repositorioE.Actualizar(empleado);

				_repositorioE.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static List<Empleados> ObtenerEmpleados(QueryParameters<Empleados> parametrosE, int _UserSign)
		{
			try
			{
				DAOCRUDNominixTM<Empleados> _repositorioE = new CreateDAO().GeneraDAOEmpleados(_UserSign);
				List<Empleados> empleados = _repositorioE.EncontrarPor(parametrosE).ToList();
				_repositorioE.Dispose();
				return empleados;
			}
			catch (Exception ex)
			{
				throw ex; 
			}
		}

		public static List<EmpleadosRelaciones> ListaEmpleadoRelaciones(String numEmpleado, int _UserSign)
		{
			List<EmpleadosRelaciones> empleadosRelaciones = new List<EmpleadosRelaciones>();
			try
			{
				DAOCRUDNominixTLM<EmpleadosRelaciones> repositorioEmp = new CreateDAO().GeneraDAOEmpleadosRelaciones(_UserSign);
				QueryParameters<EmpleadosRelaciones> parametrosr = new QueryParameters<EmpleadosRelaciones>();
				parametrosr.where = x => x.Code.Equals(numEmpleado);
				empleadosRelaciones = repositorioEmp.EncontrarPor(parametrosr).ToList();
				repositorioEmp.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleadosRelaciones;
		}

		public static String empleadoRelaciones(String numEmpleado, int _UserSign)
		{
			String empleadosrel = String.Empty;
			try
			{
				List<EmpleadosRelaciones> empleadosRelaciones = ListaEmpleadoRelaciones(numEmpleado, _UserSign);
				if (empleadosRelaciones.Count > 0)
				{
					List<String> empleados = empleadosRelaciones.Select(x => x.U_EMPREL).ToList();
					empleadosrel = String.Join(",", empleados.ToArray());
					empleadosrel = "'" + empleadosrel.Replace(",", "','") + "'";

				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return empleadosrel;
		}


		public static Notificaciones consultarNotificacionXid(int _UserSign, int Id)
		{
			Notificaciones notificaciones = new Notificaciones();
			try
			{
				DAOCRUDNominixTD<Mensajes> repoMensajes = new CreateDAO().GeneraDAOMensajes(_UserSign);
				QueryParameters<Mensajes> parametros = new QueryParameters<Mensajes>();
				parametros.where = x => x.DocEntry == Id;
				Mensajes mensajes = repoMensajes.EncontrarPor(parametros).FirstOrDefault();
				notificaciones = mapper.Map<Mensajes, Notificaciones>(mensajes);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return notificaciones;
		}

		public static List<Notificaciones> consultarNotificaciones(int _UserSign, String empleado, String perfil = "")
		{
			List<Notificaciones> notificaciones = new List<Notificaciones>();
			try
			{
				DAOCRUDNominixTD<Mensajes> repoMensajes = new CreateDAO().GeneraDAOMensajes(_UserSign);
				QueryParameters<Mensajes> parametros = new QueryParameters<Mensajes>();
				if (String.IsNullOrEmpty(perfil))
				{
					parametros.where = x => x.U_EMPDEST.Equals(empleado) && x.U_FRM == null;
				}
				else
				{
					parametros.where = x => x.U_EMPDEST.Equals(empleado) && x.U_ROLDEST.Equals(perfil) && x.U_FRM == null;
				}
				List<Mensajes> mensajes = repoMensajes.EncontrarPor(parametros).ToList();
				notificaciones = mapper.Map<List<Mensajes>, List<Notificaciones>>(mensajes);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return notificaciones;
		}

		public static Notificaciones Notificar(Notificaciones notificacion,Boolean alta,int _UserSign)
		{
			try
			{
				Mensajes mensaje = mapper.Map<Notificaciones, Mensajes>(notificacion);
				DAOCRUDNominixTD<Mensajes> repoMensajes= new CreateDAO().GeneraDAOMensajes(_UserSign);
				if (alta)
				{
					mensaje = repoMensajes.AgregarIdentity(mensaje);
				}
				else
				{
					repoMensajes.Actualizar(mensaje);
				}
				repoMensajes.Dispose();
				notificacion= mapper.Map<Mensajes,Notificaciones>(mensaje);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return notificacion;
		}

		public static string ObtenerCorreosEmpleados(String empleadosfiltro,int _UserSign)
		{
			String emailsNotificacion = String.Empty;
			try
			{
				DAOEmpleados repEmpleados = new CreateDAO().GeneraDAOEmpleadosCustom(_UserSign);
				List<String> correosEmpleados = repEmpleados.CorreosEmpleados(empleadosfiltro);
				if (correosEmpleados.Count > 0)
				{
					emailsNotificacion = String.Join(",", correosEmpleados.ToArray());
				}
				repEmpleados.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return emailsNotificacion;
		}

		public static String empleadoNotificar(String concepto,String empleadoSuperior, String empleadoRH,int _UserSign)
		{
			string empleado = String.Empty;
			try
			{
				//Tabla de criterios Header.
				DAOCRUDNominixTLM<CriteriosConceptosH> _repositorioCriteriosH = new CreateDAO().GeneraDAOCriteriosConceptosHeader(_UserSign);
				QueryParameters<CriteriosConceptosH> parametrosch = new QueryParameters<CriteriosConceptosH>();
				parametrosch.where = x => x.Code == concepto;
				List<CriteriosConceptosH> conceptos = _repositorioCriteriosH.EncontrarPor(parametrosch).ToList();
				if (conceptos.Count > 0)
				{
					String tipoNotificacion = conceptos[0].U_NOTIF;
					if (tipoNotificacion.Equals("ADMIN"))
					{
						empleado = empleadoRH;
					}
					else if(tipoNotificacion.Equals("SUP"))
					{
						empleado = empleadoSuperior;
					}
				}
				_repositorioCriteriosH.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return empleado;
		}


		public static void notificacionPortal(int _UserSign, String empleadoNotifica,
			String empleadoRecibe, String mensaje, String parametros,String identificacionProceso, int rol, String idProcesoNotificacion="")
		{
			try
			{
				Notificaciones notificacion = new Notificaciones();
				notificacion.empleadoNotifica = empleadoNotifica;
				notificacion.empleadoRecibe = empleadoRecibe;
				notificacion.Id = 0;
				notificacion.fechaEntrega = System.DateTime.Now;
				notificacion.notificacion=mensaje;
				notificacion.parametros = parametros;
				notificacion.status = "E"; //Envíado.
				notificacion.identificadorProceso = identificacionProceso;
                notificacion.rol = rol;
				//Recuperamos el rol del empleado destino.
				DAOCRUDNominixTM<Usuarios> _repoUsuarios = new CreateDAO().GeneraDAOUsuarios(_UserSign);
				QueryParameters<Usuarios> parametrosusu = new QueryParameters<Usuarios>();
				parametrosusu.where = x => x.Code.Equals(empleadoRecibe);
				Usuarios usuario = _repoUsuarios.EncontrarPor(parametrosusu).FirstOrDefault();
				notificacion.rolDestino = 0;
				if (usuario != null && usuario.U_TIPUSU!=null)
				{
					int rold = Convert.ToInt32(usuario.U_TIPUSU);
					notificacion.rolDestino = rold;
				} 				
				notificacion.idProcesoNotificacion = idProcesoNotificacion;
				notificacion = NominixGenerales.Notificar(notificacion, true, _UserSign);
				
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static RespuestaSimple notificar(int _UserSign,SendMails sm, String asunto, String detalle, String empleadoNotifica,
			String empleadoRecibe, String mensaje, String parametros, String empleadosfiltro, String emailsNotificacion,String idProceso,int rol, String idProcesoNotificacion="")
		{
			RespuestaSimple respuesta = new RespuestaSimple();
			//Error por default.
			respuesta.result = 0;
			respuesta.mensaje = String.Empty;
			try
			{
                //notificacionPortal(_UserSign,empleadoNotifica,empleadoRecibe,mensaje,parametros, idProceso);
                notificacionPortal(_UserSign, empleadoNotifica, empleadoRecibe, mensaje, parametros, idProceso,rol , idProcesoNotificacion);

                if (empleadosfiltro.IndexOf(',') > 0)
				{
					List<String> ListaEmpleados = empleadosfiltro.Split(',').ToList();
					foreach (String empleado in ListaEmpleados)
					{
                        //notificacionPortal(_UserSign, empleadoNotifica, empleado, mensaje, parametros, idProceso);
                        notificacionPortal(_UserSign, empleadoNotifica, empleado, mensaje, parametros, idProceso,rol, idProcesoNotificacion);
                    }
				}

				if (!emailsNotificacion.Equals(String.Empty))
				{
					VMMailMessage mail = new VMMailMessage();
					mail.asunto = asunto;
					mail.mensaje = mensaje;
					mail.to = emailsNotificacion;

					mail.asuntoDetalle = detalle;
					mail.mensaje = sm.mensajePlantilla(mail.asuntoDetalle, mail.mensaje, mail.asunto);
					respuesta = sm.SendMail(mail);
				}
				else
				{
					respuesta.mensaje = "No es posible notificar por medio de correo electónico.";
				}
			}
			catch (Exception ex)
			{
				respuesta.mensaje = ex.Message.ToString();
			}
			return respuesta;
		}


		public static List<Documento> permisosDocumentos(String url, String rootPath, String rolConsulta, String noEmpleado, String pRuta = "")
		{
			List<Documento> Documentos = new List<Documento>();
			try
			{
				List<String> filtroEmpleados = new List<String>();
				switch (rolConsulta)
				{
					case "1":
						Documentos = getDocumentos(url, rootPath, pRuta);
						break;
					case "2":
						filtroEmpleados = Subordinados(noEmpleado);
						filtroEmpleados.Add(noEmpleado);
						Documentos = getDocumentos(url, rootPath, pRuta, filtroEmpleados);
						break;
					case "3":
					case "4":
						filtroEmpleados.Add(noEmpleado);
						Documentos = getDocumentos(url, rootPath, pRuta, filtroEmpleados);
						break;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Documentos;
		}

		private static List<Documento> getDocumentos(String urlBase, String rootPath, String pruta, List<String> filtroEmpleados = null)
		{
			List<Documento> Doctos = new List<Documento>();
			try
			{
				List<CorreoEmpleado> correoEmpleados = Empleados();

				if (Directory.Exists(rootPath + pruta))
				{
					string[] allfiles = Directory.GetFileSystemEntries(rootPath + pruta, "*.*");
					String nombre = String.Empty;
					String[] ANombre;
					String ruta = String.Empty;
					String tipo = String.Empty;
					String raiz = String.Empty;
					String rutaCompleta = String.Empty;
					String propietario = String.Empty;
					int indexEmp = 0;
					String url = String.Empty;
					foreach (string file in allfiles)
					{
						FileInfo info = new FileInfo(file);
						FileAttributes attr = System.IO.File.GetAttributes(file);
						ANombre = file.Split('\\');
						nombre = ANombre[ANombre.Length - 1];
						tipo = "Documento";
						url = string.Empty;
						propietario = "Admin";
						if (attr.HasFlag(FileAttributes.Directory))
						{
							tipo = "Carpeta";
							if (!pruta.Equals(String.Empty))
							{
								raiz = pruta + nombre + "\\";
							}
							else
							{
								raiz = nombre + "\\";
							}
							indexEmp = correoEmpleados.FindIndex(x => x.NumEmpleado.Equals(nombre));
							if (indexEmp >= 0)
							{
								propietario = correoEmpleados[indexEmp].Nombre;
							}
						}
						else
						{
							rutaCompleta = file.Replace(rootPath, "");
							indexEmp = correoEmpleados.FindIndex(x => x.NumEmpleado.Equals(ANombre[ANombre.Length - 2]));
							if (indexEmp >= 0)
							{
								propietario = correoEmpleados[indexEmp].Nombre;
							}

							url = urlBase.Replace("Documentos", "");
							url += "EmpFiles/" + rutaCompleta.Replace("\\", "/");
						}
						if (tipo.Equals("Documento") || filtroEmpleados == null || nombre.ToLower().Equals("comprobantes") || nombre.ToLower().Equals("constancias") || (tipo.Equals("Carpeta") && filtroEmpleados != null && filtroEmpleados.Contains(nombre)))
						{
							Doctos.Add(new Documento() { Propietario = propietario, Nombre = nombre, Tipo = tipo, Fecha = (DateTime)info.LastWriteTime, Raiz = raiz, URL = url });
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return Doctos;
		}

		private static List<String> Subordinados(String empleado)
		{
			List<String> filtroEmpleados = new List<String>();
			try
			{
				ClaseAuditoria<QueryParameters<Empleados>> peticion = new ClaseAuditoria<QueryParameters<Empleados>>();
				QueryParameters<Empleados> parametrosE = new QueryParameters<Empleados>();
				parametrosE.where = x => x.U_Empsup.Equals(empleado);
				peticion.Clase = parametrosE;
				peticion.UserName = "aplics";
				peticion.UserSign = 1;
				List<Empleados> empleados = NominixGenerales.ObtenerEmpleados(peticion.Clase, peticion.UserSign);
				if (empleados.Count > 0)
				{
					filtroEmpleados = empleados.Select(x => x.Code).ToList();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return filtroEmpleados;
		}


		public static List<CorreoEmpleado> Empleados()
		{
			List<CorreoEmpleado> correosEmpleados = new List<CorreoEmpleado>();
			try
			{
				ClaseAuditoria<QueryParameters<Empleados>> peticion = new ClaseAuditoria<QueryParameters<Empleados>>();
				QueryParameters<Empleados> parametrosE = new QueryParameters<Empleados>();
				parametrosE.where = x => x.DocEntry > 0;
				peticion.Clase = parametrosE;
				peticion.UserName = "aplics";
				peticion.UserSign = 1;
				correosEmpleados = obtenerCorreosEmpleados(peticion);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return correosEmpleados;
		}

		public static List<CorreoEmpleado> obtenerCorreosEmpleados(ClaseAuditoria<QueryParameters<Empleados>> peticion)
		{
			List<CorreoEmpleado> correoEmpleados = new List<CorreoEmpleado>();
			try
			{
				List<Empleados> empleados = NominixGenerales.ObtenerEmpleados(peticion.Clase, peticion.UserSign);
				CorreoEmpleado correo = new CorreoEmpleado();
				foreach (Empleados empleado in empleados)
				{
					correo = new CorreoEmpleado();
					empleado.U_Nomp = String.IsNullOrEmpty(empleado.U_Nomp) ? "" : empleado.U_Nomp;
					empleado.U_Apepat = String.IsNullOrEmpty(empleado.U_Apepat) ? "" : empleado.U_Apepat;
					empleado.U_Apemat = String.IsNullOrEmpty(empleado.U_Apemat) ? "" : empleado.U_Apemat;
					correo.Nombre = empleado.U_Nomp + " " + empleado.U_Apepat + " " + empleado.U_Apemat;
					correo.NumEmpleado = empleado.Code;
					empleado.U_Email = String.IsNullOrEmpty(empleado.U_Email) ? "" : empleado.U_Email;
					correo.eMail = empleado.U_Email;
					correoEmpleados.Add(correo);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return correoEmpleados;
		}

		public static List<CatalogoGeneral> GetCatalogoNivelEsc()
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoNivelEsc();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		//replicar las demás

		public static List<CatalogoGeneral> GetCatalogoSituacionEsc()
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoSituacionEsc();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetCatalogoGradoEsc()
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoGradoEsc();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetCatalogoPeriodoEsc()
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoPeriodoEsc();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetCatalogoProfesiones()
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoProfesiones();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetCatalogoUniversidades()
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoUniversidades();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetCatalogoDepartamentos(String claveDepartamento)
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoDepartamentos(claveDepartamento);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetCatalogoPuestos(String clavePuesto)
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoPuestos(clavePuesto);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetCatalogoCategoria(String claveCategoria)
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetCatalogoCategoria(claveCategoria);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}

		public static List<CatalogoGeneral> GetEmpleadoSuperior(String EmpSup)
		{
			List<CatalogoGeneral> catalogo = new List<CatalogoGeneral>();
			try
			{
				catalogo = new DAONominix(new Entidades.AppContext()).GetEmpleadoSuperior(EmpSup);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return catalogo;
		}
	}
}
