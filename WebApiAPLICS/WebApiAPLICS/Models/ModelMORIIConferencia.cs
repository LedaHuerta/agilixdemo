namespace WebApiMORIIConferencia.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelMORIIConferencia : DbContext
    {
        public ModelMORIIConferencia()
            : base("name=MORIIConferenciaConexion")
        {
        }

        //public virtual DbSet<CT_Planes> CT_Planes { get; set; }
        //public virtual DbSet<Historial_Planes> Historial_Planes { get; set; }
        //public virtual DbSet<Planes_Activos> Planes_Activos { get; set; }
        public virtual DbSet<CT_Cuentas> CT_Cuentas { get; set; }
        //public virtual DbSet<CT_Rol> CT_Rol { get; set; }
        //public virtual DbSet<CT_Status> CT_Status { get; set; }
        //public virtual DbSet<CT_Tipos_Telefonos> CT_Tipos_Telefonos { get; set; }
        //public virtual DbSet<Datos_Fiscales> Datos_Fiscales { get; set; }
        //public virtual DbSet<Encuestas> Encuestas { get; set; }
        //public virtual DbSet<Inicios_Sesion_Activos> Inicios_Sesion_Activos { get; set; }
        //public virtual DbSet<Log_Inicios_Sesion> Log_Inicios_Sesion { get; set; }
        public virtual DbSet<Personas_Cliente> Personas_Cliente { get; set; }
        //public virtual DbSet<Preguntas_x_Encuesta> Preguntas_x_Encuesta { get; set; }
        public virtual DbSet<Relacion_Personas_Cuentas> Relacion_Personas_Cuentas { get; set; }
        //public virtual DbSet<Relacion_Preguntas_Respuestas_Fijas> Relacion_Preguntas_Respuestas_Fijas { get; set; }
        //public virtual DbSet<Respuestas_Participante_x_Encuesta> Respuestas_Participante_x_Encuesta { get; set; }
        //public virtual DbSet<RespuestasMostrar_x_Conferencia> RespuestasMostrar_x_Conferencia { get; set; }
        //public virtual DbSet<RespuestasMostrar_x_Conferencia_Fijas> RespuestasMostrar_x_Conferencia_Fijas { get; set; }
        //public virtual DbSet<Telefonos_Generales> Telefonos_Generales { get; set; }
        //public virtual DbSet<Ubicaciones_Generales_Cliente> Ubicaciones_Generales_Cliente { get; set; }
        public virtual DbSet<CT_Cuentas_Acceso> CT_Cuentas_Acceso { get; set; }
        public virtual DbSet<CT_Cuentas_Tokens> CT_Cuentas_Tokens { get; set; }
        //public virtual DbSet<CT_Grupos_Respuestas_Fijas> CT_Grupos_Respuestas_Fijas { get; set; }
        public virtual DbSet<Eventos> Eventos { get; set; }
        //public virtual DbSet<Encuestas_Base> Encuestas_Base { get; set; }
        //public virtual DbSet<Preguntas_x_Encuesta_Base> Preguntas_x_Encuesta_Base { get; set; }
        //public virtual DbSet<RespuestasMostrar_x_Conferencia_Base> RespuestasMostrar_x_Conferencia_Base { get; set; }
        //public virtual DbSet<Relacion_Preguntas_Respuestas_Fijas_Base> Relacion_Preguntas_Respuestas_Fijas_Base { get; set; }
        //public virtual DbSet<Motivos_Baja_Cuenta> Motivos_Baja_Cuenta { get; set; }
        //public virtual DbSet<Relacion_Personas_Eventos> Relacion_Personas_Eventos { get; set; }


        //public virtual DbSet<CrearCuentaNueva> CrearCuentaNueva { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.CT_Cuentas_Acceso)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.Inicios_Sesion_Activos)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.Log_Inicios_Sesion)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.Relacion_Personas_Cuentas)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.CT_Cuentas_Tokens)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.Motivos_Baja_Cuenta)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.Historial_Planes)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CT_Cuentas>()
                .HasMany(e => e.Planes_Activos)
                .WithRequired(e => e.CT_Cuentas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Personas_Cliente>()
                .HasMany(e => e.Encuestas)
                .WithRequired(e => e.Personas_Cliente)
                .HasForeignKey(e => e.Id_Personal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Personas_Cliente>()
                .HasMany(e => e.Relacion_Personas_Cuentas)
                .WithRequired(e => e.Personas_Cliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Personas_Cliente>()
                .HasMany(e => e.Respuestas_Participante_x_Encuesta)
                .WithRequired(e => e.Personas_Cliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Personas_Cliente>()
                .HasMany(e => e.Telefonos_Generales)
                .WithRequired(e => e.Personas_Cliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Personas_Cliente>()
                .HasMany(e => e.Ubicaciones_Generales_Cliente)
                .WithRequired(e => e.Personas_Cliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Personas_Cliente>()
                .HasMany(e => e.Eventos)
                .WithRequired(e => e.Personas_Cliente)
                .HasForeignKey(e => e.Id_Personal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Personas_Cliente>()
                .HasMany(e => e.Relacion_Personas_Eventos)
                .WithRequired(e => e.Personas_Cliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Eventos>()
                .HasMany(e => e.Respuestas_Participante_x_Encuesta)
                .WithRequired(e => e.Eventos)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Eventos>()
                .HasMany(e => e.Relacion_Personas_Eventos)
                .WithRequired(e => e.Eventos)
                .WillCascadeOnDelete(false);

        }
    }
}
