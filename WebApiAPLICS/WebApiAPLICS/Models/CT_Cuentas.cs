namespace WebApiMORIIConferencia.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CT_Cuentas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CT_Cuentas()
        {
            CT_Cuentas_Acceso = new HashSet<CT_Cuentas_Acceso>();
            Inicios_Sesion_Activos = new HashSet<Inicios_Sesion_Activos>();
            Log_Inicios_Sesion = new HashSet<Log_Inicios_Sesion>();
            Relacion_Personas_Cuentas = new HashSet<Relacion_Personas_Cuentas>();
            CT_Cuentas_Tokens = new HashSet<CT_Cuentas_Tokens>();
            Motivos_Baja_Cuenta = new HashSet<Motivos_Baja_Cuenta>();
            Historial_Planes = new HashSet<Historial_Planes>();
            Planes_Activos = new HashSet<Planes_Activos>();
        }

        [Key]
        [JsonProperty(PropertyName = "idAccount")]
        public int Id_Cuenta { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "idStatus")]
        public int Id_Status { get; set; }

        [JsonProperty(PropertyName = "idRol")]
        public int Id_Rol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CT_Cuentas_Acceso> CT_Cuentas_Acceso { get; set; }

        public virtual CT_Status CT_Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inicios_Sesion_Activos> Inicios_Sesion_Activos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Log_Inicios_Sesion> Log_Inicios_Sesion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Relacion_Personas_Cuentas> Relacion_Personas_Cuentas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CT_Cuentas_Tokens> CT_Cuentas_Tokens { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Motivos_Baja_Cuenta> Motivos_Baja_Cuenta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Historial_Planes> Historial_Planes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Planes_Activos> Planes_Activos { get; set; }

    }
}
