namespace WebApiMORIIConferencia.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CT_Cuentas_Tokens
    {
        [Key]
        [JsonProperty(PropertyName = "idRegistry")]
        public int Id_Registro { get; set; }

        [Required]
        [JsonProperty(PropertyName = "idAccount")]
        public int Id_Cuenta { get; set; }

        [Required]
        [JsonProperty(PropertyName = "idRole")]
        public int Id_Rol { get; set; }

        [Required]
        [JsonProperty(PropertyName = "typeApp")]
        public int TipoApp { get; set; }

        [Required]
        [JsonProperty(PropertyName = "currentToken")]
        public string TokenActual { get; set; }

        public virtual CT_Cuentas CT_Cuentas { get; set; }

        public virtual CT_Rol CT_Rol { get; set; }

    }
}
