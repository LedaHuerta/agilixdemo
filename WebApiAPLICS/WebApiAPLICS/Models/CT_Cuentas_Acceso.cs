namespace WebApiMORIIConferencia.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CT_Cuentas_Acceso
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty(PropertyName = "idAccount")]
        public int Id_Cuenta { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "type")]
        public string Tipo { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "idThird")]
        public string IdTercero { get; set; }

        //[Key]
        //[Column(Order = 2)]
        //[StringLength(50)]
        [JsonProperty(PropertyName = "password")]
        public byte[] Password { get; set; }

        [JsonProperty(PropertyName = "temporaryPassword")]
        public byte[] PasswordTemporal { get; set; }
        //[StringLength(50)]
        //public string PasswordTemporal { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "urlPasswordReset")]
        public string URL_seteo_password { get; set; }

        [Key]
        //[Column(Order = 3)]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty(PropertyName = "idStatus")]
        public int Id_Status { get; set; }

        public virtual CT_Cuentas CT_Cuentas { get; set; }

        public virtual CT_Status CT_Status { get; set; }
    }
}
