namespace WebApiMORIIConferencia.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Relacion_Personas_Cuentas
    {
        [Key]
        [JsonProperty(PropertyName = "idRegistry")]
        public int Id_Registro { get; set; }

        [JsonProperty(PropertyName = "idPerson")]
        public int Id_Persona { get; set; }

        [JsonProperty(PropertyName = "idAccount")]
        public int Id_Cuenta { get; set; }

        public virtual CT_Cuentas CT_Cuentas { get; set; }

        public virtual Personas_Cliente Personas_Cliente { get; set; }
    }
}
