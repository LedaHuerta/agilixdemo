﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMORIIConferencia.Models
{
    public class Error
    {
        [Required]
        [JsonProperty(PropertyName = "typeError")]
        public int TipoError { get; set; }

        [Required]
        [JsonProperty(PropertyName = "description")]
        public string Descripcion { get; set; }

        [JsonProperty(PropertyName = "value1")]
        public string Valor1 { get; set; }
    }
}