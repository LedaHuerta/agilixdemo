﻿using Newtonsoft.Json;
using System;

namespace WebApiMORIIConferencia.Models
{
    public class LoginRequest
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        //1 = Conferencista, 2 = Participante
        [JsonProperty(PropertyName = "idRole")]
        public int Id_Rol { get; set; }

        //1 = Movil, 2 = Web
        [JsonProperty(PropertyName = "appType")]
        public int TipoApp { get; set; }

    }
}