namespace WebApiMORIIConferencia.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Eventos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Eventos()
        {
            Respuestas_Participante_x_Encuesta = new HashSet<Respuestas_Participante_x_Encuesta>();
            Relacion_Personas_Eventos = new HashSet<Relacion_Personas_Eventos>();
        }

        [Key]
        [JsonProperty(PropertyName = "idEvent")]
        public int Id_Evento { get; set; }

        [Required]
        [StringLength(200)]
        [JsonProperty(PropertyName = "name")]
        public string Nombre { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "codeEvent")]
        public string Codigo_Evento { get; set; }

        [JsonProperty(PropertyName = "codeValidity")]
        public DateTime? Vigencia_Codigo { get; set; }

        [JsonProperty(PropertyName = "eventDate")]
        public DateTime? Fecha_Evento { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "eventPlace")]
        public string Lugar_Evento { get; set; }

        [JsonProperty(PropertyName = "idPoll")]
        public int Id_Encuesta { get; set; }

        [JsonProperty(PropertyName = "idPerson")]
        public int Id_Personal { get; set; }

        [JsonProperty(PropertyName = "idStatus")]
        public int Id_Status { get; set; }

        public virtual CT_Status CT_Status { get; set; }

        public virtual Encuestas Encuestas { get; set; }

        public virtual Personas_Cliente Personas_Cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Respuestas_Participante_x_Encuesta> Respuestas_Participante_x_Encuesta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Relacion_Personas_Eventos> Relacion_Personas_Eventos { get; set; }
    }
}
