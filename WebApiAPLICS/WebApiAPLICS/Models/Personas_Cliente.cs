namespace WebApiMORIIConferencia.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Personas_Cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Personas_Cliente()
        {
            Encuestas = new HashSet<Encuestas>();
            Eventos = new HashSet<Eventos>();
            Relacion_Personas_Cuentas = new HashSet<Relacion_Personas_Cuentas>();
            Respuestas_Participante_x_Encuesta = new HashSet<Respuestas_Participante_x_Encuesta>();
            Telefonos_Generales = new HashSet<Telefonos_Generales>();
            Ubicaciones_Generales_Cliente = new HashSet<Ubicaciones_Generales_Cliente>();
            Relacion_Personas_Eventos = new HashSet<Relacion_Personas_Eventos>();
        }

        [Key]
        [JsonProperty(PropertyName = "idPerson")]
        public int Id_Persona { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "name")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "sureName")]
        public string PrimerApellido { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "secondSureName")]
        public string SegundoApellido { get; set; }

        [Required]
        [JsonProperty(PropertyName = "dateOfBirth")]
        public DateTime FechaNacimiento { get; set; }

        [Required]
        [JsonProperty(PropertyName = "gender")]
        public int Sexo { get; set; }

        [JsonProperty(PropertyName = "idRole")]
        public int Id_Rol { get; set; }

        [JsonProperty(PropertyName = "idStatus")]
        public int Id_Status { get; set; }

        [Required]
        [JsonProperty(PropertyName = "idPlan")]
        public int Id_Plan { get; set; }

        public virtual CT_Rol CT_Rol { get; set; }

        public virtual CT_Status CT_Status { get; set; }

        public virtual CT_Planes CT_Planes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Encuestas> Encuestas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Eventos> Eventos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Relacion_Personas_Cuentas> Relacion_Personas_Cuentas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Respuestas_Participante_x_Encuesta> Respuestas_Participante_x_Encuesta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Telefonos_Generales> Telefonos_Generales { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ubicaciones_Generales_Cliente> Ubicaciones_Generales_Cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Relacion_Personas_Eventos> Relacion_Personas_Eventos { get; set; }

    }
}
