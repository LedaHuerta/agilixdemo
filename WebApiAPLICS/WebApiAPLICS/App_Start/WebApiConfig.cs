﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApiMORIIConferencia.Security;

namespace WebApiMORIIConferencia
{
    public static class WebApiConfig
    {

        private static string GetAllowedOrigins()
        {
            //Make a call to the database to get allowed origins and convert to a comma separated string
            return "https://misitio.com/,http://localhost:3000";
        }

        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web
            //var cors =
            //    new EnableCorsAttribute(origins: "http://localhost:3000", headers: "*", methods: "*");

            //var cors =
            //    new EnableCorsAttribute(origins: "https://show.morii.com.mx", headers: "*", methods: "*");

            //string origins = GetAllowedOrigins();
            //var cors = new EnableCorsAttribute(origins, "*", "*");

            //config.EnableCors(cors);

            // Web API configuration and services
            config.EnableCors();

            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new TokenValidationHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
