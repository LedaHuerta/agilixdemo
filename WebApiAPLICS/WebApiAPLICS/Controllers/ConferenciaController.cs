﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using WebApiMORIIConferencia.Models;
using WebApiMORIIConferencia.ModelsNoDB;
using WebApiMORIIConferencia.DataQueys;
using WebApiMORIIConferencia.Functions;
using System.Data.Entity.Validation;
using System.Configuration;

namespace WebApiMORIIConferencia.Controllers
{

    

    [Authorize]
    [RoutePrefix("api/aplics")]
    public class ConferenciaController : ApiController
    {

        private ModelMORIIConferencia db = new ModelMORIIConferencia();
        private DataInsert di = new DataInsert();
        private FunctionsGeneric fc = new FunctionsGeneric();

        [HttpGet]
        [Route("uno/dos/tres/{idEvento}")]
        public IHttpActionResult uno(int idEvento)
        {
            Error modError = new Error();

            int FlagRes = 0;

            if (int.TryParse(idEvento.ToString(), out FlagRes) == false)
            {
                modError.TipoError = 3;
                modError.Descripcion = "asdfadsf";
                modError.Valor1 = null;
                return Content(HttpStatusCode.Conflict, modError);
            }

            Eventos eventos = db.Eventos.Find(idEvento);

            string query = string.Format("exec sp_uno {0}", idEvento);
            var a = db.Database.SqlQuery<ReporteEventoResultadosGeneral>(query).ToList<ReporteEventoResultadosGeneral>();

            List<ReporteGeneral_EstructuraRespuesta> respuestasListRes = null;
            List<ReporteGeneral_EstructuraPregunta> preguntasListRes = new List<ReporteGeneral_EstructuraPregunta>();
            ReporteGeneral_EstructuraRespuesta respuestas = null;
            ReporteGeneral_EstructuraPregunta preguntas = null;
            ReporteGeneral_EstructuraBase reporteGral = null;

            var dataGeneral = a.OrderBy(x => x.id_Evento).FirstOrDefault<ReporteEventoResultadosGeneral>();
            var preguntasListDist = a.Select(x => new { x.Id_Pregunta_Encuesta, x.Pregunta }).Distinct();

            reporteGral = new ReporteGeneral_EstructuraBase();

            reporteGral.id_evento = idEvento;
            reporteGral.evento = dataGeneral.Evento;
            reporteGral.Fecha_Evento = dataGeneral.Fecha_Evento;
            reporteGral.total_asistentes = dataGeneral.totalParticipantes;

            foreach (var item in preguntasListDist)
            {
                respuestasListRes = new List<ReporteGeneral_EstructuraRespuesta>();
                preguntas = new ReporteGeneral_EstructuraPregunta();

                preguntas.id_pregunta = item.Id_Pregunta_Encuesta;
                preguntas.pregunta = item.Pregunta;

                var respuestasList = a.Where(x => x.Id_Pregunta_Encuesta == item.Id_Pregunta_Encuesta).ToList<ReporteEventoResultadosGeneral>();
                foreach (var item2 in respuestasList)
                {
                    respuestas = new ReporteGeneral_EstructuraRespuesta();
                    respuestas.id_respuesta = item2.IdRespuesta;
                    respuestas.id_tipo_respuesta = item2.Id_Grupo_Respuesta;
                    respuestas.respuesta = item2.Respuesta;
                    respuestas.total_respuestas = item2.Total;
                    respuestasListRes.Add(respuestas);
                }

                preguntas.listado_repuestas = respuestasListRes;

                preguntasListRes.Add(preguntas);
            }

            reporteGral.listado_preguntas = preguntasListRes;

            var data = new
            {
                data = reporteGral
            };

            return Ok(data);
        }

        [HttpPost]
        [Route("uno/dos/tres/{id}")]
        public IHttpActionResult dos(int id,[FromBody]ValidaPassword mod_validapassword)
        {
            try
            {
                Error modError = new Error();

                if (id <= 0)
                {
                    modError.TipoError = 3;
                    modError.Descripcion = "asdfadfs";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.Conflict, modError);
                }

                if (mod_validapassword == null)
                {
                    modError.TipoError = 3;
                    modError.Descripcion = "asdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.Conflict, modError);
                }

                CT_Cuentas ctCuentas = db.CT_Cuentas.Find(id);

                if (ctCuentas == null)
                {
                    modError.TipoError = 4;
                    modError.Descripcion = "asdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.NotFound, modError);
                }

                var res = fc.ValidamosPassword(id, mod_validapassword.password);

                if (res == 1)
                {
                    return Ok(new
                    {
                        data = new
                        {
                            passwordValidate = true
                        }
                    });
                }
                else
                {
                    return Ok(new
                    {
                        data = new
                        {
                            passwordValidate = false
                        }
                    });
                }

            }
            catch (Exception ex)
            {
                Error modError = new Error();
                modError.TipoError = 100;
                modError.Descripcion = "Error interno: " + ex.Message.ToString();
                modError.Valor1 = null;

                return Content(HttpStatusCode.InternalServerError, modError);
                throw;
            }
        }

        [HttpPut]
        [Route("uno/dos/tres/{id}")]
        public IHttpActionResult tres(int id, [FromBody]UpdatePassword mod_updatepassword)
        {
            try
            {
                Error modError = new Error();

                if (id <= 0)
                {
                    modError.TipoError = 3;
                    modError.Descripcion = "asdfasdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.Conflict, modError);
                }

                CT_Cuentas ctCuentas = db.CT_Cuentas.Find(id);

                if (ctCuentas == null)
                {
                    modError.TipoError = 4;
                    modError.Descripcion = "asdfasdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.NotFound, modError);
                }

                if (ctCuentas.Id_Status == 2)
                {
                    modError.TipoError = 4;
                    modError.Descripcion = "asdfasdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.NotFound, modError);
                }

                if (ctCuentas.Id_Status == 3)
                {
                    modError.TipoError = 4;
                    modError.Descripcion = "asdfasdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.NotFound, modError);
                }


                //obtenemos el nombre
                var relacionPersonasCuentas = db.Relacion_Personas_Cuentas.Where(x => x.Id_Cuenta == id).FirstOrDefault<Relacion_Personas_Cuentas>();
                var idPersona = relacionPersonasCuentas.Id_Persona;
                Personas_Cliente personas = db.Personas_Cliente.Find(idPersona);

                var resUpdate = di.ActualizarPassword(id, mod_updatepassword.PasswordOriginal, mod_updatepassword.PasswordNuevo);

                if (resUpdate == 2)
                {
                    modError.TipoError = 4;
                    modError.Descripcion = "asdfasdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.NotFound, modError);
                }
                
                return Ok(new
                {
                    data = new
                    {
                        valueResult = true
                    }
                });


            }
            catch (Exception ex)
            {
                Error modError = new Error();
                modError.TipoError = 100;
                modError.Descripcion = "Error interno: " + ex.Message.ToString();
                modError.Valor1 = null;

                return Content(HttpStatusCode.InternalServerError, modError);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpPut]
        [Route("uno/dos/tres/cuatro")]
        public IHttpActionResult cuatro(RestaurarPasswordAnonimo passwordAnonimoMod)
        {
            Error modError = new Error();

            if (passwordAnonimoMod == null)
            {
                modError.TipoError = 3;
                modError.Descripcion = "asdfasdf";
                modError.Valor1 = null;
                return Content(HttpStatusCode.BadRequest, modError);
            }

            //validamos que venga la informacion correctamente de acuerdo a lo establecido en el modelo.
            if (ModelState.IsValid == false)
            {
                return Content(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                var res = di.RestaurarPasswordAnonimo(passwordAnonimoMod.PasswordTemporal, passwordAnonimoMod.PasswordNuevo);

                if (res != 1)
                {
                    modError.TipoError = 100;
                    modError.Descripcion = "asdfasdf";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.InternalServerError, modError);
                }
            }
            catch (Exception ex)
            {
                //return InternalServerError();
                modError.TipoError = 100;
                modError.Descripcion = "Error interno: " + ex.Message.ToString();
                modError.Valor1 = null;

                var data = new
                {
                    error = new[]
                    {
                        modError
                    }
                };

                //return Content(HttpStatusCode.InternalServerError, modError);
                return Content(HttpStatusCode.InternalServerError, data);
                throw;
            }

            //return Ok();

            return Ok(new
            {
                data = new
                {
                    valueResult = true
                }
            });
        }

        //BORRAR UN EVENTO
        [HttpDelete]
        [Route("events/delete/{id}")]
        public IHttpActionResult BorrarEvento(int id)
        {
            try
            {
                Error modError = new Error();
                Eventos evento = db.Eventos.Find(id);

                if (evento == null)
                {
                    modError.TipoError = 4;
                    modError.Descripcion = "El id del evento a borrar no se encuentra";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.NotFound, modError);
                }

                if (evento.Id_Status == 2 || evento.Id_Status == 4)
                {
                    modError.TipoError = 4;
                    modError.Descripcion = "El id del evento a borrar no se encuentra, esta inactivo o en proceso. Revise por favor";
                    modError.Valor1 = null;
                    return Content(HttpStatusCode.NotFound, modError);
                }

                var res = di.BorrarEvento(id);

                if (res == 0)
                {
                    return InternalServerError();
                }

                return Ok(new
                {
                    data = new
                    {
                        valueResult = true
                    }
                });
            }
            catch (Exception ex)
            {
                //return InternalServerError();
                Error modError = new Error();
                modError.TipoError = 100;
                modError.Descripcion = "Error interno: " + ex.Message.ToString();
                modError.Valor1 = null;

                return Content(HttpStatusCode.InternalServerError, modError);
                throw;
            }
        }

    }
}
