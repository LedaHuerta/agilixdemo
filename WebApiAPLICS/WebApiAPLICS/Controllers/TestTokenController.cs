﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiMORIIConferencia.Controllers
{
    /// <summary>
    /// test controller class para probar el security token
    /// </summary>
    [Authorize]
    [RoutePrefix("api/testtoken")]
    public class TestTokenController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetId(int id)
        {
            var customerFake = "test-fake";
            return Ok(customerFake);
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var customersFake = new string[] { "test-1", "test-2", "test-3" };
            return Ok(customersFake);
        }
    }
}
