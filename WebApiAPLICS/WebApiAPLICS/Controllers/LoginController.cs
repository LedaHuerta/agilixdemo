﻿using System;
using System.Net;
using System.Threading;
using System.Web.Http;
using WebApiMORIIConferencia.Models;
using WebApiMORIIConferencia.Security;
using WebApiMORIIConferencia.Functions;
using System.Linq;

namespace WebApiMORIIConferencia.Controllers
{
    /// <summary>
    /// login controller class para autenticar usuarios
    /// </summary>
    [AllowAnonymous]
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {

        private ModelMORIIConferencia db = new ModelMORIIConferencia();
        private FunctionsGeneric fc = new FunctionsGeneric();

        [HttpGet]
        [Route("testping")]
        public IHttpActionResult EchoPing()
        {
            return Ok(true);
        }

        [HttpGet]
        [Route("testuserauthenticated")]
        public IHttpActionResult EchoUser()
        {
            var identity = Thread.CurrentPrincipal.Identity;
            return Ok($" Principal-user: {identity.Name} - IsAuthenticated: {identity.IsAuthenticated}");
        }

        [HttpPost]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(LoginRequest login)
        {
            Error modError = new Error();

            if (login.Id_Rol != 1 && login.Id_Rol != 2)
            {
                modError.TipoError = 3;
                modError.Descripcion = "El rol es un valor incorrecto";
                modError.Valor1 = null;
                return Content(HttpStatusCode.BadRequest, modError);
            }

            if (login.TipoApp != 1 && login.TipoApp != 2)
            {
                modError.TipoError = 3;
                modError.Descripcion = "El tipo de app es incorrecto";
                modError.Valor1 = null;
                return Content(HttpStatusCode.BadRequest, modError);
            }

            if (login == null)
            {
                modError.TipoError = 3;
                modError.Descripcion = "Faltan datos para el modelo login";
                modError.Valor1 = null;
                return Content(HttpStatusCode.BadRequest, modError);
            }

            //Valida las credenciales!!

            //buscamos la cuenta
            //var cuenta = db.CT_Cuentas.Where(x => x.Email == login.Username && x.Id_Status == 1).FirstOrDefault<CT_Cuentas>();
            //var cuenta = db.CT_Cuentas.Where(x => x.Email == login.Email).FirstOrDefault<CT_Cuentas>();
            //var cuenta = db.CT_Cuentas.Where(x => x.Email == login.Email && x.Id_Status != 2).FirstOrDefault<CT_Cuentas>();
            var cuenta = db.CT_Cuentas.Where(x => x.Email == login.Email && x.Id_Status != 2 && x.Id_Rol == login.Id_Rol).FirstOrDefault<CT_Cuentas>();

            if (cuenta == null)
            {

                modError.TipoError = 4;
                modError.Descripcion = "No se encuentra la cuenta, puede que no se haya dado de alta";
                modError.Valor1 = null;
                return Content(HttpStatusCode.Conflict, modError);

                //var customersFake = new string[] { "Status:0", "Error:No se encuentra la cuenta, puede que este inactiva o no se haya dado de alta" };
                //return Ok(customersFake);
            }

            //if (cuenta.Id_Status == 3)
            //{
            //    modError.TipoError = 4;
            //    modError.Descripcion = "La cuenta aun no ha sido validada. Valide el email por favor";
            //    modError.Valor1 = null;
            //    return Content(HttpStatusCode.Conflict, modError);
            //}

            var cuenta_acceso = db.CT_Cuentas_Acceso.Where(x => x.Id_Cuenta == cuenta.Id_Cuenta).FirstOrDefault<CT_Cuentas_Acceso>();

            //validamos que el correo ya haya sido validado

            //bool isCredentialValid = (login.Password == "123456");

            string passwordDescifrado = "";

            //bool isCredentialValid = (login.Password == cuenta_acceso.Password);
            bool isCredentialValid = (login.Password == passwordDescifrado);
            bool isTemporalPassword = false;

            if (isCredentialValid == false)
            {
                //OBTENEMOS EL PASSWORD TEMPORAL
                var passTemp = "";
                //isCredentialValid = (login.Password == cuenta_acceso.PasswordTemporal);
                isCredentialValid = (login.Password == passTemp);
                isTemporalPassword = true;
            }

            if (isCredentialValid)
            {
                //validamos que solo se genera token solo siempre y cuando no sea password temporal
                string token = null;
                if (isTemporalPassword == false)
                {
                    token = TokenGenerator.GenerateTokenJwt(login.Email);
                }

                bool isValidEmail = true; 
                if (cuenta.Id_Status == 3)
                {
                    isValidEmail = false;
                }

                if (isTemporalPassword == false)
                {
                    //validamos si ya hay un token generado para esta cuenta si es asi solo se sustituye por el nuevo token si no se genera un nuevo registro
                    var cuentas_tokens = db.CT_Cuentas_Tokens.Where(x => x.Id_Cuenta == cuenta.Id_Cuenta && x.Id_Rol == login.Id_Rol && x.TipoApp == login.TipoApp).FirstOrDefault<CT_Cuentas_Tokens>();

                    if (cuentas_tokens != null)
                    {
                        cuentas_tokens.TokenActual = token;
                    }
                    else
                    {
                        //guardamos datos del token y se asigna a la cuenta
                        CT_Cuentas_Tokens ct_cuentas_tokens = new CT_Cuentas_Tokens
                        {
                            Id_Registro = 0,
                            Id_Cuenta = cuenta.Id_Cuenta,
                            Id_Rol = login.Id_Rol,
                            TipoApp = login.TipoApp,
                            TokenActual = token
                        };

                        db.CT_Cuentas_Tokens.Add(ct_cuentas_tokens);
                    }

                    db.SaveChanges();
                }


                //return Ok(token);
                //return Ok(new
                //{
                //    tokenValue = token,
                //    idAccount = cuenta.Id_Cuenta,
                //    temporalPassword = isTemporalPassword,
                //    emailValid = isValidEmail
                //});

                return Ok(new
                {
                    data = new
                    {
                        tokenValue = token,
                        idAccount = cuenta.Id_Cuenta,
                        temporalPassword = isTemporalPassword,
                        emailValid = isValidEmail
                    }
                });

            }
            else
            {
                modError.TipoError = 4;
                modError.Descripcion = "El usuario y/o contraseña no son correctos, revise por favor";
                modError.Valor1 = null;
                return Content(HttpStatusCode.Conflict, modError);
                //return Unauthorized();
            }
        }
    }
}
