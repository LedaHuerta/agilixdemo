﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMORIIConferencia.ModelsNoDB
{
    public class ReporteGeneral_EstructuraPregunta
    {
        // seccion datos generales

        [Required]
        [JsonProperty(PropertyName = "idQuestion")]
        public int id_pregunta { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "question")]
        public string pregunta { get; set; }

        [Required]
        [JsonProperty(PropertyName = "results")]
        public List<ReporteGeneral_EstructuraRespuesta> listado_repuestas { get; set; }

    }

}