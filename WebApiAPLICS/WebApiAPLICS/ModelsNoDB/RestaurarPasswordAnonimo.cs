﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMORIIConferencia.ModelsNoDB
{
    public class RestaurarPasswordAnonimo
    {
        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "temporaryPassword")]
        public string PasswordTemporal { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "newPassword")]
        public string PasswordNuevo { get; set; }

        [Required]
        [JsonProperty(PropertyName = "idRole")]
        public int Id_Rol { get; set; }

    }
}