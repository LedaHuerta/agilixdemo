﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMORIIConferencia.ModelsNoDB
{
    public class ReporteGeneral_EstructuraBase
    {
        // seccion datos generales

        [Required]
        [JsonProperty(PropertyName = "idEvent")]
        public int id_evento { get; set; }

        [Required]
        [StringLength(200)]
        [JsonProperty(PropertyName = "event")]
        public string evento { get; set; }

        [Required]
        [JsonProperty(PropertyName = "eventDate")]
        public DateTime Fecha_Evento { get; set; }

        [Required]
        [JsonProperty(PropertyName = "totalAssistants")]
        public int total_asistentes { get; set; }

        [Required]
        [JsonProperty(PropertyName = "content")]
        public List<ReporteGeneral_EstructuraPregunta> listado_preguntas { get; set; }

    }

}