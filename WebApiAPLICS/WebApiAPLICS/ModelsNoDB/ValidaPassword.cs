﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApiMORIIConferencia.Models
{
    public class ValidaPassword
    {
        // seccion datos generales
        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "password")]
        public string password { get; set; }
    }
}