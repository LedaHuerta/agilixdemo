﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMORIIConferencia.ModelsNoDB
{
    public class ReporteGeneral_EstructuraRespuesta
    {
        // seccion datos generales

        [Required]
        [JsonProperty(PropertyName = "idAnswer")]
        public int id_respuesta { get; set; }

        [Required]
        [JsonProperty(PropertyName = "idTypeAnswer")]
        public int id_tipo_respuesta { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "answer")]
        public string respuesta { get; set; }

        [Required]
        [JsonProperty(PropertyName = "total")]
        public int total_respuestas { get; set; }


    }

}