﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMORIIConferencia.ModelsNoDB
{
    public class ReporteEventoResultadosGeneral
    {
        // seccion datos generales

        [Required]
        public int id_Evento { get; set; }

        [Required]
        [StringLength(200)]
        public string Evento { get; set; }

        [Required]
        public DateTime Fecha_Evento { get; set; }

        [Required]
        public int totalParticipantes { get; set; }

        [Required]
        [StringLength(50)]
        public string Encuesta { get; set; }

        [Required]
        [StringLength(50)]
        public string Pregunta { get; set; }

        [Required]
        public int Id_Pregunta_Encuesta { get; set; }

        [Required]
        [StringLength(50)]
        public string Respuesta { get; set; }

        [Required]
        public int IdRespuesta { get; set; }

        [Required]
        public int Id_Grupo_Respuesta { get; set; }

        [Required]
        public int Total { get; set; }
    }

}