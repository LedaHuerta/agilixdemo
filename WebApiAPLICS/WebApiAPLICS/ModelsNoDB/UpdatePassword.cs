﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMORIIConferencia.Models
{
    public class UpdatePassword
    {
        [Required]
        [JsonProperty(PropertyName = "oldPassword")]
        public string PasswordOriginal { get; set; }

        [Required]
        [JsonProperty(PropertyName = "newPassword")]
        public string PasswordNuevo { get; set; }

    }
}