﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.IdentityModel.Tokens;
using WebApiMORIIConferencia.Models;

namespace WebApiMORIIConferencia.Security
{
    /// <summary>
    /// Validacion de token por peticion de autorizacin usando DelegatinHandler
    /// </summary>
    internal class TokenValidationHandler : DelegatingHandler
    {
        private static bool TryRetrieveToken(HttpRequestMessage request, out string token)
        {
            token = null;
            IEnumerable<string> authzHeaders;

            if (!request.Headers.TryGetValues("Authorization", out authzHeaders) || authzHeaders.Count() > 1)
            {
                return false;
            }

            var bearerToken = authzHeaders.ElementAt(0);
            token = bearerToken.StartsWith("Bearer ") ? bearerToken.Substring(7) : bearerToken;

            return true;
        }

        private static bool TryRetrieveIdAccount(HttpRequestMessage request, out string IdAccount)
        {
            IdAccount = null;
            IEnumerable<string> IdAccountHeaders;

            if (!request.Headers.TryGetValues("IdAccount", out IdAccountHeaders) || IdAccountHeaders.Count() > 1)
            {
                return false;
            }

            IdAccount = IdAccountHeaders.ElementAt(0);

            //(REVISAR SI SE OCUPARA O NO) validamos que el id de la cuenta del header corresponda con el id del uri
            //var idCuentaURL = request.RequestUri.Segments[request.RequestUri.Segments.Length - 1];
            //IdAccount = IdAccountHeaders.ElementAt(0);

            //if (idCuentaURL != IdAccount)
            //{
            //    return false;
            //}

            return true;
            
        }

        private ModelMORIIConferencia db = new ModelMORIIConferencia();

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpStatusCode statusCode;
            string token;
            string IdAccount;

            // determina si un jwt existe o no
            if (!TryRetrieveToken(request, out token))
            {
                statusCode = HttpStatusCode.Unauthorized;
                return base.SendAsync(request, cancellationToken);
            }

            // determina si un el id de la cuenta se envia
            //if (!TryRetrieveIdAccount(request, out IdAccount))
            if (TryRetrieveIdAccount(request, out IdAccount) == true)
            {
                //statusCode = HttpStatusCode.Unauthorized;
                //return base.SendAsync(request, cancellationToken);

                int IdAccountValue = Convert.ToInt32(IdAccount);

                //validamos que el token y la cuenta esten relacionados de forma correcta en la base de datos
                var cuentas_tokens = db.CT_Cuentas_Tokens.Where(x => x.Id_Cuenta == IdAccountValue && x.TokenActual == token).FirstOrDefault<CT_Cuentas_Tokens>();
                if (cuentas_tokens == null)
                {
                    statusCode = HttpStatusCode.Unauthorized;
                    return base.SendAsync(request, cancellationToken);
                }

            }

            try
            {
                var secretKey = ConfigurationManager.AppSettings["JWT_SECRET_KEY"];
                var audienceToken = ConfigurationManager.AppSettings["JWT_AUDIENCE_TOKEN"];
                var issuerToken = ConfigurationManager.AppSettings["JWT_ISSUER_TOKEN"];
                var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(secretKey));

                SecurityToken securityToken;
                var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    ValidAudience = audienceToken,
                    ValidIssuer = issuerToken,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true,
                    LifetimeValidator = this.LifetimeValidator,
                    IssuerSigningKey = securityKey
                };

                //validamos que el token corresponda al id de la cuenta.


                // Extrae y asigna al Current Principal y usuario
                Thread.CurrentPrincipal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);
                HttpContext.Current.User = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return base.SendAsync(request, cancellationToken);
            }
            catch (SecurityTokenValidationException)
            {
                statusCode = HttpStatusCode.Unauthorized;
            }
            catch (Exception)
            {
                statusCode = HttpStatusCode.InternalServerError;
            }

            return Task<HttpResponseMessage>.Factory.StartNew(() => new HttpResponseMessage(statusCode) { });
        }

        public bool LifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            if (expires != null)
            {
                if (DateTime.UtcNow < expires) return true;
            }
            return false;
        }
    }
}