﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;
using WebApiMORIIConferencia.Models;

namespace WebApiMORIIConferencia.Functions
{
    public class FunctionsGeneric
    {

        private ModelMORIIConferencia db = new ModelMORIIConferencia();

        //VALIDAMOS EL PASSWORD DE UNA CUENTA ESPECIFICA
        public int ValidamosPassword(int idCuenta, string password)
        {
            var parametro3 = new SqlParameter();
            parametro3.ParameterName = "@parametro3";
            parametro3.Direction = ParameterDirection.Output;
            parametro3.SqlDbType = SqlDbType.NVarChar;
            parametro3.Size = 100;

            var solicitudC = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                "sp_ValidaPassword @idCuenta, @password, @parametro3 OUT",
                new SqlParameter("@idCuenta", idCuenta),
                new SqlParameter("@password", password),
                parametro3);

            int resultado = int.Parse(parametro3.Value.ToString());

            return resultado;
        }

    }
}