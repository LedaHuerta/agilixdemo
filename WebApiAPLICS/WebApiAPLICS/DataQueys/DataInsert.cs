﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiMORIIConferencia.Models;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;

namespace WebApiMORIIConferencia.DataQueys
{
    public class DataInsert
    {
        private ModelMORIIConferencia db = new ModelMORIIConferencia();

        //RESTAURAMOS POR UN NUEVO PASSWORD (Modo Anonimo)
        public int RestaurarPasswordAnonimo(string passtemp, string passnuevo)
        {
            var parametro3 = new SqlParameter();
            parametro3.ParameterName = "@parametro3";
            parametro3.Direction = ParameterDirection.Output;
            parametro3.SqlDbType = SqlDbType.Int;

            var solicitudC = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                "sp_RestaurarPassword @passTemp, @passNuevo, @parametro3 OUT",
                new SqlParameter("@passTemp", passtemp),
                new SqlParameter("@passNuevo", passnuevo),
                parametro3);

            int resultado = int.Parse(parametro3.Value.ToString());

            return resultado;
        }

        //BORRAMOS UN EVENTO
        public int BorrarEvento(int idEvento)
        {
            var parametro3 = new SqlParameter();
            parametro3.ParameterName = "@parametro3";
            parametro3.Direction = ParameterDirection.Output;
            parametro3.SqlDbType = SqlDbType.Int;

            var solicitudC = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                "sp_BorrarEvento @idEvento, @parametro3 OUT",
                new SqlParameter("@idEvento", idEvento),
                parametro3);

            int resultado = int.Parse(parametro3.Value.ToString());

            return resultado;
        }

        //ACTUALIZAR PASSWORD (VOLUNTARIAMENTE)
        public int ActualizarPassword(int idCuenta, string oldPassword, string newPassword)
        {
            var parametro3 = new SqlParameter();
            parametro3.ParameterName = "@parametro3";
            parametro3.Direction = ParameterDirection.Output;
            parametro3.SqlDbType = SqlDbType.Int;

            var solicitudC = db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                "sp_ActualizarPasswordVoluntario @idCuenta, @oldPassword, @newPassword, @parametro3 OUT",
                new SqlParameter("@idCuenta", idCuenta),
                new SqlParameter("@oldPassword", oldPassword),
                new SqlParameter("@newPassword", newPassword),
                parametro3);

            int resultado = int.Parse(parametro3.Value.ToString());

            return resultado;
        }

    }
}